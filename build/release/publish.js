const path = require('path');
const fs = require('fs');
const { readJsonFile, execPromise, readDirChildPaths, libDistDir, namespace } = require('../utils');
const lernaJson = require('../../lerna.json');
if (!fs.existsSync(libDistDir)) {
  console.log('请先执行 npm run build 进行代码构建！\n');
  process.exit(1);
}

// 提取lib发布地址
const lernaConfig = { registry: lernaJson.command.publish.registry };

/**
 * 发布lib
 * @param {string} libDir
 */
async function publishLib(libDir) {
  // 进入目录
  process.chdir(libDir);
  // 读取配置文件
  const packageJson = readJsonFile(path.join(libDir, 'package.json'));
  const name = packageJson.name;
  console.log(`${name} 开始发布...`);
  // 先删除远程lib
  try {
    await execPromise(
      `npm unpublish ${name}@${packageJson.version} --regist=${lernaConfig.registry}`,
    );
  } catch (error) {
    console.log(error);
  }
  // npm发布
  await execPromise(`npm publish --regist=${lernaConfig.registry}`);
  console.log(`${name} 发布成功！\n`);
}

// 发布所有包
const publishLibs = async () => {
  const dirs = readDirChildPaths(path.join(libDistDir, namespace));
  for (let i = 0; i < dirs.length; i++) {
    await publishLib(dirs[i]);
  }
};
publishLibs();
