const packageJson = require('../../package.json');
const lernaJson = require('../../lerna.json');
const { resolve, writeJsonFile, readJsonFile, namespace } = require('../utils');
const fs = require('fs');
const path = require('path');
let version = process.argv[2];
if (!version) {
  console.log('请输入版本号，如：npm run release 1.0.0\n');
  process.exit(1);
}
// 生成版本号
version = version.toLocaleLowerCase().replace('v', '');
// 更新package.json版本
writeJsonFile(resolve('package.json'), { ...packageJson, version });
// 更新lerna.json版本
writeJsonFile(resolve('lerna.json'), { ...lernaJson, version });
// 更新libs版本
const packages = resolve('packages');
// 获得libs内容
const libs = fs
  .readdirSync(packages)
  .map((dir) => {
    const filePath = path.join(packages, dir, 'package.json');
    const fileJson = readJsonFile(filePath);
    return { filePath, fileJson, version, private: fileJson.private, name: fileJson.name };
  })
  .filter((lib) => !lib.private);

// 更新版本
libs.forEach(({ filePath, fileJson }) => {
  fileJson.version = version; // 更新lib版本
  // 更新依赖版本
  Object.keys(fileJson.dependencies).forEach((name) => {
    // 以@jecloud/开头的依赖，统一更新版本
    if (name.startsWith(namespace + '/')) {
      fileJson.dependencies[name] = `^${version}`;
    }
  });
  writeJsonFile(filePath, fileJson);
  console.log(fileJson.name, version, '版本更新成功！');
});

console.log(packageJson.name, version, '版本更新成功！', '\n');
