const path = require('path');
const fs = require('fs');
const { exec } = require('child_process');
/**
 * 地址转换
 * @param {*} dir
 * @returns
 */
function resolve(...dir) {
  return path.resolve(process.cwd(), '.', ...dir);
}

/**
 * 读取json文件
 * @param {string} filePath
 * @returns {JSON}
 */
const readJsonFile = (filePath) => JSON.parse(fs.readFileSync(filePath));
/**
 * 写入json文件
 * @param {string} filePath
 * @param {JSON} json
 * @returns
 */
const writeJsonFile = (filePath, json) =>
  fs.writeFileSync(filePath, JSON.stringify(json, null, 2) + '\n');

/**
 * 异步执行
 * @param {*} commend
 */
function execPromise(commend) {
  return new Promise((resolve, reject) => {
    exec(commend, { encoding: 'utf-8' }, (error, stdout) => {
      if (error) {
        console.error(`执行出错: ${error}`);
        reject(error);
      } else {
        resolve(stdout);
      }
    });
  });
}
/**
 * 读取目录子文件夹路径
 * @param {*} dir
 * @returns Array
 */
function readDirChildPaths(dir) {
  return fs.readdirSync(dir).map((child) => path.join(dir, child));
}

module.exports = {
  resolve,
  writeJsonFile,
  readJsonFile,
  execPromise,
  readDirChildPaths,
  namespace: '@jecloud',
  libDistDir: resolve('dist/libs'),
};
