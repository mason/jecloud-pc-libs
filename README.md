# 基础库项目
## 项目介绍
基础库项目是JECloud前端所有项目的基础依赖。本项目基于 Monorepo 的 lerna 模块管理方式，包含的类库如下：
- @jecloud/cli：脚手架项目
- @jecloud/func：功能库
- @jecloud/ui： UI库
- @jecloud/utils：工具库
- examples：示例项目

## 项目目录
```bash

│  .commitlintrc.js        # Git 提交校验配置文件
│  .editorconfig           # 编辑器配置文件
│  .eslintignore           # eslint 忽略校验配置文件
│  .eslintrc.js            # eslint 校验配置文件
│  .gitattributes          # Git 配置文件，设置行末字符为LF
│  .gitignore              # Git 忽略提交配置文件
│  .prettierrc.js          # 代码格式化配置文件
│  CHANGELOG.md            # Git 提交记录
│  lerna.json              # Lerna 配置文件
│  package.json            # 项目配置
│  README.md               # 说明文档
│  LICENSE                 # 开源协议文件
│  SUPPLEMENTAL_LICENSE.md # 补充协议文件
└─packages                 # 包的统一管理目录
    ├─examples             # 示例项目
    ├─cli                  # 脚手架项目，包名：@jecloud/cli
    ├─func                 # 功能库，包名：@jecloud/func
    ├─ui                   # UI库，包名：@jecloud/ui
    └─utils                # 工具库，包名：@jecloud/utils

```
## 开发环境
### node
`v 14.18.3`

### npm 
`v 6.14.15`

### JECloud npm私服地址
http://verdaccio.jecloud.net/



## 项目命令

### 全局安装 [lerna](./docs/Lerna%20使用说明.md)，[yalc](./docs/Yalc%20使用说明.md)
```bash
npm install yalc lerna@^6.0.0 -g
```
### 安装依赖
```bash
npm run setup
```

### 启动服务
```bash
npm run dev
```

### 代码发布（本地调试）
```bash
npm run yalc:publish
```

### 代码构建
```bash
npm run build
```
### Git代码提交
项目增加了Git提交规范，强烈建议使用 `commitizen`（格式化commit message的工具）来进行Git提交操作，请使用下面命令

```bash
npm run commit
```

### 生成Git提交记录

```bash
npm run changelog
```


## 开源协议
- [MIT](./LICENSE)
- [平台证书补充协议](./SUPPLEMENTAL_LICENSE.md)


## JECloud主目录
[JECloud 微服务架构低代码平台（点击了解更多）](https://gitee.com/ketr/jecloud.git)