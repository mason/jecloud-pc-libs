import * as Store from './store';
import TreeNode from './model/tree-node';
/**
 * 数据对象
 */
export const Data = {
  Store,
  ...Store,
  /**
   * 创建树形节点model
   * @param {*} options
   * @returns TreeNodeModel
   */
  createTreeNodeModel(options) {
    return new TreeNode(options);
  },
};
