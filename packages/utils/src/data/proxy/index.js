import { useAjaxPrexy } from './ajax-proxy';
import { isFunction } from 'lodash-es';
export function useProxy(proxy) {
  if (isFunction(proxy)) {
    return { read: proxy };
  } else {
    return useAjaxPrexy(proxy);
  }
}

export default { useProxy };
