import Base from './base';
import { isPlainObject, toBoolean, forEach, toNumber, omit } from '../../lodash';

export default class TreeNode extends Base {
  constructor(options = {}) {
    super();
    this.id = options.id; // 主键
    this.text = options.text; // 标题
    this.code = options.code; // 编码
    this.icon = options.icon; // 图标
    this.iconColor = options.iconColor; // 图标颜色
    this.async = options.async || false; // 异步树
    this.checked = options.checked || false; // 选中
    this.disabled = toBoolean(options.disabled); // 禁用
    this.expandable = options.expandable; // 允许展开
    this.expanded = options.expanded; // 展开
    this.layer = toNumber(options.layer); // 节点层数
    this.nodePath = options.nodePath; // 节点路径
    this.nodeInfo = options.nodeInfo; // 配置信息
    this.nodeInfoType = options.nodeInfoType; // 配置信息类型
    this.nodeType = options.nodeType; // 节点类型
    this.orderIndex = options.orderIndex; // 排序
    this.parent = options.parent; // 父节点ID
    this.bean = options.bean || {}; // 业务bean
    this.children = []; // 子节点
    this.appendChild(options.children);
    this.leaf = this.children.length === 0 && this.nodeType === 'LEAF'; // 叶子
    // 前端业务字段
    this.hasChild = this.async && !this.leaf; // 是否有子节点
    this.loaded = options.loaded; // 异步加载标记
    // 父节点路径
    const paths = this.nodePath?.split('/') || [];
    paths.pop(); // 删除当前节点
    this.parentPath = paths.join('/');
    // 注入其他属性
    Object.assign(this, omit(options, Object.keys(this)));
  }

  /**
   * 添加子节点
   * @param {Object|Array} nodes
   */
  appendChild(nodes = []) {
    if (isPlainObject(nodes)) {
      nodes = [nodes];
    }

    nodes.length > 0 &&
      this.children.push(
        ...nodes
          .filter((node) => node)
          .map((node) => {
            return node.isModel ? node : new TreeNode({ ...node, parent: this.id });
          }),
      );
  }

  /**
   * 级联处理节点信息
   * @param {Function} fn
   * @param {boolean} includeSelf 包含当前节点信息
   * @returns
   */
  cascade(fn, includeSelf = true) {
    if (!includeSelf || fn?.(this) !== false) {
      forEach(this.children, (node) => {
        return node.cascade(fn);
      });
    } else {
      return false;
    }
  }
}
