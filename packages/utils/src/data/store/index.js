import GridStore from './grid-store';
import TreeStore from './tree-store';
import TreeGridStore from './tree-grid-store';
import { vueCall } from '../../je';
/**
 * 响应式列表store
 * @param {*} options
 * @returns
 */
export function useGridStore(options) {
  return vueCall('reactive', new GridStore(options));
}
/**
 * 响应式树形store
 * @param {*} options
 * @returns
 */
export function useTreeStore(options) {
  return vueCall('reactive', new TreeStore(options));
}
/**
 * 响应式树形表格store
 * @param {*} options
 * @returns
 */
export function treeGridStore(options) {
  return vueCall('reactive', new TreeGridStore(options));
}
