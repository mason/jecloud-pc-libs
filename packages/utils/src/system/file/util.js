// import Compressor from 'compressorjs';
import { getDDItemInfo, getSystemConfig } from '../cache';
import { getAjaxBaseURL } from '../../http';
import { isArray, split, toNumber } from '../../lodash';
import {
  API_DOCUMENT_DOWN,
  API_DOCUMENT_DOWN_ZIP,
  API_DOCUMENT_PREVIEW,
  API_DOCUMENT_THUMBNAIL,
  API_DEV_PROXY_PREFIX,
} from '../api/urls';
import JsCookie from 'js-cookie';

/**
 * 下载文件，打开新窗口进行下载
 * @param {string} key 文件key
 */
export function downloadFile(key) {
  window.open(getFileUrlByKey(key, 'download'));
}

/**
 * 预览文件，打开新窗口进行预览
 * @param {string} key 文件key
 */
export function previewFile(key) {
  window.open(getFileUrlByKey(key));
}

/**
 * 获取文件地址
 * @param {string} key 文件key
 * @param {string} [type] 地址类型 preview|download|thumbnail，默认preview，
 * @param {boolean} [useHttp] 使用window.location.origin，会自动拼接上
 * @returns {string}
 */
export function getFileUrlByKey(key, type = 'preview', useHttp) {
  if (key?.startsWith && key?.startsWith('http')) {
    return key;
  }
  const baseURL = getFileBaseURL(useHttp);
  let url = '';
  switch (type) {
    case 'download': // 下载
      if (isArray(key) && key.length > 1) {
        url = `${baseURL}${API_DOCUMENT_DOWN_ZIP}?fileKeys=${key.join(',')}`;
      } else {
        url = `${baseURL}${API_DOCUMENT_DOWN}/${key}`;
      }
      break;
    case 'thumbnail': // 缩略图
      url = `${baseURL}${API_DOCUMENT_THUMBNAIL}/${key}`;
      break;
    default:
      // 预览
      url = `${baseURL}${API_DOCUMENT_PREVIEW}/${key}`;
  }

  // 处理iframe请求头没有token
  const token = JsCookie.get('authorization');
  const localStorageToken = localStorage.getItem('authorization');
  if (localStorageToken && !token) {
    if (url.indexOf('?') == -1) {
      url += '?authorization=' + localStorageToken;
    } else {
      url += '&authorization=' + localStorageToken;
    }
  }
  return url;
}
/**
 * 获取文件地址前缀
 * @param {boolean} [useHttp] 使用window.location.origin，会自动拼接上
 * @returns {string}
 */
export function getFileBaseURL(useHttp = true) {
  let baseURL = getAjaxBaseURL();
  // 开发调试前缀，默认去掉，骨架项目已做处理
  if (baseURL == API_DEV_PROXY_PREFIX) {
    baseURL = '';
  }
  // 兼容H5环境下，image组件增加路由前缀，所以使用http绝对路径
  if (!baseURL.startsWith('http') && window && useHttp) {
    baseURL = window.location.origin + baseURL;
  }
  return baseURL;
}
/**
 * 获得系统文件上传限制大小
 * @param {number} [maxSize] 自定义大小
 * @returns {number}
 */
export function getFileSystemMaxSize(maxSize) {
  // 系统全局允许上传大小
  const systemMaxSize = toNumber(getSystemConfig('JE_SYS_UPLOADFILESIZE', 0));
  // 取最小值
  return maxSize > 0 && systemMaxSize > 0
    ? Math.min(maxSize, systemMaxSize)
    : maxSize || systemMaxSize;
}

/**
 * 获得系统文件上传包含后缀
 * @param {Array} [includeSuffix] 自定义后缀
 * @returns {Array}
 */
export function getFileSystemIncludeSuffix(includeSuffix = []) {
  // 系统全局允许上传格式
  const systemIncludeSuffix = getSystemConfig('JE_DOCUMENT_FILE_SUFFIX')?.split(',');
  let suffixs = includeSuffix;
  // 字段允许上传的后缀名，必须包含在系统设置里
  if (systemIncludeSuffix?.length) {
    if (includeSuffix.length) {
      suffixs = includeSuffix.filter((suffix) => systemIncludeSuffix.includes(suffix));
      // 和系统设置的没有交集,那么就不可上传
      if (suffixs.length <= 0) {
        suffixs = false;
      }
    } else {
      suffixs = systemIncludeSuffix;
    }
  }
  return suffixs;
}

/**
 * 根据文件名称获得文件类型
 * @param {String} fileName
 */
export function getFileTypeInfo(fileName = '') {
  const suffix = split(fileName, '.').pop();
  let type = 'file';
  switch (suffix) {
    case 'doc': // word文档
    case 'docx':
      type = 'word';
      break;
    case 'xls': // excel文档
    case 'xlsx':
      type = 'excel';
      break;
    case 'ppt': // ppt文档
    case 'pptx':
      type = 'ppt';
      break;
    case 'rar': // 压缩包文件
    case 'zip':
    case 'arj':
    case 'gz':
    case 'z':
      type = 'zip';
      break;
    case 'bmp': // 图片
    case 'gif':
    case 'jpg':
    case 'pic':
    case 'png':
    case 'tif':
    case 'jpeg':
      type = 'image';
      break;
    case 'pdf': //pdf文件
      type = 'pdf';
      break;
    case 'txt': //txt文档
      type = 'txt';
      break;
    case 'mp4': // 视频文件
    case 'flv':
    case 'f4v':
    case 'mov':
      type = 'video';
      break;
    case 'mp3': // 音频文件
    case 'wma':
      type = 'audio';
      break;
  }

  // 默认配置
  const info = {
    icon: 'fas fa-file-alt',
    color: '#a9aba9',
    type: 'file',
    suffix,
  };
  // 字典配置
  const typeInfo = getDDItemInfo('JE_META_FILESTYPE', type);
  if (typeInfo) {
    Object.assign(info, {
      icon: typeInfo.icon,
      color: typeInfo.textColor,
      type: typeInfo.code,
    });
  }
  return info;
}

/**
 * 压缩图片
 * @param {*} param0
 * @returns
 */
/* export function compressorFile(file, options) {
  // 非图片文件不处理
  if (!file.type.startsWith('image/')) {
    return Promise.resolve(file);
  }
  return new Promise((resolve, reject) => {
    new Compressor(file, {
      quality: 0.2,
      convertSize: 10000,
      ...options,
      success(result) {
        resolve(result);
      },
      error(err) {
        reject(err);
      },
    });
  });
} */
