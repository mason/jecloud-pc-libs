import { ajax } from '../../http';
import { API_RBAC_LOG_BURY } from './urls';

/**
 * 日志埋点
 * @param {*} objCode 操作对象编码
 * @param {*} objName 操作对象名称
 * @param {*} typeCode 操作类型编码
 * @param {*} typeName 操作类型名称
 * @returns
 */
export function logBuryApi({ objCode, objName, typeCode, typeName }) {
  return ajax({
    url: API_RBAC_LOG_BURY,
    params: {
      objCode,
      objName,
      typeCode,
      typeName,
    },
  });
}

export const LogActionEnum = Object.freeze({
  /**
   * 打开菜单
   */
  MENU_OPEN: { code: 'mneuOpen', text: '访问' },
  /**
   * 打开顶部菜单
   */
  TOP_MENU_OPEN: { code: 'topMenuOpen', text: '访问' },
});
