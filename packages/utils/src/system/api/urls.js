/*--------------------------------------- 系统信息 ---------------------------------------*/
/**
 * 账号
 */
export const API_RBAC_ACCOUNT = '/je/rbac/cloud/account/currentUser';
/**
 * 系统变量
 */
export const API_SYSTEM_VARIABLES = '/je/meta/variable/loadSysVariables';

/**
 * 前端系统变量
 */
export const API_SYSTEM_FRONTEND_VARIABLES = '/je/meta/setting/loadFrontendSetting';
/**
 * 登录
 */
export const API_RBAC_LOGIN = '/je/rbac/cloud/login/accountLogin';
/**
 * 退出登录
 */
export const API_RBAC_LOGOUT = '/je/rbac/cloud/login/logout';
/**
 * 验证token是否有效
 */
export const API_RBAC_VALIDATE_TOKEN = '/je/rbac/cloud/validate/validateToken';

/*--------------------------------------- 数据字典 ---------------------------------------*/
/**
 * 字典树
 */
export const API_DICTIONARY_LOAD_TREE = '/je/meta/dictionary/tree/loadTree';
/**
 * 查找异步节点
 */
export const API_DICTIONARY_FIND_ASYNC_NODES = '/je/meta/dictionary/tree/findAsyncNodes';
/**
 * 获得字典项
 */
export const API_DICTIONARY_LOAD_ITEM_BY_CODE = '/je/meta/dictionary/getDicItemByCode';
/**
 * 获得字典项，多字典
 */
export const API_DICTIONARY_LOAD_ITEM_BY_CODES = '/je/meta/dictionary/getDicItemByCodes';
/**
 * 加载字典数据
 */
export const API_DICTIONARY_INIT_LOAD = '/je/meta/dictionary/initLoad';

/*--------------------------------------- 文件接口 ---------------------------------------*/
/**
 * 文件上传
 */
export const API_DOCUMENT_FILE = '/je/document/file';
/**
 * 文件下载
 */
export const API_DOCUMENT_DOWN = '/je/document/down';
/**
 * 文件下载
 */
export const API_DOCUMENT_DOWN_ZIP = '/je/document/downZip';
/**
 * 文件预览
 */
export const API_DOCUMENT_PREVIEW = '/je/document/preview';
/**
 * 文件预览缩率图
 */
export const API_DOCUMENT_THUMBNAIL = '/je/document/thumbnail';

/*--------------------------------------- 日志接口 ---------------------------------------*/
/**
 * 操作埋点日志
 */
export const API_RBAC_LOG_BURY = '/je/rbac/cloud/log/bury';

/**
 * 功能Load接口
 */
export const API_COMMON_LOAD = '/je/common/load';

/*--------------------------------------- 权限接口 ---------------------------------------*/
/**
 * 插件权限
 */
export const API_RBAC_LOAD_PLUGIN_PERMS = '/je/rbac/permission/loadPluginPerm';

/*--------------------------------------- 模板接口 ---------------------------------------*/
/**
 * 全局脚本库
 */
export const API_META_GLOBAL_SCRIPT = '/je/meta/global/script';
/**
 * 全局样式库
 */
export const API_META_GLOBAL_CSS = '/je/meta/global/css';
/**
 * 全局图标库
 */
export const API_META_GLOBAL_ICON = '/je/meta/global/icon';
/**
 * Sql模板数据
 */
export const API_META_GLOBAL_STL = '/je/meta/global/stl';

/*--------------------------------------- 调试接口前缀 ---------------------------------------*/
export const API_DEV_PROXY_PREFIX = '/jeapi';
