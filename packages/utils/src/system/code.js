import { execScript } from '../web/sandbox';
import { createStyleSheet } from '../web/style';
import { initJsCodeApi, initCssCodeApi, loadApiDataApi, initIconCodeApi } from './api/code';
import { jsCodeMap } from './cache';
import { decode, parseUseScope } from '../lodash';
import { getFileBaseURL } from '../lodash';
import { useJE } from '../je';
let apiData = [];
/**
 * 初始化全局脚本
 * @returns
 */
export function initCodes() {
  jsCodeMap.clear();
  // 加载脚本库
  return initJsCodeApi()
    .then((jsCodes) => {
      // 缓存js脚本库
      const initJs = [];
      jsCodes
        .filter((item) => parseUseScope(item.QJJBK_ZYFW_CODE))
        .forEach((item) => {
          if (item.QJJBK_SPJZ == '1') {
            initJs.push(item.QJJBK_JBDM);
          } else {
            jsCodeMap.set(item.QJJBK_FFM, item.QJJBK_JBDM);
          }
        });
      // 首屏加载js脚本库
      const initJsCode = '__init__';
      initJs.forEach((code) => {
        jsCodeMap.set(initJsCode, code);
        callCustomFn(initJsCode);
      });
      jsCodeMap.delete(initJsCode);
      return jsCodeMap;
    })
    .catch(() => {
      console.log('js template error');
    });
}
/**
 * 全局样式
 * @returns
 */
export function initStyles() {
  const styleId = 'je_global_styles';
  const iconId = 'je_global_icons';
  return Promise.all([initCssCodeApi(), initGlobalIcons()])
    .then(([cssCodes, { iconCss, icons }]) => {
      // 解析css样式库
      const initCss = [];
      cssCodes
        .filter((item) => parseUseScope(item.QJCSS_ZYFW_CODE))
        .forEach((item) => {
          initCss.push(item.QJCSS_YSDM);
        });
      // 写入样式
      if (initCss.length > 0) {
        createStyleSheet(initCss.join('\n'), styleId);
      }

      // 解析全局图标库
      createStyleSheet(iconCss, iconId);
      return { icons };
    })
    .catch(() => {
      console.log('css,icon template error');
    });
}

/**
 * 执行全局脚本库
 * @param {string} methodName 方法名
 * @param {Object} params 参数
 * @returns {Object}
 */
export function callCustomFn(methodName, params) {
  let fnCode = jsCodeMap.get(methodName);
  if (fnCode) {
    try {
      return execScript(fnCode, { EventOptions: params || {} });
    } catch (error) {
      console.error(`js全局脚本库：【${methodName}】出错了！`);
      console.error(error);
    }
  }
}
/**
 * 更新全局脚本
 * @private
 * @param {*} code 方法名
 * @param {*} codes 代码
 */
export function updateCustomFn(code, codes) {
  if (code && codes) {
    jsCodeMap.set(code, codes);
  }
}
/**
 * 注册样式
 * @param {*} code 样式名
 * @param {*} codes 代码
 * @returns
 */
export function registCustomStyle(code, codes) {
  createStyleSheet(codes, 'je_coustom_' + code);
}

/**
 * 加载Api数据
 * @returns
 */
export function loadApiData() {
  if (apiData.length) {
    return Promise.resolve(apiData);
  } else {
    return loadApiDataApi().then((data) => {
      apiData = data.children || [];
      return apiData;
    });
  }
}
/**
 * 初始化全局图标
 * @returns
 */
export function initGlobalIcons() {
  return initIconCodeApi()
    .then(parseGlobalIcon)
    .then(({ icons, iconCss }) => {
      if (icons.length) {
        // 自定注入全局图标库
        const ui = useJE().useUi();
        ui?.setGlobalIcons?.(icons);
      }
      return { iconCss, icons };
    });
}

/**
 * 解析全局图标库
 * @param {*} iconCodes
 */
export function parseGlobalIcon(iconCodes) {
  const iconCss = [];
  const icons = [];
  const iconFileType = [
    { suffix: 'woff2', type: 'woff2' },
    { suffix: 'woff', type: 'woff' },
    { suffix: 'ttf', type: 'truetype' },
    { suffix: 'svg', type: 'svg' },
  ];
  iconCodes.forEach((item) => {
    const icon = {
      name: item.ICON_NAME,
      code: item.ICON_CODE,
      system: item.ICON_SYSTEM == '1',
      fontClass: '', // 样式
      fontPrefix: '', //前缀
      fontIcons: [], // 字体
    };
    // 自定义图标库
    if (item.ICON_SYSTEM == '0') {
      // 解析字体图标文件
      const filesUrl = [];
      const files = decode(item.ICON_FONT_FILES);
      const fileBaseUrl = `${getFileBaseURL(false)}/je/document/open/getOriginalPicture/je`;
      iconFileType.forEach((item) => {
        const file = files.find((file) => file.suffix == item.suffix);
        if (file) {
          filesUrl.push(`url(${fileBaseUrl}/${file.fileKey}) format("${item.type}")`);
        }
      });
      iconCss.push(`@font-face {
          font-family: "${item.ICON_FONT_FAMILY}";
          src: ${filesUrl.join(',')};
        }`);
      // 解析字体样式
      const fontStyle = item.ICON_FONT_STYLE;
      // eslint-disable-next-line no-useless-escape
      const regex = /([^\{\}\s][^\{\}]*)\s*\{/g;
      let matches;
      while ((matches = regex.exec(fontStyle)) !== null) {
        // 去除匹配到的样式名前后的空格并存储到集合中
        let cls = matches[1].trim().substring(1);
        if (cls.endsWith(':before')) {
          cls = cls.replace(':before', '');
          icon.fontPrefix = cls.split('-')[0];
          cls = cls.replace(icon.fontPrefix + '-', '');
          icon.fontIcons.push(cls);
        } else {
          icon.fontClass = cls;
        }
      }
      iconCss.push(fontStyle);
    }
    icons.push(icon);
  });
  return { iconCss: iconCss.join('\n'), icons };
}
