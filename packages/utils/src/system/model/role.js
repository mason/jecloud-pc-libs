/**
 * 角色
 */
export default class Role {
  /**
   * 角色ID
   */
  id;
  /**
   * 角色名称
   */
  name;
  /**
   * 角色编码
   */
  code;
}
