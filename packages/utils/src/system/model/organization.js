/**
 * 机构
 */
export default class Organization {
  /**
   * 主键ID
   */
  id;
  /**
   * 机构编码
   */
  code;
  /**
   * 机构名称
   */
  name;
}
