import {
  encode,
  decode,
  execScript4Async,
  execScript,
  ajax,
  Func,
  transformAjaxData,
} from '../index';

/**
 * 执行数据源
 * @param {string} code 系统数据源编码
 * @param {Object} [options] 配置
 * @param {Object} options.params 数据源参数
 * @param {number} options.limit 显示数量
 * @param {string} options.scriptCode 自定义js数据源，启用后code无效
 */
export async function loadDataSource(code, options = {}) {
  const { params = {}, limit, scriptCode } = options;

  // 1.查询数据源信息
  let data;
  if (scriptCode) {
    // 自定义js
    data = {
      DATASOURCE_TYPE: 'JS',
      DATASOURCE_CONFIG: scriptCode,
    };
  } else if (code) {
    // 系统数据源
    data = (
      await Func.loadGridApi({
        pd: 'meta',
        params: {
          tableCode: 'JE_CORE_DATASOURCE',
          limit: 1,
          j_query: encode([{ code: 'DATASOURCE_CODE', value: code, type: '=' }]),
        },
      })
    ).rows?.[0];
  }

  if (!data) {
    throw new Error('数据源不存在：' + code);
  }
  // 2.解析数据源
  const id = data.JE_CORE_DATASOURCE_ID;
  const type = data.DATASOURCE_TYPE;
  const config = data.DATASOURCE_CONFIG;
  let result = {};
  // js数据源
  if (type == 'JS') {
    if (!config) {
      return result;
    }
    // 执行结果
    result = await execScript4Async(
      config,
      {
        EventOptions: { params, limit },
      },
      true,
    );
    return {
      ...result,
      data: result?.data,
    };
  }

  // 后端数据源
  const executeParams = {
    JE_CORE_DATASOURCE_ID: id,
    parameterStr: encode(params),
    j_query: [],
    limit,
  };

  // 图形数据源
  if (type == 'VIEW') {
    const jsScript = decode(config)?.jsScript;
    if (jsScript) {
      // 解析js自定义条件
      executeParams.j_query =
        execScript(config.jsScript, {
          EventOptions: { params },
        })?.querys || [];
    }
  }
  // 3.执行数据源
  result = await ajax({
    url: '/je/meta/dataSource/execute',
    params: executeParams,
  }).then(transformAjaxData);

  return {
    ...result,
    data: result?.listData,
  };
}
