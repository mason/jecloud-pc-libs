import { cloneDeep, isEmpty } from 'lodash-es';
import {
  loadDDItemByCodeApi,
  loadDDItemByCodesApi,
  findDDAsyncNodesApi,
  loadDDItemByCode4TreeApi,
  initDDDataApi,
} from './api/dictionary';
import { ddCache, ddItemCache, getDDItemCache } from './cache';
import { isNotEmpty, toQuerysTemplate } from '../lodash';
export { findDDAsyncNodesApi, loadDDItemByCode4TreeApi };
export const findDDAsyncNodes = findDDAsyncNodesApi;
export const loadDDItemByCode4Tree = loadDDItemByCode4TreeApi;
export {
  getDDCache,
  getDDInfo,
  getDDItemCache,
  getDDItemInfo,
  getDDItemList,
  clearDDItemCache,
  toDDItemText,
} from './cache';

/**
 * 字典类型
 */
export const DDTypeEnum = Object.freeze({
  /**
   * 列表
   */
  LIST: 'LIST',
  /**
   * 树形
   */
  TREE: 'TREE',
  /**
   * 树形【转换】
   */
  DYNA_TREE: 'DYNA_TREE',
  /**
   * SQL【列表】
   */
  SQL: 'SQL',
  /**
   * SQL【树形】
   */
  SQL_TREE: 'SQL_TREE',
  /**
   * 程序
   */
  CUSTOM: 'CUSTOM',
});
/**
 * 加载字典项数据
 * @param {string} ddCode 字典编码
 * @param {Array} querys 查询条件
 * @returns {Promise}
 */
export function loadDDItemByCode(ddCode, querys) {
  if (isEmpty(querys) && ddItemCache.has(ddCode)) {
    return Promise.resolve(getDDItemCache(ddCode));
  }
  return loadDDItemByCodeApi({ code: ddCode, querys }).then((data) => {
    if (isEmpty(querys)) {
      ddItemCache.set(ddCode, data);
    }
    return getDDItemCache(ddCode);
  });
}
/**
 * 批量加载字典项数据
 * @param {string[]} ddCodes 字典编码数组
 * @returns {Promise}
 */
export function loadDDItemByCodes(ddCodes) {
  const uniqueCodes = ddCodes.filter((code) => !ddItemCache.has(code));
  if (uniqueCodes.length) {
    return loadDDItemByCodesApi({ codes: uniqueCodes }).then((ddMaps) => {
      Object.keys(ddMaps).forEach((ddCode) => {
        ddItemCache.set(ddCode, ddMaps[ddCode]);
      });
    });
  } else {
    return Promise.resolve();
  }
}

/**
 * 初始化字典数据
 * @returns
 */
export function initDDData() {
  // 系统的默认字典
  const defaultDDCode = [
    'JE_AUDFLAG', // 流程状态
    'JE_WF_TASKBTNS', // 流程按钮
    'JE_WF_OPERATION_TYPE', //流程任务审批操作类型
    'JE_WF_TASK_STATUS', // 流程任务状态
    'YESORNO', // 是否
    'JE_META_FILESTYPE', // 文件类型
  ];
  return Promise.all([initDDDataApi(), loadDDItemByCodes(defaultDDCode)]).then(([data]) => {
    // 字典缓存
    data.forEach((item) => {
      const ddInfo = {
        id: item.JE_CORE_DICTIONARY_ID,
        code: item.DICTIONARY_DDCODE,
        name: item.DICTIONARY_DDNAME,
        type: item.DICTIONARY_DDTYPE,
      };

      ddCache.set(ddInfo.id, ddInfo);
      ddCache.set(ddInfo.code, ddInfo);
    });
    return ddCache;
  });
}

/**
 * 解析sql，程序字典动态参数
 * @param {Object} options
 * @param {Object} options.dicValueConfig 参数配置
 * @param {Object} options.model 表单数据
 * @param {Object} options.parentModel 父功能表单数据
 */
export function parseDDCustomVariables({ dicValueConfig, model, parentModel }) {
  let customVariables = {};
  if (isNotEmpty(dicValueConfig)) {
    const cloneDicValueConfig = cloneDeep(dicValueConfig);
    toQuerysTemplate({ querys: cloneDicValueConfig, model, parentModel });
    cloneDicValueConfig.forEach((item) => {
      if (item) {
        customVariables[item.code] = item.value;
      }
    });
  }
  return customVariables;
}
