import {
  isArray,
  isString,
  isDate,
  isFunction,
  forEach,
  isObject,
  isEmpty,
} from './lodash-overrides';

/**
 * 判断数字,包含字符串数字
 * @param {Object} value 值
 * @returns {boolean}
 */
export function isNumeric(value) {
  return !Number.isNaN(parseFloat(value)) && Number.isFinite(parseFloat(value));
}

/**
 * 判断Promise对象
 * @param {Object} value
 * @returns {boolean}
 */
export function isPromise(value) {
  return isObject(value) && isFunction(value.then) && isFunction(value.catch);
}

/**
 * 判断是否window对象
 * @param {Object} value
 * @return {boolean}
 */
export function isWindow(value) {
  return (
    typeof window !== 'undefined' && Object.prototype.toString.call(value) === `[object Window]`
  );
}
/**
 * 判断是否FormData对象
 * @param {*} value
 * @return {*}
 */
export function isFormData(value) {
  return Object.prototype.toString.call(value) === '[object FormData]';
}

/**
 * 是否 非空 对象
 * @param {Object} value 对象
 * @param {boolean} onlyNullOrUndefined 只判断是否为null或undefined
 * @return {boolean}
 */
export function isNotEmpty(value, onlyNullOrUndefined) {
  return !isEmpty(value, onlyNullOrUndefined);
}
/**
 * 将字符串转成Boolean
 * @param {string} str '1'或'0'
 * @return {boolean}
 */
export function toBoolean(str) {
  return str == '1' || str == true;
}

/**
 * 取值，如果空，取默认值
 * @param {Object} value 值
 * @param {Object} defaultValue 默认值
 * @param {boolean} onlyNullOrUndefined 只判断是否为null或undefined
 * @returns {Object}
 */
export function toValue(value, defaultValue, onlyNullOrUndefined) {
  return isEmpty(value, onlyNullOrUndefined) ? defaultValue : value;
}

/**
 * 将对象转为JSON字符串
 * @param {Object} object 对象
 * @param {boolean} stringify 转字符串
 * @returns {string}
 */
export function encode(object, stringify = true) {
  if (object === null || object === undefined) {
    return 'null';
  }
  if (isDate(object)) {
    return simpleDateFormat(object, 'YYYY-MM-DD HH:mm:ss');
  }
  if (isFunction(object)) {
    return object.toString();
  }
  if (isArray(object)) {
    const array = [];
    forEach(object, (item) => {
      array.push(encode(item, false));
    });
    return stringify ? JSON.stringify(array) : array;
  }
  if (isObject(object)) {
    const obj = {};
    forEach(object, (item, key) => {
      obj[key] = encode(item, false);
    });
    return stringify ? JSON.stringify(obj) : obj;
  }
  return object;
}
/**
 * 将JSON字符串转为对象
 * @param {string} string JSON字符串
 * @returns {Object}
 */
export function decode(string) {
  if (isEmpty(string)) return null;
  let item = string;
  if (isString(string)) {
    try {
      if (window) {
        item = new Function(`return (${string})`)();
      } else {
        item = JSON.parse(string);
      }
    } catch (e) {
      console.error('decode解析出错：\n', string);
      console.error(e);
    }
  }
  return item;
}

/**
 * 生成UUID唯一码
 * @param {number} length 长度,默认19，与后台保持一致
 * @returns {string} uuid
 */
export function uuid(length = 19) {
  const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
  const uuid = [];
  const radix = chars.length;
  for (let i = 0; i < length; i++) uuid[i] = chars[0 | (Math.random() * radix)];
  return uuid.join('');
}

/**
 * 计时器，按间隔时长执行，满足条件，停止
 * @callback callback
 * @param {number} wait
 * @param {...Object} ...args 额外参数
 * @return {TimerInstance}
 */
export function timer(callback, wait, ...args) {
  let timerId = null;
  const instance = {
    /**
     * 停止执行
     */
    cancel() {
      clearTimeout(timerId);
    },
    /**
     * 立即执行
     * @param  {...any} fnArgs 参数
     */
    exec(...fnArgs) {
      instance.cancel();
      try {
        if (callback.apply(this, fnArgs || args) !== false) {
          timerId = setTimeout(() => {
            instance.exec(...fnArgs);
          }, wait);
        }
      } catch (e) {
        instance.cancel();
      }
    },
  };
  timerId = setTimeout(() => {
    instance.exec(...args);
  }, wait);
  return instance;
}

/**
 * 剔除html字符里的html标签
 * @param {string} htmlStr html内容
 * @returns {string} text
 */
export function stripHtmlTags(htmlStr) {
  return isEmpty(htmlStr) ? htmlStr : String(htmlStr).replace(/<\/?[^>]+>/gi, '');
}

/**
 * 去除数组中的空对象，返回新数组
 * @param {Array} array 待处理数组
 * @param {boolean} onlyNullOrUndefined 只判断是否为null或undefined
 * @returns {Array} newArray
 */
export function trimArray(array, onlyNullOrUndefined) {
  if (isArray(array)) {
    return array.filter((item) => isNotEmpty(item, onlyNullOrUndefined));
  } else {
    return array;
  }
}

/**
 * 简化时间格式化
 * @param {*} date
 * @param {*} format
 * @returns
 */
export function simpleDateFormat(date, format = 'YYYY-MM-DD HH:mm:ss') {
  const t = date;
  const tf = function (i) {
    return (i < 10 ? '0' : '') + i;
  };
  return format.replace(/YYYY|MM|DD|HH|mm|ss/g, function (a) {
    switch (a) {
      case 'YYYY':
        return tf(t.getFullYear());
      case 'MM':
        return tf(t.getMonth() + 1);
      case 'DD':
        return tf(t.getDate());
      case 'HH':
        return tf(t.getHours());
      case 'mm':
        return tf(t.getMinutes());
      case 'ss':
        return tf(t.getSeconds());
    }
  });
}

/**
 *
 *
 * 数字装换格式
 * @param {*} val
 * @param {*} type
 * @return {*}
 */
export function shiftNumberFormat(val, type) {
  if (!val) return val;
  let fix = 0;
  let places = 0;
  let negativeFlag = false;

  if (type == 0 && type != '0.00') {
    return val.toFixed(0);
  }

  const typeArray = type.split('.');
  if (typeArray.length > 1) {
    fix = typeArray[1].length;
  }
  const placesArray = typeArray[0].split(',');
  if (placesArray.length > 1) {
    places = placesArray[placesArray.length - 1].length;
  }

  if (val < 0) {
    val = Math.abs(val); // 将负数转为负数
    negativeFlag = true;
  }
  if (fix > 0) {
    val = val.toFixed(fix); // 保留小数
  }
  val += '';
  let temp = ''; // 临时变量
  if (places > 0) {
    let digit = 0;
    const digitArray = val.split('.');
    // 就是小数
    let int = val; // 拿到整数
    let ext = ''; // 获取到小数
    if (digitArray.length > 1) {
      digit = digitArray[1].length;
    }
    if (digit > 0) {
      int = val.slice(0, digit * -1 - 1); // 拿到整数
      ext = val.slice(digit * -1 - 1); // 获取到小数
    }

    //每个三位价格逗号
    int = int.split('').reverse().join(''); // 翻转整数
    for (let i = 0; i < int.length; i++) {
      temp += int[i];
      if ((i + 1) % places == 0 && i != int.length - 1) {
        temp += ','; // 每隔三个数字拼接一个逗号
      }
    }
    temp = temp.split('').reverse().join(''); // 加完逗号之后翻转
    temp = temp + ext; // 整数小数拼接
    if (negativeFlag) {
      temp = '-' + temp;
    }
  } else {
    temp = val;
  }

  return temp; // 返回
}

/**
 * 升级版本号
 * @param {string} version 版本号 如：1.2.3
 * @param {string} level 级别，默认patch，全部级别major(主版本),minor(子版本),patch(修订版).
 * @return {string} 新的版本号.
 */
export function upgradeVersion(version, level) {
  let [major, minor, patch] = version.split('.').map(Number);

  switch (level) {
    case 'major':
      major += 1;
      minor = 0;
      patch = 0;
      break;
    case 'minor':
      minor += 1;
      patch = 0;
      break;
    default:
      patch += 1;
      break;
  }
  return `${major}.${minor}.${patch}`;
}
