import { isArray, isString, get, isPlainObject, isEmpty } from './lodash-overrides';
import { toValue, simpleDateFormat } from './lodash-plus';
import { isApp } from '../je';
import { getCurrentAccount, getSystemConfig } from '../system/cache';
export * from '../system/cache';
export * from '../system/file/util';

/**
 * 解析动态字符串模板
 * @param {string} str 字符串模板
 * @param {Object} data 数据
 * @param {Object} options 数据
 * @param {RegExp} options.tplRE 替换模板正则
 * @returns {string}
 */
export function toTemplate(str, data, options) {
  if (!str) return str;
  // 系统默认值
  const defaultData = {};
  (str.match(/@.*?@/g) || []).forEach((item) => {
    defaultData[item] = toDefaultValue(item);
  });
  data = { ...defaultData, ...data };
  return str.replace(options?.tmplRE || /\{(.*?)\}/g, function (match, key) {
    return toValue(get(data, key.trim()), '', true);
  });
}

/**
 * 解析系统默认值
 * @param {string} value 解析的值，如：'@USER_ID@'，可以解析为用户ID
 * @returns {string}
 */
export function toDefaultValue(value = '') {
  // 只支持字符串的value
  if (!isString(value)) return value;
  const rawValue = value; // 原始值
  const account = getCurrentAccount() || {}; // 账号
  const { realUser = {}, platformOrganization = {} } = account; // 真实用户
  const { organization = {} } = realUser; // 真实机构
  value = value.trim();
  switch (value) {
    case '@USER_ID@': // 用户ID
      value = realUser.id;
      break;
    case '@USER_CODE@': // 用户编码
      value = realUser.code;
      break;
    case '@USER_NAME@': // 用户名称
      value = realUser.name;
      break;
    case '@USER_EMAIL@': // 用户邮箱
      value = realUser.email;
      break;
    case '@USER_PHONE@': // 用户手机
      value = realUser.phone;
      break;
    case '@ACCOUNT_DEPT_ID@': // 账号部门ID
      value = realUser.deptmentUserId;
      break;
    case '@DEPT_ID@': // 部门ID
      value = organization.id;
      break;
    case '@DEPT_CODE@': // 部门编码
      value = organization.code;
      break;
    case '@DEPT_NAME@': // 部门名称
      value = organization.name;
      break;
    case '@DEPT_PATH@': // 部门路径
      value = organization.path;
      break;
    case '@USER_JTGSMC@': // 集团公司ID
      value = organization.groupCompanyName;
      break;
    case '@USER_JTGSID@': // 集团公司名称
      value = organization.groupCompanyId;
      break;
    case '@USER_JTGSCODE@': // 集团公司编码
      value = organization.groupCompanyCode;
      break;
    case '@USER_ZHID@': // 租户ID
      value = account.tenantId;
      break;
    case '@USER_ZHMC@': // 租户名称
      value = account.tenantName;
      break;
    case '@USER_CARDNUM@': //卡号,TODO:需要同步字典数据
      value = account.cardNum;
      break;
    case '@ORGANIZATION_NAME@': // 机构名称
      value = platformOrganization.name;
      break;
    case '@ORGANIZATION_CODE@': // 机构编码
      value = platformOrganization.code;
      break;
    case '@ORGANIZATION_ID@': // 机构Id
      value = platformOrganization.id;
      break;
    case '@COMPANY_ID@': // 所属公司ID
      value = realUser.companyId;
      break;
    case '@COMPANY_NAME@': // 所属公司名称
      value = realUser.companyName;
      break;
    case '@NOW_DATE@': // 当前日期
      value = simpleDateFormat(new Date(), 'YYYY-MM-DD');
      break;
    case '@NOW_TIME@': // 当前日式时间
      value = simpleDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss');
      break;
    case '@NOW_YEARMONTH@': //当前年月
      value = simpleDateFormat(new Date(), 'YYYY-MM');
      break;
    case '@NOW_MONTH@': // 当前月
      value = simpleDateFormat(new Date(), 'YYYY-MM');
      break;
    case '@NOW_YEAR@': // 当前年
      value = simpleDateFormat(new Date(), 'YYYY');
      break;
    default:
      //处理系统变量
      // eslint-disable-next-line no-case-declarations
      const result = value.match(/@.*?@/g)?.[0]?.replaceAll('@', '');
      if (!isEmpty(result)) {
        // 账号信息
        if (result.startsWith('ACCOUNT.')) {
          value = get(account, result.replace('ACCOUNT.', ''));
          // 用户信息
        } else if (result.startsWith('USER.')) {
          value = get(realUser, result.replace('USER.', ''));
        } else {
          value = getSystemConfig(result);
        }
      }
      break;
  }
  return value || '';
}
/**
 * 解析查询参数
 * @param {*} param0
 */
export function toQuerysTemplate({ querys, model, parentModel = {}, useModel = true }) {
  querys?.forEach((item, index) => {
    if (!isEmpty(item)) {
      if (isString(item.value)) {
        // 父功能数据
        if (item.value?.startsWith('{parent.')) {
          item.value = toTemplate(item.value.replace('{parent.', '{'), parentModel);
        } else if (item.value?.startsWith('{') && !useModel && !item.value?.startsWith('{@')) {
          querys[index] = null;
        } else {
          item.value = toTemplate(item.value, model);
        }
      } else if (isArray(item.value)) {
        toQuerysTemplate({ querys: item.value, model, parentModel, useModel });
      } else if (isPlainObject(item.value)) {
        // 子查询
        toQuerysTemplate({ querys: item.value.conditions, model, parentModel, useModel });
      }
    }
  });
}
/**
 * 解析使用范围
 * @param {*} scope PC,ALL,APP
 * @returns
 */
export function parseUseScope(scope) {
  if (isEmpty(scope)) {
    return true;
  } else if (isApp) {
    return ['ALL', 'APP'].includes(scope);
  } else {
    return ['ALL', 'PC'].includes(scope);
  }
}
