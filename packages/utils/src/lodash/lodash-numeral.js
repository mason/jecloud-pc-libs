/* eslint-disable no-useless-escape */
import numeral from 'numeral';
import { isNumeric } from './lodash-plus';
// 注册通用自定义格式
numeral.register('format', 'custom', {
  regexps: {
    format: /[^0-9,\.]*0[0,\.]*[^0-9,\.]*/,
    unformat: /[^0-9,\.]*([\d,\.]+)[^0-9,\.]*/,
  },
  format: function (value, format, roundingFunction) {
    // 使用正则表达式提取前缀、数字格式和后缀
    var match = format.match(/([^0-9,\.]*)(0[0,\.]*)([^0-9,\.]*)/);
    var prefix = match[1];
    var numberFormat = match[2];
    var suffix = match[3];

    // 格式化数字
    var formattedNumber = numeral._.numberToFormat(value, numberFormat, roundingFunction);

    // 返回带有前缀和后缀的格式化数字
    return prefix + formattedNumber + suffix;
  },
  unformat: function (string) {
    // 使用正则表达式提取数字部分
    var match = string.match(/[^0-9,\.]*([\d,\.]+)[^0-9,\.]*/);
    return match ? numeral._.stringToNumber(match[1]) : NaN;
  },
});
/**
 * 格式化数值
 * @param {*} value
 * @param {*} format
 * @returns
 */
export function numberFormat(value, format) {
  if (value && format && isNumeric(value)) {
    return numeral(value).format(format);
  }
  return value;
}
