import { SendCommandEnum } from './enum';
import JeWebSocket from './websocket';
export * from './enum';
export { JeWebSocket };
/**
 * 系统WebSockt
 */
let currentWebSocket = null;
/**
 * 获取 WebSocket实例
 * @returns
 */
export function getWebSocket() {
  return currentWebSocket;
}
/**
 * 连接 WebSocket
 */
export function connectWebSocket() {
  currentWebSocket?.connect();
}
/**
 * 关闭 WebSocket
 */
export function closeWebSocket() {
  currentWebSocket?.close();
}

/**
 * 安装 WebSocket
 * @param {*} param0
 */
export function setupWebSocket(options) {
  const { websocket } = options;
  if (websocket) {
    currentWebSocket = websocket;
  } else {
    // 创建新的websocket
    currentWebSocket = new JeWebSocket(options);
  }
  return currentWebSocket;
}
/**
 * 发送 WebSocket消息
 * @param {*} body 消息体
 */
export function sendWebSocketMessage(body) {
  currentWebSocket?.send(SendCommandEnum.PUSH, body);
}
