/**
 * app socket
 */
export default class SocketTaskApp {
  socket = null;
  constructor(url) {
    this.socket = uni.connectSocket({
      url, //仅为示例，并非真实接口地址。
      complete: () => {},
    });
  }

  get readyState() {
    return this.socket.readyState;
  }
  set onopen(callback) {
    this.socket.onOpen(callback);
  }
  set onclose(callback) {
    this.socket.onClose(callback);
  }
  set onmessage(callback) {
    this.socket.onMessage(callback);
  }
  set onerror(callback) {
    this.socket.onError(callback);
  }

  send(data) {
    this.socket.send({ data });
  }
  close(...args) {
    this.socket.close(...args);
  }

  static valid() {
    return uni.getNetworkType().then(({ networkType }) => {
      if (networkType === 'none') {
        return Promise.reject({ type: 'unnetwork' });
      }
    });
  }
}
