import { SendCommandEnum, ReceiveMessageEnum } from './enum';
import HandshakeMessage from './model/handshake-message';
import SendMessage from './model/send-message';
import ReceiveMessage from './model/receive-message';
import { simpleDateFormat, isEmpty } from '../lodash';
import SocketTask from './task';

/**
 * 重连次数
 */
const reconnectTimes = 10;
/**
 * websocket
 */
export default class JeWebSocket {
  /**
   * socket链接
   */
  url;

  /**
   * socket实例
   */
  instance;

  /**
   * 设备信息
   */
  device = 'web';
  /**
   * 推送设备ID
   */
  cid;

  /**
   * 销毁
   */
  destroyed = false;
  /**
   * 调试，开始日志输出
   */
  debugger = false;
  /**
   * 未读消息
   */
  _unreadMessage = [];
  /**
   * 发送失败的消息
   */
  _failedMessage = [];

  /**
   * 所有消息
   */
  _onMessageEvent;
  /**
   * 系统未读消息事件
   */
  _onMessage4UnreadEvent;

  /**
   * 打开连接事件
   */
  _onOpenEvent;

  constructor(options) {
    this.url = options.url;
    this.cid = options.cid;
    this.device = options.device || this.device;
    this.debugger = options.debugger || this.debugger;
    this._onOpenEvent = options.onOpen;
    this._onWebsocketLog = options.onLog;
    this._onMessageEvent = options.onMessage;
    this._onMessage4UnreadEvent = options.onUnreadMessage;
  }

  /**
   * 启用调试
   * @param {*} debug
   */
  enableDebugger(debug = true) {
    this.debugger = !!debug;
  }

  /**
   * 是否连接
   * @return {boolean}
   */
  isConnected() {
    return this.getSocket()?.readyState == 1;
  }
  /**
   * 获得websocket对象
   * @return {WebSocket}
   */
  getSocket() {
    return this.instance;
  }
  /**
   * 连接
   * @param {string} url 链接地址
   */
  connect(url) {
    this.validConnect(url)
      .then(() => {
        this.doConnect(url);
      })
      .catch(({ type }) => {
        messageLog(this, { type });
      });
  }

  /**
   * 校验链接状态
   * @param {*} url
   * @returns
   */
  validConnect(url) {
    if (this.isConnected()) {
      if (url && this.url !== url) {
        // 关闭原有链接
        this.close();
        return Promise.resolve();
      } else {
        return Promise.reject({ type: 'unconnect' });
      }
    } else {
      return SocketTask.valid();
    }
  }

  /**
   * 创建连接
   * @param {*} url
   */
  doConnect(url) {
    // 重置销毁标识
    this.destroyed = false;
    // 链接地址
    this.url = url || this.url;
    // 创建连接
    this.instance = SocketTask.createTask(this.url);
    // 接收消息
    this.instance.onmessage = ({ data }) => {
      // 重置心跳
      this.heartBeat();
      // 空消息不处理
      if (isEmpty(data)) {
        messageLog(this, { type: 'error', message: `empty message：${data}` });
        return;
      }
      messageLog(this, { type: 'message', message: data });
      const message = new ReceiveMessage(data);
      // 握手成功，如果有发送失败消息，重新发送
      if (message.type === ReceiveMessageEnum.HANDSHAKE) {
        this._failedMessage.forEach(({ cmd, body }) => {
          this.send(cmd, body);
        });
        this._failedMessage = [];
      }
      // 处理消息
      this.onReceive(message);
    };
    // 连接打开
    this.instance.onopen = () => {
      if (this.isConnected()) {
        messageLog(this, { type: 'open' });
        // 打开心跳
        this.heartBeat();
        // 触发打开连接事件
        this._onOpenEvent?.();
      }
    };
    // 连接关闭
    this.instance.onclose = (evnt) => {
      this.handshakeSuccess = false;
      if (this.instance) {
        // 通知消息
        messageLog(this, { type: 'close', message: evnt });
        // 销毁后，禁止断线重连
        if (!this.destroyed && !this._reconnecting) {
          this.reconnect();
        }
      }
    };
    // 连接错误
    this.instance.onerror = (evnt) => {
      this.handshakeSuccess = false;
      messageLog(this, { type: 'error', message: evnt });
    };

    messageLog(this, { type: 'connect', message: this.url });
  }
  /**
   * 重连
   */
  reconnect(reset) {
    if (reset) {
      this._reconnecting = false;
      this._reconnectCheckTimes = 0;
    }
    // 重置重连
    clearTimeout(this._reconnectFn);

    // 连接成功，停止重连 || 达到重连次数，停止重连
    const reconnectFailed = this._reconnectCheckTimes == reconnectTimes;
    if (this.isConnected() || reconnectFailed) {
      if (reconnectFailed) {
        // 重连失败
        this.close();
      } else {
        messageLog(this, { type: 'reconnect' });
      }
      return;
    }

    // 重连中标记
    this._reconnecting = true;
    // 重连计数
    this._reconnectCheckTimes = this._reconnectCheckTimes || 0;
    this._reconnectCheckTimes++;

    // 连接
    messageLog(this, { type: 'reconnectNumber', message: this._reconnectCheckTimes });
    this.connect();
    // 延时重连
    this._reconnectFn = setTimeout(() => {
      this.reconnect();
    }, this._reconnectCheckTimes * 1000);
  }

  /**
   * 关闭
   */
  close() {
    this.destroyed = true;
    if (this.isConnected()) {
      // 销毁重连
      clearTimeout(this._reconnectFn);
      // 销毁心跳
      clearInterval(this._heartBeatFn);
      // 关闭连接
      this.instance.close();
    }
    this.instance = null;
    messageLog(this, { type: 'destroyed' });
  }

  /**
   * 握手成功
   */
  handshakeSuccess = false;

  /**
   * 通讯握手
   * @param {string} userId 用户id，绑定推送
   * @param {Object} [options] 其他配置信息
   */
  handshake(userId, options = {}) {
    // 防止重复握手
    if (!this.handshakeSuccess) {
      this.send(
        SendCommandEnum.HANDSHAKE,
        new HandshakeMessage({ device: this.device, cid: this.cid, userId, ...options }),
      );
      this.handshakeSuccess = true;
    }
  }
  /**
   * 踢出
   * @param {string} userId 用户id，绑定推送
   * @param {Object} [options] 其他配置信息
   */
  kick(userId, options = {}) {
    this.send(
      SendCommandEnum.KICK,
      new HandshakeMessage({ device: this.device, cid: this.cid, userId, ...options }),
    );
    this.handshakeSuccess = false;
  }

  /**
   * 心跳检测
   */
  heartBeat() {
    // 重置心跳
    clearInterval(this._heartBeatFn);

    // 没有连接，停止心跳
    if (!this.isConnected()) {
      return;
    }

    // 轮询检测心跳
    const second = 15; // 心跳时间  秒
    this._heartBeatFn = setInterval(() => {
      this.send(SendCommandEnum.HEARTBEAT);
    }, second * 1000);
  }
  /**
   * 发送消息
   */
  send(cmd = SendCommandEnum.CHAT, body) {
    const packet = new SendMessage({ cmd, body });
    if (!Object.values(SendCommandEnum).includes(cmd)) {
      console.error('非法的消息类型，请检查', cmd, body);
      return;
    }
    if (this.isConnected()) {
      this.instance.send(JSON.stringify(packet));
    } else if (
      !this.instance ||
      [this.instance.CLOSED, this.instance.CLOSING].indexOf(this.instance.readyState) != -1
    ) {
      messageLog(this, { type: 'beginReconnect' });
      this._failedMessage.push(packet);
      this.reconnect(true);
    } else if (this.instance.readyState == this.instance.CONNECTING) {
      messageLog(this, { type: 'reconnecting' });
      this._failedMessage.push(packet);
    }
  }
  /**
   * 接收消息
   * @param {*} message
   */
  onReceive(message) {
    const { type, content } = message;
    switch (type) {
      case ReceiveMessageEnum.HANDSHAKE: // 握手消息
        !content.success && messageLog(this, { type: 'handshake', message: content.msg });
        break;
      case ReceiveMessageEnum.HEARTBEAT: // 心跳消息
        break;
      case ReceiveMessageEnum.NOREAD_MESSAGE: // 未读消息
        try {
          const unreadMessage = content.map?.((item) => {
            return new ReceiveMessage(item);
          });
          this._onMessage4UnreadEvent?.(unreadMessage);
        } catch (error) {
          console.error(error);
        }
        break;
      default:
        // 其他消息
        try {
          this._onMessageEvent?.(message);
        } catch (error) {
          console.error(error);
        }
        break;
    }
  }
}

/**
 * 消息输出
 * @param {*} param0
 */
function messageLog(socket, { type, message }) {
  const info = { type: 'info', message, log: '' };
  switch (type) {
    case 'open':
      info.type = 'success';
      info.log = 'Web Socket opened!';
      break;
    case 'close':
      info.type = 'error';
      info.log = 'Web Socket closed!';
      break;
    case 'error':
      info.type = 'error';
      info.log = 'Web Socket error!';
      break;
    case 'connect':
      info.log = 'Web Socket try connect server, url=';
      break;
    case 'reconnect':
      info.type = 'success';
      info.log = 'Web Socket reconnect success !';
      break;
    case 'reconnectNumber':
      info.log = 'Web Socket reconnect count ：';
      break;
    case 'beginReconnect':
      info.log = 'Web Socket begin reconnect ...';
      break;
    case 'reconnecting':
      info.log = 'Web Socket reconnecting ...';
      break;
    case 'message':
      info.log = 'Web Socket receive message：';
      break;
    case 'handshake':
      info.type = 'error';
      info.log = 'Web Socket handshake error!';
      break;
    case 'destroyed':
      info.type = 'error';
      info.log = 'Web Socket destroyed!';
      break;
    case 'unconnect':
      info.type = 'error';
      info.log = 'Web Socket is connected!';
      break;
    case 'unnetwork':
      info.type = 'error';
      info.log = 'The network is disconnected！';
      break;
  }
  const style = {
    info: 'color:blue',
    success: 'color:green',
    error: 'color:red',
  };
  info.log = `【${simpleDateFormat(new Date())}】${info.log}`;
  socket._onWebsocketLog?.(info);
  if (socket.debugger) {
    console.log(`%c ${info.log}`, style[info.type], info.message);
  }
}
