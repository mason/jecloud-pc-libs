import { pick } from 'lodash-es';

/**
 * 全局JE对象
 */
let JE = {};
/**
 * 注册全局JE对象
 * @param {*} JEObject
 */
export function setupJE(app, { Ui, Utils, Vue, Func, CLI_ENVS }) {
  // 基础类库
  mixinJE({
    /**
     * vue实例
     */
    $vue: app,
    /**
     * UI库对象
     * @returns
     */
    useUi() {
      return Ui;
    },
    /**
     * 工具类对象
     * @returns
     */
    useUtils() {
      return Utils;
    },
    /**
     * Vue对象
     * @returns
     */
    useVue() {
      return Vue;
    },
    /**
     * 系统配置变量
     * @returns
     */
    useCliEnvs() {
      return CLI_ENVS;
    },
    /**
     * 项目版本
     * @returns
     */
    getProjectVersion() {
      return CLI_ENVS?.VUE_APP_VERSION;
    },
    /**
     * 项目名称
     * @returns
     */
    getProjectName() {
      return CLI_ENVS?.VUE_APP_PROJECT;
    },
    /**
     * 主应用函数，只有在主应用使用有效
     */
    useAdmin() {},
    useSystem() {
      console.warn('请使用JE.xxx()函数进行调用，后期会废弃掉JE.useSystem().xxx();');
      return { ...Func, watchWebSocket() {} };
    },
  });
  // JE常用方法
  mixinJE({
    ...pick(Vue, ['h']),
  });
  // 工具类
  if (Utils) {
    setupUtils(Utils);
  }
  // 安装功能对象
  if (Func) {
    setupFunc(Func);
  }
  // 配置UI库
  if (Ui) {
    Ui.ConfigProvider?.setup({ publicPath: CLI_ENVS?.PUBLIC_PATH });
  }
  return JE;
}
/**
 * 安装工具库
 * @param {*} Utils
 */
function setupUtils(Utils) {
  mixinJE({
    ...pick(Utils, [
      'uuid',
      'ajax',
      'syncAjax',
      'encode',
      'decode',
      'pinyin',
      'uploadFile',
      'isString',
      'isNumber',
      'isNumeric',
      'isBoolean',
      'isDate',
      'isPromise',
      'isArray',
      'isObject',
      'isEmpty',
      'isNotEmpty',
      'createDeferred',
      'toValue',
      'dateFormat',
      'dateParse',
      'dateClearTime',
      'getCurrentAccount',
      'getCurrentUser',
      'getSystemConfig',
      'initCurrentAccount',
      'toDefaultValue',
      'getDDInfo',
      'getDDItemInfo',
      'getDDItemList',
      'toDDItemText',
      'callCustomFn',
      'updateCustomFn',
      'registCustomStyle',
      'executeInsert',
      'executeInsertAndInit',
      'executeUpdate',
      'executeDelete',
      'executeSelect',
      'excelImport',
      'excelExport',
      'loadDataSource',
    ]),
    ...pick(Utils.Modal, ['alert', 'confirm', 'dialog', 'message', 'notice', 'window']),
  });
}

/**
 * 安装功能对象
 * @param {*} Func
 */
export function setupFunc(Func) {
  // JE常用方法
  mixinJE({
    /**
     * 功能对象
     * @returns
     */
    useFunc() {
      return Func;
    },
    ...pick(Func, [
      'showFunc',
      'showFuncForm',
      'showFuncSelect',
      'showTreeSelect',
      'showUserSelect',
      'showSelectWindow',
    ]),
  });
}

/**
 * 使用全局工具类JE
 *
 * @export
 * @return { $vue,$i18n,$router,...utils }
 */
export function useJE() {
  return JE;
}

/**
 * 混入到JE方法和变量
 *
 * @export
 * @param {*} object
 * @return {*}
 */
export function mixinJE(object) {
  return Object.assign(JE, object || {});
}
/**
 * 调用vue方法
 * @param {*} methodName
 * @param  {...any} args
 */
export function vueCall(methodName, ...args) {
  const vue = useJE().useVue();
  return vue?.[methodName]?.(...args);
}
/**
 * APP环境
 */
export let isApp = false;
/**
 * 使用App环境
 */
export function useApp() {
  isApp = true;
}
