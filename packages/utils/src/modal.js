import { useJE, vueCall } from './je';
/**
 * 全局Modal函数
 */
export const Modal = {
  alert: (...args) => uiModalAction('alert', ...args),
  confirm: (...args) => uiModalAction('confirm', ...args),
  dialog: (...args) => uiModalAction('dialog', ...args),
  message: (...args) => uiModalAction('message', ...args),
  notice: (...args) => uiModalAction('notice', ...args),
  window: (...args) => uiModalAction('window', ...args),
  toast: (...args) => uiModalAction('toast', ...args),
  status: {
    /**
     * 消息
     */
    info: 'info',
    /**
     * 成功
     */
    success: 'success',
    /**
     * 警告
     */
    warning: 'warning',
    /**
     * 错误
     */
    error: 'error',
    /**
     * 疑问
     */
    question: 'question',
  },
};

/**
 * 调用UI的Modal函数
 * @param {*} action
 * @param  {...any} args
 * @returns
 */
const uiModalAction = (action, ...args) => {
  return getGlobalModal()?.[action]?.(...args);
};

let globalModal;
/**
 * 获取全局Modal对象
 * @returns
 */
export function getGlobalModal() {
  if (globalModal) {
    // 兼容移动端
    return vueCall('isRef', globalModal) ? globalModal.value : globalModal;
  } else {
    return useJE().useUi()?.Modal;
  }
}
/**
 * 设置全局Modal对象
 * @param {*} $modal
 */
export function setGlobalModal($modal) {
  // 兼容移动端
  globalModal = $modal;
}

const registModalRefs = {};
/**
 * 注册全局modalRef，方便路由变化时关闭已打开的窗口
 * App端使用
 * @param {string} key
 * @param {Ref} modalRef
 */
export function registModalRef(key, modalRef) {
  registModalRefs[key] = modalRef;
}
/**
 * 关闭所有注册的modalRef，包含popup，只需要提供close即可
 * App端使用
 */
export function closeAllModal() {
  // 关闭默认全局modal
  globalModal?.closeAll?.();
  // 关不注册的modal
  Object.keys(registModalRefs).forEach((key) => {
    const modalRef = registModalRefs[key];
    if (modalRef.value) {
      if (modalRef.value?.closeAll) {
        modalRef.value.closeAll?.();
      } else {
        modalRef.value.close?.();
      }
    } else {
      delete registModalRefs[key];
    }
  });
}
