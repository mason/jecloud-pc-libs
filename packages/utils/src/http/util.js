import { getAjaxInstance } from './instance';
/**
 * 获取ajax基础请求链接
 * @returns {string}
 */
export function getAjaxBaseURL() {
  return getAjaxInstance().getBaseURL();
}
// 兼容
export const getAxiosBaseURL = getAjaxBaseURL;

/**
 * 检查请求状态
 * @param code 错误码
 * @param message 错误信息
 * @returns {Object} {code,message}
 */
export function checkStatus(code, message) {
  if (message) {
    return { code, message };
  }
  let errMessage = '';
  switch (code) {
    case 400:
    case 490:
      errMessage = `${message}`;
      break;
    // 401: Not logged in
    // Jump to the login page if not logged in, and carry the path of the current page
    // Return to the current page after successful login. This step needs to be operated on the login page.
    case 401:
      errMessage = '用户未登陆，请先登陆！';
      break;
    case 403:
      errMessage = '用户得到授权，但是访问是被禁止的。!';
      break;
    // 404请求不存在
    case 404:
      errMessage = '网络请求错误,未找到该资源!';
      break;
    case 405:
      errMessage = '网络请求错误,请求方法未允许!';
      break;
    case 408:
      errMessage = '网络请求超时!';
      break;
    case 500:
      errMessage = '服务器错误,请联系管理员!';
      break;
    case 501:
      errMessage = '网络未实现!';
      break;
    case 502:
      errMessage = '网络错误!';
      break;
    case 503:
      errMessage = '服务不可用，服务器暂时过载或维护!';
      break;
    case 504:
      errMessage = '网络超时!';
      break;
    case 505:
      errMessage = 'http版本不支持该请求!';
      break;
  }
  return { code, message: errMessage };
}

/**
 * 格式化数据
 *
 * @export
 * @return {*}
 */
export function transformAjaxData(data) {
  // TODO: 数据兼容，后期去掉，后台返回数据格式，参考：mock/util.js
  const _data = {};
  if (data.obj) {
    // 正常返回
    _data.data = data.obj;
  } else if (!Object.prototype.hasOwnProperty.call(data, 'success')) {
    // 直接返回对象
    _data.data = data;
    _data.success = true;
    _data.code = 200;
    _data.message = 'ok';
    data = {}; // 清空原始数据
  }
  ['obj', 'rows', 'totalCount'].forEach((key) => {
    delete data[key];
  });
  Object.assign(data, _data);

  // 返回业务数据
  if (data.success) {
    return Promise.resolve(data.data);
  } else {
    return Promise.reject(data);
  }
}
