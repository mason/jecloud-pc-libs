import { AjaxEventEnum, RequestEnum, ContentTypeEnum } from './enum';
import { getGlobalInstance, setGlobalInstance } from '../interceptor';
import { cloneDeep, isPromise } from '../lodash';
import { checkStatus } from './util';
import { GLOBAL_INTERCEPTORS, emitGlobalInterceptor } from '../interceptor';

/**
 * ajax实例
 */
let ajaxInstance;
const ajaxInstanceKey = 'global-ajax-instance';
/**
 * 获得ajax实例
 * @returns {AjaxInstance}
 */
export function getAjaxInstance() {
  let instance = ajaxInstance || getGlobalInstance(ajaxInstanceKey);
  if (!instance) {
    instance = new AjaxInstance();
    setGlobalInstance(ajaxInstanceKey, instance);
  }
  ajaxInstance = instance;
  return instance;
}
/**
 * 设置ajax实例
 * @param {*} instance
 */
export function setAjaxInstance(instance) {
  ajaxInstance = instance;
}

/**
 * ajax实例
 */
class AjaxInstance {
  constructor() {}
  /**
   * 基础接口地址
   *
   * @memberof AjaxInstance
   */
  baseURL = '';
  /**
   * 拦截器
   */
  interceptors = {};

  /**
   * 基础配置
   *
   * @memberof AjaxInstance
   */
  defaultConfig = {
    timeout: 30 * 1000, // 超时时间
    method: RequestEnum.POST, // 请求方法
    headers: { 'Content-Type': ContentTypeEnum.FORM_URLENCODED }, // 请求头
  };

  /**
   * 设置默认配置
   * @param {Object} config 默认配置
   * @param {string} config.baseURL 基础接口地址
   * @param {number} config.timeout 超时时间：30000
   * @param {string} config.method 请求方法：POST
   * @param {Object} config.headers 请求头：{'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}
   */
  setDefaultConfig(config = {}) {
    if (config.timeout < 1) {
      config.timeout = this.defaultConfig.timeout;
    }
    Object.assign(this.defaultConfig, config);
  }
  /**
   * 获取默认配置
   * @returns 默认配置
   */
  getDefaultConfig() {
    return this.defaultConfig;
  }
  /**
   * 设置基础接口地址
   * @param {string} baseURL
   */
  setBaseURL(baseURL = '') {
    if (baseURL.endsWith('/')) {
      baseURL = baseURL.substring(0, baseURL.length - 1);
    }
    this.baseURL = baseURL;
  }
  /**
   * 基础接口地址
   * @returns baseURL
   */
  getBaseURL() {
    return this.baseURL || '';
  }
  /**
   * 全局请求前事件，return false可以终止请求
   * @param {Function} fn
   */
  onBeforeRequest(fn) {
    this.interceptors[AjaxEventEnum.BEFORE_REQUEST] = fn;
  }
  /**
   * 全局请求完成事件
   * @param {Function} fn
   */
  onResponse(fn) {
    this.interceptors[AjaxEventEnum.RESPONSE] = fn;
  }
  /**
   * 全局请求失败事件
   * @param {Function} fn
   */
  onResponseCatch(fn) {
    this.interceptors[AjaxEventEnum.RESPONSE_CATCH] = fn;
  }

  /**
   * 触发拦截器
   * @param {string} eventName 事件名称
   * @param  {...any} args
   */
  fire(eventName, ...args) {
    return this.interceptors[eventName]?.(...args);
  }

  /**
   * 请求拦截器
   * @param {Object} config 请求配置
   * @param {boolean} sync 同步
   * @returns {boolean|Promise}
   */
  requestInterceptors(config, sync) {
    // 解析请求配置
    config = this.transformRequestConfig(config);
    // 全局拦截器，业务使用，可以修改请求参数
    emitGlobalInterceptor(GLOBAL_INTERCEPTORS.AJAX.BEFORE_REQUEST, { config, sync });
    // 系统拦截器
    const result = this.fire(AjaxEventEnum.BEFORE_REQUEST, { config, sync });
    // 同步拦截器
    if (sync) {
      return result;
    } else {
      if (isPromise(result)) {
        return result;
      } else if (result === false) {
        return Promise.reject(config);
      } else {
        return Promise.resolve(config);
      }
    }
  }
  /**
   * 响应拦截器处理
   * @param {*} response 响应对象
   * @param {*} config 请求配置
   * @returns 处理后的响应数据
   */
  responseInterceptors(response, config) {
    // 全局拦截器，业务使用
    emitGlobalInterceptor(GLOBAL_INTERCEPTORS.AJAX.BEFORE_RESPONSE, { config, response });
    // 禁用拦截器
    if (config.responseInterceptors === false) return response;

    // 系统拦截器
    const tempData = this.fire(AjaxEventEnum.RESPONSE, { config, response });
    return tempData || response;
  }

  /**
   * 响应错误处理
   * @param {*} error 响应错误
   * @param {*} config 请求配置
   * @returns 错误信息
   */
  responseInterceptorsCatch(error, config) {
    // 全局拦截器，业务使用
    emitGlobalInterceptor(GLOBAL_INTERCEPTORS.AJAX.BEFORE_RESPONSE_CATCH, { config, error });
    // 禁用拦截器
    if (config.responseInterceptorsCatch === false) return error;
    error.message =
      error.message ||
      checkStatus(error.status, error.data)?.message ||
      error.errMsg ||
      error.statusText;
    const tempData = this.fire(AjaxEventEnum.RESPONSE_CATCH, { config, error });
    return tempData || error;
  }

  /**
   *
   * @param {*} config
   * @returns
   */
  transformRequestConfig(config) {
    // 如果为字符串，默认为url
    if (typeof config === 'string') {
      config = { url: config };
    }
    // 默认配置
    const defaultConfig = this.getDefaultConfig();
    // baseURL 处理
    config.url = (config.baseURL !== false ? config.baseURL || this.baseURL : '') + config.url;
    // headers 处理
    config.headers = Object.assign({ ...defaultConfig.headers }, config.headers || {});
    // 请求参数解析
    // 处理开启参数加密后,加密参数错乱
    config.params = cloneDeep(config.params) || config.data || {};
    // 请求时长处理
    config.timeout = config.timeout > 0 ? config.timeout : defaultConfig.timeout;
    // 请求函数
    config.method = config.method || defaultConfig.method;
    // 非POST请求，删除Content-Type
    if (RequestEnum.POST !== config.method.toLocaleUpperCase()) {
      delete config.headers['Content-Type'];
    }
    // 原始配置信息
    config.rawConfig = cloneDeep(config);
    return config;
  }
}
