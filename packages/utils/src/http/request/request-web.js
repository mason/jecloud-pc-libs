import axios from 'axios';
import qs from 'qs';
import { ContentTypeEnum, RequestEnum } from '../enum';
/**
 * web request
 * @param {Object} config 配置信息
 * @returns {Promise}
 */
export function request4web(config) {
  // 请求参数处理，对外只需配置params，内部自行处理
  if (config.method?.toLocaleUpperCase() === RequestEnum.POST) {
    config.data = config.params;
    delete config.params;
    // 支持 form-data
    const contentType = config.headers?.['Content-Type'] || config.headers?.['content-type'];
    if (contentType === ContentTypeEnum.FORM_URLENCODED) {
      config.data = qs.stringify(config.data, { arrayFormat: 'brackets' });
    }
  } else {
    delete config.data;
  }

  return axios
    .request(config)
    .then((response) => {
      response.statusCode = response.status; // 兼容uni
      return response;
    })
    .catch((err) => err.response);
}
