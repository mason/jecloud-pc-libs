import { ContentTypeEnum } from '../enum';
import { API_DOCUMENT_FILE } from '../../system/api/urls';
import { isApp } from '../../je';
import { request4web } from './request-web';
import { request4app } from './request-app';
import { getAjaxInstance } from '../instance';
import { request4sync } from './request-sync';
/**
 * request
 * @param {*} config
 * @returns
 */
function request(config) {
  return isApp ? request4app(config) : request4web(config);
}

/**
 * ajax数据请求
 * @param {Object|String} config 配置信息，如果为String，默认为url
 * @param {string} config.url 请求链接
 * @param {Object} [config.params]  请求参数
 * @param {Object} [config.headers] 请求头，默认{ 'Content-Type': ContentTypeEnum.FORM_URLENCODED }
 * @param {string} [config.method]  请求方式，默认POST
 * @param {number} [config.timeout] 请求时长，默认：300000毫秒
 * @param {Object} [config.token]   请求时进行token校验，默认：true
 * @param {string} [config.baseURL] 基础接口地址
 * @returns {Promise}
 */
export function ajax(config) {
  const ajaxInstance = getAjaxInstance();
  return ajaxInstance
    .requestInterceptors(config) // 请求前拦截器
    .then((options) => request(options || config)) // 请求
    .then((response) => {
      const { statusCode } = response;
      if (statusCode >= 200 && statusCode < 300) {
        const result = ajaxInstance.responseInterceptors(response, config);
        return Promise.resolve(result);
      } else {
        const result = ajaxInstance.responseInterceptorsCatch(response, config);
        return Promise.reject(result);
      }
    })
    .catch((error) => Promise.reject(ajaxInstance.responseInterceptorsCatch(error, config)));
}

/**
 * 文件上传
 * @param {Object} config axios配置信息
 * @param {Object} config.fileInfo 上传信息
 * @param {string} config.fileInfo.name 文件字段名
 * @param {File} config.fileInfo.file 文件
 * @param {string} [config.fileInfo.fileName] 文件名，默认读取文件自身的名字
 * @returns {Promise}
 */
export function upload(config = {}) {
  // 默认上传路径
  const { url = API_DOCUMENT_FILE, fileInfo, params = {}, headers = {} } = config;

  if (!fileInfo) {
    return Promise.reject('上传文件参数：fileInfo为空，上传失败！');
  }
  // 上传参数
  const formData = new window.FormData();

  // 添加文件信息
  formData.append(
    fileInfo.name || 'files',
    fileInfo.file,
    fileInfo.fileName || fileInfo.file?.name,
  );

  // 添加额外参数
  Object.keys(params).forEach((key) => {
    formData.append(key, params[key]);
  });

  Object.assign(config, {
    url,
    method: 'POST',
    data: formData,
    ignoreCancelToken: true,
    headers: {
      'Content-Type': ContentTypeEnum.FORM_DATA,
      ...headers,
    },
  });
  return ajax(config);
}

/**
 * 同步ajax数据请求，仅支持web调用，非特殊情况不建议使用
 * @param {Object|String} config 配置信息，如果为String，默认为url
 * @param {string} config.url 请求链接
 * @param {Object} [config.params]  请求参数
 * @param {Object} [config.headers] 请求头，默认{ 'Content-Type': ContentTypeEnum.FORM_URLENCODED }
 * @param {string} [config.method]  请求方式，默认POST
 * @param {number} [config.timeout] 请求时长，默认：300000毫秒
 * @param {Object} [config.token]   请求时进行token校验，默认：true
 * @param {string} [config.baseURL] 基础接口地址
 * @returns {Object}
 */
export function syncAjax(config) {
  const ajaxInstance = getAjaxInstance();
  // 自定义拦截器
  const result = ajaxInstance.requestInterceptors(config, true);
  if (result !== false) {
    const response = request4sync(config);
    const statusCode = response.status;
    if (statusCode >= 200 && statusCode < 300) {
      // 兼容写法
      response.statusCode = statusCode;
      return ajaxInstance.responseInterceptors(response, config);
    } else {
      return ajaxInstance.responseInterceptorsCatch(response, config);
    }
  }
}
