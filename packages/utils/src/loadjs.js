import _loadjs from 'loadjs';
/**
 * 动态加载脚本文件
 * @param args
 * @returns
 */
export function loadjs(...args) {
  if (args.length) {
    return _loadjs(...args);
  }
  return _loadjs;
}
