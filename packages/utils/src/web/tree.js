import { forEach, isArray } from 'lodash-es';
var helperCreateTreeFunc = function (handle) {
  return function (obj, iterate, options, context) {
    var opts = options || {};
    var optChildren = opts.children || 'children';
    return handle(null, obj, iterate, context, [], [], optChildren, opts);
  };
};

function findTreeItem(parent, obj, iterate, context, path, node, parseChildren, opts) {
  if (obj) {
    var item, index, len, paths, nodes, match;
    for (index = 0, len = obj.length; index < len; index++) {
      item = obj[index];
      paths = path.concat(['' + index]);
      nodes = node.concat([item]);
      if (iterate.call(context, item, index, obj, paths, parent, nodes)) {
        return { index: index, item: item, path: paths, items: obj, parent: parent, nodes: nodes };
      }
      if (parseChildren && item) {
        match = findTreeItem(
          item,
          item[parseChildren],
          iterate,
          context,
          paths.concat([parseChildren]),
          nodes,
          parseChildren,
          opts,
        );
        if (match) {
          return match;
        }
      }
    }
  }
}

/**
 * 从树结构中查找匹配第一条数据的键、值、路径
 *
 * @param {Object} obj 对象/数组
 * @param {Function} iterate(item, index, items, path, parent, nodes) 回调
 * @param {Object} options {children: 'children'}
 * @param {Object} context 上下文
 * @return {Object} { item, index, items, path, parent, nodes }
 */
const findTree = helperCreateTreeFunc(findTreeItem);

function mapTreeItem(parent, obj, iterate, context, path, node, parseChildren, opts) {
  var paths, nodes, rest;
  var mapChildren = opts.mapChildren || parseChildren;
  return obj.map(function (item, index) {
    paths = path.concat(['' + index]);
    nodes = node.concat([item]);
    rest = iterate.call(context, item, index, obj, paths, parent, nodes);
    if (rest && item && parseChildren && item[parseChildren]) {
      rest[mapChildren] = mapTreeItem(
        item,
        item[parseChildren],
        iterate,
        context,
        paths,
        nodes,
        parseChildren,
        opts,
      );
    }
    return rest;
  });
}

/**
 * 从树结构中指定方法后的返回值组成的新数组
 *
 * @param {Object} obj 对象/数组
 * @param {Function} iterate(item, index, items, path, parent, nodes) 回调
 * @param {Object} options {children: 'children'}
 * @param {Object} context 上下文
 * @return {Object/Array}
 */
const mapTree = helperCreateTreeFunc(mapTreeItem);
/**
 * 从当前传入节点，递归循环处理属性节点，返回false，停止
 *
 * @param {*} node
 * @param {*} iterate
 */
const cascadeTree = function (node, iterate) {
  const nodes = isArray(node) ? node : [node];
  forEach(nodes, (item) => {
    if (iterate(item) !== false) {
      cascadeTree(item.children || [], iterate);
    } else {
      return false;
    }
  });
};
export { findTree, mapTree, cascadeTree };
