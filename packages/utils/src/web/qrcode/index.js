import QRCode from './qrcode';
import { isString } from 'lodash-es';
import { vueCall } from '../../je';
import { Modal } from '../../modal';

/**
 * 生成二维码
 *
 * @param {HTMLElement|string} el target element or 'id' attribute of element.
 * @param {Object|string} options
 * @param {string} options.value 文本
 * @param {number} [options.size=256] 尺寸
 * @param {string} [options.color="#000000"] 二维码色
 * @param {string} [options.bgColor="#ffffff"] 二维码背景色
 * @param {string} [options.level=M] 'L' | 'M' | 'Q' | 'H' 二维码纠错等级
 */
export function createQRCode(el, options) {
  if (isString(options)) {
    options = { value: options };
  }
  const {
    value,
    size = 400,
    color = '#000000',
    bgColor = '#ffffff',
    level = QRCode.CorrectLevel.M,
  } = options;
  const config = {
    text: value,
    width: size,
    height: size,
    colorDark: color,
    colorLight: bgColor,
    correctLevel: QRCode.CorrectLevel[level],
  };

  return new QRCode(el, config);
}
/**
 * 打开二维码窗口
 * @param {*} options
 * @returns
 */
export function showQRCodeModal(options) {
  const qrcodeRef = vueCall('ref');
  let { value, size = 300, color, bgColor, level, modalOptions = {} } = options;
  size = size < 300 ? 300 : size;
  const width = size + 22;
  const height = size + 52;
  return Modal.dialog({
    title: options.title || '请扫码',
    width,
    minWidth: width,
    height,
    minHeight: height,
    headerStyle: { height: '30px', fontSize: '16px' },
    bodyStyle: { padding: '10px' },
    showFooter: false,
    resize: false,
    ...modalOptions,
    content() {
      return vueCall('h', 'div', { ref: qrcodeRef });
    },
    onShow() {
      createQRCode(qrcodeRef.value, { value, size, color, bgColor, level });
    },
  });
}
