/**
 * 删除全局样式
 * @param {string} id 样式ID
 */
export function removeStyleSheet(id) {
  const cssDom = document.getElementById(id);
  cssDom?.parentElement?.removeChild(cssDom);
}
/**
 * 添加全局样式
 * @param {string} cssText  样式
 * @param {string} [id]   样式ID
 */
export function createStyleSheet(cssText, id) {
  removeStyleSheet(id);
  const doc = document;
  const head = doc.getElementsByTagName('head')[0];

  const styleEl = doc.createElement('style');

  styleEl.setAttribute('type', 'text/css');
  if (id) {
    styleEl.setAttribute('id', id);
  }

  try {
    styleEl.appendChild(doc.createTextNode(cssText));
  } catch (e) {
    styleEl.textContent = cssText;
  }
  head.appendChild(styleEl);
}
