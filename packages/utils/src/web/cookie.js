import JsCookie from 'js-cookie';

export const cookie = {
  get(name) {
    const localStorageData = localStorage.getItem(name);
    const cookieData = JsCookie.get(name);
    if (localStorageData && !cookieData) {
      JsCookie.set(name, localStorageData);
    }
    return cookieData || localStorageData;
  },
  set(name, value, options) {
    localStorage.setItem(name, value);
    return JsCookie.set(name, value, options);
  },
  remove(name, options) {
    localStorage.removeItem(name);
    JsCookie.remove(name, options);
  },
};
