import { pick, isEmpty } from '../../../lodash';
import { vueCall } from '../../../je';
import { Modal } from '../../../modal';
import { loadDDItemByCode } from '../../../system/dictionary';
import { WorkflowOperatorEnum } from '../enum';
import TaskAssignee from './task-assignee';
import TaskNode from './task-node';
import { getSubmitNodeUsersApi, getTaskNodeInfoApi, getSubmitNodeInfoApi } from '../api';
import { Data } from '../../../data';
/**
 * 基础状态数据
 */
const baseState = {
  activeStep: 1, // 激活的流程步骤
  activeNode: null, // 当前激活节点
  directOperation: false, // 直接操作，不需要步骤操作
  taskMessages: [], // 审批意见
  taskNodes: [], // 任务节点
  assignees: {}, // 送交人员信息
  comment: '同意', // 流程提交信息
  name: '', // 操作按钮名称
};
/**
 * 流程审批操作
 */
export default class WorkflowOperation {
  /**
   * 流程对象
   */
  $workflow = null;
  /**
   * 操作按钮
   */
  button = null;
  /**
   * 状态数据
   */
  state = vueCall('reactive', { ...baseState });
  /**
   * 参数
   */
  params = {};
  /**
   * 操作参数的keys
   */
  operationParamKeys = {
    assignee: 'assignee', // 提交人
    comment: 'comment', // 审批意见
    target: 'target', // 目标id
    sequentials: 'sequentials', // 多人审批，0 并序执行，1 顺序执行
    isJump: 'isJump', // 是否可跳跃, 1可跳跃,0不可跳跃
  };
  /**
   * 提交完成函数
   */
  finishSubmit;

  constructor({ $workflow, button, finishSubmit }) {
    this.$workflow = $workflow;
    this.init({ button, finishSubmit });
  }

  /**
   * 初始化，重置数据
   */
  init({ button, finishSubmit }) {
    // 重置基础数据
    Object.assign(this.state, baseState);
    // 操作按钮名称
    this.state.name = button.name;
    // 默认审批意见
    this.state.comment = button.customizeComments || this.state.comment;
    // 操作按钮对象
    this.button = button;
    // 当前操作节点
    this.setActiveNode(this.$workflow.state.currentTaskNode);
    // 重置函数
    this.finishSubmit = finishSubmit;
  }

  /**
   * 设置当前送交的节点信息
   */
  setActiveNode(node, activeType) {
    // 设置激活节点
    this.state.activeNode = node;
    if (activeType) {
      // 第一步切换节点时,给是否直接提交重新赋值
      this.state.directOperation = node.submitDirectly;
    }
  }

  /**
   * 设置送交节点信息
   * @param {*} param0
   * @returns
   */
  setAssigneeNodes({ nodeAssigneeInfo }) {
    let activeStep = 0;
    // 设置目标节点信息
    this.state.activeNode.setAssigneeNodes(nodeAssigneeInfo);
    // 设置目标节点送交信息
    this.state.assignees = {};
    this.state.activeNode.assigneeNodes?.forEach((node) => {
      const assignee = new TaskAssignee(node);
      // 默认选中人员数据
      assignee.setUsers(node.getDefaultSelectUsers());
      this.state.assignees[node.id] = assignee;

      // 如果只有一个节点,只有一个人默认选中
      if (this.state.activeNode.assigneeNodes.length == 1 && node.users.length == 1) {
        const root = Data.createTreeNodeModel({ children: node.users[0].children });
        const nodes = [];
        root.cascade((node) => {
          if (node.nodeInfoType === 'json') {
            nodes.push(node);
          }
        });
        // 只有一个人默认选中该人
        if (nodes.length == 1) {
          assignee.setUsers(nodes);
          // 跳转第三步 pc
          activeStep = 3;
        }
      }
    });
    return { nodes: this.state.activeNode.assigneeNodes, activeStep };
  }

  /**
   * 获取提交参数
   * @returns
   */
  getSubmitParams(params = {}) {
    const submitParams = {
      comment: this.state.comment || '', // 审批意见
      target: this.state.activeNode.target, // 目标顺序流id
      isJump: this.state.activeNode.isJump, // 是否可跳跃, 1可跳跃,0不可跳跃
    };
    // 送交信息
    const assignees = Object.values(this.state.assignees);
    // 解析送交人信息
    submitParams.assignee = TaskAssignee.parseAssigneeParam(assignees);
    // 解析会签、多人节点审批顺序传参
    submitParams.sequentials = TaskAssignee.parseSequentialParam(assignees);

    return { ...submitParams, ...params };
  }

  /**
   * 精准的参数
   * @returns
   */
  getSubmitParams4Accurate() {
    const operationId = this.commomParams.operationId;
    const paramkeys = Object.keys(this.state.commonParams);
    switch (operationId) {
      case WorkflowOperatorEnum.PROCESS_EMPTY_START_OPERATOR: // 启动
      case WorkflowOperatorEnum.PROCESS_CANCEL_OPERATOR: // 撤销
      case WorkflowOperatorEnum.TASK_RETREIEVE_OPERATOR: // 取回
      case WorkflowOperatorEnum.TASK_URGE_OPERATOR: // 催办
      case WorkflowOperatorEnum.TASK_CLAIM_OPERATOR: // 领取
      case WorkflowOperatorEnum.TASK_CANCEL_DELEGATE_OPERATOR: // 取消委托
      case WorkflowOperatorEnum.PROCESS_HANG_OPERATOR: // 挂起
      case WorkflowOperatorEnum.PROCESS_ACTIVE_OPERATOR: // 激活
        break;
      case WorkflowOperatorEnum.PROCESS_EMPTY_SPONSOR_OPERATOR: // 发起
      case WorkflowOperatorEnum.TASK_SUBMIT_OPERATOR: // 送交
        paramkeys.push(
          this.operationParamKeys.assignee,
          this.operationParamKeys.comment,
          this.operationParamKeys.target,
        );
        break;
      case WorkflowOperatorEnum.TASK_DIRECT_SEND_OPERATOR: // 直送
      case WorkflowOperatorEnum.TASK_GOBACK_OPERATOR: // 退回
      case WorkflowOperatorEnum.PROCESS_INVALID_OPERATOR: // 作废
      case WorkflowOperatorEnum.TASK_PASS_OPERATOR: //通过
      case WorkflowOperatorEnum.TASK_VETO_OPERATOR: // 否决
      case WorkflowOperatorEnum.TASK_ABSTAIN_OPERATOR: // 弃权
        paramkeys.push(this.operationParamKeys.comment);
        break;
      case WorkflowOperatorEnum.TASK_TRANSFER_OPERATOR: // 转办
      case WorkflowOperatorEnum.TASK_DELEGATE_OPERATOR: // 委托
        paramkeys.push(
          this.operationParamKeys.assignee, // 单人
          this.operationParamKeys.comment,
        );
        break;
      case WorkflowOperatorEnum.TASK_DISMISS_OPERATOR: // 驳回
        paramkeys.push(
          this.operationParamKeys.target, // 单人
          this.operationParamKeys.comment,
        );
        break;
    }
    return pick(this.getSubmitParams(), paramkeys);
  }

  /**
   * 初始节点人员信息
   * 切换节点后，更新人员信息
   * @param {*} param0
   * @returns
   */
  initAssigneeNodes() {
    const params = this.$workflow.getOperationParams({ target: this.state.activeNode.target });
    return getSubmitNodeUsersApi(params)
      .then((data) => this.setAssigneeNodes(data))
      .catch((e) => {
        Modal.alert(e.message, 'error');
      });
  }

  /**
   * 初始化按钮操作窗口信息
   */
  initOperationInfo() {
    const { operationId } = this.button;
    const { currentNode } = this.button.workflowConfig;
    const operation = this;
    let promise;
    const params = this.$workflow.getOperationParams(); // 操作参数
    switch (operationId) {
      case WorkflowOperatorEnum.TASK_DIRECT_SEND_OPERATOR: // 直送
      case WorkflowOperatorEnum.TASK_GOBACK_OPERATOR: // 退回
      case WorkflowOperatorEnum.TASK_DISMISS_OPERATOR: // 驳回
      case WorkflowOperatorEnum.TASK_SIGN_BACK_OPERATOR: //回签
      case WorkflowOperatorEnum.TASK_STAGING_OPERATOR: //暂存
        // 请求节点信息
        promise = getTaskNodeInfoApi(params).then((data) => {
          return Array.isArray(data) ? data : [data];
        });

        // 直接操作，不需要人员
        operation.state.directOperation = true;
        break;
      case WorkflowOperatorEnum.PROCESS_INVALID_OPERATOR: // 作废
      case WorkflowOperatorEnum.TASK_PASS_OPERATOR: // 通过
      case WorkflowOperatorEnum.TASK_VETO_OPERATOR: // 否决
      case WorkflowOperatorEnum.TASK_ABSTAIN_OPERATOR: // 弃权
        // 使用当前节点
        promise = Promise.resolve([currentNode]);
        // 直接操作，不需要人员
        operation.state.directOperation = true;
        break;
      case WorkflowOperatorEnum.TASK_DELEGATE_OPERATOR: // 委托
      case WorkflowOperatorEnum.TASK_TRANSFER_OPERATOR: // 转办
      case WorkflowOperatorEnum.TASK_COUNTERSIGN_OPERATOR: // 加签
        // 使用当前节点
        promise = Promise.resolve([currentNode]);
        break;
      default:
        // 正常提交操作
        promise = getSubmitNodeInfoApi(params);
        break;
    }

    return Promise.all([promise, loadDDItemByCode('JE_WF_APPROOPINION')]).then(
      ([nodes, messages]) => {
        //如果节点信息为空
        if (isEmpty(nodes)) {
          return Promise.reject();
        }
        // 处理数据
        operation.state.taskMessages = messages; // 提交常用语
        operation.state.taskNodes = nodes?.map((node) => new TaskNode(node, operation.button)); // 提交节点
        operation.setActiveNode(operation.state.taskNodes[0]); // 当前激活节点

        if (operation.state.directOperation || operation.state.activeNode.submitDirectly) {
          // 直接操作，不需要步骤
          operation.state.directOperation = true;
        } else if (operation.state.taskNodes.length === 1 && operation.state.activeNode.end) {
          // 结束节点
          operation.state.directOperation = true;
          operation.state.name = '结束';
        }
        return operation;
      },
    );
  }

  /*------------------------------APP使用------------------------------ */
  /**
   * 校验流程提交
   * TODO:只有审批窗口提交使用
   * @returns
   */
  beforeButtonSubmit() {
    const { state } = this;
    // 校验审批意见
    if (isEmpty(state.comment)) {
      return Promise.reject({ message: '请输入审批意见！', type: 'comment' });
    }
    // 校验人员
    if (!state.directOperation) {
      const errors = [];
      Object.values(state.assignees).forEach((assignee) => {
        if (isEmpty(assignee.users)) {
          errors.push({ assignee });
        }
      });
      if (errors.length > 0) {
        return Promise.reject({ message: '请选择人员！', type: 'user', errors });
      }
    }
    return Promise.resolve();
  }
}
