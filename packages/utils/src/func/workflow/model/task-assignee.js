import { encode } from '../../../lodash';
/**
 * 送交信息
 */
export default class TaskAssignee {
  constructor(options) {
    this.type = options.type;
    this.id = options.id;
    this.name = options.name;
    this.sequential = options.sequential;
    this.users = options.users || [];
  }
  /**
   * 设置用户信息
   * @param {*} users
   */
  setUsers(users = []) {
    this.users = users;
  }
  /**
   * 送交节点信息
   */
  toAssignee() {
    const assignee = { nodeId: this.id, nodeName: this.name };
    // 送交人
    assignee.assignee = this.users.map((item) => item.id).join(',');
    assignee.assigneeName = this.users
      .map((item) => (item.bean && item.bean.ACCOUNT_NAME) || item.text)
      .join(',');
    return assignee;
  }
  /**
   * 顺序，并序信息
   */
  toSequential() {
    return {
      nodeId: this.id,
      sequential: this.sequential,
    };
  }
  /**
   * 解析送交人
   * @param {*} nodes
   * @returns
   */
  static parseAssigneeParam(nodes) {
    const assignees = nodes.map((node) => node.toAssignee());
    return assignees.length > 0 ? encode(assignees) : '[]';
  }
  /**
   * 解析会签、多人节点审批顺序传参
   * @param {*} nodes
   * @returns
   */
  static parseSequentialParam(nodes) {
    const sequentials = nodes
      .filter((node) => ['countersign', 'batchtask'].includes(node.type))
      .map((node) => node.toSequential());
    return sequentials.length > 0 ? encode(sequentials) : '[]';
  }
}
