import { decode } from '../../../lodash';
import { getDDItemInfo } from '../../../system/dictionary';
/**
 * 流程历史节点
 */
export default class WorkflowHistory {
  /**
   * 审批人
   */
  assignees = [];
  /**
   * 审批人信息，包含用户信息，审批内容
   */
  comments = [];
  /**
   * 流程耗时
   */
  durationInMillis = '';
  /**
   * 审批开始时间
   */
  startTime = '';
  /**
   * 审批结束时间
   */
  endTime = '';
  /**
   * 节点ID
   */
  nodeId = '';
  /**
   * 节点名称
   */
  nodeName = '';
  /**
   * 节点类型
   */
  nodeType = '';
  /**
   * 操作类型名称
   */
  operationType = '';
  /**
   * 操作类型编码
   */
  operationTypeEnum = '';
  /**
   * 操作类型颜色
   */
  operationTypeColor = '';
  /**
   * 任务状态名称
   */
  state = '';
  /**
   * 任务状态编码
   */
  stateEnum = '';
  /**
   * 任务状态颜色
   */
  stateColor = '';
  /**
   * 显示标题
   */
  isShowTitle = '';
  /**
   * 标题
   */
  title = '';
  constructor(options) {
    Object.assign(this, options);
    //流程任务审批操作类型
    this.operationType = options.operationType;
    this.operationTypeEnum = options.operationTypeEnum;
    const operationTypeDD = getDDItemInfo('JE_WF_OPERATION_TYPE', this.operationTypeEnum);
    operationTypeDD && (this.operationTypeColor = operationTypeDD.textColor);
    // 流程任务状态
    this.state = options.stateName;
    this.stateEnum = options.state;
    const stateDD = getDDItemInfo('JE_WF_TASK_STATUS', this.stateEnum);
    stateDD && (this.stateColor = stateDD.textColor);

    // 内容信息
    this.comments = this.comments.map((item) => {
      //会签流转结果字典颜色处理
      const countersignresultDD = getDDItemInfo('JE_WF_OPERATION_TYPE', item.commentTypeEnum);
      countersignresultDD && (item.commentTypeColor = countersignresultDD.textColor);
      // 头像
      item.userAvatar = item.userAvatar ? decode(item.userAvatar)[0]?.fileKey : '';
      return item;
    });
  }
}
