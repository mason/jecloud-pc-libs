/**
 * 公共操作参数
 */
export default class TaskCommonParams {
  constructor(options) {
    this.pdid = options.pdid;
    this.piid = options.piid;
    this.taskId = options.taskId;
    this.operationId = options.operationId;
    this.tableCode = options.tableCode;
    this.funcCode = options.funcCode;
    this.funcId = options.funcId;
    this.prod = options.prod;
    this.beanId = options.beanId;
  }
}
