import { toBoolean } from '../../../lodash';
import { WorkflowStatusEnum } from '../enum';
import TaskNode from './task-node';
/**
 * 流程配置
 */
export default class WorkflowConfig {
  get isModel() {
    return true;
  }
  constructor(options) {
    if (options.isModel) return options;
    /**
     * 异步树
     */
    this.asynTree = toBoolean(options.asynTree);
    /**
     * 全选人员
     */
    this.selectAll = toBoolean(options.selectAll);
    /**
     * 简易审批
     */
    this.simpleApproval = toBoolean(options.simpleApproval);
    /**
     *简易审批 意见
     */
    this.enableSimpleComments = toBoolean(options.enableSimpleComments);
    /**
     * 流程审批状态
     */
    this.status = options.audFlag;
    this.statusText = options.audFlagName;

    /**
     * 流程结束
     */
    this.isEnd = this.status === WorkflowStatusEnum.ENDED;
    /**
     * 流程作废
     */
    this.isSuspend = this.status === WorkflowStatusEnum.SUSPEND;
    /**
     * 流程审批中
     */
    this.isWait = this.status === WorkflowStatusEnum.WAIT;
    /**
     * 流程未启动
     */
    this.isUnStart = this.status === WorkflowStatusEnum.NOSTATUS;
    /**
     * 紧急程度是否开启
     */
    this.exigency = toBoolean(options.exigency);
    /**
     * 紧急程度
     */
    this.exigencyValue = options.exigencyValue || '';
    /**
     * 当前节点
     */
    this.currentNode = new TaskNode(options);
  }
}
