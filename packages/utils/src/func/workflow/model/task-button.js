import { WorkflowOperatorEnum } from '../enum';
import { decode, kebabCase, toBoolean } from '../../../lodash';

/**
 * 任务操作按钮
 */
export default class TaskButton {
  constructor(options) {
    this.id = options.id; // id
    this.code = options.code; // 编码
    this.name = options.name; // 名称
    this.icon = options.icon; // 图标
    this.customizeComments = options.customizeComments; // 默认审批意见
    this.operationId = options.operationId; // 操作id
    this.pdid = options.pdid;
    this.piid = options.piid;
    this.taskId = options.taskId;
    // 以下是自定义属性
    this.exigency = toBoolean(options.exigency); // 紧急状态
    this.simpleApproval = toBoolean(options.simpleApproval); // 简易审批
    this.enableSimpleComments = toBoolean(options.enableSimpleComments); // 简易审批意见
    this.isEnd = options.isEnd; // 结束按钮标记
    this.workflowConfig = options.workflowConfig; // 流程配置
    this.currentNodeId = options.workflowConfig?.currentNode.id; // 当前任务节点ID
    this.events = parseEvents(options.pcListeners); // 事件

    // 启动显隐表达式
    if (options.displayExpressionWhenStarted || options.displayExpressionWhenStartedFn) {
      this.visibleExps = {
        exp: options.displayExpressionWhenStarted,
        fn: options.displayExpressionWhenStartedFn,
      };
    }
  }
  getBeforeEvent() {
    return this.events['before-click'];
  }
  getAfterEvent() {
    return this.events['after-click'];
  }
}
/**
 * 解析事件
 * @param {*} data
 * @returns
 */
function parseEvents(data) {
  const eventData = data ? decode(data) : {};
  const events = {};
  Object.keys(eventData).forEach((key) => {
    events[kebabCase(key)] = eventData[key];
  });
  return events;
}

/**
 * 切换任务按钮，前端使用
 */
export const TaskChangeButton = () => ({
  id: 'taskChangeBtn',
  code: 'taskChangeBtn',
  name: '切换任务',
  icon: 'fal fa-exchange',
  operationId: WorkflowOperatorEnum.TASK_CHANGE_OPERATOR,
});

/**
 * 流程结束按钮，前端使用
 */
export const TaskEndButton = () => ({
  id: 'taskEndBtn',
  code: 'taskEndBtn',
  name: '流程已结束',
  isEnd: true,
});

/**
 * 流程作废按钮，前端使用
 * @returns
 */
export const TaskCancellationButton = () => ({
  id: 'taskCancellationBtn',
  code: 'taskCancellationBtn',
  name: '流程已作废',
  isEnd: true,
});

/**
 * 背景色
 */
export const TaskButtonBgColor = '#edf0f1';
