import { useCustomAction } from './custom-action';
import { useOperatorAction } from './operator-action';
export function useWorkflowAction($workflow) {
  const funcs = {
    ...useOperatorAction(),
    ...useCustomAction(),
  };
  const actions = {};
  Object.keys(funcs).forEach((key) => {
    actions[key] = funcs[key].bind(this, $workflow);
  });
  return actions;
}
