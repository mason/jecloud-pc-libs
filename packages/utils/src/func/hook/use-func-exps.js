import { vueCall } from '../../je';

/**
 * 监听bean，处理字段表达式
 */
export function useWatchFieldExps({ $func }) {
  // 绑定字段的监听
  const watchFieldExps4Bind = () => {
    const funcData = $func.getFuncData();
    // 解析绑定字段的表达式，并进行监听
    Object.keys(funcData.bindExps).forEach((bindField) => {
      vueCall(
        'watch',
        () => [bindField, $func.store.activeBean[bindField], $func.store.activeBeanEmitter],
        ([name]) => {
          vueCall('nextTick', () => {
            $func.action.doFormFieldExps4BindFields({ fieldExps: funcData.bindExps[name] });
          });
        },
      );
    });
  };
  // 所有字段的监听
  const watchFieldExps4All = () => {
    vueCall(
      'watch',
      () => [$func.store.activeBean, $func.store.activeBeanEmitter],
      () => {
        vueCall('nextTick', () => {
          $func.action.doFormFieldExps().then(() => {
            // 执行流程表单权限控制
            $func.$workflow?.refreshFormConfig();
          });
        });
      },
      { deep: true },
    );
  };
  const watchFieldExps = () => {
    watchFieldExps4Bind();
    watchFieldExps4All();
  };

  return { watchFieldExps };
}
