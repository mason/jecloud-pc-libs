import { FuncManager } from '../model';
import { useInjectFunc, useInjectMainFunc } from './use-func-context';
const managers = new Map();
/**
 * 功能管理
 * @param {*} options
 * @returns
 */
export function useManager(options) {
  const { funcCode, props, context } = options;
  const $func = new FuncManager({ funcCode, props, context });
  // 父功能
  $func.setParentFunc(useInjectFunc());
  return $func;
}

export function setFuncManager($func) {
  const mainFunc = useInjectMainFunc();
  let { isWindow, isDDFunc, funcDDCode, funcCode } = $func;
  if (mainFunc?.isWindow) {
    isWindow = true;
  }
  const keys = [isDDFunc ? funcDDCode : funcCode];
  if (isWindow) {
    keys.push('window');
  }
  managers.set(keys.join('-'), $func);
}

export function getFuncManager(funcCode, isWindow) {
  return isWindow ? managers.get(`${funcCode}-window`) : managers.get(funcCode);
}
