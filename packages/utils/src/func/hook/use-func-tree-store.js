import {
  FuncTreeTypeEnum,
  TreeQueryParser,
  API_COMMON_GET_TREE,
  API_DICTIONARY_LOAD_TREE,
  parseCommonTreeUrl,
  API_COMMON_LOAD_GRID_TREE,
} from '../func';
import { Data } from '../../data';
import { omit } from 'lodash-es';

/**
 * 树形store
 * @param {Object} options
 * @param {string} options.type 类型
 * @param {string} options.url 请求链接
 * @param {Object} options.params 请求参数
 * @param {Array} options.querys 查询条件（功能树）
 * @param {Array} options.dictionarys 字典项配置（字典树）
 * @param {boolean} options.onlyItem 只返回字典项数据（字典树）
 * @param {string} options.product 产品编码（自定义树）
 * @param {FuncManager} options.$func 功能对象
 * @param {FuncTreeView} options.$tree 树形对象
 * @returns {TreeStore}
 */
export function useFuncTreeStore(options) {
  const { params, onlyItem, $func, $tree } = options;
  const { queryParser, url, headers } = parseTreeInfo(options);
  // 创建store
  const store = Data.treeGridStore({
    autoLoad: false,
    proxy: {
      type: 'ajax',
      url,
      headers,
      params,
    },
  });

  // 绑定解析器
  store.queryParser = queryParser;

  // 查询前解析查询条件
  store.on('before-load', function ({ options }) {
    // 解析功能数据参数
    let model = $func?.getBean?.() || {};
    let parentModel = $func?.getParentFuncBean?.() || {};

    // 解析树形配置查询参数
    const treeParams = queryParser.toParams({ model, parentModel });
    // 解析查询参数（排除strData，防止覆盖字典树形参数）
    const queryParams = options.params ? omit(options.params, ['strData']) : {};

    // 更新查询参数
    options.params = Object.assign(treeParams, queryParams);
  });
  // 默认展开第一级
  store.on('load', () => {
    // 1. 快速查询树，默认展开第一层
    // 2. 其他树，第一层有一条数据时展开，多条不展开
    if (!onlyItem || store.data.length === 1) {
      $tree?.setTreeExpand(store.data, true);
    }
  });
  return store;
}
/**
 * 根据类型解析树形配置
 * @param {Object} options
 * @param {string} options.type 类型
 * @param {string} options.url 请求链接
 * @param {Array} options.querys 查询条件（功能树）
 * @param {Array} options.dictionarys 字典项配置（字典树）
 * @param {boolean} options.onlyItem 只返回字典项数据（字典树）
 * @param {string} options.product 产品编码（自定义树）
 * @param {FuncManager} options.$func 功能对象
 * @returns {Obejct} {queryParser,url,headers}
 */
function parseTreeInfo({ type, url, querys, dictionarys, onlyItem, product, $func }) {
  let queryParser;
  let headers = {};
  let storeUrl = '';
  const treeOptions = $func?.getFuncData().info.treeOptions;
  switch (type) {
    case FuncTreeTypeEnum.FUNC: // 功能树
      storeUrl = parseCommonTreeUrl(
        treeOptions?.showType == 'tree' ? API_COMMON_LOAD_GRID_TREE : API_COMMON_GET_TREE,
      );
      queryParser = new TreeQueryParser({
        type,
        funcCode: $func?.funcCode,
        querys,
      });
      // 解析功能数据
      const funcData = $func?.getFuncData?.();
      if (funcData) {
        const { productCode, tableCode, funcAction } = funcData.info;
        queryParser.tableCode = tableCode;
        queryParser.initQuerys({ $func });
        headers.pd = productCode;
        storeUrl = parseCommonTreeUrl(
          treeOptions?.showType == 'tree' ? API_COMMON_LOAD_GRID_TREE : API_COMMON_GET_TREE,
          funcAction,
        );
      }
      break;
    case FuncTreeTypeEnum.DD: // 字典树
      storeUrl = API_DICTIONARY_LOAD_TREE;
      queryParser = new TreeQueryParser({
        type,
        dictionarys,
        onlyItem,
      });
      break;
    case FuncTreeTypeEnum.CUSTOM: // 普通树
      storeUrl = url || parseCommonTreeUrl(API_COMMON_GET_TREE);
      queryParser = new TreeQueryParser({
        type,
      });
      headers.pd = product;
      break;
  }

  return { queryParser, url: storeUrl, headers };
}
