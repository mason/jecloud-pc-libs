import {
  isNotEmpty,
  cloneDeep,
  isPlainObject,
  isArray,
  encode,
  toDefaultValue,
} from '../../lodash';
import { getFuncCache } from '../util';
import { QueryTypeEnum } from '../enum';

export default class FuncStore {
  funcCode = '';
  funcData = null;
  funcCrumbs = []; // 功能编码层级
  loading = false;
  activeView = ''; // 功能激活视图
  rawActiveView = ''; // 功能激活视图初始值
  hidden = false; // 子功能使用
  readonly = false; // 只读
  initBeanId = null;
  initBean = null;
  initQuerys = [];
  initOrders = [];
  activeBean = {};
  activeRawBean = {}; // 激活bean，原始数据
  activeBeanEmitter = 0; // 辅助触发器，达到手动调用watch方法
  activeRow = null; // 选中行
  activeWorkflow = null; //激活流程信息
  parentStore = null; // 父功能store
  childFuncStores = {}; // 子功能store
  childFuncConfig = {}; // 子功能配置
  formError = {
    // 错误提示
    popoverVisible: false,
    errorFields: [],
  };

  // APP当前查询信息
  appActiveSearch = {
    [QueryTypeEnum.GROUP]: {},
    [QueryTypeEnum.ORDER]: {},
    [QueryTypeEnum.STRATEGY]: '',
    [QueryTypeEnum.KEYWORD]: '',
  };

  constructor(options) {
    this.funcCode = options.funcCode;
    this.activeView = this.rawActiveView = options.activeView;
    this.initBean = options.bean;
    this.initBeanId = options.beanId;
    this.initOrders = options.orders || this.initOrders;
    this.initQuerys = options.querys || this.initQuerys;
    this.readonly = options.readonly || this.readonly;
    this.funcCrumbs.push(this.funcCode);
  }
  /**
   * 获得当前功能数据
   * @returns
   */
  getFuncData() {
    return this.funcData;
  }
  /**
   * 设置功能数据
   * @param {*} funcData
   */
  setFuncData(funcData) {
    this.funcData = funcData;
  }
  /**
   * 获得业务数据主键
   * @returns
   */
  getBeanId() {
    return this.activeBean[this.funcData?.pkCode];
  }
  /**
   * 获得当前的业务bean
   * @returns
   */
  getBean() {
    return this.activeBean;
  }
  /**
   * 添加子功能store
   * @param {*} store
   */
  addChildStore(store) {
    store.parentStore = this;
    this.childFuncStores[store.funcCode] = store;
  }
  /**
   * 添加子功能配置
   * @param {*} code 功能编码
   * @param {*} config 功能配置
   */
  addChildConfig(code, config) {
    this.childFuncConfig[code] = config;
  }
  /**
   * 获取子功能配置
   * @param {*} code
   * @returns
   */
  getChildConfig(code) {
    return code ? this.childFuncConfig[code] : this.childFuncConfig;
  }
  /**
   * 获取功能默认信息
   * @returns
   */
  getDefaults() {
    const funcData = this.getFuncData();
    const values = {};
    // 默认值
    Object.assign(values, funcData.getDefaultValues());
    Object.keys(values).forEach((key) => {
      values[key] = toDefaultValue(values[key]);
    });
    // 父功能信息
    const parentInfo = this.getParentInfo();
    return {
      values: { ...values, ...parentInfo.values },
      querys: parentInfo.querys,
    };
  }
  /**
   * 父功能信息
   */
  getParentInfo(bean) {
    const info = { values: {}, querys: [], config: [] };
    // 父功能数据
    const { parentStore } = this;
    if (parentStore) {
      const activeBean = bean || parentStore.activeBean;
      if (activeBean) {
        // 父功能数据
        const parentFuncData = getFuncCache(parentStore.funcCode);
        // 关联关系
        const child = parentFuncData.basic.children.get(this.funcCode);
        child?.associations.forEach((item) => {
          const value = activeBean[item.parentCode];
          // 传值
          if (item.enableValue && isNotEmpty(value, true)) {
            info.values[item.code] = value;
          }
          // 查询
          if (item.enableQuery) {
            if (item.queryType === 'idit') {
              info.querys.push(...item.customQuerys);
            } else {
              info.querys.push({ code: item.code, type: item.queryType, value });
            }
          }
        });
        info.config = child?.associations || [];
      } else {
        info.querys = [{ code: 1, type: '!=', value: 1 }];
      }
    }
    return info;
  }
  /**
   * 设置功能操作的列表row
   * @param {*} row
   */
  setActiveRow(row) {
    this.activeRow = row;
  }
  /**
   * 设置功能激活bean数据
   * @param {*} bean
   */
  setActiveBean(bean) {
    this.activeRawBean = cloneDeep(bean);
    this.activeBean = bean;
  }
  /**
   * 更新bean数据
   * @param {*} bean
   */
  commitActiveBean(bean) {
    Object.assign(this.activeRawBean, bean);
    Object.assign(this.activeBean, bean);
  }
  /**
   * 重置bean数据
   */
  resetActiveBean() {
    this.activeBean = {};
    this.activeRawBean = {};
  }
  /**
   * 获得改变的数据
   * @returns
   */
  getBeanChanges() {
    const changes = {};
    Object.keys(this.activeBean).forEach((key) => {
      let oldValue = this.activeBean[key];
      let newValue = this.activeRawBean[key];
      // 处理数组，对象，转成字符串进行比较
      oldValue = isArray(oldValue) || isPlainObject(oldValue) ? encode(oldValue) : oldValue;
      newValue = isArray(newValue) || isPlainObject(newValue) ? encode(newValue) : newValue;
      if (oldValue != newValue) {
        changes[key] = {
          field: key,
          oldValue: this.activeRawBean[key],
          newValue: this.activeBean[key],
        };
      }
    });
    return changes;
  }
}
