import { ajax, transformAjaxData } from '../../http';
import {
  API_RBAC_USER_LOAD_PRESONINFO,
  API_RBAC_USER_ADD_COMMONUSER,
  API_RBAC_USER_GET_PRESONINFOECHO,
  API_RBAC_USER_FIND_ASYNC_NODES,
} from './urls';

/**
 * 人员选择器加载左侧树(部门人员,角色人员,岗位人员,常用人员)
 * @param {*} param0
 * @returns
 */
export function loadUserPersoninfoApi({ params }) {
  return ajax({
    url: API_RBAC_USER_LOAD_PRESONINFO,
    params,
  }).then(transformAjaxData);
}

/**
 * 人员选择器添加常用人员
 * @param {*} param0
 * @returns
 */
export function addUserCommonUserApi({ params }) {
  return ajax({
    url: API_RBAC_USER_ADD_COMMONUSER,
    params,
  }).then(transformAjaxData);
}

/**
 * 人员选择去回显查询人员信息
 * @param {*} param0
 * @returns
 */
export function getUserPersonInfoEchoApi({ params }) {
  return ajax({
    url: API_RBAC_USER_GET_PRESONINFOECHO,
    params,
  }).then(transformAjaxData);
}
/**
 * 人员选择器异步查询
 * @param {*} param0
 * @returns
 */
export function findAsyncUsers(params) {
  return ajax({
    url: API_RBAC_USER_FIND_ASYNC_NODES,
    params,
  }).then(transformAjaxData);
}
