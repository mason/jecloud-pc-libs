import { ajax, transformAjaxData } from '../../http';
import { getCurrentUser } from '../../lodash';
import {
  API_MESSAGE_COLLECTION_FORM_DATA,
  API_MESSAGE_SHARE_FORM_DATA,
  API_MESSAGE_SHARE_FORM_WHETHER,
} from './urls';
/**
 * 收藏表单数据
 * @param {*} param0
 * @returns
 */
export function collectionFormDataApi({ tableCode, funcCode, funcName, pkValue, title }) {
  const user = getCurrentUser();
  return ajax({
    url: API_MESSAGE_COLLECTION_FORM_DATA,
    params: {
      tableCode,
      funcCode,
      funcName,
      pkValue,
      userId: user.id,
      deptId: user.realOrg.id,
      COLLECTION_SCBT: title,
    },
  }).then(transformAjaxData);
}
/**
 * 共享表单数据
 * @param {*} param0
 * @returns
 */
export function shareFormDataApi({
  tableCode,
  funcCode,
  funcName,
  pkValue,
  title,
  accountIds,
  userNames,
}) {
  return ajax({
    url: API_MESSAGE_SHARE_FORM_DATA,
    params: {
      tableCode,
      funcCode,
      funcName,
      pkValue,
      accountIds,
      userNames,
      SHARE_TITLE: title,
    },
  }).then(transformAjaxData);
}

/**
 * 表单是否共享
 */
export function loadWhetherShareFormApi(params) {
  return ajax({
    url: API_MESSAGE_SHARE_FORM_WHETHER,
    params,
  }).then(transformAjaxData);
}
