import { ajax, transformAjaxData } from '../../http';
import { encode, isEmpty, getCurrentUser } from '../../lodash';
import {
  API_FUNC_LOAD_FUNC,
  API_FUNC_LOAD_FUNC_INFO,
  API_COMMON_LOAD,
  API_FUNC_LOAD_MODEL_FIELDS,
  parseCommonUrl,
  API_FUNC_LOAD_PERMS,
  API_FUNC_LOAD_GROUP_QUERY,
  API_FUNC_CLEAR_STATICIZE,
  API_META_GRID_EXPORT_KEY,
  API_META_GRID_EXPORT_EXCEL,
  API_FUNC_LOAD_FILE_INFO,
} from './urls';

/**
 * 功能信息查询信息
 * @returns
 */
export function loadFunctionApi({ funcCode }) {
  // 元数据
  return loadFuncInfoApi({ funcCode }).then((data) => {
    if (isEmpty(data)) {
      return Promise.reject({ code: '404' });
    }
    const params = { funcId: data.JE_CORE_FUNCINFO_ID, tableCode: data.FUNCINFO_TABLENAME };
    return Promise.all([
      Promise.resolve(data),
      loadColumnsApi(params).then((data) => data.rows),
      loadFieldsApi(params).then((data) => data.rows),
      loadButtonsApi(params).then((data) => data.rows),
      loadChildrenApi(params).then((data) => data.rows),
      loadStrategyApi({ funcCode }),
      loadModelFieldsApi(params),
      loadGroupQueryApi(params),
    ]).then(([info, columns, fields, buttons, children, strategys, model, groupQuerys]) => {
      return {
        info,
        columns,
        fields,
        buttons,
        children,
        strategys,
        model,
        groupQuerys,
      };
    });
  });
}
/**
 * 功能信息查询信息
 * @returns
 */
export function loadFuncInfoApi({ funcId, funcCode }) {
  return ajax({
    url: API_FUNC_LOAD_FUNC_INFO,
    params: {
      tableCode: 'JE_CORE_FUNCINFO',
      FUNCINFO_FUNCCODE: funcCode,
      funcId,
      funcCode,
    },
  }).then(transformAjaxData);
}
/**
 * 列数据查询信息
 * @returns
 */
export function loadColumnsApi({ funcId }) {
  return ajax({
    url: API_FUNC_LOAD_FUNC,
    params: {
      tableCode: 'JE_CORE_RESOURCECOLUMN',
      funcId,
      j_query: encode({
        custom: [{ code: 'RESOURCECOLUMN_FUNCINFO_ID', value: funcId, type: '=', cn: 'and' }],
        order: [{ code: 'SY_ORDERINDEX', type: 'ASC' }],
      }),
    },
  }).then(transformAjaxData);
}

/**
 * 字段数据查询信息
 * @returns
 */
export function loadFieldsApi({ funcId }) {
  return ajax({
    url: API_FUNC_LOAD_FUNC,
    params: {
      tableCode: 'JE_CORE_RESOURCEFIELD',
      funcId,
      j_query: encode({
        custom: [{ code: 'RESOURCEFIELD_FUNCINFO_ID', value: funcId, type: '=', cn: 'and' }],
        order: [{ code: 'SY_ORDERINDEX', type: 'ASC' }],
      }),
    },
  }).then(transformAjaxData);
}

/**
 * 按钮数据查询信息
 * @returns
 */
export function loadButtonsApi({ funcId }) {
  return ajax({
    url: API_FUNC_LOAD_FUNC,
    params: {
      tableCode: 'JE_CORE_RESOURCEBUTTON',
      funcId,
      j_query: encode({
        custom: [{ code: 'RESOURCEBUTTON_FUNCINFO_ID', value: funcId, type: '=', cn: 'and' }],
        order: [{ code: 'SY_ORDERINDEX', type: 'ASC' }],
      }),
    },
  }).then(transformAjaxData);
}
/**
 * 子功能数据查询信息
 * @returns
 */
export function loadChildrenApi({ funcId }) {
  return ajax({
    url: API_FUNC_LOAD_FUNC,
    params: {
      tableCode: 'JE_CORE_FUNCRELATION',
      funcId,
      j_query: encode({
        custom: [{ code: 'FUNCRELATION_FUNCINFO_ID', value: funcId, type: '=', cn: 'and' }],
        order: [{ code: 'SY_ORDERINDEX', type: 'ASC' }],
      }),
    },
  })
    .then(transformAjaxData)
    .then((data) => {
      if (isEmpty(data.rows)) {
        return data;
      }

      // 查询级联关系
      const ids = data.rows.map((item) => {
        return item.JE_CORE_FUNCRELATION_ID;
      });

      return ajax({
        url: API_FUNC_LOAD_FUNC,
        params: {
          tableCode: 'JE_CORE_ASSOCIATIONFIELD',
          funcId,
          j_query: encode({
            custom: [{ code: 'ASSOCIATIONFIELD_FUNCRELAT_ID', value: ids, type: 'in', cn: 'and' }],
            order: [{ code: 'SY_ORDERINDEX', type: 'ASC' }],
          }),
        },
      })
        .then(transformAjaxData)
        .then((fields) => {
          data.rows.forEach((item) => {
            item.associations = fields.rows.filter(
              (field) => field.ASSOCIATIONFIELD_FUNCRELAT_ID === item.JE_CORE_FUNCRELATION_ID,
            );
          });
          return data;
        });
    });
}

/**
 * 查询策略
 * @param {*} param0
 * @returns
 */
export function loadStrategyApi({ funcCode }) {
  const userId = getCurrentUser()?.id;
  return ajax({
    url: parseCommonUrl(API_COMMON_LOAD),
    headers: { pd: 'meta' },
    params: {
      tableCode: 'JE_CORE_QUERYSTRATEGY',
      limit: -1,
      j_query: encode({
        custom: [
          { code: 'QUERYSTRATEGY_FUNCCODE', type: '=', value: funcCode },
          {
            type: 'and',
            value: [
              { code: 'QUERYSTRATEGY_USERID', type: '=', value: userId },
              { cn: 'or', code: 'QUERYSTRATEGY_USERID', type: '=', value: '' },
              { cn: 'or', code: 'QUERYSTRATEGY_USERID', type: 'isNull' },
            ],
          },
        ],
        order: [
          { code: 'QUERYSTRATEGY_USERID', type: 'ASC' },
          { code: 'SY_ORDERINDEX', type: 'ASC' },
        ],
      }),
    },
  })
    .then(transformAjaxData)
    .then((data) => {
      return data?.rows;
    });
}
/**
 * 加载高级查询
 * @param {*} param0
 * @returns
 */
export function loadGroupQueryApi({ funcId }) {
  return ajax({
    url: API_FUNC_LOAD_GROUP_QUERY,
    params: {
      funcId,
    },
  })
    .then(transformAjaxData)
    .then((data) => data.rows);
}
/**
 * 加载功能字段信息
 * @param {*} param0
 * @returns
 */
export function loadModelFieldsApi({ tableCode }) {
  return ajax({
    url: API_FUNC_LOAD_MODEL_FIELDS,
    params: {
      tableName: tableCode,
    },
  }).then(transformAjaxData);
}
/**
 * 加载功能权限
 * @param {*} param0
 * @returns
 */
export function loadFuncPermsApi(funcCode) {
  return ajax({
    url: API_FUNC_LOAD_PERMS,
    params: { funcCode },
    method: 'GET',
  }).then(transformAjaxData);
}

/**
 * 清除功能缓存
 * @param {*} param0
 * @returns
 */
export function clearFuncStaticizeApi({ funcCode, tableCode }) {
  return ajax({
    url: API_FUNC_CLEAR_STATICIZE,
    params: {
      funcCode,
      tableCode,
    },
  }).then(transformAjaxData);
}

/**
 * 获得列表导出excel的key
 * @param {*} param0
 * @returns
 */
export function loadExportExcelKeyApi({ params }) {
  return ajax({
    url: API_META_GRID_EXPORT_KEY,
    params: params,
  }).then(transformAjaxData);
}

/**
 * 列表导出excel
 * @param {*} param0
 * @returns
 */
export function exportGridExcelApi({ params, pd }) {
  return ajax({
    method: 'GET',
    url: API_META_GRID_EXPORT_EXCEL,
    params: params,
    headers: { pd },
  }).then(transformAjaxData);
}
/**
 * 获得功能下附件字段信息
 * @param {*} param0
 * @returns
 */
export function loadFuncFileInfoApi({ url, pd, params }) {
  const options = {
    url: url ? url : API_FUNC_LOAD_FILE_INFO,
    params: params,
  };
  if (pd) {
    options.headers = { pd };
  }
  return ajax(options).then(transformAjaxData);
}
