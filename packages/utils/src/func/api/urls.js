/*--------------------------------------- 功能配置 ---------------------------------------*/
/**
 * 获得功能配置
 */
export const API_FUNC_LOAD_FUNC = '/je/meta/funcInfo/loadFunc';
/**
 * 获得功能配置表单信息
 */
export const API_FUNC_LOAD_FUNC_INFO = '/je/meta/funcInfo/getInformationById';
/**
 * 获得功能字段信息
 */
export const API_FUNC_LOAD_MODEL_FIELDS = '/je/meta/doAct/doAct/getModelFields';
/**
 * 批量获得功能字段信息
 */
export const API_FUNC_LOAD_MODELS_FIELDS = '/je/meta/doAct/doAct/loadMultiModels';
/**
 * 功能权限
 */
export const API_FUNC_LOAD_PERMS = '/je/rbac/permission/loadFuncPerm';
/**
 * 字典功能权限
 */
export const API_FUNC_LOAD_PLUGIN_PERMS = '/je/rbac/permission/loadPluginPerm';

/**
 * 高级查询
 */
export const API_FUNC_LOAD_GROUP_QUERY = '/je/meta/advancedQuery/getData';
/**
 * 清空功能缓存
 */
export const API_FUNC_CLEAR_STATICIZE = '/je/meta/staticize/clearStaticize';
/**
 * 字典树
 */
export const API_DICTIONARY_LOAD_TREE = '/je/meta/dictionary/tree/loadTree';
/**
 * 获取功能下附件字段信息
 */
export const API_FUNC_LOAD_FILE_INFO = '/je/meta/funcInfo/getFileInfoList';

/*--------------------------------------- 消息接口 ---------------------------------------*/
/**
 * 收藏表单数据
 */
export const API_MESSAGE_COLLECTION_FORM_DATA = '/je/message/myCollection/saveCollection';
/**
 * 共享表单数据
 */
export const API_MESSAGE_SHARE_FORM_DATA = '/je/message/myShare/saveShare';
/**
 * 表单是否共享
 */
export const API_MESSAGE_SHARE_FORM_WHETHER = '/je/message/myCollection/loadCollection';

/*--------------------------------------- 功能通用接口 ---------------------------------------*/
/**
 * 查询
 */
export const API_COMMON_LOAD = '/load';
/**
 * 增加
 */
export const API_COMMON_SAVE = '/doSave';
/**
 * 删除
 */
export const API_COMMON_REMOVE = '/doRemove';
/**
 * 修改
 */
export const API_COMMON_UPDATE = '/doUpdate';
/**
 * 批量修改
 */
export const API_COMMON_UPDATE_LIST = '/doUpdateList';
/**
 * 批量修改，根据选中数据或者SQL
 */
export const API_COMMON_BATCH_MODIFY_LIST_DATA = '/batchModifyListData';
/**
 * 异步树查询
 */
export const API_COMMON_GET_TREE_LIST_BY_SEARCH_NAME = '/getTreeListBySearchName';
/**
 * 上传
 */
export const API_COMMON_UPLOAD = '/upload';
/**
 * 树形数据
 */
export const API_COMMON_GET_TREE = '/getTree';
/**
 * 树形功能列表树数据
 */
export const API_COMMON_LOAD_GRID_TREE = '/loadGridTree';
/**
 * 树形数据转移
 */
export const API_COMMON_MOVE = '/move';
/**
 * 获取单条数据
 */
export const API_COMMON_GET_BEAN = '/getInfoById';
/**
 * 字段值唯一性验证
 */
export const API_COMMON_CHECK_FIELD_UNIQUE = '/checkFieldUnique';
/**
 * 获得列表统计数据
 */
export const API_COMMON_STATISTICS = '/statistics';

/*--------------------------------------- 功能通用接口 END---------------------------------------*/

/**
 * 人员选择器加载左侧树(部门人员,角色人员,岗位人员,常用人员)
 */
export const API_RBAC_USER_LOAD_PRESONINFO = '/je/rbac/personConstructor/loadPersonInfo';

/**
 * 人员选择器添加常用人员
 */
export const API_RBAC_USER_ADD_COMMONUSER = '/je/rbac/personConstructor/addCommonUser';

/**
 * 人员选择去回显查询人员信息
 */
export const API_RBAC_USER_GET_PRESONINFOECHO = '/je/rbac/personConstructor/getPersonInfoEcho';

/**
 * 人员选择器异步查询
 */
export const API_RBAC_USER_FIND_ASYNC_NODES = '/je/rbac/personConstructor/findAsyncNodes ';

/**
 * 获得列表导出excel的key
 */
export const API_META_GRID_EXPORT_KEY = '/je/meta/excel/downloadKey';

/**
 * 列表导出execl
 */
export const API_META_GRID_EXPORT_EXCEL = '/je/meta/excel/expAsync';

/**
 * 解析公共链接，根据功能配置可以自行设置
 * @param {*} url
 * @param {*} action
 */
export function parseCommonUrl(url, action) {
  action = action || '/je/common';
  return action + url;
}
/**
 * 解析树形公共链接，根据功能配置可以自行设置
 * @param {*} url
 * @param {*} action
 * @returns
 */
export function parseCommonTreeUrl(url, action) {
  action = action || '/je/common/tree';
  return action + url;
}
