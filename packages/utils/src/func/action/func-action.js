import { FuncChangeViewActionEnum } from '../enum';
import { vueCall } from '../../je';
import { isNotEmpty } from '../../lodash';
/**
 * 功能Action
 * @returns
 */
export function useFuncAction() {
  return {
    doFuncLoad,
    doFuncReset,
    doFuncFieldUpdate,
  };
}

/**
 * 加载功能
 * @param {*} $func
 */
function doFuncLoad($func) {
  const { $modal } = $func;
  const funcData = $func.getFuncData();
  // 弹出功能，更新窗口标题
  if ($modal && $modal.title) {
    $modal.title.value = funcData.info.funcName;
  }
  return funcData;
}
/**
 * 重置功能
 * @param {*} $func
 */
function doFuncReset($func) {
  // 恢复原始页面
  $func.setActiveView($func.store.rawActiveView, FuncChangeViewActionEnum.FUNC_RESET);
  // 刷新子组件
  vueCall('nextTick', () => {
    Object.values($func.getRefMaps()).forEach((refItem) => {
      refItem.value?.reset?.();
    });
  });
}
/**
 * 子功能集合修改
 * @param {*} param0
 * @returns
 */
function doFuncFieldUpdate($func, { bean }) {
  const funcFields = $func.getChildFuncFields();
  const ps = [];
  const funFieldChanges = {};
  Object.values(funcFields).forEach((field) => {
    // 子功能集合改变的数据
    const changes = field.getFuncGrid()?.store.getChanges(true);
    if (isNotEmpty(changes)) {
      funFieldChanges[field.funcCode] = changes;
    }
    ps.push(field.doUpdateList(bean));
  });
  return Promise.all(ps).then(() => {
    return { funFieldChanges };
  });
}
