import { pick, omit } from 'lodash-es';
import { bindEvents4Promise, bindEvents4Sync } from '../util';
import { FuncRefEnum } from '../enum';
/**
 * 异步事件
 */
const asyncEvents = {
  grid: [
    'cell-click',
    'before-cell-click',
    'after-cell-click',
    'cell-dblclick',
    'before-cell-dblclick',
    'after-cell-dblclick',
  ],
  button: ['click', 'before-click', 'after-click'],
  action: ['click', 'before-click', 'after-click'],
  form: [],
  column: [],
  field: [],
};

/**
 * 系统事件绑定Action
 * @returns
 */
export function useEventAction() {
  return {
    bindButtonEvents,
    bindActionButtonEvents,
    bindGridEvents,
    bindFormEvents,
    bindFormFieldEvents,
    bindGridColumnEvents,
  };
}

/**
 * 按钮事件
 * @param {*} $func
 * @param {*} param1
 */
function bindButtonEvents($func, options) {
  const { button } = options;
  options.$button = button;
  const defaultEvents = {
    click(eventOptions) {
      return $func.action.doButtonDefaultEvents({ ...options, eventOptions });
    },
  };
  const onEvents = bindEvents4Promise({
    $func,
    events: pick(button.events, asyncEvents.button),
    defaultEvents,
    options,
  });
  button.listeners = onEvents;
  return onEvents;
}

/**
 * 列操作按钮事件
 * @param {*} $func
 * @param {*} param1
 */
function bindActionButtonEvents($func, options) {
  const { button } = options;
  const events = button.events;
  options.$button = button;
  // 系统默认事件
  const defaultEvents = {
    click(eventOptions) {
      return $func.action.doButtonDefaultEvents({ type: 'action', ...options, eventOptions });
    },
  };
  const clickEventKeys = asyncEvents.action;
  // 点击事件，使用异步
  const clickEvents = bindEvents4Promise({
    $func,
    events: pick(events, clickEventKeys),
    defaultEvents,
    options,
  });
  // 其他事件，使用同步
  const otherEvents = bindEvents4Sync({ $func, events: omit(events, clickEventKeys), options });
  const onEvents = { ...clickEvents, ...otherEvents };
  button.listeners = onEvents;
  return onEvents;
}

/**
 * 列表事件
 * @param {*} $func
 * @returns
 */
function bindGridEvents($func, options) {
  const funcData = $func.getFuncData();
  const { gridEvents } = funcData.info;
  const { defaultEvents } = options;
  const eventOptions = () => {
    return { $grid: $func.getFuncGrid(), gridRef: $func.getRefMaps(FuncRefEnum.FUNC_GRID) };
  };
  const asyncEventKeys = asyncEvents.grid;
  // 同步事件
  const syncEvents = bindEvents4Sync({
    $func,
    events: omit(gridEvents, asyncEventKeys),
    options: eventOptions,
  });
  // 异步事件
  const promiseEvents = bindEvents4Promise({
    $func,
    events: pick(gridEvents, asyncEventKeys),
    defaultEvents,
    options: eventOptions,
  });
  return { ...promiseEvents, ...syncEvents };
}
/**
 * 表单事件
 * @param {*} $func
 * @returns
 */
function bindFormEvents($func, options) {
  const funcData = $func.getFuncData();
  const eventOptions = () => {
    return { $form: $func.getFuncForm(), formRef: $func.getRefMaps(FuncRefEnum.FUNC_FORM) };
  };
  const { formEvents } = funcData.info;
  const { defaultEvents } = options;
  return bindEvents4Sync({ $func, events: formEvents, defaultEvents, options: eventOptions });
}
/**
 * 表单字段事件
 * @param {*} $func
 * @returns
 */
function bindFormFieldEvents($func, options) {
  const { field } = options;
  options.$field = field;
  return bindEvents4Sync({ $func, events: field.events, options, arrayArguments: true });
}
/**
 * 表格列事件
 * @param {*} $func
 * @returns
 */
function bindGridColumnEvents($func, options) {
  const { column } = options;
  options.$column = column;
  return bindEvents4Sync({ $func, events: column.events, options });
}
