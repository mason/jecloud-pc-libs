import { vueCall, useJE } from '../../je';
import { isEmpty, encode, createDeferred, isNotEmpty } from '../../lodash';
import { Modal } from '../../modal';
import { FuncRefEnum, FuncTypeEnum, FuncButtonTypeEnum, FuncChangeViewActionEnum } from '../enum';
import { doRemoveApi, doTreeRemoveApi, doTreeUpdateListApi, doUpdateListApi } from '../api';

export function useGridAction() {
  return {
    doBaseGridRemove,
    doBaseGridUpdateList,
    doBaseGridInsert4Grid,
    doBaseGridInsert4Form,
    doBaseGridInsert4Multi,
    doGridBeforeInsert,
    doGridBeforeEdit,
    doGridUpdate4View,
    doGridSave4View,
  };
}

/**
 * 列表删除
 * @param {*} $func
 */
function doBaseGridRemove($func, options = {}) {
  const { row, removeType } = options;
  const { productCode, tableCode, funcAction, funcCode, funcType } = $func.getFuncData().info;
  const $grid = $func.getRefMaps(FuncRefEnum.FUNC_GRID).value;
  const { $workflow } = $func;
  const { store } = $grid;
  let rows = row ? [row] : $grid.getSelectedRecords();
  // 树形功能点击左侧树删除按钮
  if (removeType == 'tree') {
    rows = [row.bean];
  }
  if (isEmpty(rows)) {
    Modal.alert('请选择要删除的数据！', 'warning');
    return Promise.resolve();
  } else {
    //流程已启动的数据不可删除
    if ($workflow.disableRemoveRecords(rows)) {
      Modal.alert('流程已启动不能被删除...', 'warning');
      return Promise.resolve();
    }
    const deferred = createDeferred();
    Modal.confirm(`确定删除选中的【${rows.length}】条数据吗`, () => {
      const ids = [];
      rows.forEach((item) => {
        if (!store.isInsertRecord(item)) {
          ids.push(store.getRecordId(item));
        }
      });
      // 删除操作
      let removePromise;
      if (ids.length) {
        const removeApi = funcType === FuncTypeEnum.TREE ? doTreeRemoveApi : doRemoveApi;
        removePromise = removeApi({
          params: { tableCode, funcCode, ids: ids.join(',') },
          pd: productCode,
          action: funcAction,
        });
      } else {
        removePromise = Promise.resolve();
      }
      removePromise
        .then(() => {
          const recordIds = rows.map((row) => store.getRecordId(row));
          $grid.store.remove(recordIds);
          $grid.store.totalCount -= recordIds.length;
          // 刷新选择数据
          $grid.refreshSelection();
          Modal.notice(`${rows.length} 条记录被删除`, 'success');
          deferred.resolve({ ids: recordIds });
        })
        .catch((err) => {
          Modal.alert(err?.message || '请求出错了，请查看控制台！', 'error');
          console.error(err);
          deferred.reject();
        });
    });
    return deferred.promise;
  }
}

/**
 * 添加前
 * @param {*} $func
 * @param {*} options
 */
function doGridBeforeInsert($func, options = {}) {
  const { parentFunc } = $func;
  const { row } = options;
  // 子功能添加控制
  if (parentFunc) {
    const beanId = parentFunc.store.getBeanId();
    if (!beanId) {
      const message = '请先保存主功能数据！';
      Modal.alert(message, Modal.status.warning);
      return Promise.reject({ message });
    }
  }

  // 默认值
  const values = $func.getDefaultValues({ buttonCode: FuncButtonTypeEnum.GRID_INSERT_BUTTON, row });
  // 自定义默认值
  if (options.bean) {
    Object.assign(values, options.bean);
  }
  //表单录入
  return Promise.resolve({ bean: values });
}

/**
 * 表单添加
 * @param {*} $func
 * @param {*} param1
 * @returns
 */
function doBaseGridInsert4Form($func, { bean }) {
  // 清除表单校验
  const $form = $func.getFuncForm();
  $form?.clearValidate();
  $func.store.setActiveBean(bean);
  // 跳转页面
  $func.setActiveView(FuncRefEnum.FUNC_EDIT, FuncChangeViewActionEnum.FORM_INNER);
  return vueCall('nextTick').then(() => ({ bean }));
}
/**
 * 列表添加
 * @param {*} $func
 * @param {*} options
 */
function doBaseGridInsert4Grid($func, options = {}) {
  let { records = [] } = options;
  const $grid = $func.getFuncGrid();
  let insertRecords = [];
  if (records.length) {
    insertRecords = $grid.store.insert(records);
    // 设置激活行，可编辑
    $grid.setActiveRow?.(insertRecords[0]);
  }
  return Promise.resolve({ insertRecords });
}

/**
 * 多选添加
 * @param {*} $func
 * @param {*} options
 * @returns
 */
function doBaseGridInsert4Multi($func, options = {}) {
  if (options.type && options.configInfo) {
    const deferred = createDeferred();
    const func = useJE().useFunc?.();
    func.showSelectWindow({
      title: options.title || '批量添加',
      multiple: true,
      ...options,
      callback(options) {
        const { rows, config } = options;
        // 默认值
        const values = $func.getDefaultValues({
          buttonCode: FuncButtonTypeEnum.GRID_INSERT_BATCH_BUTTON,
        });
        const records = [];
        rows.forEach((row) => {
          const record = {};
          config.sourceFields?.forEach((field, index) => {
            record[config.targetFields[index]] = row[field];
          });
          records.push({ ...values, ...record });
        });
        deferred.resolve({ records, ...options });
      },
    });
    return deferred.promise;
  } else {
    return Promise.reject('请配置批量添加的信息');
  }
}

/**
 * 页面编辑
 * @param {*} $func
 * @param {*} param1
 */
function doGridBeforeEdit($func, options = {}) {
  let errorMessage = '';
  // 编辑权限 action可以正常编辑
  if (!$func.hasGridEditPerm() && !options.actionBtn)
    return Promise.reject({ message: '没有编辑权限' });

  const $grid = $func.getFuncGrid();
  const selectedRecords = $grid.getSelectedRecords();
  let row = options.row || selectedRecords[0];
  if (!row) {
    errorMessage = '请选择要操作的数据！';
    Modal.alert(errorMessage, Modal.status.warning);
    return Promise.reject({ message: errorMessage });
  }
  // 列表上的编辑按钮才需要该提示
  if (!options.row && selectedRecords.length > 1) {
    errorMessage = `请选择【1】条要编辑的数据，您已选择【${selectedRecords.length}】条数据！`;
    Modal.alert(errorMessage, Modal.status.warning);
    return Promise.reject({ message: errorMessage });
  }
  $func.store.setActiveRow(row);
  const { treeOptions, funcType } = $func.getFuncData().info;
  // 如果是树形功能并且是树形展示模式
  if (treeOptions.showType == 'tree' && funcType === FuncTypeEnum.TREE) {
    row = row.bean;
  }
  return $func.getBeanByRow(row);
}

/**
 * 列表批量修改
 * @param {*} $func
 * @param {*} options
 * @returns
 */
function doBaseGridUpdateList($func, options = {}) {
  const { row, showMessage = true, customParams = {} } = options;
  const $grid = $func.getFuncGrid();
  const funcData = $func.getFuncData();
  const { productCode, tableCode, funcCode, funcType, funcAction, treeOptions, pkCode } =
    funcData.info;
  const funcTree = funcType === FuncTypeEnum.TREE;
  const { codeGenFieldInfo } = funcData.fields;
  const { store } = $grid;
  const changes = store.getChanges(row, true);
  if (isEmpty(changes)) {
    showMessage && Modal.notice('保存成功！', 'success');
    return Promise.resolve({ nodes: [], funcTree });
  } else {
    const updates = [];
    const validRows = [];
    changes.forEach((change) => {
      let item = {};
      const record = change._record;
      let idProperty = store.idProperty;
      // 如果是树形功能并且是树形展示需要换真实字段主键
      if (funcTree && treeOptions.showType == 'tree') {
        idProperty = pkCode;
      }
      // 添加
      if (store.isInsertRecord(record)) {
        item = vueCall('toRaw', record);
        // 修改
      } else {
        Object.keys(change).forEach((key) => {
          // 去掉 null值
          item[key] = change[key].newValue != null ? change[key].newValue : '';
        });
        item[idProperty] = store.getRecordId(record);
      }
      delete item._record;
      updates.push(item);
      validRows.push(record);
    });
    const updateListApi = funcTree ? doTreeUpdateListApi : doUpdateListApi;
    // 提交数据
    return $grid
      .validate(validRows)
      .then(() => {
        return updateListApi({
          params: {
            tableCode,
            funcCode,
            strData: encode(updates),
            codeGenFieldInfo: encode(codeGenFieldInfo), // 编号
            ...customParams,
          },
          pd: productCode,
          action: funcAction,
        })
          .then(({ dynaBeans, nodes }) => {
            // 添加数据留痕
            addHistoryMark({ $func });
            // 常规来说不nodes不会返回__action__，因为返回这个值导致删除的时候即便有id也会删除不成功
            // TODO 最好后端直接去掉
            if (nodes && nodes?.length > 0) {
              nodes.forEach((item) => {
                delete item.bean.__action__;
              });
            }
            // 树形功能并且是树形展示需要把node的bean解构出来
            if (funcTree && treeOptions.showType == 'tree') {
              nodes.forEach((item) => {
                Object.assign(item, { ...item.bean });
              });
            }
            showMessage && Modal.notice('保存成功！', 'success');
            doGridUpdate4View($func, { records: dynaBeans, nodes });
            return { dynaBeans, nodes, funcTree };
          })
          .catch((err) => {
            Modal.alert(err?.message || '请求出错了，请查看控制台！', 'error');
            console.error(err);
          });
      })
      .catch((errorMaps) => {
        console.log('列表校验失败：', errorMaps);
        return Promise.reject(errorMaps);
      });
  }
}
/**
 * 修改数据
 * @param {*} param0
 * @returns
 */
function doGridUpdate4View($func, { records, nodes }) {
  const grid = $func.getFuncGrid();
  // 如果是树形功能树形展示模式
  if (nodes) {
    const { treeOptions, funcType } = $func.getFuncData().info;
    if (treeOptions.showType == 'tree' && funcType === FuncTypeEnum.TREE) {
      return $func.action.doTreeUpdate4View({ treeView: grid, nodes });
    }
  }
  //循环更新
  records.forEach((record) => {
    // 格式化处理
    grid?.store.transformInsertRecord(record);
    grid?.store.commitRecord(record);
  });
}

/**
 * 添加数据
 * @param {*} param0
 * @returns
 */
function doGridSave4View($func, { record, node }) {
  const grid = $func.getFuncGrid();
  // 如果是树形功能树形展示模式
  if (node) {
    const { treeOptions, funcType } = $func.getFuncData().info;
    if (treeOptions.showType == 'tree' && funcType === FuncTypeEnum.TREE) {
      return $func.action.doTreeSave4View({ treeView: grid, node });
    }
  }
  if (grid) {
    const records = grid.store.insert(record);
    return records[0];
  } else {
    return vueCall('reactive', record);
  }
}

/**
 * 添加数据留痕
 */
function addHistoryMark({ $func }) {
  const $grid = $func.getFuncGrid();
  const { store } = $grid;
  const funcData = $func.getFuncData();
  // 配置历史留痕的字段
  const historyFields = funcData?.fields.history || [];
  const pkCode = funcData?.pkCode;
  const funcCode = funcData?.funcCode;
  const strData = [];
  // 配置了历史留痕
  if (isNotEmpty(historyFields)) {
    // 列表修改的数据
    const changeDatas = store.getChanges();
    if (isNotEmpty(changeDatas)) {
      // 封装数据留痕的数据
      changeDatas.forEach((item) => {
        const historyArray = [];
        historyFields.forEach((field) => {
          const changeData = item[field];
          if (isNotEmpty(changeData)) {
            // 字段对应的name值
            const fieldName = funcData.basic.columns.get(field).title;
            // 改变数据
            historyArray.push({
              fieldName,
              fieldCode: field,
              type: funcData.basic.fields.get(field).xtype,
              oldValue: $func.action.getDicText({
                field,
                value: changeData.oldValue,
                record: item,
                valueType: 'oldValue',
              }),
              newValue: $func.action.getDicText({
                field,
                value: changeData.newValue,
                record: item,
                valueType: 'newValue',
              }),
            });
          }
        });
        // strData参数最终封装
        if (isNotEmpty(historyArray)) {
          strData.push({
            PAGENICKED_NICKED: encode(historyArray),
            PAGENICKED_BUSINESSID: item._record[pkCode],
            PAGENICKED_FUNCCODE: funcCode,
            __action__: 'doInsert',
          });
        }
      });
    }
    if (isNotEmpty(strData)) {
      // 调用接口保存数据
      doUpdateListApi({
        params: { tableCode: 'JE_CORE_PAGENICKED', strData: encode(strData) },
        pd: 'meta',
      });
    }
  }
}
