import { Modal } from '../../modal';
import { vueCall } from '../../je';
import {
  encode,
  isEmpty,
  simpleDateFormat,
  cloneDeep,
  isArray,
  createDeferred,
  getCurrentUser,
  isNotEmpty,
  getDDItemList,
} from '../../lodash';
import {
  FuncRefEnum,
  FuncTypeEnum,
  FuncChangeViewActionEnum,
  FuncChildrenLayoutEnum,
  FuncFieldTypeEnum,
} from '../enum';
import { doSaveApi, doTreeSaveApi, checkFieldUniqueApi, doUpdateListApi } from '../api';
import { parseConfigInfo } from '../util/field-config-info';
export function useFormAction() {
  return {
    doFormSave,
    doFormValidate,
    doFormBack,
    doFormAudit,
    doFormAuditExecute,
    doFormFieldUniqueCheck,
    doFormUpdateById,
    getDicText,
  };
}
/**
 * 表单校验
 * @param {*} $func
 * @returns
 */
function doFormValidate($func) {
  const $form = $func.getFuncForm();
  return $form
    .validate()
    .then(() => {
      // 表单验证通过，重置错误信息
      $func.store.formError = {
        popoverVisible: false,
        errorFields: [],
      };
      return Promise.resolve();
    })
    .catch((options) => {
      // TODO 暂时解决表单字段校验不通过
      if (options.errorFields.length <= 0) {
        $func.store.formError = {
          popoverVisible: false,
          errorFields: [],
        };
        return Promise.resolve();
      }
      console.log('表单验证未通过：', options);
      options.formError = true;
      $func.store.formError = {
        popoverVisible: true,
        errorFields: options.errorFields,
      };
      return Promise.reject(options);
    });
}

/**
 * 表单保存
 * @param {*} $func
 * @returns
 */
function doFormSave($func) {
  const { store } = $func;
  const funcData = $func.getFuncData();
  const { tableCode, pkCode, productCode, funcCode, funcType, funcAction, treeOptions } =
    funcData.info;
  const { codeGenFieldInfo } = funcData.fields;
  const funcTree = funcType === FuncTypeEnum.TREE;
  const bean = vueCall('toRaw', store.activeBean);
  const beanId = bean[pkCode];
  return $func.action.doFormValidate().then(() => {
    // 修改状态下保存子功能数据
    if (isNotEmpty(beanId)) {
      $func.action.doChildSave?.($func);
    }
    const saveApi = funcTree ? doTreeSaveApi : doSaveApi;
    return saveApi({
      params: {
        funcCode,
        tableCode,
        pkCode,
        codeGenFieldInfo: encode(codeGenFieldInfo), // 编号
        ...bean,
      },
      pkValue: beanId,
      pd: productCode,
      action: funcAction,
    })
      .then((options) => {
        // 公共逻辑处理
        const { dynaBean } = options;
        // 子功能集合修改
        return $func.action.doFuncFieldUpdate({ bean: dynaBean }).then(({ funFieldChanges }) => {
          return { ...options, funFieldChanges };
        });
      })
      .then(({ dynaBean, node, funFieldChanges }) => {
        if (beanId) {
          // 数据留痕
          addHistoryMark({ $func, beanId, funFieldChanges });
          // 更新bean数据
          store.commitActiveBean(dynaBean);
          store.activeBeanEmitter++;
        } else {
          // 重新绑定bean
          store.setActiveBean(dynaBean);
          // 列表分页总数+1;
          const $grid = $func.getFuncGrid();
          $grid && $grid.store.totalCount++;
        }
        const eventResult = { insert: !beanId, bean: dynaBean, node };
        // 树形功能并且是树形展示需要把node的bean解构出来
        if (funcTree && treeOptions.showType == 'tree') {
          Object.assign(eventResult.node, { ...node.bean });
        }
        $func.action.doFormSaveAfter?.(eventResult);
        Modal.notice(beanId ? '保存成功！' : '添加成功！', 'success');
        return eventResult;
      })
      .catch((err) => {
        Modal.alert(err?.message || '请求出错了，请查看控制台！', 'error');
        console.error(err);
      });
  });
}
/**
 * 页面返回
 * @param {*} $func
 */
function doFormBack($func) {
  const { bindButtonEvents } = $func.action;
  const $form = $func.getFuncForm();
  const changes = $form?.getChanges();
  const saveButton = $form?.getButtons('formSaveBtn');
  // 如果没有保存按钮 || 没有改变值，直接返回
  if (saveButton?.hidden || isEmpty(changes)) {
    $func.setActiveView(FuncRefEnum.FUNC_DATA, FuncChangeViewActionEnum.FORM_BACK);
    return vueCall('nextTick');
  } else {
    const deferred = createDeferred();
    // 返回方法
    const back = () => {
      $func.store.formError = {
        popoverVisible: false,
        errorFields: [],
      };
      $form?.clearValidate();
      $func.store.resetActiveBean();
      $func.setActiveView(FuncRefEnum.FUNC_DATA, FuncChangeViewActionEnum.FORM_BACK);
      deferred.resolve();
    };
    Modal.dialog({
      content: '数据有改动，未保存，确定返回吗？',
      status: Modal.status.question,
      buttons: [
        {
          text: '保存返回',
          type: 'primary',
          handler: () => {
            $form
              .validate()
              .then(() => {
                // 执行保存按钮事件
                const events = bindButtonEvents({ type: 'form', button: saveButton });
                events.onClick?.().then(back);
              })
              .catch((options) => {
                $func.store.formError = {
                  popoverVisible: true,
                  errorFields: options.errorFields,
                };
              });
          },
        },
        {
          text: '确定返回',
          handler: back,
        },
        { text: '取消' },
      ],
    });
    return deferred;
  }
}

/**
 * 表单审核操作
 * @param {*} param0
 */
function doFormAudit($func, options = {}) {
  const deferred = createDeferred();
  const { ackFlag, button } = options;
  const { text } = button;
  const { store } = $func;
  const funcData = $func.getFuncData();
  const { tableCode, pkCode, productCode, funcCode, funcAction } = funcData.info;
  Modal.confirm(`您是否确认${text}？`, () => {
    const bean = cloneDeep(store.activeBean);
    // 数据主键
    const beanId = bean[pkCode];
    // 用户信息
    const userData = getCurrentUser();
    // 参数封装
    bean.SY_ACKFLAG = ackFlag; //审核标识
    bean.SY_ACKUSERNAME = userData.name; //审核人
    bean.SY_ACKUSERID = userData.id; //审核人Id
    bean.SY_ACKTIME = simpleDateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'); //审核时间
    // 调用接口保存数据
    doSaveApi({
      params: {
        funcCode,
        tableCode,
        pkCode,
        ...bean,
      },
      pkValue: beanId,
      pd: productCode,
      action: funcAction,
    })
      .then(({ dynaBean }) => {
        // 更新bean数据
        store.commitActiveBean(dynaBean);
        // 更新表单
        doFormAuditExecute($func);
        // 更新列表数据
        $func.action.doFormSaveAfter?.({ bean: dynaBean });
        Modal.notice(`${text}成功！`, 'success');
        deferred.resolve();
      })
      .catch((err) => {
        Modal.alert(err?.message || '请求出错了，请查看控制台！', 'error');
        console.error(err);
        deferred.reject();
      });
  });

  return deferred.promise;
}

/**
 * 执行表单审核
 * @param {*} param0
 */
function doFormAuditExecute($func) {
  // 获得到表单对象
  const $form = $func.getFuncForm();
  if ($form) {
    // 获得功能信息startAudit(启用审核功能)enableWorkflow(启动工作流)
    const { startAudit, enableWorkflow } = $form.getFuncData().info;
    if (startAudit && !enableWorkflow) {
      // 获得审核标识
      const { SY_ACKFLAG } = $func.store.activeBean;
      // 数据审核
      if (SY_ACKFLAG === '1') {
        $form.setReadOnly(true);
      } else {
        // 数据取消审核,重置表单
        $form.resetMetaConfig();
      }
    }
  }
}

/**
 * 字段值唯一性验证
 * @param {*} $func
 * @param {*} options
 * @returns
 */
function doFormFieldUniqueCheck($func, fieldCode, value) {
  const { productCode, funcCode, funcAction, pkCode } = $func.getFuncData().info;
  let fields = [];
  if (isArray(fieldCode)) {
    fields = fieldCode;
  } else {
    fields.push({ fieldCode, value });
  }
  if (isEmpty(fields)) {
    return Promise.resolve(true);
  }
  const pkValue = $func.store.activeBean[pkCode];
  return checkFieldUniqueApi({
    params: { funcCode, strData: encode(fields), pkValue },
    action: funcAction,
    pd: productCode,
  }).then((data) => data.success);
}
/**
 * 根据ID进行表单数据跟新
 * @param {*} $func
 * @param {*} beanId
 * @param {*} model
 */
function doFormUpdateById($func, beanId, bean) {
  if (!beanId) return Promise.reject({ code: 'empty' });
  const funcData = $func.getFuncData();
  const { tableCode, pkCode, productCode, funcCode, funcAction } = funcData.info;
  bean[pkCode] = beanId;
  return doSaveApi({
    params: {
      funcCode,
      tableCode,
      pkCode,
      ...bean,
    },
    pkValue: beanId,
    pd: productCode,
    action: funcAction,
  });
}
/**
 * 保存子功能数据
 * @param {*} $func
 */
function doChildSave($func) {
  const childFunc = $func?.getChildFunc();
  if (isNotEmpty(childFunc)) {
    Object.keys(childFunc).forEach((Childkey) => {
      const childItem = childFunc[Childkey];
      // 排除子功能集合
      if (!childItem.isFuncField) {
        const funData = childItem.getFuncData();
        const { childLayout } = funData?.info;
        // 子功能是 表单(内部)横向显示/表单(内部)纵向显示
        if (
          [
            FuncChildrenLayoutEnum.FORM_INNER_HORIZONTAL,
            FuncChildrenLayoutEnum.FORM_INNER_VERTICAL,
          ].includes(childLayout)
        ) {
          const childGrid = childItem.refMaps?.funcGrid?.value;
          const changes = childGrid?.store.getUpdatedRecords() || [];
          const childGridSaveBtn = childGrid?.getButtons('gridUpdateBtn');
          // 子功能列表有数据改变,并且列表上有保存按钮
          if (changes.length > 0 && childGridSaveBtn && !childGridSaveBtn?.hidden) {
            childGridSaveBtn.click();
          }
        }
      }
    });
  }
}
/**
 * 添加数据留痕
 */
function addHistoryMark({ $func, beanId, funFieldChanges }) {
  const $form = $func.getFuncForm();
  const funcData = $func.getFuncData();
  // 配置历史留痕的字段
  const historyFields = funcData?.fields.history || [];
  const funcCode = funcData?.funcCode;
  const strData = [];
  // 配置了历史留痕
  if (isNotEmpty(historyFields)) {
    const historyArray = [];
    const changeDatas = $form.getChanges();
    const childFuncFields = [];
    // 有改变的数据
    historyFields.forEach((field) => {
      const changeData = changeDatas[field];
      const funcFields = funcData.basic.fields;
      // 如果该字段有改动
      if (isNotEmpty(changeData)) {
        const fieldName = funcFields.get(field).label;
        historyArray.push({
          fieldName,
          fieldCode: field,
          type: funcFields.get(field).xtype,
          oldValue: getDicText($func, {
            field,
            value: changeData.oldValue,
            record: changeDatas,
            valueType: 'oldValue',
          }),
          newValue: getDicText($func, {
            field,
            value: changeData.newValue,
            record: changeDatas,
            valueType: 'newValue',
          }),
        });
      }
      // 获得子功能集合字段
      const funcFieldObj = funcFields.get(field);
      if (funcFieldObj.xtype == FuncFieldTypeEnum.FUNC_CHILD_FIELD.xtype) {
        childFuncFields.push(funcFieldObj);
      }
    });
    //处理子功能集合数据
    if (isNotEmpty(funFieldChanges) && childFuncFields.length > 0) {
      childFuncFields.forEach((childField) => {
        const fieldName = childField.label;
        const fieldCode = childField.code;
        const childFuncCode = childField.configInfo?.split(',')[0];
        const changeDatas = funFieldChanges[childFuncCode];
        const $childFunc = $func.getChildFuncFields(childFuncCode);
        // 获得子功能集合列表的列
        const childFuncColumns = cloneDeep(
          $childFunc?.getFuncData()?.getColumns('func') || [],
        ).filter((col) => !['seq', 'action'].includes(col.type));
        // 存在改变的数据
        if (isNotEmpty(changeDatas)) {
          changeDatas.forEach((item) => {
            const keys = Object.keys(item).filter(
              (fieldKey) => !['SY_ORDERINDEX', '_record'].includes(fieldKey),
            );
            // 排除只改变排序的数据
            if (keys.length > 0) {
              const _record = cloneDeep(item._record);
              const newRecord = [];
              const oldRecord = [];
              childFuncColumns.forEach((columnItem) => {
                const newObj = {};
                const oldObj = {};
                const { title, field } = columnItem;
                newObj[title] = getDicText($childFunc, {
                  field,
                  value: _record[field],
                  record: item,
                  valueType: 'newValue',
                });
                newRecord.push(newObj);
                // 修改
                if (!(item.__action__?.newValue == 'doInsert')) {
                  oldObj[title] = getDicText($childFunc, {
                    field,
                    value: item[field] ? item[field].oldValue : _record[field],
                    record: item,
                    valueType: 'oldValue',
                  });
                  oldRecord.push(oldObj);
                }
              });
              historyArray.push({
                fieldName,
                fieldCode,
                type: FuncFieldTypeEnum.FUNC_CHILD_FIELD.xtype,
                oldValue: isNotEmpty(oldRecord) ? encode(oldRecord) : '',
                newValue: encode(newRecord),
              });
            }
          });
        }
      });
    }
    // strData参数最终封装
    if (isNotEmpty(historyArray)) {
      strData.push({
        PAGENICKED_NICKED: encode(historyArray),
        PAGENICKED_BUSINESSID: beanId,
        PAGENICKED_FUNCCODE: funcCode,
        __action__: 'doInsert',
      });
    }
  }
  if (isNotEmpty(strData)) {
    // 调用接口保存数据
    doUpdateListApi({
      params: { tableCode: 'JE_CORE_PAGENICKED', strData: encode(strData) },
      pd: 'meta',
    }).then(() => {
      $func.store.activeBeanEmitter++;
    });
  }
}
/**
 * 单选框,复选框,下拉框字段值转text值
 * @param {*} param0
 * @returns
 */
function getDicText($func, options = {}) {
  const { field, value, record, valueType } = options;
  let textValue = value;
  const funcData = $func.getFuncData();
  const funcFields = funcData.basic.fields;
  const fieldConfig = funcFields.get(field);
  // 如果是单选框,复选框,下拉框
  if (
    [
      FuncFieldTypeEnum.RADIO_GROUP.xtype,
      FuncFieldTypeEnum.CHECKBOX_GROUP.xtype,
      FuncFieldTypeEnum.SELECT.xtype,
    ].includes(fieldConfig?.xtype) &&
    isNotEmpty(value)
  ) {
    const configInfo = parseConfigInfo({ configInfo: fieldConfig.configInfo });
    // 1.如果当前字段不是text值
    const valueFlag = configInfo.fieldMaps.targetToSource[field];
    if (valueFlag != 'text') {
      //2. 找是否有text对应的字段值
      const sourceToTargetText = configInfo.fieldMaps.sourceToTarget?.text;
      if (isNotEmpty(sourceToTargetText)) {
        if (valueType) {
          textValue = record[sourceToTargetText[0]][valueType];
        } else {
          textValue = record[sourceToTargetText[0]];
        }
      } else {
        // 3.如果没有就只能从字典数据中获取了
        const ddItemList = getDDItemList(configInfo.code);
        const textValues = [];
        value.split(',').forEach((val) => {
          const item = ddItemList.find((item) => item[valueFlag] === val);
          if (isNotEmpty(item)) {
            textValues.push(item.text);
          }
        });
        if (textValues.length > 0) {
          textValue = textValues.join(',');
        }
      }
    }
  }
  return textValue;
}
