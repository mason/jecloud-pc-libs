import { uuid } from '../../../lodash';
import { vueCall } from '../../../je';
export default class BaseView {
  id = uuid();
  props = {};
  context = {};
  refMaps = {};
  pluginRef = null;

  constructor(options) {
    this.$func = options.$func; // 功能对象
    this.props = options.props; // 属性
    this.context = options.context; // 上下文
    this.pluginRef = options.pluginRef; // 插件ref对象
    this.panelItem = options.panelItem;
  }

  /**
   * 功能编码
   *
   * @readonly
   * @memberof BaseView
   */
  get funcCode() {
    return this.$func.funcCode;
  }

  /**
   * 面板项
   */
  getPanelItem() {
    return this.panelItem;
  }
  /**
   * 设置面板展开
   * @param {boolean} expand 展开，默认true
   */
  setPanelExpand(expand = true) {
    this.getPanelItem()?.setExpand(expand);
  }
  /**
   * 设置面板显示
   * @param {boolean} visible 显示，默认true
   */
  setPanelVisible(visible = true) {
    this.getPanelItem()?.setVisible(visible);
  }
  /**
   * 功能数据
   * @returns
   */
  getFuncData() {
    return this.$func.getFuncData();
  }
  /**
   * 获得插件对象
   * @returns
   */
  getPlugin() {
    return vueCall('isRef', this.pluginRef) ? this.pluginRef?.value : this.pluginRef;
  }
  /**
   * 执行插件方法
   * @param {*} name 方法名
   * @param  {...any} args 参数
   * @returns
   */
  pluginCall(name, ...args) {
    return this.getPlugin()?.[name]?.(...args);
  }
  /**
   * 混入
   * @param {Object} options
   */
  mixin(options = {}) {
    Object.assign(this, options);
  }
  /**
   * 获取子元素ref对象
   * @param {*} key
   * @returns
   */
  getRefMaps(key) {
    // 生成ref对象
    if (key && !this.refMaps[key]) {
      this.refMaps[key] = vueCall('ref');
    }
    return key ? this.refMaps[key] : this.refMaps;
  }
  /**
   * 设置子元素ref对象
   * @param {*} key
   * @returns
   */
  setRefMaps(key, item) {
    this.refMaps[key] = item || vueCall('ref');
  }
  /**
   * 查找元素ref
   * @param {*} key
   * @returns
   */
  hasRefMaps(key) {
    return !!this.refMaps[key];
  }
}
