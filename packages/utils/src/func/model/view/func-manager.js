import { FuncChangeViewActionEnum, FuncRefEnum, FuncTypeEnum } from '../../enum';
import FuncStore from '../../store/func-store';
import { cloneDeep, isFunction, uuid, isEmpty, getDDCache } from '../../../lodash';
import { vueCall } from '../../../je';
import { doEvents4Sync, loadFuncData, bindEvents4Sync, initFuncSecurity } from '../../util';
import { getBeanApi, loadGrid4singleApi } from '../../api';
import { useAction } from '../../action';
import FuncPerm from '../func/func-perm';
import GridQueryParser from '../query/grid-query-parser';
import { WorkflowManager } from '../../workflow';
/**
 * 功能管理器
 */
export default class FuncManager {
  id = uuid();
  funcCode = '';
  loading = vueCall('ref', true);
  isFuncForm = false;
  isDDFunc = false; // 字典功能
  funcDDCode = null; // 字典编码
  isWindow = false;
  isFuncField = false; // 子功能集合
  loadFirstData = false;
  props = {};
  context = {};
  refMaps = {};
  params = {}; // 传参，用于外部调用功能使用
  funcFieldManagers = {};
  parentFunc = null;
  childFuncs = {};
  childFuncManagers = {};
  $workflow; // 流程对象
  // 响应式按钮数据
  reactiveObject = {
    gridButtons: [],
    formButtons: [],
    actionButtons: [],
    childFuncConfig: [],
    formAnchors: [],
  };

  constructor(options) {
    let { funcCode, props = {}, context } = options;
    const {
      isFuncForm,
      isDDFunc,
      isFuncField,
      loadFirstData,
      beanId,
      bean = {},
      readonly,
      isWindow,
      querys = [],
      orders,
      params = {},
    } = props;
    // 字典功能
    if (isDDFunc) {
      // 字典信息
      const ddId = getDDCache(funcCode)?.id;
      // 字典功能的编码是字典编码
      this.funcDDCode = funcCode;
      this.isDDFunc = isDDFunc;
      // 使用系统默认字典项功能
      funcCode = 'JE_CORE_DICTIONARYITEM';
      // 默认查询条件
      querys.push({
        code: 'DICTIONARYITEM_DICTIONARY_ID',
        type: '=',
        value: ddId,
      });
      // 默认值
      Object.assign(bean, { DICTIONARYITEM_DICTIONARY_ID: ddId });
    }
    this.funcCode = funcCode;
    this.isFuncForm = isFuncForm;
    this.isFuncField = isFuncField;
    this.loadFirstData = loadFirstData;
    this.isWindow = isWindow;
    this.props = props;
    this.context = context;
    this.params = params;
    const activeView = isFuncForm
      ? FuncRefEnum.FUNC_EDIT
      : props.activeView || FuncRefEnum.FUNC_DATA;
    this.store = vueCall(
      'reactive',
      new FuncStore({ funcCode, $func: this, beanId, bean, readonly, querys, orders, activeView }),
    );
    this.perm = new FuncPerm(this);
    this.action = useAction(this);
    this.$workflow = new WorkflowManager({ $func: this });
  }
  /**
   * 获得外部传参，业务使用
   * @returns {Object}
   */
  getParams() {
    return this.params;
  }
  /**
   * 切换激活面板
   * @param {*} code
   */
  setActiveView(code, action) {
    // 标签导航
    if (action === FuncChangeViewActionEnum.CRUMB_CLICK) {
      const { crumb, crumbs } = code;
      // 找到点击功能的下一级功能
      const funcCode = crumbs[crumbs.indexOf(crumb) + 1];
      // 循环查找父功能
      let parentFunc = this;
      while (parentFunc.funcCode !== funcCode) {
        parentFunc = parentFunc.getParentFunc();
      }
      // 恢复初始视图
      parentFunc.store.activeView = parentFunc.store.rawActiveView;
    } else {
      this.store.activeView = code;
    }
  }

  /**
   * 获取子元素ref对象
   * @param {*} key
   * @returns
   */
  getRefMaps(key) {
    // 生成ref对象
    if (key && !this.refMaps[key]) {
      this.refMaps[key] = vueCall('ref');
    }
    return key ? this.refMaps[key] : this.refMaps;
  }
  /**
   * 设置子元素ref对象
   * @param {*} key
   * @returns
   */
  setRefMaps(key, item) {
    this.refMaps[key] = item || vueCall('ref');
  }
  /**
   * 查找元素ref
   * @param {*} key
   * @returns
   */
  hasRefMaps(key) {
    return !!this.refMaps[key];
  }

  /**
   * 获得功能数据
   * @returns {Func}
   */
  getFuncData() {
    return this.store.getFuncData();
  }
  /**
   * 加载功能数据
   * @returns
   */
  loadFunc(reload) {
    return this.perm.loadFuncPerm().then(() => this.loadFunc4ExcloudPerm(reload));
  }
  /**
   * 加载功能数据，没有权限数据
   * @returns
   */
  loadFunc4ExcloudPerm(reload) {
    // 解析功能数据
    this.loading.value = true;
    return Promise.all([
      this.loadFuncData(reload).then(this.action.doFuncLoad),
      initFuncSecurity(this),
    ])
      .then(([funcData]) => funcData)
      .finally(() => {
        this.loading.value = false;
      });
  }

  /**
   * 加载功能数据
   * @param {boolean} reload 重新加载
   * @returns {Promise}
   */
  loadFuncData(reload) {
    const { funcCode, funcDDCode } = this;
    // 重新加载，不需要缓存，从新load功能数据
    return loadFuncData({ funcCode, funcDDCode, cache: !reload }).then((data) => {
      const funcData = cloneDeep(data);
      this.store.setFuncData(funcData);
      this.initReactiveObject();
      return funcData;
    });
  }

  /**
   * 加载功能权限
   * @returns
   */
  loadFuncPerm() {
    return this.perm.loadFuncPerm();
  }

  /**
   * 刷新功能
   * @returns
   */
  reload() {
    return this.loadFunc(true);
  }
  /**
   * 恢复功能状态
   * @returns
   */
  resetFunc() {
    return this.action.doFuncReset();
  }
  /**
   * 设置子功能集合字段信息
   * @param {string} funcCode
   * @param {FuncManager} $func
   */
  addChildFuncField(funcCode, $func) {
    this.funcFieldManagers[funcCode] = $func;
  }
  /**
   * 添加子功能$func
   */
  addChildFunc(code, $func) {
    this.childFuncManagers[code] = $func;
  }
  /**
   * 设置主功能
   * @param {*} parentFunc
   */
  setParentFunc(parentFunc) {
    if (parentFunc) {
      this.parentFunc = parentFunc;
      // 绑定store
      this.parentFunc.store.addChildStore(this.store);
      this.parentFunc.addChildFunc(this.funcCode, this);
      // 功能层级
      this.store.funcCrumbs = [...this.parentFunc.store.funcCrumbs, ...this.store.funcCrumbs];
    }
  }
  /**
   * 获取子功能集合字段
   * @param {string} funcCode
   * @returns FuncManager | Object
   */
  getChildFuncFields(funcCode) {
    return funcCode ? this.funcFieldManagers[funcCode] : this.funcFieldManagers;
  }
  /**
   * 获得父功能
   * @returns {FuncManager}
   */
  getParentFunc() {
    return this.parentFunc;
  }

  /**
   * 获得功能bean数据
   * @returns {Object}
   */
  getBean() {
    return this.store.getBean();
  }
  /**
   * 通过行数据获得完整的Bean数据
   */
  getBeanByRow(row) {
    let next = Promise.resolve(row);
    const { productCode, tableCode, funcCode, funcAction, pkCode } = this.getFuncData().info;
    if (row?.[pkCode]) {
      next = getBeanApi({
        params: { tableCode, funcCode, pkValue: row[pkCode] },
        action: funcAction,
        pd: productCode,
      });
    }

    return next.then((bean) => {
      // 处理点击首行录入的列表数据进表单,因为该种数据的主键id是uuid生成的,掉接口查回来是空值,需要给默认值
      if (isEmpty(bean)) {
        bean = this.getDefaultValues();
      }
      return bean;
    });
  }
  /**
   * 获得父功能的bean数据
   * @returns {Object}
   */
  getParentFuncBean() {
    return this.parentFunc?.store.getBean();
  }

  /**
   * 获得初始化sql
   */
  getInitQuerys() {
    const queryParser = new GridQueryParser({ $func: this });
    const funcData = this.getFuncData();
    const { defaultStrategy, info } = funcData;
    const ps = [];
    // 默认查询策略设置
    if (defaultStrategy) {
      ps.push(queryParser.setStrategyQuerys({ querys: defaultStrategy }));
    }
    // 树形功能
    if (info.funcType === FuncTypeEnum.TREE) {
      ps.push(queryParser.setTreeFuncQuerys());
    }
    // 父功能查询条件
    const parentFunc = this.getParentFunc();
    if (parentFunc) {
      const { querys } = this.store.getDefaults();
      ps.push(queryParser.setParentQuerys({ querys }));
    }
    return Promise.all(ps).then(() => queryParser.toJQuery());
  }

  /**
   * 获得功能列表
   * @returns {FuncGrid}
   */
  getFuncGrid() {
    return this.getRefMaps(FuncRefEnum.FUNC_GRID)?.value;
  }
  /**
   * 获得功能表单
   * @returns {FuncForm}
   */
  getFuncForm() {
    return this.getRefMaps(FuncRefEnum.FUNC_FORM)?.value;
  }
  /**
   * 获得功能树
   * @returns {FuncTree}
   */
  getFuncTree() {
    return this.getRefMaps(FuncRefEnum.FUNC_TREE)?.value;
  }
  /**
   * 获得树形查询
   * @returns {FuncTree}
   */
  getTreeQueryPanel() {
    return this.getFuncTree();
  }
  /**
   * 获得高级查询
   * @returns {FuncGroupQuery}
   */
  getTopQueryPanel() {
    return this.getRefMaps(FuncRefEnum.FUNC_QUERY_GROUP)?.value;
  }

  /**
   * 获得子功能配置
   * @param {*} funcCode
   * @returns
   */
  getChildFuncConfig(funcCode) {
    const children = this.reactiveObject.childFuncConfig;
    return funcCode ? children.find((child) => child.code === funcCode) : children;
  }
  /**
   * 获得子功能配置初始化兄弟面板数据
   */
  getSiblingFuncConfig() {
    const children = this.reactiveObject.childFuncConfig;
    return children.filter((child) => child.layout == 'parentFuncRow' && !child.hidden);
  }

  /**
   * 获得子功能$func
   * @param {string} funcCode
   * @returns {FuncManager}
   */
  getChildFunc(funcCode) {
    return funcCode ? this.childFuncManagers[funcCode] : this.childFuncManagers;
  }
  /**
   * 设置功能只读
   * @param {boolean} readonly
   */
  setReadOnly(readonly) {
    this.store.readonly = readonly;
  }
  /**
   * 显示子功能
   * @param {string} funcCode
   */
  showChildFunc(funcCode) {
    this.getChildFuncConfig(funcCode)?.show?.();
  }
  /**
   * 隐藏子功能
   * @param {string} funcCode
   */
  hideChildFunc(funcCode) {
    this.getChildFuncConfig(funcCode)?.hide?.();
  }
  /**
   * 设置子功能标题
   * @param {string} funcCode
   * @param {string} title
   */
  setChildFuncTitle(funcCode, title) {
    this.getChildFuncConfig(funcCode)?.setTitle?.(title);
  }
  /**
   * 功能导航条显隐
   */
  setCrumbsVisible(visible) {
    const $edit = this.getRefMaps(FuncRefEnum.FUNC_EDIT)?.value;
    if ($edit) {
      $edit.setCrumbsVisible?.(visible);
    }
  }

  /**
   * 树形功能
   * @returns
   */
  isTreeFunc() {
    return this.getFuncData().info.funcType === FuncTypeEnum.TREE;
  }
  /**
   * 获得功能新增默认值
   * @param {Object} options
   * @returns {Object}
   */
  getDefaultValues(options = {}) {
    const funcData = this.getFuncData();
    // 功能默认值
    let { values } = this.store.getDefaults();
    const { buttonCode, row } = options;
    // 树形快速查询选中的默认值
    const treeValues = this.action?.getTreeDefaultValues?.({ row }) || {};
    Object.assign(values, treeValues);
    // 功能初始值
    const { initBean } = this.store;
    if (initBean) {
      // 方法，格式化默认值
      if (isFunction(initBean)) {
        const bean = initBean({ $func: this, defaultValues: values });
        if (bean) {
          Object.assign(values, bean);
        }
      } else {
        Object.assign(values, initBean);
      }
    }

    // 格式化插入的默认值
    const transformEvent = funcData.info.gridEvents['transform-default-values'];
    if (transformEvent) {
      const tempValues = doEvents4Sync({
        $func: this,
        eventOptions: { values, buttonCode },
        code: transformEvent,
      });
      if (tempValues) {
        values = tempValues;
      }
    }
    return values;
  }

  /**
   * 获得业务数据bean
   * @param {string} id 主键
   * @returns {Promise}
   */
  getBeanById(id) {
    const funcData = this.getFuncData();
    const { productCode, tableCode, funcCode, funcAction } = funcData.info;
    return getBeanApi({
      params: { tableCode, funcCode, pkValue: id },
      pd: productCode,
      action: funcAction,
    });
  }

  /**
   * 获得列表单条数据
   * @returns
   */
  getGridSingleData() {
    const funcData = this.getFuncData();
    const { productCode, tableCode, funcCode, funcAction } = funcData.info;
    return this.getInitQuerys().then(({ j_query }) =>
      loadGrid4singleApi({
        params: { tableCode, funcCode, j_query },
        pd: productCode,
        action: funcAction,
      }),
    );
  }

  /**
   * 获得表单初始数据
   * @returns
   */
  getFormInitData() {
    let p = Promise.resolve();
    // 初始业务主键
    if (this.store.initBeanId) {
      p = this.getBeanById(this.store.initBeanId);
      // 加载单条数据
    } else if (this.loadFirstData) {
      p = this.getGridSingleData();
    }
    return p.then((bean) => {
      // 默认值
      const defaultValues = this.getDefaultValues();
      return bean || defaultValues;
    });
  }

  /**
   * 获取表单保存权限
   */
  hasFormSavePerm() {
    return this.perm.hasFormSavePerm();
  }

  /**
   * 获取表格保存权限
   * @returns
   */
  hasGridSavePerm() {
    return this.perm.hasGridSavePerm();
  }
  /**
   * 获取表格编辑权限
   * @returns
   */
  hasGridEditPerm() {
    return this.perm.hasGridEditPerm();
  }

  /**
   * 配置权限
   * @returns
   */
  hasConfigPerm() {
    return this.perm.hasConfigPerm();
  }

  /**
   * 功能使用权限
   * @returns
   */
  hasShowPerm() {
    return this.perm.hasShowPerm();
  }
  /**
   * 表单返回按钮
   */
  isFormBackButton(code) {
    return this.perm.isFormBackButton(code);
  }

  /**
   * 获得当前字典功能的字典信息
   * @param ddCode 字典编码
   * @returns {Object}
   */
  getDDInfo(ddCode) {
    return getDDCache(ddCode || this.funcDDCode);
  }

  /**
   * 获得功能绑定的事件
   * @returns
   */
  getFuncEvents() {
    const funcData = this.getFuncData();
    return bindEvents4Sync({ $func: this, events: funcData.info.funcEvents });
  }
  /**
   * 初始化响应数据
   */
  initReactiveObject() {
    const funcData = this.getFuncData();
    // 按钮
    ['grid', 'form', 'action'].forEach((type) => {
      const buttons = this.perm.getPermButtons(type) || [];
      this.reactiveObject[type + 'Buttons'] = buttons.map((button) => vueCall('reactive', button));
    });
    // 子功能
    this.reactiveObject.childFuncConfig = this.perm
      .getPermChildren()
      .map((item) => vueCall('reactive', item));
    // 表单锚点
    this.reactiveObject.formAnchors = funcData?.info.formOptions.anchor
      ? this.perm.getPermAnchors().map((item) => vueCall('reactive', item))
      : [];
  }
  /**
   * 获得列表按钮
   * @param {string} code
   * @returns
   */
  getGridButtons(code) {
    const buttons = this.reactiveObject.gridButtons;
    return code ? buttons?.find((button) => button.code === code) : buttons;
  }
  /**
   * 获得表单按钮
   * @param {string} code
   * @returns
   */
  getFormButtons(code) {
    const buttons = this.reactiveObject.formButtons;
    return code ? buttons?.find((button) => button.code === code) : buttons;
  }
  /**
   * 获得列action按钮
   * @param {string} code
   * @returns
   */
  getActionButtons(code) {
    const buttons = this.reactiveObject.actionButtons;
    return code ? buttons?.find((button) => button.code === code) : buttons;
  }
  /**
   * 获得表单锚点数据
   * @param {string} code
   * @returns
   */
  getFormAnchors(code) {
    const anchors = this.reactiveObject.formAnchors;
    return code ? anchors.find((anchor) => anchor.code === code) : anchors;
  }
}
