import { FuncGridCommon } from './func-grid';

export default class FuncTreeView extends FuncGridCommon {
  constructor(options) {
    super(options);
    this.type = options.type;
  }
  /**
   * 刷新数据
   */
  reload() {
    this.setAllTreeExpand(false);
    this.getStore()?.reload();
  }

  /**
   * 加载异步节点
   * @param {*} record
   * @returns
   */
  loadAsyncNode(record) {
    const { store } = this;
    const params = store.queryParser.toParams({
      nodeInfo: record.nodeInfo,
      rootId: record.id.split('_')[0],
    });
    record.loaded = true;
    return store.proxy.read({ params }).then((data) =>
      data.children.map((item) => {
        // 是否可以展开属性
        item.hasChild = !item.leaf;
        return store._addCache(item);
      }),
    );
  }

  /**
   * 清空查询
   */
  doResetQuery() {
    this.pluginCall('resetTreeSearchValue');
    this.clearSelectedRecords();
  }

  /**
   * 展开节点
   * @param {Boolean} record 节点
   * @param {boolean} expand 展开，默认true
   * @returns Promise
   */
  setTreeExpand(record, expand = true) {
    return this.pluginCall('setTreeExpand', record, expand);
  }
  /**
   * 展开节点
   * @param {Boolean} record 节点
   * @param {boolean} expand 展开，默认true
   * @returns Promise
   */
  setTreeExpand4Path(record, expand = true) {
    return this.pluginCall('setTreeExpand4Path', record, expand);
  }
  /**
   * 展开全部节点
   * @param {boolean} expand 展开，默认true
   * @returns Promise
   */
  setAllTreeExpand(expand = true) {
    return this.pluginCall('setAllTreeExpand', expand);
  }
}
