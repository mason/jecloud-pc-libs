import { pick, isPlainObject, decode, isFunction, clone, parseUseScope } from '../../../lodash';
import { FuncFieldTypeEnum, FuncButtonTypeEnum, FuncChildrenLayoutEnum } from '../../enum';
import FuncColumn from './func-column';
import FuncField from './func-field';
import FuncButton from './func-button';
import FuncStrategy from './func-strategy';
import FuncChildren from './func-children';
import FuncGroupQuery from './func-group-query';
/**
 * 解析数据
 * @param {*} param0
 */
export function parseFunc({ func, options }) {
  const { columns, fields, buttons, children, strategys, groupQuerys } = options;
  parseFields({ func, data: fields });
  parseColumns({ func, data: columns });
  parseButtons({ func, data: buttons });
  parseChildren({ func, data: children });
  parseStrategys({ func, data: strategys });
  parseGroupQuerys({ func, data: groupQuerys });
}

/**
 * 解析字段信息
 * @param {*} data
 */
function parseFields({ func, data }) {
  // 基础数据
  const fields = data
    .map((item) => {
      const field = new FuncField(item);
      field.metaData = parseMetaData(field);
      //给readonly赋初始值
      field.metaData.readonly = field.metaData.disabled;
      func.basic.fields.set(field.name, field);
      func.basic.ids.set(field.id, field.name);

      // 编号配置信息
      if (field.xtype === FuncFieldTypeEnum.INPUT_CODE.xtype) {
        func.fields.codeGenFieldInfo.push({
          code: field.name,
          configInfo: field.configInfo,
          ...pick(func.info, ['funcId', 'funcCode', 'funcName', 'tableCode']),
        });
        // 字典处理
      } else if (
        [
          FuncFieldTypeEnum.CHECKBOX_GROUP.xtype,
          FuncFieldTypeEnum.RADIO_GROUP.xtype,
          FuncFieldTypeEnum.SELECT.xtype,
          FuncFieldTypeEnum.INPUT_SELECT_TREE.xtype,
        ].includes(field.xtype)
      ) {
        const ddCode = field.configInfo?.split(',')[0];
        func.basic.dictionarys.set(field.code, ddCode);
      } else if (field.xtype === FuncFieldTypeEnum.FUNC_CHILD_FIELD.xtype) {
        // 子功能集合
        const funcCode = field.configInfo?.split(',')[0];
        func.fields.childFunc.push({
          code: field.name,
          configInfo: field.configInfo,
          funcCode,
        });
      }

      // 表达式
      parseFieldExps(func, field);

      // 锚点处理
      if (field.xtype === FuncFieldTypeEnum.FUNC_CHILD.xtype) {
        func.anchors.push('__child__'); // 增加占位符，后面处理子功能
      } else if (
        field.xtype === FuncFieldTypeEnum.FIELDSET.xtype &&
        !field.hidden &&
        field.anchor
      ) {
        func.anchors.push({ code: field.name, text: field.label });
      }

      // 表单显示的附件字段
      if (field.xtype === FuncFieldTypeEnum.UPLOAD_INPUT.xtype && !field.hidden) {
        func.fields.uploadFile.push(field.name);
      }

      // 表单显示的多附件字段
      if (field.xtype === FuncFieldTypeEnum.UPLOAD.xtype && !field.hidden) {
        func.fields.updateFiles.push(field.name);
      }

      // 表单启用历史留痕的字段
      if (field.history) {
        func.fields.history.push(field.name);
      }

      return field;
    })
    .filter((field) => parseUseScope(field.scope));
  // 解析展示的字段
  func.fields.form = parseDiaplayKeys(fields);
}
/**
 * 解析列信息
 * @param {*} data
 */
function parseColumns({ func, data }) {
  // 基础数据
  const columns = data.map((col) => {
    const column = new FuncColumn(col);
    column.metaData = parseMetaData(column);
    func.basic.columns.set(column.field, column);
    func.basic.ids.set(column.id, column.field);
    // 绑定列表字典信息
    if (func.basic.dictionarys.has(column.field)) {
      column.dictionary = func.basic.dictionarys.get(column.field);
    }

    return column;
  });
  // 查询选择
  const selectColumns = columns.filter((col) => col.quickColumn);
  func.columns.data = columns.map((col) => col.code);
  // 解析展示的列
  func.columns.func = parseDiaplayKeys(columns);
  func.columns.select = parseDiaplayKeys(selectColumns, '', true);
  // 解析统计列
  func.columns.statistics = columns.filter((col) => col.statistics.enable).map((col) => col.code);
  // 解析合并的列
  func.columns.merge = columns.filter((col) => col.mergeCells).map((col) => col.code);

  parseQuerys({ func, columns });
}
/**
 * 解析查询参数
 */
function parseQuerys({ func, columns }) {
  // 解析全部的字段,隐藏的字段也会解析
  const fieldsMap = func.basic.fields;
  columns
    .filter((column) => fieldsMap.has(column.code))
    .forEach((column) => {
      const { code } = column;
      // 高级查询
      func.querys.func.groupAll.push(code);
      func.querys.select.groupAll.push(code);
      // 关键字查询
      if (column.columnQueryType !== 'no') {
        func.querys.func.keyword.push(code);
        func.querys.select.keyword.push(code);
      }
      // 快速查询
      if (column.treeQuery) {
        func.querys.func.tree.push(code);
      }
      if (column.quickTreeQuery) {
        func.querys.select.tree.push(code);
      }
    });
}

/**
 * 解析查询策略
 * @param {*} param0
 */
function parseStrategys({ func, data = [] }) {
  // 设置查询策略
  func.strategys = data.map((item) => new FuncStrategy(item)) || [];
  // 默认查询策略
  func.defaultStrategy = func.strategys.find((item) => item.default);
}

/**
 * 解析高级查询
 * @param {*} param0
 */
function parseGroupQuerys({ func, data = [] }) {
  data.forEach((item) => {
    const groupQuery = new FuncGroupQuery(item);
    groupQuery.field = func.basic.fields.get(groupQuery.name);
    func.groupQuerys.push(groupQuery);
  });
}

/**
 * 解析按钮
 * @param {*} param0
 */
function parseButtons({ func, data }) {
  // 基础数据
  const buttons = data.map((item) => {
    const button = new FuncButton(item);
    button.metaData = parseMetaData(button);
    func.basic.buttons.set(button.code, button);
    func.basic.ids.set(button.id, button.code);
    return button;
  });
  // 对应位置按钮
  buttons
    .filter((button) => parseUseScope(button.scope))
    .filter((button) => {
      // 排除禁用按钮
      if (button.disabled) {
        return false;
      } else if (func.info.enableWorkflow) {
        // 如果是工作流启用了,去掉审核和弃审按钮，列表保存
        return ![
          FuncButtonTypeEnum.FORM_SUBMIT_BUTTON,
          FuncButtonTypeEnum.FORM_CANCEL_BUTTON,
          FuncButtonTypeEnum.GRID_UPDATE_BUTTON,
          FuncButtonTypeEnum.ACTION_UPDATE_BUTTON,
        ].includes(button.code);
      } else {
        return true;
      }
    })
    .forEach((button) => {
      switch (button.postion) {
        case 'FORM':
          func.buttons.form.push(button.code);
          // 表达式
          if (button.expsEnable) {
            func.exps.buttons.push(button.code);
          }
          break;
        case 'GRID':
          func.buttons.grid.push(button.code);
          break;
        case 'ACTION':
          func.buttons.action.push(button.code);
          break;
      }
    });
}

/**
 * 解析子功能
 * @param {*}} param0
 */
function parseChildren({ func, data }) {
  const isGroupLayout = func.info.childLayout === FuncChildrenLayoutEnum.FORM_INNER_GROUP;
  // 基础数据
  const children = data.map((item) => {
    const child = new FuncChildren(item);
    //TODO: 启用分组布局，其他布局方式失效
    if (isGroupLayout) {
      child.layout = '';
    }
    child.metaData = parseMetaData(child);
    func.basic.children.set(child.code, child);
    func.basic.ids.set(child.id, child.code);
    // 表达式
    if (child.expsEnable) {
      func.exps.children.push(child.code);
    }
    return child;
  });

  const childCodes = [];
  const anchors = [];
  children
    .filter((child) => parseUseScope(child.scope) && child.enable)
    .forEach((child) => {
      // 子功能数据
      childCodes.push(child.code);
      // 子功能锚点
      if (child.anchor) {
        anchors.push({ code: child.code, text: child.title, child: true });
      }
    });

  // 子功能数据
  func.children = childCodes;
  // 增加子功能锚点
  func.anchors.splice(func.anchors.indexOf('__child__'), 1, ...anchors);
}
/**
 * 解析列表，表单展示项的key
 * @param {Array} data 功能列数据集
 * @param {String} groupName 分组
 * @param {Boolean} useHidden 是否过滤hide的数据 默认过滤
 * @returns
 */
function parseDiaplayKeys(data, groupName = '', useHidden = false) {
  const items = [];
  data.forEach((item) => {
    const code = item.code;
    // 启用隐藏项 查询选择不考虑列隐藏
    if ((useHidden || !item.hidden) && item.groupName === groupName) {
      const children = parseDiaplayKeys(data, code, useHidden);
      if (children.length) {
        items.push({ code: code, children });
      } else {
        items.push(code);
      }
    }
  });
  return items;
}

/**
 * 根据key获得原始数据
 * @param {*} param
 * @param {*} param.tiled 平铺数据
 * @returns
 */
export function keyToData({ keys, sourceMap, tiled = false }) {
  const items = [];
  keys.forEach((key) => {
    if (key.code) {
      const item = sourceMap.get(key.code);
      const children = keyToData({ keys: key.children, sourceMap, tiled });
      if (tiled) {
        items.push(item, ...children);
      } else {
        item.children = children;
        items.push(item);
      }
    } else {
      items.push(sourceMap.get(key));
    }
  });
  return items;
}

/**
 * 树形功能配置
 */
export function parseFuncTreeConfig(config) {
  const funcTreeConfig = {
    parent: 'SY_PARENT',
    nodePath: 'SY_PATH',
    nodeType: 'SY_NODETYPE',
    layer: 'SY_LAYER',
  };
  decode(config || '[]').forEach((item) => {
    funcTreeConfig[item.code] = item.value;
  });

  return funcTreeConfig;
}

/**
 * 解析原始数据
 * @param {*} item
 */
function parseMetaData(item) {
  const metaData = {};
  for (let p in item) {
    const val = item[p];
    if (!isFunction(val)) {
      metaData[p] = isPlainObject(val) ? clone(val) : val;
    }
  }
  return metaData;
}
/**
 * 解析绑定字段的表达式
 * @param {*} funcData
 * @param {*} field
 */
function parseFieldExps(funcData, field) {
  if (field.expsEnable) {
    // 全局表达式
    funcData.exps.fields.push(field.name);
  } else if (field.bindExpsEnable) {
    // 绑定字段表达式
    Object.keys(field.bindExps).forEach((key) => {
      const item = field.bindExps[key];
      // 解析绑定字段编码
      const fields = item.exp.split(',');
      fields.forEach((name) => {
        funcData.bindExps[name] = funcData.bindExps[name] || {};
        funcData.bindExps[name][field.name] = funcData.bindExps[name][field.name] || {};
        funcData.bindExps[name][field.name][key] = { fn: item.fn };
        // 绑定字段编码/本字段编码/表达式类型 = {fn:表达式函数}
      });
    });
  }
}
