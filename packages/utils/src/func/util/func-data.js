import { isObject, toValue } from '../../lodash';
import { loadDDItemByCodes } from '../../system/dictionary';
import { loadFunctionApi } from '../api';
import Func from '../model/func/func';
/**
 * 元数据缓存
 */
const metaCache = new Map();
/**
 * model缓存
 */
const modelCache = new Map();

/**
 * 获取功能元数据
 * @param {*} funcCode
 * @param {*} cache
 * @returns
 */
export function getFuncMetaData({ funcCode, cache = true }) {
  let metaData = cache ? metaCache.get(funcCode) : null;
  if (metaData) {
    return Promise.resolve(metaData);
  }
  // 元数据
  return loadFunctionApi({ funcCode }).then((data) => {
    // 设置元数据缓存
    metaCache.set(funcCode, data);
    return data;
  });
}

/**
 * 获取功能解析数据
 * @param {*} funcCode
 * @returns
 */
export function loadFuncData(funcCode) {
  // 缓存
  let cache = true;
  if (isObject(funcCode)) {
    const options = funcCode;
    cache = toValue(options.cache, cache, true);
    funcCode = options.funcCode;
  }
  // 缓存数据
  let model = cache ? getFuncCache(funcCode) : null;
  if (model) {
    return Promise.resolve(model);
  }

  // 读取元数据
  return getFuncMetaData({ funcCode, cache }).then((metaData) => {
    const func = new Func(metaData);
    console.log(`${func.funcName}【${func.funcCode}】：功能数据加载完成`);
    modelCache.set(funcCode, func);
    // 加载功能字典缓存
    return loadDDItemByCodes(Array.from(func.basic.dictionarys.values())).then(() => func);
  });
}

/**
 * 获取功能缓存数据
 * @param {*} funcCode
 * @returns
 */
export function getFuncCache(funcCode) {
  return modelCache.get(funcCode);
}

/**
 * 清除缓存
 * @param {*} funcCode
 */
export function clearFuncCache(funcCode) {
  metaCache.delete(funcCode);
  modelCache.delete(funcCode);
}
