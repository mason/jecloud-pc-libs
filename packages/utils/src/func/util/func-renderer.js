import { toBoolean, isNotEmpty, isString, toNumber, decode, isArray } from '../../lodash';
import { vueCall } from '../../je';
import { parseConfigInfo } from './field-config-info';
import { getDDItemCache, getDDCache } from '../../system/dictionary';
/**
 * 数据字典
 * @param {string} text 内容
 * @param {Object} options
 * @param {Object} options.dictionary 字典配置
 * @param {Object} options.dictionary.code 字典编码
 * @param {Object} options.dictionary.enableIcon 启用字典图标
 * @param {Object} options.dictionary.enableColor 启用字典文字颜色
 * @param {Object} options.dictionary.enableBgColor 启用字典背景颜色
 * @param {Object} options.configInfo 字典带值配置，配合行数据，进行格式化
 * @param {Object} options.row 行数据
 * @returns {VNode|string}
 */
export function renderer4Dictionary(text, { dictionary, configInfo, row }) {
  // 字典处理
  const ddCache = getDDCache(dictionary.code);
  if (text && ddCache) {
    const ddItemList = getDDItemCache(dictionary.code);
    if (['LIST', 'TREE'].indexOf(ddCache?.type) != -1 && ddItemList) {
      const codes = text.split(',');
      // 启用字典图标
      const enableIcon = dictionary.enableIcon;
      // 启用字典颜色
      const enableColor = dictionary.enableColor;
      // 启用字典背景颜色
      const enableBgColor = dictionary.enableBgColor;
      let color = '';
      let bgColor = '';
      const nodes = [];
      codes.forEach((code) => {
        const item = ddItemList.find((item) => item.code === code || item.text === code);
        let node = code;
        if (item) {
          // 字体颜色值
          enableColor && (color = item.textColor);
          enableBgColor && (bgColor = item.backgroundColor);
          // 图标
          node =
            enableIcon && item.icon
              ? vueCall('h', 'i', { class: ['je-column-dictionary-icon', item.icon] })
              : item.text;
        }
        nodes.push(node, ',');
      });
      // 删除最后的逗号
      nodes.pop();

      // 颜色处理
      text = vueCall(
        'h',
        'div',
        {
          style: {
            backgroundColor: bgColor,
            color,
            ...(enableBgColor
              ? {
                  padding: '4px 8px',
                  borderRadius: '6px',
                  lineHeight: 1,
                  display: 'inline-block',
                }
              : {}),
          },
        },
        nodes,
      );
    } else if (configInfo) {
      const config = parseConfigInfo({ configInfo });
      const sourceToTargetText = config.fieldMaps.sourceToTarget?.text;
      if (isNotEmpty(sourceToTargetText)) {
        text = row[sourceToTargetText[0]];
      }
    }
  }
  return text;
}

/**
 * 复选框样式
 * @param {string} text 内容
 * @param {Object} options
 * @param {string} options.checkedIcon 选中图标
 * @param {string} options.unCheckedIcon 未选择图标
 * @returns {VNode}
 */
export function renderer4Checkbox(text, options = {}) {
  const checked = toBoolean(text);
  const { checkedIcon = 'fas fa-check-square', unCheckedIcon = 'fal fa-square' } = options;
  return vueCall(
    'h',
    'div',
    { class: 'switch-cell switch-layout-checkbox' },
    vueCall('h', 'i', {
      style: 'font-size:18px',
      class: {
        'switch-cell-item-checked': checked,
        [checkedIcon]: checked,
        [unCheckedIcon]: !checked,
      },
    }),
  );
}
/**
 * 附件
 * @param {string} text 内容
 * @param {Object} options
 * @param {Function} options.onDownload 下载方法
 * @param {Function} options.onPreview 预览方法
 * @param {Function} options.onEdit 编辑方法
 * @returns {VNode}
 */

export function renderer4Files(text, options = {}) {
  const { onDownload, onPreview, onEdit } = options;
  // 附件
  const files = decode(text) || [];
  if (!isArray(files)) return vueCall('h', 'div', text);

  // 编辑
  const editVNode = onEdit
    ? vueCall('h', 'i', { class: 'fal fa-edit je-column-upload-cell-edit', onClick: onEdit })
    : null;
  if (files.length === 0)
    return vueCall('h', 'div', { class: 'je-column-upload-cell' }, [editVNode]);
  // 附件vnode
  const fileVNodes = files.map((item) =>
    vueCall('h', 'div', { class: 'je-column-upload-cell-content' }, [
      vueCall('h', 'div', { class: 'je-column-upload-cell-left' }, item.relName),
      vueCall(
        'h',
        'div',
        { class: 'je-column-upload-cell-right' },
        [
          { text: '预览', fn: onPreview, enable: !!onPreview },
          { text: '|', enable: onPreview && onDownload },
          { text: '下载', fn: onDownload, enable: !!onDownload },
        ].map((action) =>
          action.enable
            ? action.fn
              ? vueCall(
                  'h',
                  'span',
                  {
                    class: 'je-column-upload-cell-content-span',
                    onClick() {
                      action.fn(item);
                    },
                  },
                  action.text,
                )
              : action.text
            : null,
        ),
      ),
    ]),
  );

  return vueCall('h', 'div', { class: 'je-column-upload-cell' }, [...fileVNodes, editVNode]);
}

/**
 * 超链接
 * @param {string} text 内容
 * @returns {VNode}
 */
export function renderer4Link(text) {
  return text ? vueCall('h', 'span', { class: 'je-column-link-cell' }, text) : null;
}

/**
 * 高亮
 * @param {string} text 内容
 * @param {string} keyword 关键字
 * @returns {VNode|string}
 */
export function renderer4Highlight(text, keyword) {
  let highlightText = text;
  if (isNotEmpty(text) && isString(text)) {
    if (keyword) {
      const splitStr = '`__`';
      let vns = [];
      const keys = text
        .toString?.()
        ?.replaceAll(keyword, splitStr + keyword + splitStr)
        ?.split(splitStr);
      vns.push(
        keys.map((key) => {
          return key === keyword ? vueCall('h', 'span', { class: 'keyword' }, key) : key;
        }),
      );
      highlightText = vueCall('h', 'div', { class: 'je-highlight-text' }, vns);
    }
  }
  return highlightText;
}
/**
 * 评星
 * @param {string} text 内容
 * @param {Object} options
 * @param {number} options.count 数量
 * @param {number} options.size 文字大小
 * @param {string} options.color 默认颜色
 * @param {string} options.onColor 激活颜色
 * @param {string} options.icon 默认图标
 * @param {string} options.onIcon 激活图标
 * @returns {VNode}
 */
export function renderer4Star(text, options = {}) {
  const {
    count = 5,
    size = 18,
    icon = 'fas fa-star',
    onIcon = 'fas fa-star',
    color = '#f0f0f0',
    onColor = '#fadb14',
  } = options;
  const val = toNumber(text);
  const vns = [];
  for (let i = 0; i < count; i++) {
    vns.push(
      vueCall('h', 'i', {
        style: {
          color: val <= i ? color : onColor,
          lineHeight: 1,
          marginRight: i + 1 == count ? '0' : '8px',
        },
        class: val <= i ? icon : onIcon,
      }),
    );
  }
  return vueCall(
    'h',
    'span',
    {
      style: {
        fontSize: size + 'px',
      },
    },
    vns,
  );
}
