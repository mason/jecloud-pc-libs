/*
功能事件代码编写说明：

1. 事件传入参数
  编辑器内，暴露了可以直接在代码中使用的全局变量如下：

  - EventOptions：事件参数，包含了当前事件中的所有参数
      const $func = EventOptions.$func; // 功能对象，所有事件都包含
      ... 其他事件参数

  - JE: 公共类库，提供了常用的类库
      const vue = JE.useVue();        // Vue库
      const ui = JE.useUi();          // UI库
      const utils = JE.useUtils();    // 工具库
      const admin = JE.useAdmin();  // 主应用类库

2. 事件返回参数
  编辑器内，可以直接使用return关键字，进行返回参数

  - 普通出参：直接 return ... 即可
      return true;

  - 异步出参：return Promise
      const utils = JE.useUtils(); // 工具库
      const deferred = utils.createDeferred(); // Promise异步队列函数
      // 业务逻辑执行完毕，可以调用 deferred.resolve()
      // 业务逻辑执行失败，可以调用 deferred.reject();
      return deferred.promise;

  - 视图出参：必须返回 VNode，否则无法正确显示
      const { h } = JE.useVue();
      return h('div',{style:{}},'html内容');

*/
import {
  isPromise,
  isFunction,
  isEmpty,
  isPlainObject,
  camelCase,
  kebabCase,
  decode,
  isNotEmpty,
  parseUseScope,
} from '../../lodash';
import { execScript } from '../../web/sandbox';
/**
 * 绑定事件
 * @param {Object} $func 功能对象
 * @param {Object} events 事件对象
 * @param {Object} defaultEvents 系统默认事件，如果有相同的自定义事件，会覆盖系统默认事件
 * @param {Object} options 事件公共参数
 * @returns Events
 */
export function bindEvents4Promise({ $func, events = {}, defaultEvents = {}, options }) {
  // 事件拦截器前缀
  const beforeEventPrefix = 'before-';
  const afterEventPrefix = 'after-';
  const onEvents = {};
  // 排除拦截器事件，内部单独处理
  const enableEvents = Object.keys(events).filter(
    (key) => !key.startsWith(beforeEventPrefix) && !key.startsWith(afterEventPrefix),
  );
  // 追加默认事件
  enableEvents.push(...Object.keys(defaultEvents).filter((key) => !enableEvents.includes(key)));

  enableEvents.forEach((key) => {
    // 事件名称
    const eventName = key;
    const eventCode = events[eventName];
    // 事件拦截器，自定义扩展使用，可以拦截系统默认事件
    const beforeEventCode = events[beforeEventPrefix + eventName];
    const afterEventCode = events[afterEventPrefix + eventName];
    // 绑定事件
    const onEventName = formatEventName(eventName, true);
    onEvents[onEventName] = function (...args) {
      // 事件参数
      const eventOptions = parseEventOptions({ $func, options, args, onEventName });
      // 事件拦截器-前
      return doEvents4Promise({
        eventOptions,
        code: beforeEventCode,
      })
        .then(() => {
          if (eventCode) {
            // 自定义事件，会覆盖系统默认事件
            return doEvents4Promise({ eventOptions, code: eventCode });
          } else {
            // 系统默认事件
            return defaultEvents[eventName]?.(...args);
          }
        })
        .then((eventResult) => {
          // 事件拦截器-后
          return doEvents4Promise({
            eventOptions: { eventResult, ...eventOptions },
            code: afterEventCode,
          });
        })
        .catch((message) => {
          console.log(message);
        });
    };
  });
  return onEvents;
}

/**
 * 执行事件 - Promise
 * @param {Object} $func 功能对象
 * @param {Object} eventOptions 事件参数
 * @param {string} code 事件代码
 * @param {boolean} before 是否拦截器前事件
 * @returns {Promise}
 */
export function doEvents4Promise({ $func, eventOptions, code }) {
  if (isEmpty(code)) return Promise.resolve();
  const result = doEvents4Sync({ $func, eventOptions, code, async: true });
  return parseEventResult4Promise(result);
}

/**
 * 解析事件返回结果为Promise
 * @param {boolean|Promise} result
 * @returns {Promise}
 */
export function parseEventResult4Promise(result) {
  const returnResult = (flag) => {
    // 拦截器事件，如果返回true，false，特殊处理
    return flag !== false ? Promise.resolve(flag) : Promise.reject(flag);
  };
  if (isPromise(result)) {
    return result.then(returnResult);
  } else {
    return returnResult(result);
  }
}

/**
 * 绑定事件 - 同步
 * @param {Object} $func 功能对象
 * @param {Object} events 事件对象
 * @param {Object} defaultEvents 系统默认事件，如果有相同的自定义事件，会覆盖系统默认事件
 * @param {Object} options 事件公共参数
 * @returns Events
 */
export function bindEvents4Sync({ $func, events = {}, options, arrayArguments }) {
  const onEvents = {};
  Object.keys(events).forEach((key) => {
    // 事件名称
    const eventName = key;
    const eventCode = events[eventName];
    // 绑定事件
    const onEventName = formatEventName(eventName, true);
    onEvents[onEventName] = function (...args) {
      // 事件参数
      const eventOptions = parseEventOptions({ $func, options, args, onEventName, arrayArguments });
      return doEvents4Sync({ $func, eventOptions, code: eventCode });
    };
  });
  return onEvents;
}

/**
 * 执行事件 - 同步
 * @param {Object} eventOptions 事件参数
 * @param {string} code 事件代码
 * @returns {Promise}
 */
export function doEvents4Sync({ $func, eventOptions, code, async }) {
  if (isEmpty(code)) return;
  const options = { EventOptions: { $func, ...eventOptions } };
  return isFunction(code) ? code(options) : execScript(code, options, async);
}
/**
 * 解析事件参数
 * @param {*} $func
 * @param {Object} options 系统默认参数
 * @param {Array} args 事件参数
 * @returns
 */
function parseEventOptions({ $func, options, args = [], onEventName, arrayArguments }) {
  // 处理参数
  let first = args[0];
  if (!isPlainObject(first)) {
    // 数组参数 && change事件，特殊处理
    if (arrayArguments && ['onChange'].includes(onEventName)) {
      first = { value: first, valueOptions: args[1] };
    } else {
      first = {};
    }
  }
  // 事件参数
  let baseOptions = options;
  if (isFunction(options)) {
    // 函数参数，为了方便在事件进行ref对象的获取；
    baseOptions = options();
  }
  const eventOptions = { $func, ...baseOptions, ...first, $arguments: args };
  return eventOptions;
}

/**
 * 格式化事件名称，统一使用kebab命名
 * @param {*} name
 * @param {*} on
 * @returns
 */
export function formatEventName(name, on) {
  return on ? camelCase(`on-${name}`) : kebabCase(name);
}
/**
 * 解析功能脚本
 * @param {*} data
 * @returns
 */
export function parseEvents(data) {
  let items = decode(data) || [];
  // 兼容旧数据
  if (isPlainObject(items)) {
    items = [items];
  }
  const events = {};
  items.forEach?.((item) => {
    if (item.fire == '1' && isNotEmpty(item.func)) {
      const eventName = formatEventName(item.code);
      if (parseUseScope(item.scope)) {
        events[eventName] = item.func;
      }
    }
  });
  return events;
}
