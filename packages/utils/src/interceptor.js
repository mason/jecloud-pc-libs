import { mitt } from './lodash';
let Interceptor;
/**
 * 全局拦截器
 */
export function getGlobalInterceptor() {
  if (!Interceptor) {
    Interceptor = initGlobalInterceptor();
  }
  return Interceptor;
}
/**
 * 设置全局拦截器实例
 * @param {*} instance
 */
export function setGlobalInterceptor(instance) {
  Interceptor = instance;
}
/**
 * 添加拦截器
 * @param {string} name 事件名
 * @param {Function} handler 函数
 */
export function addGlobalInterceptor(name, handler) {
  return getGlobalInterceptor().on(name, handler);
}
/**
 * 删除拦截器
 * @param {string} name 事件名
 * @param {Function} handler 函数
 */
export function removeGlobalInterceptor(name, handler) {
  return getGlobalInterceptor().off(name, handler);
}
/**
 * 触发拦截器
 * @param {string} name 事件名
 * @param {...} args 操作参数
 */
export function emitGlobalInterceptor(name, ...args) {
  return getGlobalInterceptor().emit(name, ...args);
}
/**
 * 系统全局拦截器
 *
 */
export const GLOBAL_INTERCEPTORS = {
  // ajax事件
  AJAX: {
    BEFORE_REQUEST: 'global-ajax-before-request',
    BEFORE_RESPONSE: 'global-ajax-before-response',
    BEFORE_RESPONSE_CATCH: 'global-before-response-catch',
  },
};
/**
 * 获取全局实例
 * @param {*} name
 * @returns
 */
export function getGlobalInstance(name) {
  return getGlobalInterceptor().getGlobalInstance(name);
}
/**
 * 设置全局实例
 * @param {*} name
 * @param {*} instance
 * @returns
 */
export function setGlobalInstance(name, instance) {
  getGlobalInterceptor().setGlobalInstance(name, instance);
}

/**
 * 全局实例对象
 * 方便主子应用使用
 */
function initGlobalInterceptor() {
  // 拦截器实例
  const interceptor = new mitt();
  // 全局实例对象，方便主子应用使用
  interceptor.GLOBAL_INSTANCES = {};
  // 获取全局实例对象
  interceptor.getGlobalInstance = (key) => {
    return key ? interceptor.GLOBAL_INSTANCES[key] : interceptor.GLOBAL_INSTANCES;
  };
  // 设置全局实例对象
  interceptor.setGlobalInstance = (key, instance) => {
    interceptor.GLOBAL_INSTANCES[key] = instance;
  };
  return interceptor;
}
