import { WorkflowHistory } from '../src';
import { watch, ref, nextTick } from 'vue';

export function useWorkflowHistory($func) {
  // 功能数据
  const { $workflow } = $func;
  if (!$workflow.isEnable()) {
    return { workflowHistorySlot() {} };
  }
  const wfHistory = ref();
  watch(
    () => $workflow.state.refreshEmitter,
    () => {
      // 刷新数据
      nextTick(() => {
        wfHistory.value?.refresh({ beanId: $func.store.getBeanId() });
      });
    },
  );
  const workflowHistorySlot = ({ colSpan }) => {
    return <WorkflowHistory ref={wfHistory} colSpan={colSpan} />;
  };
  return {
    workflowHistorySlot,
  };
}
