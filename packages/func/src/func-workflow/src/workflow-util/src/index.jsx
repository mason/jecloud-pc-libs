import { Button, Dropdown, Menu } from '@jecloud/ui';
import { WorkflowOperatorEnum } from '../../workflow-manager';
import {
  showTaskChange,
  showWorkflowOperation,
  showWorkflowMain,
  showTaskChangeUser,
  showTaskPassRound,
  showTaskCheckApprove,
  showTaskOperationModal,
} from '../../workflow-modal';
import { isRef } from 'vue';
import { isNotEmpty, isEmpty } from '@jecloud/utils';

/**
 * 流程追踪
 */
export { showWorkflowMain };

/**
 * 流程按钮操作
 * @param {*} param0
 */
export function doWorkflowOperation({ button, $func }) {
  const { $workflow } = $func;
  const { operationId } = button;
  // 公共操作
  $workflow.action
    .doButtonOperation({
      // 执行操作
      button,
    })
    .then((data) => {
      if (!data.custom) return;
      switch (operationId) {
        case WorkflowOperatorEnum.TASK_URGE_OPERATOR: // 催办
          showTaskOperationModal({ data, button, $func });
          break;
        case WorkflowOperatorEnum.TASK_CHANGE_OPERATOR: // 切换任务
          showTaskChange({ data, button, $func });
          break;
        case WorkflowOperatorEnum.TASK_PERSONNEL_ADJUSTMENTS_OPERATOR: // 人员调整
        case WorkflowOperatorEnum.TASK_CHANGE_ASSIGNEE_OPERATOR: // 更换负责人
          showTaskChangeUser({ data, button, $func });
          break;
        case WorkflowOperatorEnum.TASK_PASSROUND_OPERATOR: //传阅
          showTaskPassRound({ data, button, $func });
          break;
        case WorkflowOperatorEnum.TASK_PASSROUND_READ_OPERATOR: //审阅
        case WorkflowOperatorEnum.TASK_WITH_DRAW_OPERATOR: // 撤回
          showTaskCheckApprove({ button, $func });
          break;
        default:
          showWorkflowOperation({ button, $func });
          break;
      }
    });
}

/**
 * 流程按钮插槽
 * @returns
 */
export const useWorkflowButtonSlot = ({ $func, buttons, hideLine }) => {
  const { $workflow } = $func;
  buttons = buttons || $workflow.getOperationButtons();

  //获得展示按钮 应对流程追踪有多个发起按钮
  const getButton = (button) => {
    //如果是发起按钮就处理启动表单式的逻辑
    if (!$workflow.doStartExpression(button)) {
      return null;
    }
    return (
      <Button
        icon={button.icon}
        class="workflow-btn"
        style="color:#2196F3;border-color:#e6e6e6"
        onClick={() => {
          doWorkflowOperation({ $func, button });
        }}
      >
        {button.name}
      </Button>
    );
  };

  return () => {
    const buttonSlot = (isRef(buttons) ? buttons.value : buttons).map((button) => {
      return button.isEnd ? <span>{button.name}</span> : getButton(button);
    });
    /** 插入竖线 */
    doWorkflowLine({ buttonSlot, hideLine, $func });
    return buttonSlot;
  };
};
/**
 *  插入竖线
 * @param {*} param0
 */
const doWorkflowLine = ({ buttonSlot, hideLine, $func }) => {
  const $form = $func.getFuncForm();
  const formButtons = Object.values($form.getButtons());
  const { isFuncForm } = $func;
  // 有效按钮，功能表单去除返回按钮
  let visibleButtons = formButtons.filter(
    (button) => !button.hidden && (!$func.isFormBackButton(button.code) || !isFuncForm),
  );
  // 显示按钮
  visibleButtons = visibleButtons.filter((button) => !button.hidden);
  // 有流程按钮&&流程追踪面板不显示竖线&&功能表单有按钮
  const workflowBtn = buttonSlot.filter((item) => isNotEmpty(item));
  if (workflowBtn?.length > 0 && !hideLine && visibleButtons?.length > 0) {
    buttonSlot.unshift(<div class="je-workflow-line"></div>);
  }
};
