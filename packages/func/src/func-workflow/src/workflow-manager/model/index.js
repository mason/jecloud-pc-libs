import { Func } from '@jecloud/utils';
export const {
  TaskAssignee,
  TaskButton,
  TaskButtonBgColor,
  TaskCancellationButton,
  TaskChangeButton,
  TaskEndButton,
  TaskCommonParams,
  TaskNode,
  WorkflowConfig,
  WorkflowFormConfig,
  WorkflowOperation,
} = Func;
