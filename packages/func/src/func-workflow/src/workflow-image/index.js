import JeWorkflowImage from './src';
import { withInstall } from '../../../utils';
export const WorkflowImage = withInstall(JeWorkflowImage);
export default WorkflowImage;
