import { defineComponent, computed } from 'vue';
import { Toolbar, Button } from '@jecloud/ui';
import { useInjectWorkflow } from './hooks/context';

export default defineComponent({
  name: 'WorkflowOperationTbar',
  inheritAttrs: false,
  setup() {
    const { $operation, $workflow } = useInjectWorkflow();
    const { state } = $operation;
    const submitting = computed(() => $workflow.state.submitting);
    return () => (
      <Toolbar class="je-workflow-operation-bbar">
        <Toolbar.Fill />
        <Button
          onClick={() => $operation.doOperationSetp({ next: false })}
          style={{ display: state.activeStep === 1 ? 'none' : undefined }}
        >
          上一步
        </Button>
        <Button
          type="primary"
          loading={submitting.value}
          onClick={() => $operation.doOperationSetp({ next: true })}
        >
          {state.activeStep === 3 ? state.name : '下一步'}
        </Button>
      </Toolbar>
    );
  },
});
