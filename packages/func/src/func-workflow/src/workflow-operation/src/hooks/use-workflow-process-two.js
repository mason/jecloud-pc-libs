import { reactive, watch, ref } from 'vue';
import { useInjectWorkflow } from './context';
import { isNotEmpty, getSystemConfig } from '@jecloud/utils';
import { Modal } from '@jecloud/ui';
import { WorkflowOperatorEnum } from '../../../workflow-manager';
export function useWorkflowProcessTwo() {
  const { $func, button, $operation } = useInjectWorkflow();
  const { state } = $operation;
  const loading = ref(true);
  const $user = ref();
  const userConfig = reactive({
    nodes: [],
    types: [],
    users: [],
    activeNode: null,
    activeNodeId: null,
    activeTypeId: null,
    resultUsers: [],
    wfButton: button,
    activeStepChange: false,
  });
  // 同步人员选择数据
  watch(
    () => userConfig.resultUsers,
    (users) => {
      userConfig.activeNodeId && $operation.setAssigneeUsers(userConfig.activeNodeId, users);
    },
    { deep: true },
  );
  // 监听任务节点切换
  watch(
    () => userConfig.activeNodeId,
    () => {
      userConfig.activeNode = userConfig.nodes.find((item) => item.id === userConfig.activeNodeId);
      if (userConfig.activeNode) {
        // 设置人员结果集
        userConfig.resultUsers = $operation.getAssigneeUsers(userConfig.activeNodeId);
        // 设置人员类型
        userConfig.types = userConfig.activeNode.users ?? [];
        userConfig.activeTypeId = userConfig.types[0]?.id;
        // 固定人,随机节点默认全部选中
        if (['to_assignee', 'random'].indexOf(userConfig.activeNode.type) != -1) {
          // 加签操作除外
          if (button?.operationId != WorkflowOperatorEnum.TASK_COUNTERSIGN_OPERATOR) {
            userConfig.activeNode.selectAll = true;
            userConfig.activeNode.multiple = true;
          }
        }
      } else {
        Modal.alert('节点配置有误,请联系管理员！', 'error');
      }
    },
  );
  // 监听筛选条件切换
  watch(
    () => [userConfig.activeTypeId, userConfig.activeStepChange],
    () => {
      // 节点的人员为空
      if (!userConfig.activeTypeId) {
        $user.value.loadUser([]);
        // 开启该变量,流程人员为空也可提交
        if (getSystemConfig('JE_WORKFLOW_CLOSE_PERSONNEL_ANOMLY') != '1') {
          Modal.alert('当前节点未配置可处理人信息，请联系管理员进行操作！', 'error');
          return;
        }
      }
      const activeType = userConfig.types.find((item) => item.id === userConfig.activeTypeId) || [];
      $user.value.loadUser(activeType.children, { async: activeType.async == true });
    },
  );
  // 切换节点，更新用户数据
  watch(
    () => state.activeStep,
    (nv, ov) => {
      if (ov === 1 && nv === 2) {
        // 重置第二步选中的人员
        $user.value?.resetActiveUser();
        loading.value = true;
        $operation.initAssigneeNodes().then(({ nodes, activeStep }) => {
          // 重新加载数据
          if (userConfig.activeNodeId == nodes[0]?.id) {
            userConfig.activeStepChange = !userConfig.activeStepChange;
          }
          loading.value = false;
          userConfig.nodes = nodes;
          userConfig.activeNodeId = userConfig.nodes[0]?.id;
          if (activeStep) {
            state.activeStep = 3;
          }
        });
      }
      // 第一步到第三步需要把选的人员清除
      if (ov === 1 && nv === 3) {
        $user.value?.resetActiveUser();
        state.assignees = {};
      }
    },
  );
  //选中常用人员
  const selectUser = ({ row }) => {
    if (isNotEmpty(row) && isNotEmpty(row.userId) && userConfig.activeNode.personnelAdjustments) {
      $user.value.selectHistoryUser({ userId: row.userId });
    }
  };

  //会签节点审批顺序值 添加到主数据中
  const changeSequential = (data) => {
    userConfig.activeNode.sequential = data;
    if (isNotEmpty(state.assignees[userConfig.activeNodeId])) {
      state.assignees[userConfig.activeNodeId].sequential = data;
    }
  };

  return { userConfig, loading, state, $func, $user, selectUser, changeSequential };
}
