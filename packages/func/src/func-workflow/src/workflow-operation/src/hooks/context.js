import { inject, provide } from 'vue';

export const WorkflowContextKey = Symbol('jeWorkflowContextKey');

export const useProvideWorkflow = (state) => {
  provide(WorkflowContextKey, state);
};

export const useInjectWorkflow = () => {
  return inject(WorkflowContextKey, null);
};
