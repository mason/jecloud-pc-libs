import { defineComponent } from 'vue';
import { userWorkFlowCommonUser } from './hooks/user-workflow-common-user';
import { Dropdown, Menu } from '@jecloud/ui';

export default defineComponent({
  name: 'WorkflowCommonUser',
  inheritAttrs: false,
  setup(props, context) {
    const { commonUser, selectUser, handlerMenu } = userWorkFlowCommonUser(props, context);
    return () => (
      <div>
        {commonUser.showUserDatas.length > 0 ? (
          <div class="task-user-history">
            <span class="task-user-history-span">常用：</span>
            {commonUser.showUserDatas.map((item) => (
              <span
                class={
                  commonUser.selectUserId == item.userId
                    ? 'checked task-user-history-item'
                    : 'task-user-history-item'
                }
                onClick={() => selectUser({ row: item })}
              >
                {item.userName}
              </span>
            ))}
            {commonUser.menuUserDatas.length > 0 ? (
              <Dropdown
                trigger={['click']}
                v-slots={{
                  overlay() {
                    return (
                      <Menu
                        style="width:100px;max-height:256px;overflow: overlay;"
                        class="task-user-history-menu"
                        onClick={handlerMenu}
                      >
                        {commonUser.menuUserDatas.map((item) => {
                          return <Menu.Item key={item.userId}>{item.userName}</Menu.Item>;
                        })}
                      </Menu>
                    );
                  },
                }}
              >
                <span class="task-user-history-more">
                  更多 <i style="margin-left:3px" class="fal fa-angle-down"></i>
                </span>
              </Dropdown>
            ) : null}
          </div>
        ) : null}
      </div>
    );
  },
});
