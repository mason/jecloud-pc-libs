import { defineComponent } from 'vue';
import { Row, Col } from '@jecloud/ui';
import { useInjectWorkflow } from './hooks/context';

export default defineComponent({
  name: 'WorkflowOperationBbar',
  inheritAttrs: false,
  setup() {
    const { $operation } = useInjectWorkflow();
    const { state } = $operation;
    return () => {
      // 直接提交，省去人员
      const setps = state.directOperation
        ? ['第一步，选择路径', '第二步，填写意见']
        : ['第一步，选择路径', '第二步，选择处理人', '第三步，填写意见'];
      return (
        <Row class="je-workflow-operation-tbar" align="middle">
          {setps.map((message, index) => {
            return (
              <Col span={24 / setps.length} class={{ 'is--active': index < state.activeStep }}>
                <i class="icon fal fa-check-circle" />
                {message}
              </Col>
            );
          })}
        </Row>
      );
    };
  },
});
