import { ref, watch } from 'vue';
import { Data } from '@jecloud/utils';
export function operationGrid({ props, context }) {
  const { $workflow } = props.funcObj;
  const loading = ref(true);
  const gridStore = Data.Store.useGridStore({ data: [] });
  const columnData = ref([]);
  const onPageChange = () => {
    const type = props.typeKey == 'iAskedForIt' ? 'FROM_' : 'TO_';
    initGridData(type);
  };

  // 初始化列信息
  const initColumns = ({ type }) => {
    if (type == 'iAskedForIt') {
      columnData.value = [
        { title: '被催办人', field: 'TO_NAME_', width: 130 },
        { title: '类型', field: 'REMINDER_NAME', width: 50 },
        { title: '催办方式', field: 'REMINDER_METHOD_NAME', width: 150 },
        { title: '催办时间', field: 'CREATE_TIME_', width: 150 },
        { title: '内容', field: 'CONTENT_', minWidth: 200 },
      ];
    } else {
      columnData.value = [
        { title: '催办人', field: 'FROM_NAME_', width: 130 },
        { title: '类型', field: 'REMINDER_NAME', width: 50 },
        { title: '催办方式', field: 'REMINDER_METHOD_NAME', width: 150 },
        { title: '催办时间', field: 'CREATE_TIME_', width: 150 },
        { title: '内容', field: 'CONTENT_', minWidth: 200 },
      ];
    }
  };

  // 初始化列表数据
  const initGridData = (type) => {
    loading.value = true;
    $workflow.api
      .getUrgeDataApi({
        type,
        piid: props.taskOperationParams.piid,
        taskId: props.taskOperationParams.taskId,
        currentNodeId: props.taskOperationParams.currentNodeId,
        limit: -1,
      })
      .then((data) => {
        gridStore.loadData(data.rows);
        loading.value = false;
      });
  };
  watch(
    () => props.type,
    (newVal) => {
      if (newVal == props.typeKey) {
        initColumns({ type: newVal });
        const type = newVal == 'iAskedForIt' ? 'FROM_' : 'TO_';
        initGridData(type);
      }
    },
    { immediate: true },
  );
  return { loading, gridStore, columnData, onPageChange };
}
