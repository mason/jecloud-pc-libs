import { ref } from 'vue';
import { Modal } from '@jecloud/ui';
/**
 * 切换任务
 * @param {*} param0
 */
export function showTaskChange({ data, button, $func }) {
  const { currentTaskId, tasks } = data;
  const { $workflow } = $func;
  let activeTask = ref(currentTaskId);
  Modal.dialog({
    title: '切换任务',
    icon: button.icon,
    width: 340,
    height: 540,
    content: () => {
      return (
        <div class="je-workflow-task-change" style="border:1px solid #e6e6e6;height:100%">
          {tasks.map((task) => {
            const { workflowConfig } = task;
            return (
              <div
                class={{
                  'task-item': true,
                  'is--active': workflowConfig.currentNode.id === activeTask.value,
                }}
                onClick={() => {
                  activeTask.value = workflowConfig.currentNode.id;
                }}
              >
                {workflowConfig.currentNode.name}
              </div>
            );
          })}
        </div>
      );
    },
    okButton() {
      const task = tasks.find((item) => item.workflowConfig.currentNode.id === activeTask.value);
      $workflow.changeTask(task);
      // 切换节点要刷新表单状态
      $workflow.refreshForm();
      Modal.notice(`切换【${task.workflowConfig.currentNode.name}】成功！`, 'success');
    },
  });
}
