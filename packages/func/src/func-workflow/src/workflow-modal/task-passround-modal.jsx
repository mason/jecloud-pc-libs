import { ref, reactive, watch } from 'vue';
import { getBodyHeight, isEmpty } from '@jecloud/utils';
import { Modal, Tabs, Panel } from '@jecloud/ui';
import WorkflowOperationComment from '../workflow-operation/src/operation-comment';
import WorkflowUser from '../workflow-operation/src/operation-user';
import WorkflowCommonUser from '../workflow-operation/src/operation-common-user';
import { WorkflowOperatorEnum } from '../workflow-manager';
/**
 * 传阅操作
 * @param {*} param0
 */
export function showTaskPassRound({ data, button, $func }) {
  const { $workflow } = $func;
  const workflowUser = ref();
  const userConfig = reactive({
    activeTypeId: '',
    types: [],
    users: [],
    resultUsers: [],
  });
  // 加载传阅人员信息
  if (data.passRoundUsers.length > 0) {
    userConfig.types = data.passRoundUsers;
    userConfig.activeTypeId = data.passRoundUsers[0].assignmentConfigType;
    userConfig.users = data.passRoundUsers[0].user;
  }
  // 常用人员组件选择人
  const selectUser = ({ row }) => {
    workflowUser.value.selectHistoryUser({ userId: row.userId });
  };
  Modal.dialog({
    title: button.name,
    icon: button.icon,
    width: 790,
    minWidth: 790,
    class: 'je-workflow-task-passround',
    height: getBodyHeight() - 100,
    headerStyle: { height: '50px' },
    content: () => {
      return (
        <Panel>
          {userConfig.types.length > 1 ? (
            <Panel.Item region="tbar">
              <div class="task-user-type">
                <div class="task-user-type-label">筛选条件：</div>
                <Tabs
                  class="task-user-type-items"
                  type="card"
                  v-model:activeKey={userConfig.activeTypeId}
                >
                  {userConfig.types.map((item) => {
                    return (
                      <Tabs.TabPane
                        class={{
                          'task-user-type-items-item': true,
                          'is--active': item.assignmentConfigType === userConfig.activeTypeId,
                        }}
                        key={item.assignmentConfigType}
                        tab={item.assignmentConfigTypeName}
                      />
                    );
                  })}
                </Tabs>
              </div>
            </Panel.Item>
          ) : null}
          <Panel.Item>
            <WorkflowUser
              funcObj={$func}
              ref={workflowUser}
              draggable={false}
              userDisabled={true}
              multiple={true}
              users={userConfig.users}
              v-model:result={userConfig.resultUsers}
            ></WorkflowUser>
          </Panel.Item>
          <Panel.Item region="bbar" style="height:35px">
            <WorkflowCommonUser onSelectUser={selectUser} />
          </Panel.Item>
        </Panel>
      );
    },
    okButton: {
      text: '传阅',
      closable: false,
      handler({ $modal, button }) {
        button.loading = true;
        // 获得选中的人员信息
        if (userConfig.resultUsers.length > 0) {
          $workflow.action.doTaskPassRoundSubmit({ users: userConfig.resultUsers }).finally(() => {
            button.loading = false;
            $modal.close();
          });
        } else {
          button.loading = false;
          Modal.alert('请选择人员信息！', Modal.status.warning);
        }
      },
    },
  });
  // 监听筛选条件值,刷新人员选择数据
  watch(
    () => userConfig.activeTypeId,
    (newVal) => {
      if (newVal && workflowUser.value) {
        const activeType = userConfig.types.find((item) => item.assignmentConfigType === newVal);
        workflowUser.value.loadUser(activeType.user);
      }
    },
  );
}
/**
 * 审阅，撤回操作
 * @param {*} param0
 */
export function showTaskCheckApprove({ button, $func }) {
  const taskComment = ref('已阅');
  // 撤回操作默认意见
  if (button?.operationId == WorkflowOperatorEnum.TASK_WITH_DRAW_OPERATOR) {
    taskComment.value = '任务撤回';
  }
  const { $workflow } = $func;
  Modal.dialog({
    title: button.name,
    icon: button.icon,
    width: 790,
    minWidth: 790,
    height: getBodyHeight() - 200,
    class: 'je-workflow-task-checkapprove',
    headerStyle: { height: '50px' },
    content: () => {
      return (
        <WorkflowOperationComment
          v-model:taskComment={taskComment.value}
        ></WorkflowOperationComment>
      );
    },
    buttons: [
      {
        text: button.name,
        type: 'primary',
        handler: ({ $modal, button }) => {
          button.loading = true;
          if (isEmpty(taskComment.value)) {
            Modal.alert('请填写审批意见！', 'warning');
            return (button.loading = false);
          }

          $workflow.action
            .doButtonSubmit({
              params: {
                comment: taskComment.value,
              },
            })
            .finally(() => {
              button.loading = false;
              $modal.close();
            });
        },
      },
    ],
  });
}
