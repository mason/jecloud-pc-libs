import JeWorkflowUser from '../workflow-operation/src/operation-user';

import { withInstall } from '../../../utils';
export const WorkflowUser = withInstall(JeWorkflowUser);
export default WorkflowUser;
