import { defineComponent, nextTick, onMounted, ref } from 'vue';
import { Grid, Input, Select, Switch, InputSelect, Button, Panel } from '@jecloud/ui';
import { useFuncQueys } from './hooks/use-func-querys';
import { isNotEmpty } from '@jecloud/utils';

export default defineComponent({
  name: 'JeFuncQuerys',
  components: { Grid, Input, Select, Switch, InputSelect, Button },
  inheritAttrs: false,
  props: {
    queryParams: Object,
  },
  emits: ['rendered'],
  setup(props, context) {
    const { expose } = context;
    const textareaHeight = ref();
    const bottomPanel = ref(null);
    const {
      state,
      addColumn,
      deleteColumn,
      getSelectLabel,
      getPopupContainer,
      disposeSql,
      codeSwitching,
      okHandler,
    } = useFuncQueys({
      props,
      context,
    });
    onMounted(() => {
      nextTick(() => {
        textareaHeight.value = bottomPanel.value.$el.offsetHeight - 400;
      });
    });
    expose({ state, okHandler });
    return () => (
      <Panel class="func-queys" ref={bottomPanel}>
        <Panel.Item region="top">
          <div class="prompt">
            <p class="p">{state.contextText}</p>
          </div>
        </Panel.Item>
        <Panel.Item region="default">
          <Grid
            class="table"
            border
            show-overflow
            size="mini"
            keep-source
            sync-resize
            auto-resize
            height="180px"
            header-align="center"
            edit-config={{
              trigger: 'click',
              mode: 'cell',
            }}
            store={state.tableData}
          >
            <Grid.Column
              width="40"
              align="center"
              v-slots={{
                header() {
                  return (
                    <i
                      style="font-size: 16px; cursor: pointer"
                      class="fal fa-plus"
                      onClick={() => addColumn()}
                    ></i>
                  );
                },
                default({ row }) {
                  return (
                    <i
                      style="font-size: 16px; cursor: pointer"
                      class="fal fa-times"
                      onClick={() => deleteColumn(row)}
                    ></i>
                  );
                },
              }}
            ></Grid.Column>
            <Grid.Column
              field="cn"
              title="逻辑运算符"
              width="120"
              align="center"
              edit-render={{}}
              v-slots={{
                edit({ row }) {
                  return (
                    <Select v-model:value={row.cn} allow-clear>
                      {state.SQLFILTER_CN_OPTIONS.map((item, index) => {
                        return (
                          <Select.Option key={index} value={item.value}>
                            {item.label}
                          </Select.Option>
                        );
                      })}
                    </Select>
                  );
                },
                default({ row }) {
                  return getSelectLabel(row.cn, state.SQLFILTER_CN_OPTIONS);
                },
              }}
            ></Grid.Column>
            <Grid.Column
              field="leftvalue"
              title="左括号"
              width="120"
              align="center"
              v-slots={{
                default({ row }) {
                  return <Switch v-model:value={row.leftvalue} mode="Checkbox" />;
                },
              }}
            ></Grid.Column>
            <Grid.Column
              field="code"
              title="查询项"
              align="center"
              min-width="120"
              edit-render={state.inputRender}
              v-slots={{
                edit({ row }) {
                  return isNotEmpty(props.queryParams.funcId) ||
                    isNotEmpty(props.queryParams.tableId) ? (
                    <InputSelect.Grid
                      v-model:value={row.code}
                      name="code"
                      editable
                      config-info={state.configInfo}
                      orders={state.orders}
                      querys={state.querys}
                    ></InputSelect.Grid>
                  ) : (
                    <Input name="code" v-model:value={row.code}></Input>
                  );
                },
              }}
            ></Grid.Column>
            <Grid.Column
              field="type"
              title="查询类型"
              width="120"
              align="center"
              edit-render={{}}
              v-slots={{
                edit({ row }) {
                  return (
                    <Select
                      v-model:value={row.type}
                      allow-clear
                      get-popup-container={getPopupContainer}
                    >
                      {state.SQLFILTER_TYPE_OPTIONS.map((item, index) => {
                        return (
                          <Select.Option key={index} value={item.value}>
                            {item.label}
                          </Select.Option>
                        );
                      })}
                    </Select>
                  );
                },
                default({ row }) {
                  return getSelectLabel(row.type, state.SQLFILTER_TYPE_OPTIONS);
                },
              }}
            ></Grid.Column>
            <Grid.Column
              field="value"
              title="查询条件"
              align="center"
              min-width="200"
              edit-render={state.inputRender}
              v-slots={{
                edit({ row }) {
                  return <Input v-model:value={row.value}></Input>;
                },
              }}
            ></Grid.Column>
            <Grid.Column
              field="rightvalue"
              title="右括号"
              width="120"
              align="center"
              v-slots={{
                default({ row }) {
                  return <Switch v-model:value={row.rightvalue} mode="Checkbox"></Switch>;
                },
              }}
            ></Grid.Column>
          </Grid>
        </Panel.Item>
        <Panel.Item region="bottom" style="top: 265px">
          <div class="sqlClcik">
            <Button
              type="text"
              onClick={() => disposeSql(false)}
              v-slots={{
                icon() {
                  return <i class="fal fa-angle-double-up"></i>;
                },
              }}
            >
              导入查询配置
            </Button>
            <Button
              type="text"
              style="margin-left: 15px"
              onClick={() => disposeSql(true)}
              v-slots={{
                icon() {
                  return <i class="fal fa-angle-double-down"></i>;
                },
              }}
            >
              生成查询配置
            </Button>
          </div>

          <Input.TextArea
            v-model:value={state.textareaValue}
            class="textarea"
            rows={4}
            allow-clear
            style={{ height: textareaHeight.value + 'px' }}
          ></Input.TextArea>

          <div class="codeFormat">
            <Button type="text" onClick={() => codeSwitching(false)}>
              压缩代码
            </Button>
            <Button type="text" style="margin-left: 15px" onClick={() => codeSwitching(true)}>
              格式化代码
            </Button>
          </div>
        </Panel.Item>
      </Panel>
    );
  },
});
