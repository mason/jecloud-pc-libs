import { defineComponent } from 'vue';
import { Dropdown, Menu, Button, Qrcode } from '@jecloud/ui';
import { cookie } from '@jecloud/utils';
export default defineComponent({
  name: 'JeFuncConfigMenu',
  props: {
    menus: Array,
    funcCode: String,
  },
  slots: ['title'],
  emits: ['click'],
  setup(props, { emit }) {
    const clickMenu = (options) => {
      emit('click', options);
    };
    // 获取扫码地址
    const getQrcodeUrl = () => {
      const funcCode = props.funcCode;
      const token = cookie.get('authorization');
      return `${window.location.origin}/app/micro/func/#/pages/func/data/${funcCode}?authorization=${token}`;
    };
    return () => (
      <Dropdown
        trigger={['click']}
        placement="bottomRight"
        v-slots={{
          overlay: () => (
            <Menu
              onClick={clickMenu}
              class={{ 'je-func-config-menu': true, 'is-empty': props.menus?.length == 0 }}
            >
              {props.menus.map((menu) => {
                return [
                  <div style={{ background: menu.background }} class="title">
                    {menu.text}
                  </div>,
                  <div class="child-container">
                    {menu.layout == 'flex' ? (
                      <Qrcode size="65" value={getQrcodeUrl()} class="qrcode-value"></Qrcode>
                    ) : null}
                    <div class={menu.layout == 'flex' ? 'child-content-app' : 'child-content'}>
                      {menu.children?.map((item) => {
                        return (
                          <Menu.Item
                            class="child-item"
                            key={item.key}
                            icon={item.icon}
                            iconColor={item.color}
                          >
                            {item.text}
                          </Menu.Item>
                        );
                      })}
                    </div>
                  </div>,
                ];
              })}
              <div class="refresh-button">
                <Button
                  icon="jeicon jeicon-refresh"
                  onClick={() => {
                    clickMenu({ key: 'FuncRefresh' });
                  }}
                >
                  刷新
                </Button>
              </div>
            </Menu>
          ),
        }}
      >
        <Button icon="fal fa-cog" class="bgcolor-grey"></Button>
      </Dropdown>
    );
  },
});
