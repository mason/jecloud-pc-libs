import { defineComponent, ref, onMounted, watch, nextTick } from 'vue';
import { Tree, Data } from '@jecloud/ui';
import { isNotEmpty, cloneDeep, encode, isEmpty, pinyin, split } from '@jecloud/utils';
import { loadUserPersoninfoApi, findAsyncUsers } from '../../../../func-manager';

export default defineComponent({
  name: 'JeFuncSelectUserTree',
  inheritAttrs: false,
  props: {
    activeKey: String,
    multipleType: String,
    unCheckDatas: Object,
    checkedUser: Object,
    panelType: String,
    showDeveloper: String,
    querys: Array,
    async: Boolean,
  },
  emits: ['changeSelectData', 'changeTabActiveKey'],
  setup(props, context) {
    const { slots, attrs, emit, expose } = context;
    const $treeView = ref();
    const treeStore = Data.Store.useTreeStore({
      data: [],
    });
    const loading = ref(true);
    const treeConfig = ref({
      rowField: 'id',
      parentField: 'parentId',
      checkbox: true,
      textField: 'text',
    });
    // 取消选中节点
    const setCheckedNode = (data, checked) => {
      if (checked) {
        $treeView.value.setAllCheckboxRow(false);
      }
      let selectNodes = [];
      if (props.activeKey == 'role') {
        data.forEach((item) => {
          treeStore.cascade((node) => {
            if (node.nodeInfoType == 'user' && node.bean?.JE_RBAC_ACCOUNTDEPT_ID == item.id) {
              selectNodes.push(node);
            }
          }, treeStore.data);
        });
      } else {
        data.forEach((item) => {
          const nodeData = $treeView.value.getRowById(item.id);
          selectNodes.push(nodeData);
        });
      }
      $treeView.value.setSelectRow(selectNodes, checked);
    };

    //tree数据加载
    const loadTreeData = (key) => {
      loading.value = true;
      let type = 'commonUser';
      const params = {};
      if (key == 'role') {
        type = 'roleUser';
      } else if (key == 'dept') {
        type = 'deptUser';
        params.async = props.async || false;
        params.node = 'root';
      } else if (key == 'organ') {
        type = 'orgUser';
        //根据配置项判断是否加载岗位开发者
        if (props.showDeveloper == '1') {
          params.showDeveloper = '1';
        }
      }
      params.type = type;
      if (isNotEmpty(props.querys)) {
        params.j_query = encode(props.querys);
      }
      loadUserPersoninfoApi({ params }).then((data) => {
        treeStore.loadData(data.children);
        loading.value = false;
        // 如果常用人员没有数据就默认切换到部门
        // if (params.type == 'commonUser' && data.children.length <= 0) {
        //   emit('changeTabActiveKey', 'dept');
        // }
        //选中数据
        nextTick(() => {
          setTimeout(() => {
            setCheckedNode(props.checkedUser, true);
          }, 500);
        });
      });
    };

    onMounted(() => {
      loadTreeData(props.activeKey);
    });

    //树选中事件 {checked,records,row}
    const handlerSelect = ({ records, checked = true, row, newValue, selected }) => {
      const data = [];
      //如果是多选
      if (['M', 'M_'].includes(props.multipleType)) {
        const getUserTreeNode = (nodeData) => {
          if (nodeData.nodeInfoType == 'user') {
            data.push(nodeData);
          }
          if (nodeData.children && nodeData.children.length > 0) {
            nodeData.children.forEach((item) => {
              getUserTreeNode(item);
            });
          }
        };
        getUserTreeNode(cloneDeep(row));
        //}

        // 数据去重
        const newData = [];
        const nodeDatas = cloneDeep(data);
        nodeDatas.forEach((item) => {
          if (!newData.some((newItem) => newItem.id == item.id) || newData.length <= 0) {
            // 如果在角色tab页就需要替换id
            if (props.activeKey == 'role') {
              //item.oldId = item.id;
              item.id = item.bean.JE_RBAC_ACCOUNTDEPT_ID;
            }
            newData.push(item);
          }
        });

        // 如果是角色,需要级联操作相同的人
        /* if (props.activeKey == 'role') {
          const selectNodes = [];
          newData.forEach((item) => {
            treeStore.cascade((node) => {
              if (
                node.nodeInfoType == 'user' &&
                node.bean?.JE_RBAC_ACCOUNTDEPT_ID == item.id &&
                node.id != item?.oldId
              ) {
                selectNodes.push(node);
              }
            }, treeStore.data);
          });
          setTimeout(() => {
            $treeView.value.setSelectRow(selectNodes, checked);
          }, 1000);
        } */

        emit('changeSelectData', { checked, data: newData });
      } else {
        //如果是单选
        if (isNotEmpty(newValue) || selected) {
          checked = true;
          if (row.nodeInfoType == 'user') {
            data.push(row);
          }
        } else {
          checked = false;
          if (row.nodeInfoType == 'user') {
            data.push(row);
          }
        }
        if (data.length > 0) {
          // 如果在角色tab页就需要替换id
          const nodeDatas = cloneDeep(data);
          if (props.activeKey == 'role') {
            nodeDatas.forEach((item) => {
              item.id = item.bean.JE_RBAC_ACCOUNTDEPT_ID;
            });
          }
          emit('changeSelectData', { checked, data: nodeDatas });
        }
      }
    };

    //左侧树搜索选中
    const doSearchSelect = ({ row }) => {
      const selectNodeData = [];
      //如果是多选
      if (['M', 'M_'].includes(props.multipleType)) {
        const getUserTreeNode = (nodeData) => {
          if (nodeData.nodeInfoType == 'user') {
            selectNodeData.push(nodeData);
          }
          if (nodeData.children.length > 0) {
            nodeData.children.forEach((item) => {
              getUserTreeNode(item);
            });
          }
        };
        getUserTreeNode(row);
      } else {
        //如果是单选
        if (row.nodeInfoType == 'user') {
          selectNodeData.push(row);
        }
      }
      //触发选择方法
      if (selectNodeData.length > 0) {
        // 如果在角色tab页就需要替换id
        const nodeDatas = cloneDeep(selectNodeData);
        if (props.activeKey == 'role') {
          nodeDatas.forEach((item) => {
            item.id = item.bean.JE_RBAC_ACCOUNTDEPT_ID;
          });
        }
        emit('changeSelectData', { checked: true, data: nodeDatas });
      }
    };
    // 异步查询节点
    const loadAsyncNode = (record) => {
      const params = {
        node: record.id.split('_')[0],
        type: 'deptUser',
        async: true,
      };
      record.loaded = true;

      return loadUserPersoninfoApi({ params }).then((data) => {
        const nodeData = data.children.map((item) => {
          return treeStore._addCache(item);
        });
        // 回显
        setTimeout(() => {
          setCheckedNode(props.checkedUser, true);
        }, 500);
        return nodeData;
      });

      /* return loadUserPersoninfoApi({ params }).then((data) => {
        const newNodes = data.children.map((item) => {
          // 是否可以展开属性
          item.hasChild = !item.leaf;
          return item;
        });
        debugger;
        treeStore.appendChild(newNodes, record);

        /*  const selected = treeStore.getSelectedRecords()[0];
        const row = treeStore.appendChild(newNodes, selected);
        $treeView.value.setTreeExpand(selected, true); */
      /* nextTick(() => {
          setTimeout(() => {
            setCheckedNode(props.checkedUser, true);
          }, 500);
        }); */
      // }); */
    };

    //取消选中的节点
    watch(
      () => props.unCheckDatas,
      (newData) => {
        if (newData && newData.length > 0 && props.panelType == props.activeKey) {
          setCheckedNode(newData, false);
        }
      },
      /*  { deep: true }, */
    );

    //激活的面板切换tree节点选中
    watch(
      () => props.activeKey,
      (newVale) => {
        // 部门数支持异步
        if (newVale == 'dept' && props.async) {
          treeConfig.value = {
            rowField: 'id',
            parentField: 'parentId',
            checkbox: true,
            textField: 'text',
            lazy: props.async,
            loadMethod({ row }) {
              return loadAsyncNode(row);
            },
          };
        }
        loadTreeData(props.activeKey);

        /*  if (props.checkedUser) {
          if (props.panelType == newVale) {
            setCheckedNode(props.checkedUser, true);
          }
        } */
      },
      { deep: true },
    );
    /**
     * 解析树形顶部查询项的值
     * @param {*} item
     * @returns
     */
    const parseTreeSearchItem = (item, async) => {
      item.async = async;
      if (!item.id || !item.nodeInfo) return item;
      // 处理节点路径，拼接nodeInfo
      const ids = split(item.nodePath, '/').filter((id) => id);
      const path = ids.map((id) => `${id}_${item.nodeInfo}`).join('/');

      return {
        id: item.id,
        text: item.text,
        code: item.code,
        value: item.text,
        async: item.async,
        nodePath: path,
      };
    };
    const onSearch = (keyword) => {
      if (isEmpty(keyword)) {
        return Promise.resolve([]);
      } else if (props.async && props.activeKey == 'dept') {
        // 异步查询
        return findAsyncUsers({
          searchName: keyword,
        }).then((data = []) => {
          const nodeDatas = data.map((item) => {
            return parseTreeSearchItem(item, true);
          });
          return nodeDatas;
        });
      } else {
        // 同步查询
        const items = [];
        keyword = keyword.toLocaleLowerCase();
        treeStore.cascade((item) => {
          const code = item.code?.toString().toLocaleLowerCase() ?? '';
          const text = item.text?.toString().toLocaleLowerCase() ?? '';
          const pinyinText = pinyin(text);
          if (code.includes(keyword) || text.includes(keyword) || pinyinText.includes(keyword)) {
            items.push(parseTreeSearchItem(item, false));
          }
        });
        return Promise.resolve(items);
      }
    };
    /**
     * 单选情况下双击子节点可关闭弹窗
     * @param {*} param0
     */
    const onTreeCellDblclick = ({ row }) => {
      if (!['M', 'M_'].includes(props.multipleType) && row?.nodeInfoType == 'user') {
        // 保存数据关闭弹窗
        emit('changeSelectData', { checked: true, data: [row], closeModal: true });
      }
    };
    return () =>
      loading.value ? (
        <div v-loading={loading.value} />
      ) : (
        <Tree
          ref={$treeView}
          class="je-func-select-user-tree"
          height="100%"
          size="mini"
          tree-config={treeConfig.value}
          store={treeStore}
          multiple={['M', 'M_'].includes(props.multipleType)}
          search={(keyword, resolve) => {
            onSearch(keyword).then(resolve);
          }}
          onSelectionChange={handlerSelect}
          onCellDblclick={onTreeCellDblclick}
          onSearchSelect={doSearchSelect}
          v-slots={{
            nodeDefault({ row }) {
              return props.activeKey == 'common' ? (
                <div class="node-context">
                  {row.text}
                  <span class="node-deptname">{row.bean.DEPARTMENT_NAME}</span>
                </div>
              ) : (
                <div>{row.text}</div>
              );
            },
          }}
        ></Tree>
      );
  },
});
