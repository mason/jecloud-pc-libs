import { defineComponent, watch, ref, onMounted, nextTick, h } from 'vue';
import { Panel, Button, Grid, Input } from '@jecloud/ui';
import { isNotEmpty, encode, decode, getFileUrlByKey } from '@jecloud/utils';
import Sortable from 'sortablejs';

export default defineComponent({
  name: 'JeFuncSelectUserResult',
  inheritAttrs: false,
  props: { selectData: Object },
  emits: ['uncheckUser', 'getSelectData'],
  setup(props, context) {
    const { slots, attrs, emit, expose } = context;
    const displayMode = ref('menu');
    const grid = ref();
    //列表的数据源store
    const selectUserData = ref(props.selectData.value);
    const searchVal = ref('');
    const gridStore = ref(props.selectData.value);
    const jeFuncSelectUserResultContext = ref();

    watch(
      () => props.selectData.value,
      (newVal) => {
        if (newVal) {
          let data = newVal;
          gridStore.value = data;
          selectUserData.value = data;
          //onSearch();
        }
      },
      { deep: true },
    );

    //删除用户数据
    const deleteUser = (data) => {
      if (data == 'all') {
        emit('uncheckUser', selectUserData.value, []);
        selectUserData.value = [];
        gridStore.value = [];
      } else {
        const arrayData = decode(encode(gridStore.value));
        const deleteData = arrayData.splice(data, 1);
        gridStore.value = arrayData;
        emit('getSelectData', { checked: false, data: deleteData });
      }
    };

    //搜索
    const onSearch = () => {
      if (isNotEmpty(searchVal.value)) {
        const newData = [];
        selectUserData.value.forEach((item) => {
          if (item.text.indexOf(searchVal.value) != -1) {
            newData.push(item);
          }
        });
        gridStore.value = newData;
      } else {
        gridStore.value = selectUserData.value;
      }
    };

    //列表拖拽方法
    const onDrop = ({ $table, row, type, placeNum }) => {
      let fullData = [];
      //如果是菜单模式
      if (type == 'menu') {
        fullData = gridStore.value.filter((item) => {
          return item.id != row.id;
        });
        fullData.splice(placeNum, 0, row);
        //如果是列表模式
      } else {
        fullData = $table.getTableData().fullData;
      }
      gridStore.value = fullData;
      let nextRow = {};
      //拖拽节点的相对位置
      let place = 'top';
      fullData.forEach((item, index) => {
        if (item.id == row.id) {
          //如果拖拽的节点就是数据的第一位,那么它的目标节点就是下一个节点
          if (index == 0) {
            nextRow = fullData[index + 1];
            place = 'bottom';
            //否则目标节点就是上一个节点
          } else {
            nextRow = fullData[index - 1];
          }
        }
      });
      if (isNotEmpty(nextRow)) {
        //过滤都主数据的拖拽节点数据
        const arrayData = selectUserData.value.filter((item) => {
          return item.id != row.id;
        });
        //开始把拖拽节点放入主数据中
        let iNum = 0;
        //找到目标节点的位置
        arrayData.forEach((item, index) => {
          if (item.id == nextRow.id) {
            iNum = index;
          }
        });
        //如果目标节点在第一位,并且相对位置不是在上面
        if (iNum == 0 && place != 'top') {
          arrayData.unshift(row);
          //如果目标节点在最后位,并且相对位置不是在下面
        } else if (iNum == arrayData.length - 1 && place != 'bottom') {
          arrayData.push(row);
        } else {
          if (place == 'top') {
            arrayData.splice(iNum + 1, 0, row);
          } else {
            arrayData.splice(iNum, 0, row);
          }
        }
        selectUserData.value = arrayData;

        emit('getSelectData', { checked: 'drop', data: arrayData });
      }
    };

    //初始化拖拽组件
    onMounted(() => {
      nextTick(() => {
        setTimeout(() => {
          let ops2 = {
            animation: 300,
            delay: 0,
            sort: true,
            forceFallback: true, // 不使用H5原生拖动
            // 拖拽元素改变位置的时候
            fallbackClas: true,
            onEnd: function (evt) {
              if (evt.oldIndex != evt.newIndex) {
                const id = evt.item.getAttribute('data-rowid');
                const row = gridStore.value.filter((item) => {
                  return item.id == id;
                });
                onDrop({ type: 'menu', row: row[0], placeNum: evt.newIndex });
              }
            },
          };
          Sortable.create(jeFuncSelectUserResultContext.value, ops2);
        }, 100);
      });
    });

    //获得用户头像
    const getUserPhoto = (row) => {
      let userPhotoHtml = '';
      if (isNotEmpty(row.bean.USER_AVATAR)) {
        const fileKey = decode(row.bean.USER_AVATAR)[0].fileKey;
        const imgSrc = getFileUrlByKey(fileKey);
        userPhotoHtml = h('img', { src: imgSrc, class: 'userphoto-img' });
      } else {
        userPhotoHtml =
          row.text && h('span', { textContent: row.text.slice(-2), class: 'userphoto-span' });
      }
      return userPhotoHtml;
    };

    return () => (
      <Panel class="je-func-select-user-result">
        <Panel.Item region="top" size="40">
          <div class="top-panel">
            <Input.Search
              v-model:value={searchVal.value}
              allow-clear
              style="width:100%"
              placeholder="请输入关键字..."
              class="default-search"
              onSearch={onSearch}
              v-slots={{
                prefix() {
                  return <i class="fal fa-search" />;
                },
              }}
            ></Input.Search>
            <div class="div-icon">
              <i
                class={['icon fas fa-list-ul default', displayMode.value == 'list' ? 'select' : '']}
                onClick={() => {
                  displayMode.value = 'list';
                }}
              ></i>
              <i
                class={['icon jeicon jeicon-work1', displayMode.value == 'menu' ? 'select' : '']}
                onClick={() => {
                  displayMode.value = 'menu';
                }}
              ></i>
            </div>
          </div>
        </Panel.Item>
        <Panel.Item>
          <div
            class="context-panel"
            ref={jeFuncSelectUserResultContext}
            style={[
              { display: displayMode.value == 'menu' ? 'block' : 'none' },
              'width:100%',
              'height:100%',
            ]}
          >
            {gridStore.value.map((item, index) => {
              return (
                <div class="user-item" data-rowid={item.id} key={item.id}>
                  <span>{item.text}</span>
                  <i class="icon fal fa-times" onClick={() => deleteUser(index)}></i>
                </div>
              );
            })}
          </div>
          <Grid
            style={{ display: displayMode.value == 'list' ? 'block' : 'none' }}
            ref={grid}
            auto-resize
            show-overflow="ellipsis"
            class="context-grid"
            keep-source
            show-header-overflow
            resizable
            size="medium"
            height="100%"
            data={gridStore.value}
            border="0"
            align="center"
            draggable
            onDrop={onDrop}
          >
            <Grid.Column
              field="photo"
              title=""
              width="80"
              v-slots={{
                default({ row }) {
                  return <span class="je-user-photo">{getUserPhoto(row)}</span>;
                },
              }}
            ></Grid.Column>
            <Grid.Column field="text" title="名称" width="80"></Grid.Column>
            <Grid.Column field="code" title="账号" min-width="120"></Grid.Column>
            <Grid.Column
              field="deptName"
              title="部门"
              min-width="120"
              v-slots={{
                default({ row }) {
                  return <span>{row.bean.DEPARTMENT_NAME}</span>;
                },
              }}
            ></Grid.Column>
            <Grid.Column
              title="操作"
              width="74"
              v-slots={{
                default({ rowIndex }) {
                  return (
                    <span onClick={() => deleteUser(rowIndex)} class="delete-span">
                      删除
                    </span>
                  );
                },
              }}
            ></Grid.Column>
          </Grid>
        </Panel.Item>
        <Panel.Item region="bottom" size="32">
          <div class="bottom-panel">
            <span>已选{gridStore.value.length}</span>
            <Button type="link" style="float:right" onClick={() => deleteUser('all')}>
              清空
            </Button>
          </div>
        </Panel.Item>
      </Panel>
    );
  },
});
