import { ref, h } from 'vue';
import { Modal } from '@jecloud/ui';
import { useUserSelect } from './hooks/use-renderer';
import { getBodyHeight, getBodyWidth } from '@jecloud/utils';

/**
 *
 * @param {Object} options
 * @param {String} configInfo 配置项
 * @param {String|Number} width 宽
 * @param {String|Number} height 高
 */

export function userSelect(options) {
  const {
    width = getBodyWidth() > 1000 ? 1000 : '100%',
    height = getBodyHeight() > 700 ? 700 : '100%',
    title = '人员选择',
    model,
  } = options;
  const modal = ref();
  const { pluginSlot, modalOptions } = useUserSelect({
    selectOptions: options,
    modal: modal,
    model,
  });

  modal.value = Modal.window({
    title,
    width,
    height,
    resize: false,
    content: () => pluginSlot(),
    ...modalOptions,
    buttonAlign: 'right',
    className: 'je-func-select-window',
  });
  return modal.value;
}
