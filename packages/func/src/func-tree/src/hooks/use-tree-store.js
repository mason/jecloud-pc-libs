import { useFuncTreeStore } from '../../../func-manager';
export function useTreeStore({
  type,
  url,
  params,
  querys,
  dictionarys,
  product,
  onlyItem,
  $func,
  $tree,
}) {
  return useFuncTreeStore({
    type,
    url,
    params,
    querys,
    dictionarys,
    product,
    onlyItem,
    $func,
    $tree,
  });
}
