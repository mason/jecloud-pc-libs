import { useBase } from './use-base';
import { useTreeStore } from './use-tree-store';
import { FuncTreeTypeEnum } from '../../../func-manager/enum';
import { useInjectFunc } from '../../../func-manager';
/**
 * 字典树
 */
export function useTreeDD({ props, context }) {
  const $func = useInjectFunc();
  const { $tree, treeSlot } = useBase({ props, context });
  let { store, onlyItem } = props;
  if (!store) {
    store = useTreeStore({
      type: FuncTreeTypeEnum.DD,
      dictionarys: props.dictionarys,
      onlyItem,
      $func,
      $tree,
    });
  }
  $tree.setStore(store);
  $tree.mixin({
    reset() {
      store.load();
    },
  });
  return { $tree, treeSlot };
}
