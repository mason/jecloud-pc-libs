import { useTreeDD } from './use-tree-dd';
import { useTreeFunc } from './use-tree-func';
import { FuncTreeTypeEnum } from '../../../func-manager/enum';
export function useTree({ props, context }) {
  switch (props.type) {
    case FuncTreeTypeEnum.DD:
      return useTreeDD({ props, context });
    case FuncTreeTypeEnum.FUNC:
      return useTreeFunc({ props, context });
  }
}
