import { defineComponent, onMounted, nextTick } from 'vue';
import { useTree } from './hooks/use-tree';
import { FuncTreeTypeEnum } from '../../func-manager/enum';

export default defineComponent({
  name: 'JeFuncTree',
  inheritAttrs: false,
  props: {
    funcCode: String,
    dictionarys: Array,
    treeConfig: Object,
    storeConfig: Object,
    store: Object,
    multiple: { type: Boolean, default: false },
    async: Boolean,
    product: String,
    onlyItem: { type: Boolean, default: true },
    type: {
      type: String,
      default: FuncTreeTypeEnum.DD,
      validator: (value) =>
        [FuncTreeTypeEnum.DD, FuncTreeTypeEnum.FUNC, FuncTreeTypeEnum.CUSTOM].includes(value),
    },
    title: String,
    autoLoad: { type: Boolean, default: true },
    editable: Boolean,
    enableSelectMode: Boolean,
    enableSearchMode: Boolean,
    helpMessage: String,
    querys: Array,
  },
  emits: ['rendered', 'update:multiple'],
  setup(props, context) {
    const { emit } = context;
    const { treeSlot, $tree } = useTree({ props, context });
    onMounted(() => {
      nextTick(() => {
        if (props.autoLoad) {
          $tree.store.load().then(() => {
            emit('rendered', { $tree });
          });
        } else {
          emit('rendered', { $tree });
        }
      });
    });
    return () => treeSlot();
  },
});
