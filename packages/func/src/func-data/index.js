import JeFuncData from './src/func-data';
import JeFuncDataSelect from './src/func-data-select';
import { withInstall } from '../utils';
export { showExportForm } from './src/hooks/plugin/use-export-button';

JeFuncData.installComps = {
  // 注册依赖组件
  Select: { name: JeFuncDataSelect.name, comp: JeFuncDataSelect },
};

export const FuncData = withInstall(JeFuncData);
export default FuncData;
