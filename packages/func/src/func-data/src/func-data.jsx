import { defineComponent, ref } from 'vue';
import { Panel } from '@jecloud/ui';
import { useFuncData } from './hooks/use-func-data';
import { useExtendPanel } from './hooks/use-extend-panel';

export default defineComponent({
  name: 'JeFuncData',
  inheritAttrs: false,
  props: {
    funcCode: String,
    orders: Array,
    querys: Array,
  },
  setup(props, context) {
    const { attrs } = context;
    const outerPanelRef = ref();
    const innerPanelRef = ref();
    // 树形，列表
    const { gridSlot, treeSlot, groupQuerySlot, $func, $grid } = useFuncData({
      props,
      context,
    });
    // 扩展面板
    const { extendPanelInner, extendPanelOuter } = useExtendPanel({
      $func,
      $grid,
      outerPanelRef,
      innerPanelRef,
    });
    return () => (
      <Panel {...attrs} ref={outerPanelRef} class="je-func-data">
        {/* 扩展面板 外部 */}
        {extendPanelOuter()}
        <Panel class="je-func-data-center">
          {/* 树形查询 */}
          {treeSlot()}
          <Panel ref={innerPanelRef}>
            {/* 高级查询 */}
            {groupQuerySlot()}
            {/* 列表 */}
            {gridSlot()}
            {/* 扩展面板 内部 */}
            {extendPanelInner()}
          </Panel>
        </Panel>
      </Panel>
    );
  },
});
