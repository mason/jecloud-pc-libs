import { defineComponent, nextTick, onMounted, ref } from 'vue';
import { useInjectFunc } from '../../func-manager';
import { isFunction } from '@jecloud/utils';
import { Loading, Panel } from '@jecloud/ui';

export default defineComponent({
  name: 'JeFuncDataPanel',
  inheritAttrs: false,
  props: {
    region: String,
    renderer: Function,
    emitter: Number,
  },
  setup(props) {
    const $func = useInjectFunc();
    const $panel = Panel.useInjectPanel();
    const slotRef = ref();
    const rendered = ref(false);
    const getParams = () => ({
      $func,
      $panel,
      $grid: $func.getFuncGrid(),
      region: props.region,
    });
    let rendererSlot;
    onMounted(() => {
      nextTick(() => {
        const args = getParams();
        try {
          // 解析插槽函数
          const result = props.renderer(args);
          if (isFunction(result)) {
            rendererSlot = result;
          }
        } catch (error) {
          error;
        }
        rendered.value = true;
        // 渲染插槽
        nextTick(() => {
          rendererSlot?.(slotRef.value, args)?.then(() => {
            $panel.refreshLayout();
          });
        });
      });
    });

    return () => {
      if (!rendered.value) {
        return <Loading />;
      } else if (rendererSlot) {
        return <div class="je-func-data-extend-micro" ref={slotRef}></div>;
      } else {
        const { renderer, emitter } = props;
        return emitter > 0 && renderer?.(getParams());
      }
    };
  },
});
