import { useFuncDataEvents } from '../../../func-manager';
import { useRenderer } from './use-renderer';
import { useBase } from './use-base';
import { FuncDataTypeEnum } from '../../../func-manager';
export function useFuncData({ props, context }) {
  const type = FuncDataTypeEnum.FUNC;
  const { funcCode } = props;
  const { $func, $tree, $grid, $groupQuery } = useBase({ props, context, type });
  // 功能数据
  const funcData = $func.getFuncData();
  const { gridOptions } = funcData.info;
  // 列表配置
  // 事件
  const {
    onTreeSelectChange,
    onTreeBeforeDrop,
    onTreeDrop,
    onGridCellDblclick,
    onGridCellClick,
    onGridHeaderCellDblclick,
    onGridHeaderCellClick,
  } = useFuncDataEvents({ $func });
  const { bindGridEvents } = $func.action;
  // 列表事件
  const gridEvents = bindGridEvents({
    defaultEvents: {
      'cell-dblclick': onGridCellDblclick,
      'cell-click': onGridCellClick,
      'header-cell-click': onGridHeaderCellClick,
      'header-cell-dblclick': onGridHeaderCellDblclick,
    },
  });
  // 插槽
  const { treeSlot, gridSlot, groupQuerySlot } = useRenderer({
    $func,
    $tree,
    $grid,
    $groupQuery,
    funcCode,
    type,
    gridProps: {
      ...gridEvents,
      querys: props.querys || $func.store.initQuerys,
      orders: props.orders || $func.store.initOrders,
      autoLoad: gridOptions.autoLoad,
    },
    treeProps: {
      onSelectionChange: onTreeSelectChange,
      onBeforeDrop: onTreeBeforeDrop,
      onDrop: onTreeDrop,
    },
  });

  return { treeSlot, gridSlot, groupQuerySlot, $func, $tree, $grid };
}
