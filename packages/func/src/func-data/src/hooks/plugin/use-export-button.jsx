import { reactive, ref } from 'vue';
import { Form, Input, Modal, Radio } from '@jecloud/ui';
import {
  dateFormat,
  excelExport,
  encode,
  isNotEmpty,
  useJE,
  decode,
  getAxiosBaseURL,
} from '@jecloud/utils';
import { loadExportExcelKeyApi, exportGridExcelApi } from '../../../../func-manager';
/**
 * 打开导出表单
 * @param {*} param0
 */
export function showExportForm({ $func, templateCode, customParams, config }) {
  // 文件加载时间
  const webNum = ref(0);
  // 文件的uuid 唯一标识
  const fileUuid = ref('');
  const admin = useJE()?.useAdmin();
  let watchFn = null;
  // 接收webSoclet消息
  if (admin && !isNotEmpty(templateCode)) {
    watchFn = admin?.watchWebSocket(({ busType, content }) => {
      if (busType == 'excelExp' && isNotEmpty(content)) {
        const fileData = decode(content);
        // 失败
        if (!fileData.success) {
          // 关闭定时器
          webNum.value = 0;
          clearInterval(timer);
          // 关闭弹窗
          excelExpModal?.close();
          return Modal.alert('导出异常，请联系管理员！');
        }
        // 是同一个文件
        if (fileUuid.value == fileData?.uuid) {
          // 关闭定时器
          clearInterval(timer);
          webNum.value = 99;
          window.open(`${getAxiosBaseURL()}/je/document/down/${fileData.fileKey}`);
          excelExpModal?.close();
        }
      }
    });
  }

  // 加载附件定时器
  let timer = null;
  const doUpdateNum = () => {
    if (webNum.value < 99) {
      webNum.value++;
    } else {
      // 读数大于99 关闭定时器
      clearInterval(timer);
    }
  };

  const verifyData = [
    {
      required: true,
      message: '该输入项为必填项',
    },
    {
      max: 40,
      message: '不能大于40个字符',
    },
    {
      pattern: '^[^\\\\/*?:"\'<>|\x22]+$',
      message: '不能输入非法字符',
    },
  ];
  const { tableCode, funcId, funcCode, funcName, pkCode, productCode } = $func.getFuncData().info;
  const $grid = $func.getFuncGrid();
  const model = reactive({
    title: config?.title || funcName,
    fileName: config?.fileName || funcName + '_' + dateFormat(new Date(), 'YYYY-MM-DD') + '.xls',
    exportType: config?.exportType || 'NOWPAGE',
  });
  const rulesRef = reactive({
    title: verifyData,
    fileName: verifyData,
  });
  const { validate, validateInfos } = Form.useForm(model, rulesRef);

  const pluginSlot = () => {
    return webNum.value ? (
      <div class="je-export-form-loading">
        <div class="je-export-form-loading-num">{webNum.value + '%'}</div>
        <div class="je-export-form-loading-text">附件加载中...</div>
        <div class="je-select-loading">
          <div class="sk-circle">
            <div class="sk-circle1 sk-child"></div>
            <div class="sk-circle2 sk-child"></div>
            <div class="sk-circle3 sk-child"></div>
            <div class="sk-circle4 sk-child"></div>
            <div class="sk-circle5 sk-child"></div>
            <div class="sk-circle6 sk-child"></div>
            <div class="sk-circle7 sk-child"></div>
            <div class="sk-circle8 sk-child"></div>
            <div class="sk-circle9 sk-child"></div>
            <div class="sk-circle10 sk-child"></div>
            <div class="sk-circle11 sk-child"></div>
            <div class="sk-circle12 sk-child"></div>
          </div>
        </div>
      </div>
    ) : (
      <Form model={model} layout="vertical">
        <Form.Item colon={false} label="标题" name="title" {...validateInfos.title}>
          <Input v-model:value={model.title}></Input>
        </Form.Item>
        <Form.Item colon={false} label="文件名" name="fileName" {...validateInfos.fileName}>
          <Input v-model:value={model.fileName}></Input>
        </Form.Item>
        <Form.Item colon={false} label="数据设定" name="exportType">
          <Radio.Group v-model:value={model.exportType} name="exportType">
            <Radio value="NOWPAGE">当前页</Radio>
            <Radio value="SELECTION">选择数据</Radio>
            <Radio value="ALL">全部</Radio>
          </Radio.Group>
        </Form.Item>
      </Form>
    );
  };

  const excelExpModal = Modal.dialog({
    title: '导出设定',
    width: 460,
    minHeight: 378,
    content: () => pluginSlot(),
    class: 'je-export-modal',
    buttonAlign: 'center',
    okButton: {
      closable: false,
      handler({ $modal, button }) {
        button.loading = true;
        validate()
          .then(() => {
            // 开启定时器 模板导出execl不开启
            if (!isNotEmpty(templateCode)) {
              timer = setInterval(doUpdateNum, 1000);
            }
            doExportExcel({ $modal, button, templateCode, customParams, fileUuid });
          })
          .catch((e) => {
            button.loading = false;
          });
      },
    },
    onClose() {
      // 停止监听，防止重复执行
      watchFn && watchFn();
      clearInterval(timer);
    },
  });

  /**
   * 导出excel
   * @param {*} param0
   * @returns
   */
  const doExportExcel = ({ $modal, button, templateCode, customParams, fileUuid }) => {
    // 封装参数
    const params = {
      title: model.title,
      fileName: model.fileName,
      tableCode,
      funcCode,
      funcId,
      styleType: 'GRID',
      j_query: [],
      pd: productCode,
    };
    //当前页
    const ids = [];
    if (model.exportType == 'NOWPAGE') {
      const gridData = $grid.store.data;
      gridData.forEach((item) => {
        ids.push(item[pkCode]);
      });
    } else if (model.exportType == 'SELECTION') {
      const checkboxRecords = $grid.getSelectedRecords();
      checkboxRecords.forEach((item) => {
        ids.push(item[pkCode]);
      });
    }
    if (ids.length > 0 || model.exportType == 'ALL') {
      if (model.exportType != 'ALL') {
        params.j_query.push({
          code: pkCode,
          type: 'in',
          value: ids,
          cn: 'and',
        });
      } else {
        params.j_query = $grid.store.queryParser.getQuerys();
      }
      // 拼接自定义参数
      if (isNotEmpty(customParams)) {
        Object.assign(params, customParams);
      }
      // 如果有导出模板code
      if (isNotEmpty(templateCode)) {
        params.j_query = encode(params.j_query);
        excelExport(templateCode, params);
        webNum.value = 0;
        clearInterval(timer);
        button.loading = false;
        $modal.close();
      } else {
        // 调用接口导出excel
        loadExportExcelKeyApi({ params: { data: encode(params) } })
          .then((data) => {
            exportGridExcelApi({ params: { key: data }, pd: productCode })
              .then((obj) => {
                // 返回文件的标识
                fileUuid.value = obj;
              })
              .catch((e) => {
                button.loading = false;
                webNum.value = 0;
                clearInterval(timer);
                Modal.alert(e.message, 'error');
              });
          })
          .catch((e) => {
            webNum.value = 0;
            clearInterval(timer);
            button.loading = false;
            Modal.alert(e.message, 'error');
          });
      }
    } else {
      // 没有选中数据
      Modal.alert('没有可导出的数据！', 'warning');
      button.loading = false;
      webNum.value = 0;
      clearInterval(timer);
      return;
    }
  };
}
