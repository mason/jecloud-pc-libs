import { ref } from 'vue';
import {
  FuncRefEnum,
  FuncTypeEnum,
  FuncDataTypeEnum,
  useFuncCommon,
  useInjectFunc,
} from '../../../func-manager';
export function useBase({ props, context, type }) {
  const { expose } = context;
  const { funcCode } = props;
  const $tree = ref();
  const $grid = ref();
  const $groupQuery = ref();
  const $func = useInjectFunc();
  const $funcData = {
    ...useFuncCommon({ props, context, funcCode }),
    getFuncData() {
      return $func.getFuncData();
    },
  };
  // 对外暴露对象
  expose($funcData);
  const funcData = $func.getFuncData();
  // 绑定功能ref
  $func.setRefMaps(FuncRefEnum.FUNC_GRID, $grid);
  $func.setRefMaps(FuncRefEnum.FUNC_QUERY_GROUP, $groupQuery);
  // 特殊处理，树形功能已经绑定树形ref对象，不在需要绑定
  // 非树形功能 || 查询选择
  if (funcData.info.funcType !== FuncTypeEnum.TREE || FuncDataTypeEnum.SELECT === type) {
    $func.setRefMaps(FuncRefEnum.FUNC_TREE, $tree);
  }

  return { $grid, $tree, $func, $groupQuery };
}
