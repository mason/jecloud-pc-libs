import FuncDataPanel from '../func-data-panel';
import { bindEvents4Sync } from '../../../func-manager';
import { camelCase } from '@jecloud/utils';
import { Panel } from '@jecloud/ui';
import { onMounted, nextTick, ref } from 'vue';
export function useExtendPanel({ $func, $grid, outerPanelRef, innerPanelRef }) {
  const funcData = $func.getFuncData();
  const events = bindEvents4Sync({
    $func,
    events: funcData.info.extendPanelEvents,
  });
  // renderer触发器
  const emitter = ref(0);
  // 解析面板配置
  const panelConfig = (regions) =>
    regions
      .map((region) => ({ region, renderer: events[camelCase(`on-${region}-renderer`)] }))
      .filter((item) => item.renderer);
  const outerPanels = panelConfig(['top', 'bottom', 'left', 'right']);
  const innerPanels = panelConfig(['tbar', 'bbar', 'lbar', 'rbar']);
  // 基础renderer
  const baseRenderer = (config) => (
    <Panel.Item region={config.region}>
      <FuncDataPanel {...config} emitter={emitter.value} />
    </Panel.Item>
  );
  // 扩展面板 外
  const extendPanelOuter = () => {
    return outerPanels.map((config) => baseRenderer(config));
  };
  // 扩展面板 内
  const extendPanelInner = () => {
    return innerPanels.map((config) => baseRenderer(config));
  };

  // 刷新面板
  if (outerPanels.length || innerPanels.length) {
    onMounted(() => {
      nextTick(() => {
        $grid.value.store.on('load', () => {
          emitter.value++;
          outerPanels.length && outerPanelRef.value.refreshLayout();
          innerPanels.length && innerPanelRef.value.refreshLayout();
        });
      });
    });
  }

  return {
    extendPanelInner,
    extendPanelOuter,
  };
}
