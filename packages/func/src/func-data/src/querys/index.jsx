import { useStrategyQuery } from './hooks/use-strategy-query';
import { useKeywordQuery } from './hooks/use-keyword-query';
import { useTreeQuery } from './hooks/use-tree-query';
export function useQuerys(options) {
  return { ...useStrategyQuery(options), ...useKeywordQuery(options), ...useTreeQuery(options) };
}
