import { computed, resolveComponent, h, watch, ref } from 'vue';
import { Hooks, Button } from '@jecloud/ui';
import {
  FuncFieldTypeEnum,
  QueryConnectorEnum,
  QueryConnectorTextEnum,
  parseConfigInfo,
} from '../../../../func-util';
import { pick, toQuerysTemplate, split } from '@jecloud/utils';
import { useInjectFunc } from '../../../../func-manager';
import { useInjectFuncTopQuery } from './use-query-context';
import { parseFieldEvents } from '../../../../func-form/src/util';
export function useGroupQueryItem({ props, context }) {
  const { emit } = context;
  const $func = useInjectFunc();
  const $topQuery = useInjectFuncTopQuery();
  const model = Hooks.useModelValue({ props, context });
  // 多选
  const multiple = computed(() => {
    return [QueryConnectorEnum.IN, QueryConnectorEnum.NOT_IN].includes(model.value.type);
  });
  // 区间
  const between = computed(() => {
    return [QueryConnectorEnum.BETWEEN].includes(model.value.type);
  });
  // 模糊可编辑
  const editable = computed(() => {
    return [QueryConnectorEnum.LIKE].includes(model.value.type);
  });
  const { field } = props;

  // 字段
  const { component, componentAttr } = parseFieldAttr({
    field: field.field,
    model,
    props,
    $func,
    $topQuery,
  });

  const fieldSlot = (betweenField) => {
    const name = betweenField ? 'betweenValue' : 'value';
    const configInfo = parseFieldConfigInfo({ field: field.field, name });
    const params = {
      ...componentAttr,
      name,
      configInfo,
      editable: editable.value,
      model: model.value.values,
      'onUpdate:model': function (val) {
        model.value.values = val;
      },
      value: model.value[name],
      multiple: multiple.value,
      'onUpdate:value': function (val) {
        model.value[name] = val;
        model.value.values[componentAttr.name] = val;
      },
      class: 'query-value',
      getPopupContainer: () => document.body,
    };
    // 高级查询 下拉框根据表单配置 可选可过滤
    if ([FuncFieldTypeEnum.SELECT.xtype].includes(field.field.xtype)) {
      params.showSearch = field.field.otherConfig?.optionalAndQuery == '1' || false;
    }
    // 读取人员选择器的otherConfig配置
    if ([FuncFieldTypeEnum.INPUT_SELECT_USER.xtype].includes(field.field.xtype)) {
      const { queryType } = field.field.otherConfig;
      const allTab = ['dept', 'role', 'organ', 'common'];
      const hideTab = split(queryType, ',');
      const displayTab = allTab.filter((tab) => !hideTab.includes(tab));
      if (!params.selectOptions) {
        params.selectOptions = {};
      }
      params.selectOptions['queryType'] = displayTab.join(',');
    }
    return !betweenField || between.value ? h(resolveComponent(component), params) : null;
  };
  // 查询项
  const queryTypes = parseQueryTypes(component);
  model.value.type = (model.value.type || queryTypes[0].value).trim();

  // 排序
  const onSortClick = () => {
    switch (model.value.sort) {
      case 'ASC':
        model.value.sort = 'DESC';
        break;
      case 'DESC':
        model.value.sort = '';
        break;
      case '':
        model.value.sort = 'ASC';
        break;
    }
    emit('sort-click', { code: field.name, type: model.value.sort });
  };
  // 排序图标
  const sortIcon = computed(() => {
    switch (model.value.sort) {
      case 'ASC':
        return 'fas fa-sort-alpha-down';
      case 'DESC':
        return 'fas fa-sort-alpha-up';
      default:
        return 'fas fa-sort-alt';
    }
  });
  const sortSlot = () => {
    return field.enableSort ? (
      <Button class="query-order" icon={sortIcon.value} onClick={onSortClick}></Button>
    ) : null;
  };

  return { sortIcon, onSortClick, fieldSlot, sortSlot, model, queryTypes };
}
/**
 * 解析字段属性
 * @param {*} field
 * @returns
 */
function parseFieldAttr({ field, model, props, $func, $topQuery }) {
  const { formModel } = $topQuery;
  let component = FuncFieldTypeEnum.getComponent(field.xtype);
  const componentAttr = pick(field, ['name', 'configInfo', 'selectExp', 'querys']);
  // 带值处理
  const values = { [field.name]: null };
  // 功能属性
  Object.assign(componentAttr, $func.action.parseFieldProps4Common({ field }));
  if (
    [FuncFieldTypeEnum.CHECKBOX_GROUP.xtype, FuncFieldTypeEnum.RADIO_GROUP.xtype].includes(
      field.xtype,
    )
  ) {
    // 单选，多选，转成下拉框
    component = FuncFieldTypeEnum.SELECT.component;
  } else if (
    [FuncFieldTypeEnum.TEXTAREA.xtype, FuncFieldTypeEnum.INPUT_CODE.xtype].includes(field.xtype)
  ) {
    // 文本域，编号字段，转成文本框
    component = FuncFieldTypeEnum.INPUT.component;
  }
  // 处理带值配置
  if (
    [
      FuncFieldTypeEnum.SELECT.xtype,
      FuncFieldTypeEnum.INPUT_SELECT_GRID.xtype,
      FuncFieldTypeEnum.INPUT_SELECT_TREE.xtype,
      FuncFieldTypeEnum.INPUT_SELECT_USER.xtype,
    ].includes(field.xtype)
  ) {
    const config = parseConfigInfo({ configInfo: field.configInfo });
    // 增加查询选择字段带值属性，用于条件过滤使用
    if (config.targetFields.length > 0) {
      config.targetFields.forEach((tf) => {
        values[tf] = null;
      });
    }
    Object.assign(componentAttr, {
      dicValueConfig: field.dicValueConfig,
      onBeforeSelect({ options }) {
        // 解析参数
        if (options.querys) {
          toQuerysTemplate({
            querys: options.querys,
            model: formModel,
          });
        }
        // 树形配置
        if (options.dicValueConfig) {
          toQuerysTemplate({ querys: options.dicValueConfig, model: formModel });
        }
      },
    });
  }
  // 带值处理
  model.value.values = values;
  // 解析事件
  componentAttr.events = field.queryEvents;
  const events = parseFieldEvents({
    field: componentAttr,
    model: formModel,
    type: 'topQuery',
    $func,
    $topQuery,
  });
  Object.assign(componentAttr, events);
  return { component, componentAttr };
}

/**
 * 解析配置信息
 * @param {*} field
 * @param {*} name
 * @returns
 */
function parseFieldConfigInfo({ field, name }) {
  let configInfo = field.configInfo;
  // 处理带值配置
  if (
    [
      FuncFieldTypeEnum.SELECT.xtype,
      FuncFieldTypeEnum.INPUT_SELECT_GRID.xtype,
      FuncFieldTypeEnum.INPUT_SELECT_TREE.xtype,
      FuncFieldTypeEnum.INPUT_SELECT_USER.xtype,
    ].includes(field.xtype)
  ) {
    const config = parseConfigInfo({ configInfo: field.configInfo });
    // 目标字段
    const sourceField = config.fieldMaps?.targetToSource[field.name];
    if (sourceField) {
      config.sourceFields.push(sourceField);
      config.targetFields.push(name);
    }
    configInfo = `${config.code},${config.targetFields.join('~')},${config.sourceFields.join('~')}`;
    // 多选处理
    if (config.multiple) {
      configInfo += ',M';
    } else {
      configInfo += ',S';
    }
    // 异步
    if (config.async) {
      configInfo += '_';
    }
  }
  return configInfo;
}
/**
 * 解析查询项
 * @param {*} component
 * @returns
 */
function parseQueryTypes(component) {
  let types = [];
  if ([FuncFieldTypeEnum.SELECT.component].includes(component)) {
    // 下拉框，单选框，多选框
    types = [
      QueryConnectorEnum.EQ,
      QueryConnectorEnum.IN,
      QueryConnectorEnum.NOT_IN,
      QueryConnectorEnum.NOT_NULL,
    ];
  } else if (
    [
      FuncFieldTypeEnum.INPUT_NUMBER.component,
      FuncFieldTypeEnum.DATE_PICKER.component,
      FuncFieldTypeEnum.TIME_PICKER.component,
    ].includes(component)
  ) {
    // 数值框，进度条，日期，时间
    types = [
      QueryConnectorEnum.BETWEEN,
      QueryConnectorEnum.EQ,
      QueryConnectorEnum.LIKE,
      QueryConnectorEnum.GT,
      QueryConnectorEnum.GE,
      QueryConnectorEnum.LT,
      QueryConnectorEnum.LE,
      QueryConnectorEnum.NOT_IN,
      QueryConnectorEnum.NOT_NULL,
    ];
  } else {
    types = [
      QueryConnectorEnum.LIKE,
      QueryConnectorEnum.EQ,
      QueryConnectorEnum.IN,
      QueryConnectorEnum.NOT_IN,
      QueryConnectorEnum.NOT_NULL,
    ];
  }
  return types.map((item) => ({ label: QueryConnectorTextEnum[item], value: item }));
}
