import { defineComponent } from 'vue';
import { Tree, Panel } from '@jecloud/ui';
import { useFuncChildrenGroup } from '../hooks/use-func-children-group';

export default defineComponent({
  name: 'JeFuncChildrenGroup',
  inheritAttrs: false,
  props: {
    funcCode: String,
    layout: String, // 展示位置
    activeItem: String,
    form: Object, // 表单功能
  },
  setup(props, context) {
    const { attrs, expose } = context;
    const {
      treeStore,
      activeItem,
      children,
      $child,
      $tree,
      rowClassName,
      doBack,
      onTreeSelectChange,
      itemSlot,
    } = useFuncChildrenGroup({
      props,
      context,
    });
    expose($child);
    return () => (
      <Panel class="je-func-children-group" {...attrs}>
        <Panel.Item region="left" size="240" collapsible split>
          <Tree
            ref={$tree}
            class="je-func-children-group-tree"
            store={treeStore}
            allowDeselect={false}
            rowClassName={rowClassName}
            search={{ subLabelField: false }}
            treeConfig={{ expandAll: true }}
            onSelectionChange={onTreeSelectChange}
          />
        </Panel.Item>
        <Panel.Item>
          <div class="je-func-children-group-body">
            <div class="je-func-children-group-body-title">
              <i class={activeItem.icon}></i>
              {activeItem.title}
              <div class="je-func-children-backbutton">
                <div class="je-func-children-backbutton-text" onClick={doBack}>
                  <i class="fas fa-reply" /> 返回
                </div>
              </div>
            </div>
            <div class="je-func-children-group-body-wrapper">
              {children.value.map((child) => {
                return child.hidden || !child.rendered ? null : itemSlot(child);
              })}
            </div>
          </div>
        </Panel.Item>
      </Panel>
    );
  },
});
