import { defineComponent, watch, ref } from 'vue';
import { PreviewFile } from '@jecloud/ui';
import { loadFuncFileInfoApi, useInjectFunc } from '../../func-manager';
import { isNotEmpty } from '@jecloud/utils';
/**
 * 附件子功能
 */
export default defineComponent({
  name: 'JeFuncChildrenFile',
  props: {
    model: Object,
    dataCode: String,
  },
  setup(props) {
    const $func = useInjectFunc();
    const funcData = $func.getFuncData();
    const { funcCode, tableCode } = funcData;
    const child = funcData
      .getChildren()
      .find((item) => item.type === 'file' && item.code == props.dataCode);
    // 附件数据
    const fileList = ref([]);
    //子功能附件类型刷新
    const refreshFileList = () => {
      const { id, configInfo } = child;
      const options = {
        params: {
          funcRelationId: id, // 子功能关联ID
          dataId: $func.store.getBeanId(), // 当前一条数据ID
          funcCode, //功能code
          tableCode,
        },
      };
      // 支持接口地址
      if (isNotEmpty(configInfo) && configInfo.indexOf('/') > -1) {
        const configInfoArray = configInfo.split(',');
        if (configInfoArray[0]) {
          options.url = configInfoArray[0];
        }
        if (configInfoArray[1]) {
          options.pd = configInfoArray[1];
        }
      }
      loadFuncFileInfoApi(options).then((res) => {
        fileList.value = res?.children;
      });
    };
    // 获取文件类型子功能数据
    watch(() => [$func.store.activeBean, $func.store.activeBeanEmitter], refreshFileList, {
      immediate: true,
    });
    return () => (
      <PreviewFile
        onRefreshFileList={refreshFileList}
        fileList={fileList.value}
        treeDataType
        showRefresh
        showOpenNewView
      />
    );
  },
});
