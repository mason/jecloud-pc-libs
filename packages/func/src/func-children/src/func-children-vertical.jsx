import { defineComponent } from 'vue';
import { isEmpty } from '@jecloud/utils';
import { useFuncChildren } from '../hooks/use-func-children';
/**
 * 子功能纵向展示
 */
export default defineComponent({
  name: 'JeFuncChildrenVertical',
  inheritAttrs: false,
  props: {
    funcCode: String,
    layout: String,
    height: Number,
    childAttrs: Object,
    groupName: String,
    groupLayout: Boolean,
    config: { type: Object, default: () => {} },
  },
  setup(props, context) {
    const { expose, attrs } = context;
    // 子功能数据
    const { children, $child, computeHeight, childItemSlot } = useFuncChildren({
      props,
      context,
    });
    expose($child);

    return () =>
      isEmpty(children.value) ? null : (
        <div
          class={{
            'je-func-children-vertical': true,
            'is-single': children.value.length === 1,
            'is-grouplayout': props.groupLayout,
          }}
          {...attrs}
        >
          {children.value.map((child) => {
            return child.hidden ? null : (
              <div class="je-func-children-vertical-col" {...props.childAttrs} data-anchor-parent>
                <div class="je-func-children-title">
                  <div class="je-func-children-title-content" style={{ color: child.titleColor }}>
                    {child.icon ? <i class={['title-icon', child.icon]} /> : null}
                    {child.title}
                  </div>
                </div>
                <div
                  class="je-func-children-wrapper"
                  style={{ height: computeHeight(child.height) }}
                >
                  {childItemSlot(child, {
                    title: true,
                    style: { height: '100%' },
                  })}
                </div>
              </div>
            );
          })}
        </div>
      );
  },
});
