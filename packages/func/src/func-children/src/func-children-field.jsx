import { defineComponent, reactive, ref } from 'vue';
import { Hooks } from '@jecloud/ui';
import FuncGrid from '../../func-grid';
import { useFuncChildrenField } from '../hooks/use-func-children-field';

export default defineComponent({
  name: 'JeFuncChildrenField',
  inheritAttrs: false,
  props: {
    field: Object,
    funcCode: String,
    fkCode: String,
    width: Number,
    height: Number,
    disabled: Boolean,
    readonly: Boolean,
    actionColumn: Object,
    model: Object,
    isFuncField: { type: Boolean, default: true },
    maximize: Boolean,
    parentFields: Array,
    childFields: Array,
    pageSize: String,
    name: String,
  },
  emits: ['change', 'private-change', 'rendered'],
  setup(props, context) {
    const { attrs } = context;
    const style = reactive({ height: '192px', ...Hooks.useStyle4Size({ props }) });
    const { state, gridRef, disabled } = useFuncChildrenField({
      props,
      context,
      style,
    });
    // 全屏操作
    const fullScreen = ref(false);

    return () => {
      return state.loading ? (
        <div v-loading={state.loading} style={style} />
      ) : props.funcCode ? (
        <FuncGrid
          ref={gridRef}
          class={{
            'je-func-children-field': true,
            'is--fullscreen': fullScreen.value,
            'is--disabled': disabled.value,
          }}
          {...style}
          {...attrs}
          {...state.events}
          fkCode={props.fkCode}
          readonly={disabled.value}
          multiple={!disabled.value && state.enableRemoves}
          enableSort={!disabled.value && state.enableDrag}
          funcCode={props.funcCode}
          pagerConfig={false}
          pageSize={-1}
          size="mini"
          v-slots={{
            tbar: () => {
              // 标题
              return fullScreen.value ? (
                <div class="je-func-children-field-tbar">
                  <span class="title">{props.field.label}</span>
                  <i
                    class="jeicon jeicon-times close"
                    onClick={() => {
                      fullScreen.value = false;
                    }}
                  />
                </div>
              ) : null;
            },
            bbar: () => {
              // 打开全屏
              return !props.maximize || fullScreen.value ? null : (
                <div class="je-func-children-field-bbar">
                  {state.errorMessage ? (
                    <span class="ant-form-item-explain-error">{state.errorMessage}</span>
                  ) : null}
                  <span
                    class="open"
                    onClick={() => {
                      fullScreen.value = true;
                    }}
                  >
                    打开数据面板
                  </span>
                </div>
              );
            },
          }}
        />
      ) : (
        <div class="no-configinfo">请前往子功能集合进行配置！</div>
      );
    };
  },
});
