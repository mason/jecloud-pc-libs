import { defineComponent, watch, ref, onMounted, nextTick, onUnmounted } from 'vue';
import { mitt, useJE } from '@jecloud/utils';
import { Loading } from '@jecloud/ui';
import { useInjectFunc } from '../../func-manager';
/**
 * 微应用子功能
 */
export default defineComponent({
  name: 'JeFuncChildrenMicro',
  props: {
    eventCode: String,
    microCode: String,
    readonly: Boolean,
  },
  setup(props, { expose }) {
    const $func = useInjectFunc();
    const microRef = ref();
    const { microCode } = props;
    const admin = useJE().useAdmin?.();
    const errorMessage = ref('');
    const loading = ref(true);
    const $child = {
      emitter: mitt(),
      on(...args) {
        return $child.emitter.on(...args);
      },
      getFunc() {
        return $func;
      },
      getModel() {
        return $func.getBean();
      },
    };
    expose($child);
    const getParams = () => {
      return {
        $child,
        $func,
        model: $func.getBean(),
      };
    };
    // 获取文件类型子功能数据
    const emitModelChange = () => {
      $child.emitter.emit('model-change', getParams());
    };
    const modelWatchFn = watch(
      () => [$func.store.activeBean, $func.store.activeBeanEmitter],
      emitModelChange,
    );
    // 属性监听
    const propsWatchFn = watch(
      () => props,
      () => {
        $child.emitter.emit('props-change', props);
      },
    );
    onUnmounted(() => {
      modelWatchFn();
      propsWatchFn();
    });
    onMounted(() => {
      nextTick(() => {
        loading.value = true;
        // 触发微应用子功能事件
        admin
          .emitMicroEvent(props.microCode, props.eventCode, microRef.value, getParams())
          .then(emitModelChange)
          .finally(() => {
            loading.value = false;
          });
      });
    });
    if (!admin) {
      errorMessage.value = '微应用只支持在主应用中使用！';
    } else if (!microCode) {
      errorMessage.value = '请配置微应用编码';
    }

    return () => {
      if (errorMessage.value) {
        return <div>{errorMessage.value}</div>;
      } else {
        return (
          <div class="je-children-micro" ref={microRef}>
            {loading.value ? <Loading loading={loading.value} /> : null}
          </div>
        );
      }
    };
  },
});
