import { defineComponent, watch, ref } from 'vue';
import { DataTrance } from '@jecloud/ui';
import { useInjectFunc } from '../../func-manager';
/**
 * 历史留痕子功能
 */
export default defineComponent({
  name: 'JeFuncChildrenHistory',
  props: {
    model: Object,
  },
  setup() {
    const $dataTrance = ref();
    const $func = useInjectFunc();
    const funcData = $func.getFuncData();
    const { funcCode } = funcData;
    const beanId = $func.store.getBeanId();
    watch(
      () => [$func.store.activeBean, $func.store.activeBeanEmitter],
      () => {
        $dataTrance.value?.refresh({ funcCode, beanId: $func.store.getBeanId() });
      },
      {
        immediate: true,
      },
    );
    return () => <DataTrance ref={$dataTrance} funcCode={funcCode} beanId={beanId} />;
  },
});
