import { useFuncChildren } from './use-func-children';
import { Data, uuid, pinyin, cloneDeep } from '@jecloud/utils';
import { nextTick, onMounted, reactive, ref, watch } from 'vue';
export function useFuncChildrenGroup({ props, context }) {
  const { children, $child, $func, childItemSlot } = useFuncChildren({ props, context });
  const $tree = ref();
  // 安装表单
  const form = $child.setupForm();
  // 激活项
  const defaultActiveItem = {
    id: uuid(),
    code: form.code,
    title: form.title,
    icon: 'fal fa-memo-pad',
  };
  const activeItem = reactive(cloneDeep(defaultActiveItem));
  // 树形store
  const treeStore = Data.useTreeStore();
  treeStore.appendChild({
    id: activeItem.id,
    code: activeItem.code,
    text: activeItem.title,
    icon: activeItem.icon,
    nodeInfoType: 'FUNC',
  });
  const childFuncs = buildTreeData({ treeStore, $func });
  children.value = [form, ...childFuncs];
  // 设置渲染标记
  watch(
    () => activeItem.code,
    () => {
      children.value.find((item) => item.code === activeItem.code).rendered = true;
    },
    { immediate: true },
  );
  // 备份数据
  const storeData = cloneDeep(treeStore.data);
  watch(
    () => children.value,
    () => {
      treeStore.loadData(storeData);
    },
    { deep: true },
  );
  // 表达式显隐
  const rowClassName = ({ row }) => {
    const child = children.value.find((item) => item.id === row.id);
    return child?.hidden ? 'is-hidden' : '';
  };
  const itemSlot = (child) => {
    return childItemSlot(child, {
      style: { display: activeItem.code === child.code ? undefined : 'none' },
    });
  };
  const doTreeSelect = (id) => {
    const record = treeStore.getRecordById(id);
    $tree.value.setSelectedRecords(record);
  };

  // 切换功能
  const onTreeSelectChange = ({ records }) => {
    const record = records[0];
    if (record?.nodeInfoType === 'FUNC') {
      Object.assign(activeItem, {
        id: record.id,
        code: record.code,
        icon: record.icon,
        title: record.nodeInfo || record.text,
      });
    }
  };

  const doBack = () => {
    $func.action.doFormBack();
    doTreeSelect(defaultActiveItem.id);
  };
  onMounted(() => {
    nextTick(() => {
      doTreeSelect(activeItem.id);
    });
  });

  return {
    treeStore,
    $tree,
    $child,
    rowClassName,
    doBack,
    onTreeSelectChange,
    itemSlot,
    activeItem,
    children,
  };
}
/**
 * 构建树形数据
 * 分组命名规则：组名1>组名11>组名111，普通展示，所有的子功能会挂接到对应的分组下面
 * 分组命名规则：组名1>组名11>组名111*，最后一级带*号，表示下方不会挂接子功能，点击后所有子功能将会平铺展示
 * @param {*} param0
 */
function buildTreeData({ treeStore, $func }) {
  // 子功能数据
  const childrenConfig = $func.getChildFuncConfig();
  const children = [];
  childrenConfig.forEach((child) => {
    const childNode = {
      id: child.id,
      code: child.code,
      text: child.title,
      icon: 'fal fa-cube',
      nodeInfoType: 'FUNC',
    };
    if (child.groupName) {
      const groupNames = child.groupName.split('>');
      let lastGroupNode;
      groupNames.forEach((name, index) => {
        const parentId = pinyin(groupNames.slice(0, index).join('>'));
        const code = groupNames.slice(0, index + 1).join('>');
        const id = pinyin(code);
        lastGroupNode = treeStore.getRecordById(id);
        if (!lastGroupNode) {
          lastGroupNode = treeStore.appendChild(
            {
              id,
              code,
              text: name.replace('*', ''),
              nodeInfo: code.replace('*', ''),
              icon: 'fal fa-cubes',
              nodeInfoType: name.endsWith('*') ? 'FUNC' : 'GROUP',
            },
            treeStore.getRecordById(parentId),
          )[0];
        }
      });
      // 追加功能数据
      if (lastGroupNode.nodeInfoType === 'FUNC') {
        // 纵向布局
        children.push(
          reactive({
            id: lastGroupNode.id,
            code: lastGroupNode.code,
            title: lastGroupNode.text,
            type: 'vertical',
          }),
        );
      } else {
        treeStore.appendChild(childNode, lastGroupNode);
        children.push(child);
      }
    } else {
      // 追加功能数据
      treeStore.appendChild(childNode);
      children.push(child);
    }
  });
  return children;
}
