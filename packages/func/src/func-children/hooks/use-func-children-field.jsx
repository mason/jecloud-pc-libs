import { computed, nextTick, watch } from 'vue';
import { useFuncChildrenField as _ } from '../../func-manager';
import { isEmpty, isNotEmpty } from '@jecloud/utils';
import { Form } from '@jecloud/ui';
import { useFuncDataEvents } from '../../func-manager';
import { Form as AForm } from 'ant-design-vue';

export function useFuncChildrenField({ props, context, style }) {
  const { $childField, $func, gridRef, state } = _({ props, context });
  Object.assign($childField, {
    fieldName: props.field.name,
    fieldLabel: props.field.label,
    validator(rule = {}) {
      // 如果没有配置功能就通过必填校验
      if (isEmpty(props.funcCode)) {
        return Promise.resolve();
      }
      if (rule.required && isEmpty(gridRef.value.store.data)) {
        state.errorMessage = rule.message;
        return Promise.reject(rule.message);
      } else {
        rule.message = '列表校验未通过';
        state.errorMessage = '';
        return gridRef.value.validate(true);
      }
    },
    clearValidate() {
      state.errorMessage = '';
    },
    setReadOnly(readonly) {
      $func.store.readonly = readonly;
      if (!gridRef.value) return;
      const actionColumnObj = gridRef.value.getColumnByField('actionColumn');
      // 如果组件可编辑,并且有操作按钮,action列就显示,否则隐藏
      if (
        !readonly &&
        (state.enableAdd || state.enableAdds || state.enableRemove || state.enableRemoves)
      ) {
        gridRef.value.showColumn(actionColumnObj);
      } else {
        gridRef.value.hideColumn(actionColumnObj);
      }
    },
    style,
  });
  // 注册formitem插槽对象
  const formItemContext = Form.useInjectFormItemContext();
  formItemContext?.addSlotItem($childField);
  // 重置FormItem，防止列表编辑组建触发本子功能集合的校验函数
  AForm.useInjectFormItemContext();

  //监听是否可编辑
  const disabled = computed(() => props.readonly || props.disabled);
  watch(
    () => disabled.value,
    () => {
      $childField.setReadOnly(disabled.value);
    },
  );
  // 事件处理
  const { onGridHeaderCellDblclick } = useFuncDataEvents({
    $func,
  });
  // 加载操作列
  const onGridRendered = () => {
    // 解析列
    parseActionColumn($childField);
    // 设置状态
    $childField.setReadOnly(disabled.value);
    // 重置滚动条
    gridRef.value.store.on('before-load', () => {
      gridRef.value?.clearScroll();
    });
    gridRef.value.store.on('load', () => {
      nextTick(() => {
        initChildHeight($childField);
      });
    });
    nextTick(() => {
      initChildHeight($childField);
      context.emit('rendered', { $childField });
    });
  };

  // 加载功能数据
  state.loading = true;
  $childField
    .loadFunc()
    .then(() => {
      // 列表事件
      const events = $func.action.bindGridEvents({});
      Object.assign(events, {
        onHeaderCellDblclick: onGridHeaderCellDblclick,
        onRendered: onGridRendered,
      });
      state.events = events;
    })
    .finally(() => {
      state.loading = false;
    });

  return {
    $func,
    gridRef,
    state,
    disabled,
  };
}

/**
 * 解析操作列，子功能集合使用
 */
function parseActionColumn($childField) {
  const { $func, state } = $childField;
  const $grid = $func.getFuncGrid();

  // 操作按钮数量，计算列宽使用
  const count = [state.enableAdd, state.enableAdds, state.enableRemoves].filter(
    (item) => item,
  ).length;
  // 操作列
  const column = {
    width: count > 1 ? count * 20 + 22 : 42,
    resizable: false,
    align: 'center',
    field: 'actionColumn',
    headerAlign: 'center',
    className: 'je-func-children-field-action-cell',
    headerClassName: 'je-func-children-field-action-header',
    slots: {
      header: () => {
        const buttons = [];
        // 添加
        if (state.enableAdd) {
          buttons.push(
            <i
              class="action-item green fas fa-plus-circle"
              onClick={() => $childField.action.doActionAdd()}
            ></i>,
          );
        }
        // 批量添加
        if (state.enableAdds) {
          buttons.push(
            <i
              class="action-item green fas fa-list-ul"
              onClick={() => $childField.action.doActionAdds()}
            ></i>,
          );
        }
        // 清空
        if (state.enableRemoves) {
          buttons.push(
            <i
              class="action-item red fas fa-trash-alt"
              onClick={() => $childField.action.doActionRemoves()}
            ></i>,
          );
        }
        return buttons;
      },
      default: (options) => {
        // 删除按钮
        return state.enableRemove ? (
          <i
            class="action-item red fas fa-times-circle"
            onClick={() => $childField.action.doActionRemove(options)}
          ></i>
        ) : null;
      },
    },
  };
  // 操作列
  const { collectColumn } = $grid.getTableColumn();
  // 按钮位置
  if (state.buttonAlign === 'right') {
    column.fixed = 'right';
    collectColumn.push(column);
  } else {
    column.fixed = 'left';
    collectColumn.unshift(column);
  }
  $grid.loadColumn(collectColumn);
}
/**
 * 初始化子功能高度
 * @param {*} $childField
 */
function initChildHeight($childField) {
  if (isNotEmpty($childField.props.pageSize)) {
    let num = 0;
    // 打开数据面板按钮高度
    if ($childField.props.maximize) {
      num = 22;
    }
    // 默认设置的高度
    const childHeight = $childField.props.height || 192;
    // 根据pagesize计算的高度
    const customHeight = $childField.props.pageSize * 32 + 32;
    // 根据实际条数计算的高度
    const length = $childField.gridRef.value?.getData().length;
    const theoryHeight = length * 32 + 32;
    if (childHeight < customHeight) {
      if (theoryHeight > childHeight && theoryHeight > customHeight) {
        $childField.style.height = customHeight + num + 'px';
      } else {
        if (theoryHeight < childHeight) {
          $childField.style.height = $childField.props.height || 192;
        } else {
          $childField.style.height = theoryHeight + num + 'px';
        }
      }
      if (length <= 0) {
        $childField.style.height = $childField.props.height || 192;
        if (num > 0) {
          $childField.style.height += 24;
        }
      }
    }
  }
}
