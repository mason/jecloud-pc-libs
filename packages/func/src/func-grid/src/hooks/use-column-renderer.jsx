import {
  downloadFile,
  isNotEmpty,
  isEmpty,
  rendererDateValue,
  toValue,
  renderer4Checkbox,
  renderer4Dictionary,
  renderer4Files,
  renderer4Highlight,
  renderer4Link,
  renderer4Star,
} from '@jecloud/utils';
import { parseConfigInfo } from '../../../func-util';
import { FuncFieldTypeEnum, FuncDataTypeEnum } from '../../../func-manager/enum';
import { File, Modal, Upload } from '@jecloud/ui';
import { parseColumnFieldProps, useModelValue } from '../../../func-form/src/util';
import { reactive, h, resolveComponent } from 'vue';
/**
 * 列renderer
 * @param {Object} options
 * @param {Object} options.column 列配置
 * @param {Object} options.$func 功能对象
 * @param {string} options.type 功能类型
 * @return {Object}
 */
export function useColumnRenderer({ column, $func, type }) {
  const field = $func.getFuncData().getFieldConfig(column.field);
  const defaultSlot = ({ $table, row }) => {
    let text = row[column.field];
    text = toValue(text?.toString?.(), text, true); // 转为字符串，方便后续操作
    const options = {
      column,
      field,
      row,
      $func,
      $table,
      type,
    };
    text = dictionaryRenderer(text, options);
    text = fileRenderer(text, options);
    text = dateRenderer(text, options);
    text = starRenderer(text, options);
    text = linkRenderer(text, options);
    text = highlightRenderer(text, options);
    return text;
  };
  return defaultSlot;
}
/**
 * 头部renderer
 * @param {*} param0
 * @returns
 */
export function useColumnHeaderRenderer({ column, $func, type }) {
  const field = $func.getFuncData().getFieldConfig(column.field);
  if (!field || type === FuncDataTypeEnum.SELECT) {
    // 支持其他列头修改文字颜色
    return () => <span style={{ color: column.titleColor }}>{column.title}</span>;
  }

  const headerSlot = () => {
    // 帮助信息
    const helpNode = field.help?.enable ? (
      <i
        class="je-func-grid-header-action jeicon jeicon-doubt-s"
        onClick={() => showColumnHelp({ field })}
      ></i>
    ) : null;

    let checkboxNode = null;
    // 如果是必填，并且不是只读的时候，column.type == 'switch',多附件,图片选择器必填不生效， 因为xtable中column 没有添加editRender属性，所以必填头部自己来渲染
    if (field.required && !field.disabled) {
      if (column.type == 'switch' || ['uxfilesfield', 'imagepickerfield'].includes(field.xtype)) {
        checkboxNode = <i class="vxe-cell--required-icon" />;
      }
    }
    // 批量编辑
    const batchEditNode =
      column.batchEditable &&
      FuncFieldTypeEnum.includesColumnXtype(field.xtype) &&
      !field.readonly ? (
        <i
          class="je-func-grid-header-action fal fa-pen-line"
          onClick={() => showBatchEditer({ field, column, $func })}
        ></i>
      ) : null;
    return (
      <span style={{ color: column.titleColor }}>
        {checkboxNode}
        {helpNode}
        {column.title}
        {batchEditNode}
      </span>
    );
  };
  return headerSlot;
}

/**
 * 字典
 * @param {*} param0
 */
function dictionaryRenderer(text, { column, field, row }) {
  if (!column.dictionary) return text;
  // 复选框样式
  if (column.dictionary === 'YESORNO' && column.switchStyle.enable) {
    return renderer4Checkbox(text, column.switchStyle);
  } else if (text) {
    return renderer4Dictionary(text, {
      row,
      configInfo: field.configInfo,
      dictionary: { code: column.dictionary, ...column.ddColor },
    });
  }
}
/**
 * 附件
 * @param {*} param0
 */
function fileRenderer(text, { column, field, $func, row }) {
  if (
    [
      FuncFieldTypeEnum.UPLOAD_INPUT.xtype,
      FuncFieldTypeEnum.UPLOAD.xtype,
      FuncFieldTypeEnum.UPLOAD_IMAGE.xtype,
    ].includes(field?.xtype)
  ) {
    // 单附件
    const singleFile = field.xtype === FuncFieldTypeEnum.UPLOAD_INPUT.xtype;
    // 可编辑
    const editable = !singleFile && column.editable && !$func.store.readonly;
    const fns = {
      onPreview(item) {
        File.previewFile(item);
      },
      onDownload(item) {
        downloadFile(item.fileKey);
      },
      onEdit: editable
        ? () => {
            doFilesEdit({ row, column, field, $func });
          }
        : null,
    };
    return renderer4Files(text, fns);
  }
  return text;
}
/**
 * 日期
 * @param {*} param0
 * @returns
 */
function dateRenderer(text, { field }) {
  if (
    [FuncFieldTypeEnum.DATE_PICKER.xtype, FuncFieldTypeEnum.TIME_PICKER.xtype].includes(
      field?.xtype,
    )
  ) {
    // 日期处理
    const format = field.configInfo;
    const type =
      FuncFieldTypeEnum.TIME_PICKER.xtype === field.xtype ? 'time' : field.otherConfig.dateType;
    text = rendererDateValue(text, { type, format });
  }

  return text;
}
/**
 * 评星
 * @param {*} param0
 * @returns
 */
function starRenderer(text, { field }) {
  if (field?.xtype == FuncFieldTypeEnum.STAR.xtype) {
    return renderer4Star(text);
  }
  return text;
}
/**
 * 超链接
 * @param {*} param0
 * @returns
 */
function linkRenderer(text, { column, type }) {
  // 超链接
  if (column.link.enable && type === FuncDataTypeEnum.FUNC) {
    return renderer4Link(text);
  }
  return text;
}

/**
 * 高亮
 * @param {*} param0
 * @returns
 */
function highlightRenderer(text, { column, $func }) {
  const keywordFields = $func.getFuncData().querys.func.keyword || [];
  const keywordText = $func.getFuncGrid()?.getKeywordText();
  if (keywordFields.includes(column.field) && isNotEmpty(text) && isNotEmpty(keywordText)) {
    return renderer4Highlight(text, keywordText);
  }
  return text;
}

/**
 * 多附件编辑
 * @param {*} param0
 */
const doFilesEdit = ({ row, field, column, $func }) => {
  const funcData = $func.getFuncData();
  const props = $func.action.parseFieldProps4Upload({ field });
  Upload.showUploadModal({
    ...props,
    title: column.title,
    value: row[column.field],
    type: field.xtype == FuncFieldTypeEnum.UPLOAD_IMAGE.xtype ? 'image' : 'files',
    afterFileRemove({ value }) {
      // 删除后，更新代码
      const pkValue = row[funcData.info.pkCode];
      if (pkValue) {
        $func.action.doFormUpdateById(pkValue, { [field.name]: value });
        row[column.field] = value;
      }
    },
  }).then((value) => {
    row[column.field] = value;
  });
};
/**
 * 展示帮助信息
 * @param {*} param0
 */
function showColumnHelp({ field }) {
  const { help, label } = field;
  Modal.window({
    title: `【${label}】帮助提示`,
    width: help.width,
    height: help.height,
    content() {
      return help.html ? <div class="column-help-content" v-html={help.message} /> : help.message;
    },
  });
}

/**
 * 批量修改
 */
function showBatchEditer({ column, field, $func }) {
  const $grid = $func.getFuncGrid();
  // 1. 检查是否有新增或未保存的数据
  const changes = $grid.store.getChanges(true);
  if (isNotEmpty(changes)) {
    Modal.alert('列表有数据变动，请先保存再进行批量修改！');
    return;
  }

  // 2. 生成数据
  const { component, props } = parseColumnFieldProps({ field, $func });
  const data = { [field.name]: '' };
  // 2.1 处理带值配置
  if (
    [
      FuncFieldTypeEnum.SELECT.component,
      FuncFieldTypeEnum.INPUT_SELECT_GRID.component,
      FuncFieldTypeEnum.INPUT_SELECT_TREE.component,
      FuncFieldTypeEnum.INPUT_SELECT_USER.component,
    ].includes(component)
  ) {
    // 增加查询选择字段带值属性
    const config = parseConfigInfo({ configInfo: field.configInfo });
    config.targetFields.forEach((targetField) => {
      data[targetField] = '';
    });
  }
  // 2.2 生成model数据
  const model = reactive(data);
  let parentModel = {};
  if ($func && isNotEmpty($func.parentFunc)) {
    parentModel = $func.parentFunc.store.activeBean || {};
  }
  Object.assign(props, {
    model,
    parentModel,
    name: field.name,
    style: { width: '100%' },
    ...useModelValue({ model, key: field.name }),
  });

  // 3. 打开编辑窗口
  const state = reactive({
    type: 'query',
    message: '',
  });
  Modal.window({
    title: `【${column.title}】数据修改`,
    width: 450,
    minWidth: 450,
    height: 220,
    minHeight: 220,
    content: () =>
      h('div', { class: '' }, [
        h(resolveComponent(component), props),
        h(resolveComponent(FuncFieldTypeEnum.RADIO_GROUP.component), {
          options: [
            { label: '列表筛选后的全部数据', value: 'query' },
            { label: '列表选中的数据', value: 'select' },
          ],
          value: state.type,
          'onUpdate:value': function (val) {
            state.type = val;
          },
          style: {
            width: '100%',
            'justify-content': 'center',
            paddingTop: '10px',
          },
        }),
        h(
          'div',
          { style: 'color:red;line-height: 18px;height: 18px;text-align:center;' },
          state.message,
        ),
      ]),
    okButton: {
      closable: false,
      handler: ({ $modal }) => {
        if (isEmpty(model[field.name])) {
          state.message = '请输入修改的数据！';
          return;
        }
        state.message = '';
        $func.action
          .doGridBatchModify({
            type: state.type,
            bean: model,
            ids: $grid.getSelectedIds(),
            j_query: $grid.getQueryParser().toJQuery().j_query,
          })
          .then(() => {
            Modal.message('更新成功！');
            $modal.close();
          })
          .catch((error) => {
            state.message = error?.message;
          });
      },
    },
    cancelButton: true,
    buttonAlign: 'center',
  });
}
