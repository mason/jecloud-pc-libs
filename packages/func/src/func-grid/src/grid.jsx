import { defineComponent } from 'vue';
import { Grid, Toolbar, Input } from '@jecloud/ui';
import { useGrid } from './hooks/use-grid';

export default defineComponent({
  name: 'JeFuncGrid',
  components: { Toolbar, Grid, Input },
  inheritAttrs: false,
  props: {
    funcCode: String,
    multiple: { type: Boolean, default: undefined },
    product: String,
    orders: Array,
    querys: Array,
    autoLoad: { type: Boolean, default: true },
    storeConfig: Object,
    pageSize: Number,
    enableSort: Boolean,
    fkCode: String,
    readonly: Boolean,
    type: {
      type: String,
      default: 'func',
      validator: (value) => ['func', 'select'].includes(value),
    },
  },
  emits: ['rendered'],
  setup(props, context) {
    const { attrs, slots } = context;
    const { $plugin, store, emtpySlot, gridAttrs, footerMethod } = useGrid({
      props,
      context,
    });
    return () => (
      <Grid
        class="je-grid-func"
        ref={$plugin}
        store={store}
        idProperty={store.idProperty}
        bodyBorder
        footer-method={footerMethod}
        {...gridAttrs}
        {...attrs}
        v-slots={{ empty: emtpySlot, ...slots }}
      />
    );
  },
});
