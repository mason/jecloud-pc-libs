import { inject, provide } from 'vue';

export const FuncGridKey = Symbol('jeFuncGridKey');

export const useProvideFuncGrid = (state) => {
  provide(FuncGridKey, state);
};

export const useInjectFuncGrid = () => {
  return inject(FuncGridKey, null);
};
