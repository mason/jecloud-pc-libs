import { ref, h, resolveDynamicComponent } from 'vue';
import {
  FuncChildrenLayoutEnum,
  FuncRefEnum,
  useFuncCommon,
  useInjectFunc,
  useProvideFuncEdit,
} from '../../../func-manager';
export function useFuncEdit({ props, context }) {
  const { expose } = context;
  const loading = ref(true);
  const hiddenTabs = ref(false);
  const funcCode = props.funcCode;
  const $func = useInjectFunc();
  // 表单对象
  const form = {
    code: funcCode,
    title: '详细信息',
    component: 'JeFuncForm',
    ref: ref(),
    events: $func.action.bindFormEvents({ defaultEvents: {} }),
  };
  // 子功能插槽
  const childrenSlot = () => {
    const funcData = $func.getFuncData();
    // 表单标题设置
    const { title } = funcData.info.formOptions;
    form.title = title || form.title;
    form.readonly = $func.store.readonly;

    // 布局
    const isGroupLayout = funcData.info.childLayout === FuncChildrenLayoutEnum.FORM_INNER_GROUP;
    const layout = isGroupLayout
      ? FuncChildrenLayoutEnum.FORM_INNER_GROUP
      : FuncChildrenLayoutEnum.FORM_OUTER_HORIZONTAL;
    // 组件
    const component = isGroupLayout ? 'JeFuncChildrenGroup' : 'JeFuncChildrenHorizontal';
    return h(resolveDynamicComponent(component), {
      form,
      layout,
      funcCode: props.funcCode,
      hiddenTabs: hiddenTabs.value,
      activeItem: activeItem.value,
      'onUpdate:activeItem': function (val) {
        activeItem.value = val;
      },
    });
  };
  // 当前激活面板
  const activeItem = ref(form.code);
  // 编辑页面
  const $edit = {
    ...useFuncCommon({ props, context, funcCode }),
    activeItem,
    setTabsVisible(visible) {
      hiddenTabs.value = !visible;
    },
    setActiveItem(code) {
      activeItem.value = code;
    },
    load(reload) {
      // 解析功能数据
      loading.value = true;
      // 重新加载功能权限
      $func.perm.loadFuncPerm().then(() => {
        // 重新加载，不需要缓存，从新load功能数据
        $func.loadFuncData(reload).then((funcData) => {
          loading.value = false;
          activeItem.value = form.code;
          // 处理表单数据刷新
          $func.getBeanByRow($func.store.activeBean).then((bean) => {
            $func.store.setActiveBean(bean);
          });
          return funcData;
        });
      });
    },
    reload() {
      return $edit.load(true);
    },
    getFuncData() {
      return $func.getFuncData();
    },
  };
  // 暴露对象
  expose($edit);
  useProvideFuncEdit($edit);

  $edit.setRefMaps(FuncRefEnum.FUNC_FORM, form.ref);
  $func.setRefMaps(FuncRefEnum.FUNC_FORM, form.ref);

  return { $edit, hiddenTabs, loading, activeItem, childrenSlot };
}
