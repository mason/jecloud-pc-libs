import { Panel } from '@jecloud/ui';

export function useChildTree(options) {
  options;
  const childTreeSlot = () => {
    return (
      <Panel.Item region="lbar" hidden>
        树形导航
      </Panel.Item>
    );
  };

  return { childTreeSlot };
}
