import { defineComponent } from 'vue';
import { Panel, Loading } from '@jecloud/ui';
// import { useChildNavbar } from './hooks/use-child-navbar';
import { useFuncEdit } from './hooks/use-func-edit';

export default defineComponent({
  name: 'JeFuncEdit',
  inheritAttrs: false,
  props: {
    funcCode: String,
  },
  setup(props, context) {
    const { attrs } = context;
    const { $edit, loading, childrenSlot } = useFuncEdit({
      props,
      context,
    });
    // 导航条
    // const { childNavbarSlot } = useChildNavbar({ $edit });
    // 加载功能
    $edit.load();

    return () => {
      return loading.value ? (
        <Loading loading={loading.value} />
      ) : (
        <Panel class="je-func-edit" {...attrs}>
          {/*  导航条  */}
          {/* {childNavbarSlot()} */}
          <Panel.Item class="je-func-edit-body">
            {/* 表单外横向展示，自动增加表单组件 */}
            {childrenSlot()}
          </Panel.Item>
        </Panel>
      );
    };
  },
});
