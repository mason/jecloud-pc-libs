import { withInstall } from '../utils';
import JeFuncForm from './src/form';
import JeFuncFormItem from './src/form-item';

JeFuncForm.installComps = {
  // 注册依赖组件
  Item: { name: JeFuncFormItem.name, comp: JeFuncFormItem },
};

export const FuncForm = withInstall(JeFuncForm);

export default FuncForm;
