import { ref, watch, nextTick } from 'vue';
import { Form, Hooks } from '@jecloud/ui';
import { pick } from '@jecloud/utils';
import {
  useProvideFuncForm,
  useWatchFieldExps,
  useInjectFunc,
  FuncFormView,
} from '../../../func-manager';

export function useForm({ props, context }) {
  const formBody = ref();
  const { expose } = context;
  const $plugin = ref();
  const $func = useInjectFunc();
  const $form = new FuncFormView({
    $func,
    props,
    context,
    pluginRef: $plugin,
  });

  // 字段ref对象
  const refFields = {};
  $form.mixin({
    getFormBody() {
      return formBody.value?.$el;
    },
    getFields4Ref: (name) => {
      return name ? refFields[name] : refFields;
    },
    addFields4Ref: (name, fieldRef) => {
      refFields[name] = fieldRef;
    },
    // 继承plugin方法
    ...Hooks.useExtendMethods({ plugin: $plugin, keys: Form.MethodKeys }),
  });

  expose($form);
  useProvideFuncForm($form);

  // 字段
  const fields = ref(props.fields);
  // 列数
  const cols = ref(props.cols);
  // 锚点数据
  const anchors = ref([]);
  // 表单配置
  const formProps = { wrapperStyle: {}, style: {}, mainStyle: {} };

  // 功能表单
  if (props.funcCode) {
    const funcData = $func.getFuncData();
    const { formOptions } = funcData.info;
    fields.value = funcData.getFields('form');
    anchors.value = $func.getFormAnchors();
    cols.value = formOptions.cols ?? 3;
    // 标签宽度
    if (formOptions.labelWidth) {
      formProps.labelCol = { style: { width: `${formOptions.labelWidth}px` } };
    }
    // 表单宽度
    if (formOptions.width) {
      if (formOptions.width.endsWith('%')) {
        formProps.wrapperStyle.width = formOptions.width;
        // 最小宽度
        if (formOptions.minWidth) {
          const minWidth = formOptions.minWidth + 'px';
          formProps.wrapperStyle.minWidth = minWidth;
          formProps.style.minWidth = minWidth;
        }
      } else {
        const width = `${formOptions.width}px`;
        formProps.wrapperStyle.width = width;
        formProps.style.minWidth = width;
      }
    } else {
      formProps.style.display = 'inherit';
    }

    // 背景色
    if (formOptions.backgroundColor) {
      formProps.mainStyle.backgroundColor = formOptions.backgroundColor;
    }

    Object.assign(formProps, pick(formOptions, ['size']));

    // 注册字段信息
    const reactiveFields = parseFieldInfo(fields.value);
    Object.keys(reactiveFields).forEach((key) => {
      $form.addField(key, reactiveFields[key]);
    });

    // 监听字段表达式
    const { watchFieldExps } = useWatchFieldExps({ $func });
    watchFieldExps();

    // 注册reset方法
    $form.mixin({
      reset() {
        // 初始化表单数据
        if ($func.isFuncForm) {
          $func.getFormInitData().then((bean) => {
            // 流程默认只读，防止出现闪动效果
            if (funcData.info.enableWorkflow) {
              if (!['', 'NOSTATUS'].includes(bean.SY_AUDFLAG)) {
                $form.setReadOnly(true);
              }
            }
            nextTick(() => {
              $func.store.setActiveBean(bean);
            });
          });
        }
      },
    });
  }
  watch(
    () => props.readonly,
    () => {
      $form.setReadOnly(props.readonly);
    },
  );

  return { $form, $func, $plugin, fields, cols, formProps, anchors, formBody };
}

/**
 * 解析字段配置信息
 * @param {*} fields
 * @returns
 */
function parseFieldInfo(fields) {
  const info = {};
  fields?.forEach?.((item) => {
    item.name && (info[item.name] = item);
    Object.assign(info, parseFieldInfo(item.children));
  });
  return info;
}
