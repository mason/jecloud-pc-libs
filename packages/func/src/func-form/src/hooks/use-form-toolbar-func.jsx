import { Toolbar, Button, Popover } from '@jecloud/ui';
import { pick, isNotEmpty, createElement } from '@jecloud/utils';
import { useInjectFunc, useInjectFuncEdit } from '../../../func-manager';
import { useFuncConfig } from '../../../func-config/hooks/use-func-config';
import { useFuncWorkflow } from '../../../func-workflow/hooks';
import { useStarButton } from './use-form-star-button';
import { useHelpButton } from '../../../func-data/src/hooks/use-buttons';

/**
 * 功能表单工具条
 * @param {*} param0
 * @returns
 */
export function useFuncToolbar({ $form }) {
  const $func = useInjectFunc();
  const funcData = $func.getFuncData();
  const { hiddenTbar } = funcData.info.formOptions;
  const { configMenuSlot } = usefuncConfig({ $form, $func });
  const { workflowButtonSlot, workflowMainButtonSlot } = useFuncWorkflow({ $func, $form });
  const { starButtonSlot } = useStarButton({ $func, $form });
  const formButtonSlot = useFormButtons({ $form, $func });
  const helpButtonSlot = useHelpButton($func);
  const tbarSlot = function () {
    return hiddenTbar ? null : (
      <Toolbar class="je-form-func-tbar">
        {/* 表单按钮 */}
        {formButtonSlot()}
        {/* 流程按钮 */}
        {workflowButtonSlot()}
        <Toolbar.Fill />
        {/* 收藏 */}
        {starButtonSlot()}
        {/* 流程追踪 */}
        {workflowMainButtonSlot()}
        {/* 帮助说明 */}
        {helpButtonSlot()}
        {/* 添加功能配置菜单 */}
        {configMenuSlot()}
      </Toolbar>
    );
  };
  return tbarSlot;
}
/**
 * 表单按钮
 * @param {*} param0
 * @returns
 */
function useFormButtons({ $form, $func }) {
  const { bindButtonEvents } = $func.action;
  const buttons = Object.values($form.getButtons());
  const groupButtons = []; // 依附按钮
  buttons.forEach((button) => {
    if (isNotEmpty(button.children)) {
      groupButtons.push(...button.children);
    }
  });

  // 菜单按钮事件
  const menuButtons = groupButtons;
  const menuBtnEvents = {};
  const menuClick = ({ key }) => {
    let events = menuBtnEvents[key];
    if (!events) {
      const button = menuButtons.find((button) => button.code === key);
      events = menuBtnEvents[key] = bindButtonEvents({ type: 'form', button });
    }
    events.onClick?.();
  };
  return () => {
    const visibleButtons = funcButtonPerm($func, buttons);
    return visibleButtons.map((button) => {
      return (
        <Button.Group>
          {button.code == 'formSaveBtn' ? (
            useFormSaveButton({ button, $form, $func })
          ) : (
            <Button
              type="primary"
              data-code={button.code}
              style={button.children?.length > 0 ? 'padding-right:4px;' : ''}
              loading={button.loading}
              {...pick(button, ['icon', 'iconColor', 'bgColor', 'fontColor', 'borderColor'])}
              {...bindButtonEvents({ type: 'form', button: button })}
            >
              {button.text}
            </Button>
          )}
          {/* 依附按钮 */}
          {button.children?.length > 0 ? (
            <Dropdown
              trigger={['click']}
              v-slots={{
                overlay: () => (
                  <Menu onClick={menuClick}>
                    {button.children.map((item) => {
                      return (
                        <Menu.Item icon={item.icon} key={item.code} style="padding-right:17px;">
                          {item.text}
                        </Menu.Item>
                      );
                    })}
                  </Menu>
                ),
              }}
            >
              <Button
                type="primary"
                icon="fal fa-chevron-down"
                style="padding-left:4px;padding-right:10px;"
                {...pick(button, ['iconColor', 'bgColor', 'fontColor', 'borderColor'])}
              />
            </Dropdown>
          ) : null}
        </Button.Group>
      );
    });
  };
}
/**
 * 表单保存按钮
 * @param {*} param0
 * @returns
 */
function useFormSaveButton({ button, $form, $func }) {
  const { bindButtonEvents } = $func.action;
  // 关闭窗体
  const onClose = () => {
    $func.store.formError.popoverVisible = false;
  };
  // 获得字段的label
  const getFieldLable = (code) => {
    return $form.getFields(code)?.label || code;
  };
  // 定位
  const scrollTo = (el) => {
    const fieldCode = el.target.getAttribute('data-code');
    const target = createElement($form.getFormBody());
    const targetWrapper = target.up('.je-form-func-wrapper');
    const item = target.down(`[data-code=${fieldCode}]`);
    if (item) {
      targetWrapper.scrollBy(item);
      item.highlight();
    }
  };
  return (
    <Popover
      overlayClassName="je-form-popover-error"
      placement="bottomLeft"
      v-model:visible={$func.store.formError.popoverVisible}
      title="操作前满足以下条件"
      overlayStyle={{ width: '260px', maxHeight: '400px' }}
      trigger="contextmenu"
      v-slots={{
        content() {
          return (
            <div class="je-form-popover-error-div">
              <div class="je-form-popover-error-div-content">
                {$func.store.formError.errorFields.map((item) => {
                  return (
                    <div class="je-form-popover-error-div-content-item">
                      <div class="je-form-popover-error-div-content-item-filed">
                        {getFieldLable(item.name[0])}
                      </div>
                      <div
                        class="je-form-popover-error-div-content-item-error"
                        data-code={item.name[0]}
                        onClick={scrollTo}
                      >
                        <div class="je-form-popover-error-div-content-item-error-icon"></div>
                        {item.errors[0]}
                      </div>
                    </div>
                  );
                })}
              </div>
              <div class="je-form-popover-error-div-button" onClick={onClose}>
                关闭提示
              </div>
            </div>
          );
        },
      }}
    >
      <Button
        type="primary"
        data-code={button.code}
        style={button.children?.length > 0 ? 'padding-right:4px;' : ''}
        loading={button.loading}
        {...pick(button, ['icon', 'iconColor', 'bgColor', 'fontColor', 'borderColor'])}
        {...bindButtonEvents({ type: 'form', button: button })}
      >
        {button.text}
      </Button>
    </Popover>
  );
}
/**
 * 功能配置按钮
 * @param {*} param0
 * @returns
 */
function usefuncConfig({ $form, $func }) {
  const { type } = $form;
  const funcEditContext = useInjectFuncEdit();
  const { configMenuSlot } = useFuncConfig({
    $func,
    parent: funcEditContext,
    disabled: type !== 'func',
  }); // 功能配置菜单
  return { configMenuSlot };
}

/**
 * 功能表单按钮权限
 */
function funcButtonPerm($func, buttons) {
  const { isFuncForm } = $func;
  // 有效按钮，功能表单去除返回按钮
  let visibleButtons = buttons.filter(
    (button) => !button.hidden && (!$func.isFormBackButton(button.code) || !isFuncForm),
  );
  // 系统权限

  // 显示按钮
  visibleButtons = visibleButtons.filter((button) => !button.hidden);

  return visibleButtons;
}
