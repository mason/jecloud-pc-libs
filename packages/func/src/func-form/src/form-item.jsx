import { defineComponent, ref, getCurrentInstance } from 'vue';
import { useFuncFormItem } from './hooks/use-form-item';

export default defineComponent({
  name: 'JeFuncFormItem',
  inheritAttrs: false,
  props: {
    field: Object,
    colSpan: { type: Number, default: 1 },
    model: Object,
    form: Object,
    hidden: Boolean,
  },
  setup(props, context) {
    const { expose } = context;
    const $plugin = ref();
    const $item = {
      instance: getCurrentInstance(),
      props,
      context,
      $plugin,
    };
    expose($item);
    const { itemSlot, itemAttrs } = useFuncFormItem({ props, context });
    return () => {
      return props.hidden ? null : (
        <div class={['je-form-func-item', props.field?.cls]} {...itemAttrs}>
          {itemSlot()}
        </div>
      );
    };
  },
});
