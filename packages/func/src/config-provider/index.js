import { withInstall } from '../utils';
import JeFuncConfigProvider from './src/config-provider';

const globalConfig = {
  // appConfig Vue全局对象
};
export const setup = (config) => {
  Object.assign(globalConfig, config ?? {});
};
/**
 * 获取系统配置
 * @param {String} key 属性值
 * @param {String} defaultVal 默认值
 * @returns 配置信息
 */
export const getGlobalConfig = (key, defaultVal) => {
  return globalConfig[key] ?? defaultVal;
};

Object.assign(JeFuncConfigProvider, {
  setup,
  getGlobalConfig,
});

export const ConfigProvider = withInstall(JeFuncConfigProvider, (app) => {
  setup({ appContext: app });
});

export default ConfigProvider;
