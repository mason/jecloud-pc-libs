import { h, ref, resolveDynamicComponent, watch } from 'vue';
import { isString, isNotEmpty } from '@jecloud/utils';
import { Panel, Tabs } from '@jecloud/ui';
import { FuncRefEnum } from '../../../func-manager';
import { useFuncTree } from './use-func-tree';

export function useFuncRenderer({ props, context, $func }) {
  const { slots, attrs } = context;
  const { funcCode, store } = $func;
  const { funcTreeSlot } = useFuncTree({ $func, props, editable: true }); // 功能树
  const children = [
    { code: FuncRefEnum.FUNC_DATA, component: 'JeFuncData', ref: ref() },
    { code: FuncRefEnum.FUNC_EDIT, component: 'JeFuncEdit', ref: ref() },
  ];
  children.forEach((child) => {
    $func.setRefMaps(child.code, child.ref);
  });
  // 设置渲染标记
  watch(
    () => store.activeView,
    () => {
      children.find((item) => item.code === store.activeView).rendered = true;
    },
    { immediate: true },
  );

  // 功能插槽
  const funcSlot = function () {
    return children.map((child) =>
      store.activeView === child.code || child.rendered
        ? h(resolveDynamicComponent(child.component), {
            funcCode,
            key: child.code,
            ref: child.ref,
            style: { display: store.activeView === child.code ? undefined : 'none' },
          })
        : null,
    );
  };
  // 功能标题插槽
  const titleSlot = function () {
    const funcData = $func.getFuncData();
    return props.title ? (
      <Panel.Item class="je-func-title" region="top">
        <div class="je-func-title-content">
          {slots.title?.({ $func }) ?? [
            <i class={funcData.info.icon} />,
            isString(props.title) ? props.title : funcData.info.funcName,
          ]}
        </div>
      </Panel.Item>
    ) : null;
  };

  // 功能面板插槽
  const funcPanelSlot = function () {
    return (
      <Panel class={['je-func', funcCode]} data-func={funcCode} {...attrs}>
        {/* {titleSlot()} */}
        {funcTreeSlot()}
        <Panel.Item class="je-func-body">{funcSlot()}</Panel.Item>
      </Panel>
    );
  };
  const tabActiveKey = ref($func.funcCode);

  // 功能布局插槽
  const funcLayoutSlot = function () {
    const funcData = $func.getFuncData();
    const siblingFuncConfig = $func.getSiblingFuncConfig();
    return siblingFuncConfig.length > 0 ? (
      <Tabs v-model:activeKey={tabActiveKey.value} class="je-func-tabs">
        (
        <Tabs.TabPane
          key={funcData.funcCode}
          v-slots={{
            tab() {
              return (
                <div>
                  {isNotEmpty(funcData.icon) ? (
                    <i class={funcData.icon} style="margin-right:5px" />
                  ) : null}
                  {funcData.funcName}
                </div>
              );
            },
            default() {
              return funcPanelSlot();
            },
          }}
        />
        )
        {siblingFuncConfig.map((sibling) => {
          return sibling.hidden ? null : (
            <Tabs.TabPane
              key={sibling.code}
              v-slots={{
                tab() {
                  return (
                    <div style={{ color: sibling.titleColor }}>
                      {isNotEmpty(sibling.icon) ? (
                        <i class={sibling.icon} style="margin-right:5px" />
                      ) : null}
                      {sibling.title}
                    </div>
                  );
                },
                default() {
                  return h(resolveDynamicComponent('JeFunc'), {
                    funcCode: sibling.code,
                    readonly: sibling.readonly,
                    isFuncForm: sibling.isFuncForm,
                    loadFirstData: sibling.isFuncForm,
                    isChildFunc: true,
                    ...sibling.events,
                  });
                },
              }}
            />
          );
        })}
      </Tabs>
    ) : (
      funcPanelSlot()
    );
  };

  return { funcSlot, titleSlot, funcTreeSlot, funcLayoutSlot };
}
