import {
  setFuncManager,
  useFuncManager,
  useInjectMainFunc,
  useProvideMainFunc,
} from '../../../func-manager';
export function useFunc({ funcCode, props, context }) {
  // 功能管理器
  const $func = useFuncManager({ funcCode, props, context });
  // 设置主应用
  if (!useInjectMainFunc()) {
    useProvideMainFunc($func);
  }
  setFuncManager($func);
  return { $func };
}
