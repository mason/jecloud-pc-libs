import { computed, ref, watch } from 'vue';
import { FuncTreeTypeEnum, FuncTypeEnum, FuncRefEnum } from '../../../func-manager';
import { useFuncTreeEvents } from '../../../func-manager';
import { Panel, Menu } from '@jecloud/ui';
import { execScript4Return, isNotEmpty, toTemplate, isEmpty } from '@jecloud/utils';
import FuncTree from '../../../func-tree';
/**
 * 功能树
 * @param {*} param0
 * @returns
 */
export function useFuncTree({ $func, props }) {
  const { funcCode, loading, isFuncForm } = $func;
  const querys = $func.store.initQuerys;
  const $tree = ref();
  // 绑定re对象
  $func.setRefMaps(FuncRefEnum.FUNC_TREE, $tree);

  // 表单页面，不做处理
  if (isFuncForm) {
    return { funcTreeSlot: () => null };
  }
  // 事件
  const { onTreeSelectChange, onTreeActionClick, onTreeMove, onTreeBeforeMove } = useFuncTreeEvents(
    {
      $func,
    },
  );
  const hidden = computed(() => {
    // 禁用检索模式 && 编辑面板
    return !$tree.value?.singleMode && $func.store.activeView === FuncRefEnum.FUNC_EDIT;
  });
  watch(
    () => $tree.value?.singleMode,
    (mode) => {
      // 切换功能面板
      $func.store.activeView = mode ? FuncRefEnum.FUNC_EDIT : FuncRefEnum.FUNC_DATA;
      // 启用检索模式，切换到编辑面板，并检索数据
      // 如果从多选模式切换过来，清空数据，让用户自行选择
      if (mode) {
        onTreeSelectChange({
          records: $tree.value.multiple ? [] : $tree.value?.getSelectedRecords(),
        });
      }
    },
  );

  // 树形懒加载，树形功能树，默认懒加载
  const lazyRendered = ref(false);
  const treeCollapsed = ref(true);
  const watchLazy = watch(
    () => treeCollapsed.value,
    (collapsed) => {
      if (!collapsed && !lazyRendered.value) {
        lazyRendered.value = true;
        // 取消监听
        watchLazy();
      }
    },
  );
  // 功能数据加载完毕，再进行懒加载处理
  watch(
    () => loading.value,
    (nv) => {
      // 初始
      const funcData = $func.getFuncData();
      if (!nv && funcData) {
        let { lazy } = funcData.info.treeQueryOptions;
        lazyRendered.value = !lazy;
        treeCollapsed.value = lazy;
      }
    },
  );
  /**
   * 树形功能树插槽
   * @returns
   */
  const funcTreeSlot = () => {
    const funcData = $func.getFuncData();
    const { treeQueryOptions, funcType, treeOptions } = funcData.info;
    const { title, width, helpMessage, enableSearchMode, closeDragSort } = treeQueryOptions;
    const treeWidth = width || 230;
    const treeTitle = title || '数据结构';
    const checkboxConfig = { checkStrictly: true, cascadeParent: false, cascadeChild: true };
    // 如果是树形功能并且不是树形展示方式
    return funcType === FuncTypeEnum.TREE && treeOptions.showType != 'tree' ? (
      <Panel.Item
        region="left"
        split
        collapsible
        hidden={hidden.value}
        size={treeWidth}
        v-model:collapsed={treeCollapsed.value}
      >
        {lazyRendered.value ? (
          <FuncTree
            ref={$tree}
            title={treeTitle}
            class="je-tree-func-search"
            type={FuncTreeTypeEnum.FUNC}
            funcCode={funcCode}
            querys={querys}
            async={treeOptions.lazy}
            enableSelectMode
            enableSearchMode={enableSearchMode} //可以检索
            draggable={!closeDragSort} //可以拖拽
            helpMessage={helpMessage}
            onSelectionChange={onTreeSelectChange}
            onActionClick={onTreeActionClick}
            onBeforeDrop={onTreeBeforeMove}
            checkboxConfig={checkboxConfig}
            onDrop={onTreeMove}
            v-slots={{ action: funcTreeActionSlots() }}
          />
        ) : null}
      </Panel.Item>
    ) : null;
  };

  /**
   * 树形操作action
   * @param {*} options
   * @returns
   */
  const funcTreeActionSlots = () => {
    const { bindButtonEvents } = $func.action;
    const funcData = $func.getFuncData();
    const { treeOptions } = funcData.info;
    const buttons = funcData.info.funcTreeActionButtons
      .filter((item) => {
        // 如果配置了权限依赖按钮需要按照对应的按钮权限进行过滤
        return (
          isEmpty(item.relyCode) ||
          (isNotEmpty(item.relyCode) && $func.perm.hasPerm({ type: 'button', code: item.relyCode }))
        );
      })
      .map((button) => {
        // 绑定功能脚本
        Object.assign(button, bindButtonEvents({ type: 'tree', button }));
        return button;
      });

    return treeOptions.action && buttons.length
      ? (options) => (
          <Menu
            selectable={false}
            onClick={({ key }) => {
              options.tooltip.visible = false;
              const button = buttons.find((btn) => btn.code === key);
              button?.onClick?.({ ...options, button });
            }}
          >
            {buttons
              .filter((button) => {
                // 显隐表达式
                if (button.expression) {
                  return execScript4Return(toTemplate(button.expression, options.row));
                } else {
                  return true;
                }
              })
              .map((button) => {
                return button.type === 'divider' ? (
                  <Menu.Divider />
                ) : (
                  <Menu.Item icon={button.icon} key={button.code}>
                    {button.text}
                  </Menu.Item>
                );
              })}
          </Menu>
        )
      : null;
  };

  return { funcTreeSlot };
}
