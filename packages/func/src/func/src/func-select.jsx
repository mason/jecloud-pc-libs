import { defineComponent } from 'vue';
import { Loading } from '@jecloud/ui';
import { useFuncSelect } from './hooks/use-func-select';
import FuncDataSelect from '../../func-data/src/func-data-select';
export default defineComponent({
  name: 'JeFuncSelect',
  inheritAttrs: false,
  props: {
    funcCode: String,
    selectOptions: Object,
  },
  slots: ['title'],
  setup(props, context) {
    const { attrs } = context;
    const { $func } = useFuncSelect({ props, context });
    const { loading } = $func;
    // 加载功能
    $func.loadFunc4ExcloudPerm();

    return () => {
      return loading.value ? (
        <Loading loading={loading.value} />
      ) : (
        <FuncDataSelect
          funcCode={$func.funcCode}
          selectOptions={props.selectOptions}
          class={['je-func', 'je-func-select', $func.funcCode]}
          data-func={$func.funcCode}
          {...attrs}
        />
      );
    };
  },
});
