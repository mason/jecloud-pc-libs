import { ref, getCurrentInstance } from 'vue';
/**
 * 公共方法
 * @returns
 */
export function useFuncCommon({ funcCode, props, context }) {
  const refMaps = {};
  /**
   * 获取子元素ref对象
   * @param {*} key
   * @returns
   */
  const getRefMaps = function (key) {
    // 生成ref对象
    if (key && !refMaps[key]) {
      refMaps[key] = ref();
    }
    return key ? refMaps[key] : refMaps;
  };
  /**
   * 设置子元素ref对象
   * @param {*} key
   * @returns
   */
  const setRefMaps = function (key, item) {
    refMaps[key] = item ?? ref();
  };

  return {
    funcCode,
    instance: getCurrentInstance(),
    props,
    context,
    getRefMaps,
    setRefMaps,
  };
}
