import { FuncRefEnum, FuncTypeEnum, QueryTypeEnum, FuncDataTypeEnum } from '../enum';
import { get, createDeferred, cloneDeep, Modal, Data } from '@jecloud/utils';
import { showTreeSelect } from '../../func-util';
import { doTreeMoveApi } from '../api';
export function useTreeAction() {
  return {
    doTreeSave4View,
    doTreeRemove4View,
    doTreeUpdate4View,
    doTreeMove,
    doTreeTransfer,
    doTreeSelect,
    getTreeDefaultValues,
  };
}

/**
 * 移动树形节点
 * @param {*} $func
 * @param {*} param1
 * @returns
 */
function doTreeMove($func, { id, toId, place, treeView }) {
  const { productCode, tableCode, funcCode, funcAction } = $func.getFuncData().info;
  const $grid = $func.getFuncGrid();
  const $tree = treeView || $func.getFuncTree();
  return doTreeMoveApi({
    params: { id, toId, place, tableCode, funcCode },
    action: funcAction,
    pd: productCode,
  }).then(() => {
    Modal.notice('转移成功！', 'success');
    if (!treeView) {
      $grid.setQuerys({
        type: 'tree',
        querys: $tree.getSelectedRecords() || [],
        $tree,
      });
    }
  });
}

/**
 * 树形节点转移
 * @param {*} $func
 * @param {*} options
 */
function doTreeTransfer($func, options) {
  const { row } = options;
  const deferred = createDeferred();
  const $tree = $func.getFuncTree();
  const moveNode = ({ $plugin, $modal, place }) => {
    const targetRow = $plugin.value.getSelectedRecords()[0];
    if (targetRow) {
      if (targetRow.nodePath.startsWith(row.nodePath)) {
        Modal.alert('不允许转移到自身节点以及子节点！');
        deferred.reject();
      } else {
        doTreeMove($func, { id: row.id, toId: targetRow.id, place }).then(() => {
          $modal.close();
          $tree.store.reload();
          deferred.resolve();
        });
      }
    } else {
      Modal.alert('请选择需要转移的节点！');
      deferred.reject();
    }
  };
  // 创建新的store
  const newTreeStore = Data.Store.useTreeStore({
    data: cloneDeep($tree.store.data),
  });

  showTreeSelect({
    title: '移动到选中节点',
    store: newTreeStore,
    autoLoad: false,
    buttons: [
      {
        text: '上面',
        type: 'primary',
        bgColor: '#02a863',
        closable: false,
        borderColor: '#02a863',
        handler({ $modal, $plugin }) {
          moveNode({ $modal, $plugin, place: 'above' });
        },
      },
      {
        text: '下面',
        type: 'primary',
        bgColor: '#02a863',
        borderColor: '#02a863',
        closable: false,
        handler({ $modal, $plugin }) {
          moveNode({ $modal, $plugin, place: 'below' });
        },
      },
      {
        text: '里面',
        type: 'primary',
        closable: false,
        handler({ $modal, $plugin }) {
          moveNode({ $modal, $plugin, place: 'inside' });
        },
      },
      {
        text: '取消',
        handler() {
          deferred.reject();
        },
      },
    ],
  });

  return deferred.promise;
}

/**
 * 添加树形节点
 * @param {*} param0
 * @returns
 */
function doTreeSave4View($func, { treeView, node, selectNode = false }) {
  if (!$func.isTreeFunc()) return;
  const $tree = treeView || $func.getFuncTree();
  const { funcType, treeOptions } = $func.getFuncData().info;
  // 插入节点
  if ($tree) {
    const store = $tree.store;
    const selected = $tree.getSelectedRecords()[0];
    const row = store.appendChild(node, selected);
    $tree.setTreeExpand(selected, true);
    selectNode && $tree.setSelectRow(row);
    // 树形列表需要commit数据
    if (funcType === FuncTypeEnum.TREE && treeOptions.showType == 'tree') {
      $tree.store.commitRecord(row);
    }

    return row;
  }
}
/**
 * 修改树形节点
 * @param {*} param0
 * @returns
 */
function doTreeUpdate4View($func, { treeView, nodes }) {
  if (!$func.isTreeFunc()) return;
  const $tree = treeView || $func.getRefMaps(FuncRefEnum.FUNC_TREE).value;

  //循环更新树形节点
  $tree &&
    nodes.forEach((node) => {
      const row = $tree.getRowById(node.id);
      if (row) {
        const values = {};
        Object.keys(node).forEach((key) => {
          const val = node[key];
          if (!['children'].includes(key) && val != 'null') {
            values[key] = val;
          }
        });
        // 把后台返回的数据全部赋值给row(因为后台可能会生成新的属性)
        Object.assign(row, values);
        $tree.reloadRow(row);
        $tree?.store.commitRecord(row);
      } else {
        doTreeSave4View($func, { treeView, node });
      }
    });
}
/**
 * 删除树形节点
 * @param {*} param0
 * @returns
 */
function doTreeRemove4View($func, { ids = [] }) {
  if (!$func.isTreeFunc()) return;
  const $tree = $func.getRefMaps(FuncRefEnum.FUNC_TREE).value;
  $tree?.store.remove(ids);
}

/**
 * 选中树形数据，进行下步操作
 * @param {*} param0
 * @returns
 */
function doTreeSelect($func, { row, selectGrid }) {
  if (!$func.isTreeFunc()) return Promise.resolve();

  const $tree = $func.getFuncTree();
  const treeSelectPremise = $tree.setSelectRow(row);
  if (selectGrid) {
    const $grid = $func.getRefMaps(FuncRefEnum.FUNC_GRID).value;
    $grid.clearCheckboxRow();
    return $grid.setSelectRow($grid.getRowById(row.id));
  } else {
    return treeSelectPremise;
  }
}

/**
 * 获得树形默认值
 * @param {*} $func
 * @returns
 */
function getTreeDefaultValues($func, options) {
  // 默认值
  const values = {};
  const funcData = $func.getFuncData();
  const { funcType, treeOptions } = funcData.info;
  let $tree = $func.getFuncTree();
  const treeFunc = funcType === FuncTypeEnum.TREE;
  // 如果是树形功能并且是树形展示
  if (treeFunc && treeOptions.showType == 'tree') {
    $tree = $func.getFuncGrid();
  }
  if ($tree) {
    const selections = $tree.getPlugin().getSelectedRecords() || [options?.row];
    // 树形功能
    if (treeFunc) {
      const node = selections[0] ?? $tree.store.rootNode;
      const { funcTreeConfig } = funcData.info;
      values[funcTreeConfig.parent] = node.id;
    } else {
      // 快速查询带值配置
      const dictionarys = funcData.getQuerys(QueryTypeEnum.TREE, FuncDataTypeEnum.FUNC) ?? [];
      selections.forEach((item) => {
        const { config, nodeInfo } =
          dictionarys.find((dd) => dd.enableValue && dd.nodeInfo === item.nodeInfo) ?? {};
        // 启用带值，并且不是Root节点
        if (nodeInfo && item.nodeType !== 'ROOT') {
          config.targetFields?.forEach((code, index) => {
            const nodeField = config.sourceFields[index];
            values[code] = get(item, nodeField);
          });
        }
      });
    }
  } else if (treeFunc) {
    // 树形功能，默认ROOT
    const { funcTreeConfig } = funcData.info;
    values[funcTreeConfig.parent] = 'ROOT';
  }

  return values;
}
