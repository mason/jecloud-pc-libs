import { FuncChildrenLayoutEnum } from '../enum';
import { isNotEmpty } from '@jecloud/utils';

/**
 * 扩展action
 * @returns
 */
export function useExtendAction() {
  return { doFormSaveAfter, doChildSave };
}
/**
 * 表单保存后业务处理
 * @param {*} $func
 * @param {*} param1
 */
function doFormSaveAfter($func, { insert, bean, node }) {
  // 弹出表单处理
  const $baseFunc = $func.getParams()?.$baseFunc || $func;
  if (insert) {
    // 树形添加
    node && $baseFunc.action.doTreeSave4View({ node });
    // 绑定row
    const row = $baseFunc.action.doGridSave4View({ record: bean, node });
    $baseFunc.store.setActiveRow(row);
  } else {
    // 树形修改
    node && $baseFunc.action.doTreeUpdate4View({ nodes: [node] });
    // 更新列表数据
    $baseFunc.action.doGridUpdate4View({ records: [bean], nodes: [node] });
  }
  // 刷新统计数据
  $baseFunc.getFuncGrid()?.store.loadStatistics();
}

/**
 * 保存子功能数据
 * @param {*} $func
 */
function doChildSave($func) {
  const childFunc = $func?.getChildFunc();
  if (isNotEmpty(childFunc)) {
    Object.keys(childFunc).forEach((Childkey) => {
      const childItem = childFunc[Childkey];
      // 排除子功能集合
      if (!childItem.isFuncField) {
        const funData = childItem.getFuncData();
        const { childLayout } = funData?.info;
        // 子功能是 表单(内部)横向显示/表单(内部)纵向显示
        if (
          [
            FuncChildrenLayoutEnum.FORM_INNER_HORIZONTAL,
            FuncChildrenLayoutEnum.FORM_INNER_VERTICAL,
          ].includes(childLayout)
        ) {
          const childGrid = childItem.refMaps?.funcGrid?.value;
          const changes = childGrid?.store.getUpdatedRecords() || [];
          const childGridSaveBtn = childGrid?.getButtons('gridUpdateBtn');
          // 子功能列表有数据改变,并且列表上有保存按钮
          if (changes.length > 0 && childGridSaveBtn && !childGridSaveBtn?.hidden) {
            childGridSaveBtn.click();
          }
        }
      }
    });
  }
}
