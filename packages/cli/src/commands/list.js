#!/usr/bin/env node
const { showTable, getAllTemplates } = require(`../util`);

module.exports = () => {
  showTable(getAllTemplates());
};
