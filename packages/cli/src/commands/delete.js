const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const inquirer = require('inquirer');
const cusRegs = require('../json/customregist.json');
const symbols = require('log-symbols');
const { getSelectTemplates, showTable, emptyTemplatesLog } = require('../util');

function onDel() {
  const choices = getSelectTemplates(cusRegs);
  if (choices.length === 0) {
    emptyTemplatesLog();
    return;
  }
  inquirer
    .prompt([
      {
        type: 'list',
        name: 'projectTemplate',
        message: '请选择要删除的本地项目模板?',
        choices,
      },
      {
        type: 'confirm',
        name: 'choice',
        message: '确定删除吗:',
        default: true,
      },
    ])
    .then((answers) => {
      const { projectTemplate, choice } = answers;
      if (choice) {
        let newJson = {};
        Object.keys(cusRegs).forEach((key) => {
          if (key !== projectTemplate) {
            newJson[key] = cusRegs[key];
          }
        });
        const cliPath = path.join(__dirname, '/../json/customregist.json');
        fs.writeFileSync(cliPath, JSON.stringify(newJson, null, '\t'));
        console.log(chalk.green(symbols.success), chalk.green(`删除成功，请查看最新模板列表：\n`));
        showTable(newJson);
      } else {
        process.exit();
      }
    });
}

module.exports = onDel;
