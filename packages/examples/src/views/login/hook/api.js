import { ajax, transformAjaxData } from '@jecloud/utils';
/**
 * 登录
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export const API_RBAC_LOGIN = '/je/rbac/cloud/login/accountLogin';
export function doLogin(params) {
  return ajax({ url: API_RBAC_LOGIN, params: params }).then(transformAjaxData);
}
