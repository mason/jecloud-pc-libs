import { defineComponent } from 'vue';
import { useDraggable } from './hooks/use-draggable';
import { Col } from 'ant-design-vue';

export default defineComponent({
  name: 'DargContainerField',
  components: { Col },
  inheritAttrs: false,
  props: {
    data: {
      type: Object,
      default() {
        return {};
      },
    },
    span: { type: Number, default: 8 },
  },
  setup(props) {
    const { dragClass, removeFn } = useDraggable({ owner: 'field', props });

    return () => (
      <Col span={props.span} key={props.data.id} data-id={props.data.id} data-type="field">
        <div class={['container-field', dragClass]}>
          {props.data.text}
          <div class="actions">
            <i class="fal fa-times remove-icon" onClick={removeFn}></i>
          </div>
        </div>
      </Col>
    );
  },
});
