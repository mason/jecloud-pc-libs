export const dragClass = 'is--draggable';
export const dragGroup = 'je-form-design';
export const draggableClass = `${dragGroup}-draggable-item`;
export const dragGhostClass = `${dragGroup}-sortable-ghost`;
export const provideDrag = dragGroup;
export const columns = [1, 2, 3, 4, 6, 12, 24].map((item) => ({ value: item, label: item }));
