import { defineComponent, computed, ref } from 'vue';
import { useDraggable } from './hooks/use-draggable';
import { Row, Col } from 'ant-design-vue';
import { Select } from '@jecloud/ui';
import { columns as columnOptions } from './hooks/config';
export default defineComponent({
  name: 'DargContainerGroup',
  components: { Row, Col, Select },
  inheritAttrs: false,
  props: {
    data: {
      type: Object,
      default() {
        return {};
      },
    },
    columns: { type: Number, default: 1 },
    span: { type: Number, default: 8 },
  },
  setup(props, { slots }) {
    const { dragEl, dragClass, removeFn } = useDraggable({ owner: 'group', props });
    const columns = ref(props.columns);
    const span = computed(() => {
      return Math.ceil(24 / columns.value);
    });

    return () => {
      const { id, text } = props.data;
      return (
        <Col span={props.span} key={id} data-id={id} data-type="group">
          <div class={['container-group']}>
            <div class={['group-header', dragClass]}>
              {text}
              <div class="actions">
                列数：
                <Select v-model:value={columns.value} size="small" options={columnOptions} />
                <i class="fal fa-times remove-icon" onClick={removeFn}></i>
              </div>
            </div>
            <Row class="group-body" gutter={[4, 4]} ref={dragEl} data-id={id}>
              {slots.default?.({ span: span.value })}
            </Row>
          </div>
        </Col>
      );
    };
  },
});
