// 加载antd国际化文件
import antdLocale from 'ant-design-vue/es/locale/zh_CN';
import { buildLocales } from '../utils';
const files = import.meta.globEager('./zh_CN/**/*.js');
const locales = {
  ...buildLocales(files),
  antdLocale, // antd
};

export default locales;
