export default {
  title: '系统登录',
  username: '用户名',
  usernamePlaceholder: '请输入用户名',
  password: '密码',
  passwordPlaceholder: '请输入密码',
  language: '语言',
  loginText: '登录',
  resetText: '重置',
  loginSuccess: '登录成功！',
};
