import { setupJE } from './je';
import { setupIi8n } from '../locales';
import { setupAjax } from './http';
import { createPinia } from 'pinia';
import { initStyles, initSystemConfig } from '@jecloud/utils';

export async function setupCommon(vue) {
  // Store
  vue.use(createPinia());
  // JE
  setupJE(vue);
  // ajax
  setupAjax();
  // I18n
  await setupIi8n(vue);
  // SystemConfig
  await initSystemConfig();
  // GlobalStyles
  await initStyles();
}
