import { menus } from './menus';

export function useRoutes() {
  const routes = parseRoutes();
  menus.forEach((root) => {
    const itemRoutes = parseRoutes(root.children, root);
    itemRoutes.length &&
      routes.push(
        {
          path: `/${root.code}`,
          redirect: itemRoutes[0].path,
          meta: { root: root.code, ...root },
        },
        ...itemRoutes,
      );
  });

  return routes;
}

function parseRoutes(children, root) {
  const routes = [];
  children?.forEach((menu) => {
    if (menu.children) {
      routes.push(...parseRoutes(menu.children, root));
    } else if (!menu.url) {
      routes.push({
        path: `/${root.code}/${menu.code}`,
        component: () => import(`../components/${root.code}/${menu.code}.vue`),
        meta: { root: root.code, ...menu },
      });
    }
  });
  return routes;
}
