import Mock from 'mockjs';
import { tableFields } from './table';
export default function () {
  const childrenData = function ({ parent, count }) {
    parent = parent ?? { text: '节点' };
    return Mock.mock({
      'children|0-10': [
        {
          'index|+1': 1,
          'id|+1': (parent.id ?? 1) * 10,
          code: function () {
            return this.id;
          },
          text: function () {
            return `${parent.text + (parent.id ? '.' : '')}${this.index}`;
          },
          parentId: parent.id,
          children: function () {
            if (count === 0) {
              return [];
            }
            return childrenData({ parent: this, count: --count });
          },
          checked: false,
          'badge|0-100': 2,
          ...tableFields,
        },
      ],
    }).children;
  };

  return childrenData({ count: 3 });
}
