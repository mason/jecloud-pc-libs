import Mock from 'mockjs';
export const tableFields = {
  'role|1': ['Develop', 'Test', 'PM', 'Designer'],
  'age|20-40': 20,
  'rate|1-5': 1,
  'flag|1-2': true,
  'type|1': ['mp3', 'mp4', 'avi', 'html'],
  'size|100-1024': 100,
  date: '@date',
};
export default function (options = {}) {
  const { page = 1, pageSize = 30, totalCount = 100 } = options;
  const count = page * pageSize - totalCount;
  const data = Mock.mock({
    [`array|${count > 0 ? pageSize - count : pageSize}`]: [
      {
        'index|+1': (page - 1) * pageSize + 1,
        'id|+1': 10000,
        'name|+1': function () {
          return 'Test' + this.index;
        },
        ...tableFields,
      },
    ],
  }).array;

  return { rows: data, totalCount };
}
