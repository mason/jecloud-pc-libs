import { defineConfig } from 'vite';
import { usePlugins, pathResolve, loadEnvs } from './build/vite/plugins';
import { configProxy } from './build/vite/proxy';
import { resolve } from 'path';

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  // 加载系统配置
  const config = loadEnvs(mode);
  const { VUE_APP_SERVICE_PORT } = config;
  // 加载插件
  const { plugins } = usePlugins(config, command);
  return {
    define: { __CLI_ENVS__: JSON.stringify(config) },
    plugins: [...plugins],
    resolve: {
      alias: {
        '@': pathResolve('src'),
        micro: pathResolve('micro'),
        'vue-i18n': 'vue-i18n/dist/vue-i18n.cjs.js',
      },
    },
    server: {
      host: '0.0.0.0',
      port: VUE_APP_SERVICE_PORT,
      proxy: configProxy(config),
      fs: {
        allow: [
          // 搜索工作区的根目录
          '../../',
        ],
      },
    },
    css: {
      preprocessorOptions: {
        less: {
          modifyVars: {
            hack: `true; @import (reference) "${resolve(
              'src/assets/themes/theme-variable.less',
            )}";`,
          },
          javascriptEnabled: true,
        },
        scss: {
          // additionalData: `@import "@/assets/theme/scss/default.scss";`,// TODO:主题调试放开
        },
      },
    },
    optimizeDeps: {
      include: [
        'ant-design-vue/es/locale/zh_CN',
        'ant-design-vue/es/locale/en_US',
        '@ant-design/icons-vue',
      ],
      exclude: ['@zougt/vite-plugin-theme-preprocessor/dist/browser-utils'],
    },
  };
});
