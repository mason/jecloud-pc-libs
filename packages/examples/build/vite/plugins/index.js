import { loadEnv } from 'vite';
import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';
import { resolve } from 'path';
import { configHtmlPlugin } from './html';
// import { configMonacoPlugin } from './monaco';
import { configStyleImportPlugin } from './style-import';
/**
 * vite 插件
 *
 * @export
 * @param {*} envs 变量
 * @return {*}
 */
export function usePlugins(envs, command) {
  const plugins = [vue(), vueJsx(), configHtmlPlugin(envs, command), configStyleImportPlugin()];
  return {
    plugins,
  };
}

/**
 * 加载系统参数
 *
 * @export
 * @param {*} mode
 * @return {*}
 */
export function loadEnvs(mode) {
  const envs = loadEnv(mode, process.cwd(), 'VUE_APP_');
  for (let key in envs) {
    let val = envs[key];
    if (isNumeric(val)) {
      envs[key] = Number(val);
    } else if (['true', 'false'].includes(val)) {
      envs[key] = val == 'true';
    }
  }
  envs.NODE_ENV = mode;
  return envs;
}

/**
 * 获得绝对路径
 *
 * @export
 * @param {*} dir
 * @return {*}
 */
export function pathResolve(dir) {
  return resolve(process.cwd(), '.', dir);
}

export function isNumeric(value) {
  return !Number.isNaN(parseFloat(value)) && Number.isFinite(parseFloat(value));
}
