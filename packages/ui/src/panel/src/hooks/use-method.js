import {
  nextTick,
  onMounted,
  onBeforeUnmount,
  onDeactivated,
  onActivated,
  ref,
  watch,
  reactive,
} from 'vue';
import {
  getPaddingSize,
  getBorderSize,
  removeResizeListener,
  addResizeListener,
  debounce,
  hasClass,
  uniqueId,
} from '@jecloud/utils';
import { useProvidePanel, useInjectPanel } from '../context';
export function useMehtod({ refMaps, props, emit, computeStyle }) {
  // 作为主面板，提供绑定子面板的元素
  const items = [];
  const panelItems = {};
  const panel = {
    id: uniqueId('panel-'),
    refMaps,
    activated: ref(true), // 激活状态
    /**
     * 刷新子面板
     */
    refreshChildPanel() {
      items.forEach((item) => {
        item?.refreshLayout?.();
      });
    },
    /**
     * 添加子面板
     * @param {*} item
     */
    addChildPanel(item) {
      items.push(item);
    },
    /**
     * 设置子项，用于splitter拖动使用
     * @param {*} region
     * @param {*} config
     */
    addPanelItem(region, panelItem) {
      panelItems[region] = panelItem;
    },
    getPanelItem(region) {
      return panelItems[region];
    },
  };
  useProvidePanel(panel);

  // 作为子面板，注册到主面板
  const $jePanel = useInjectPanel();
  const registerChildPanel = () => {
    $jePanel?.addChildPanel(panel);
  };

  // 计算尺寸
  const computedSize = (parent, posItems, center) => {
    const parentNode = parent;
    const parentPs = getPaddingSize(parentNode);
    const parentBs = getBorderSize(parentNode);
    // 中间面板初始位置和大小
    const centerBox = {
      height:
        parentNode.offsetHeight - parentPs.top - parentPs.bottom - parentBs.top - parentBs.bottom,
      width:
        parentNode.offsetWidth - parentPs.left - parentPs.right - parentBs.left - parentBs.right,
      top: 0,
      left: 0,
    };

    // 计算其他面板
    posItems.forEach((posItem) => {
      const { item, splitter, region } = posItem;
      const width = item.offsetWidth ?? 0;
      const height = item.offsetHeight ?? 0;
      const collapsible = hasClass(splitter, 'is--collapsible'); // 可收起，不可收起时，隐藏占位
      const collapsed = hasClass(splitter, 'is--collapsed'); // 收起状态
      const sWidth = 0; //collapsible && collapsed ? splitter?.offsetWidth ?? 0 : 0;
      const sHeight = 0; //collapsible && collapsed ? splitter?.offsetHeight ?? 0 : 0;
      let itemStyle = {};
      switch (region) {
        case 'top':
          centerBox.top = height + sHeight;
          centerBox.height = centerBox.height - height - sHeight;
          itemStyle = { top: 0, left: 0, width: '100%' };
          break;
        case 'bottom':
          centerBox.height = centerBox.height - height - sHeight;
          itemStyle = { left: 0, bottom: 0, width: '100%' };
          break;
        case 'left':
          centerBox.left = width + sWidth;
          centerBox.width = centerBox.width - width - sWidth;
          itemStyle = { top: `${centerBox.top}px`, left: 0, height: `${centerBox.height}px` };
          break;
        case 'right':
          centerBox.width = centerBox.width - width - sWidth;
          itemStyle = { top: `${centerBox.top}px`, right: 0, height: `${centerBox.height}px` };
          break;
      }
      Object.assign(item.style, itemStyle);
      // 分隔条
      if (splitter) {
        if (['top', 'bottom'].includes(region)) {
          Object.assign(splitter.style, {
            [region]:
              collapsible && collapsed ? `${height}px` : `${height - splitter.offsetHeight}px`,
            left: 0,
            width: '100%',
          });
        } else {
          Object.assign(splitter.style, {
            top: `${centerBox.top}px`,
            [region]: collapsible && collapsed ? `${width}px` : `${width - splitter.offsetWidth}px`,
            height: `${centerBox.height}px`,
          });
        }
      }
    });
    // 中间面板
    Object.assign(center?.style ?? {}, {
      top: `${centerBox.top}px`,
      left: `${centerBox.left}px`,
      width: `${centerBox.width}px`,
      height: `${centerBox.height}px`,
    });
  };

  // 获得区域dom
  const getPanelDomItems = (type) => {
    const parent = type === 'bars' ? refMaps.center.value : refMaps.body.value;
    const items = [];
    for (let i = 0; i < parent.children.length; i++) {
      const child = parent.children[i];
      if (hasClass(child, 'je-panel-item')) {
        const region = child.getAttribute('data-region') ?? 'default';
        if (region !== 'default') {
          let splitter = null;
          if (hasClass(child.nextSibling, 'je-panel-splitter-' + region)) {
            splitter = child.nextSibling;
          }
          items.push({
            region,
            item: child,
            splitter,
          });
        }
      }
    }
    return items;
  };

  // 刷新布局
  const refreshLayout = () => {
    nextTick(() => {
      if (!refMaps.body.value) return;

      const visible = computeStyle.value.display !== 'none';
      const bodyWidth = refMaps.body.value.offsetWidth;
      const bodyHeight = refMaps.body.value.offsetHeight;
      // 显示 && 激活状态 && 宽高必须都大于0
      if (visible && panel.activated.value && bodyWidth > 0 && bodyHeight > 0) {
        if (refMaps.center.value) {
          const defaultDom = refMaps.center.value.querySelector('.je-panel-item-region-default');
          // 子面板
          computedSize(refMaps.body.value, getPanelDomItems('items'), refMaps.center.value);
          // 工具条
          computedSize(refMaps.center.value, getPanelDomItems('bars'), defaultDom);
          emit('layout-refresh', { $panel: panel });
        }
        panel.refreshChildPanel();
      }
    });
  };
  panel.refreshLayout = refreshLayout;

  // 代理属性，兼容props为非响应式属性
  const propsReactive = reactive(props);
  // 监听属性
  Object.keys(props).forEach((key) => {
    watch(
      () => propsReactive[key],
      () => {
        emit('update:' + key, propsReactive[key]);
      },
      { deep: true },
    );
  });

  // 更新props属性
  const updateProps = (pos, key, val) => {
    if (propsReactive[pos]) {
      propsReactive[pos][key] = val;
    }
  };

  // 防止频繁刷新
  const _refreshLayout = debounce(() => {
    refreshLayout(true);
  }, 100);
  // 初始刷新布局
  onMounted(() => {
    nextTick(() => {
      refreshLayout();
      registerChildPanel();
      addResizeListener(refMaps.el.value, _refreshLayout);
    });
  });

  // 销毁resize事件
  onBeforeUnmount(() => {
    removeResizeListener(refMaps.el.value, _refreshLayout);
  });

  // keep-alive切换时触发
  onActivated(() => {
    panel.activated.value = true;
    refreshLayout();
  });
  // keep-alive切换时触发
  onDeactivated(() => {
    panel.activated.value = false;
  });

  Object.assign(panel, {
    propsReactive,
    refreshLayout,
    getPanelDomItems,
    setSize(pos, size) {
      updateProps(pos, 'size', size);
    },
    setVisible(pos, visible) {
      updateProps(pos, 'hidden', !visible);
    },
    setExpand(pos, expand) {
      updateProps(pos, 'collapsed', !expand);
    },
    collapse(pos) {
      updateProps(pos, 'collapsed', true);
    },
    expand(pos) {
      updateProps(pos, 'collapsed', false);
    },
  });
  return panel;
}
