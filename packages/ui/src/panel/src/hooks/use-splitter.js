import { hasClass, forEach, toggleClass, debounce } from '@jecloud/utils';
import { setDrawerSize } from '../config';
/**
 * 调整器
 * @param {} param
 * @returns
 */
export function useSplitter({ panel, refMaps }) {
  // 获取移动信息
  const getSplitContainerBox = function (dom, dragInfo = true) {
    let box;
    // 查找是否有效
    if (hasClass(dom, 'je-panel-splitter')) {
      forEach(panel.getPanelDomItems('items'), ({ item, splitter, region }) => {
        if (dom === splitter) {
          box = { region, container: item, splitter };
          return;
        }
      });
    }
    if (!box || !dragInfo) return;
    // 系统配置
    const panelItem = panel.getPanelItem(box.region);
    if (!panelItem.split) return;
    // 中间容器的size
    const { offsetHeight: dHeight, offsetWidth: dWidth } = refMaps.center.value;
    // 调整容器的size
    const { offsetHeight: height, offsetWidth: width } = box.container;
    // 由于collapsed是v-model，内部有修改，所有单独赋值使用
    const config = Object.assign({}, panelItem?.props, { collapsed: panelItem?.collapsed });
    // 拖动方向
    const vertical = ['top', 'bottom'].includes(box.region);
    // 最大尺寸
    const maxSize = vertical ? height + dHeight : width + dWidth;
    // 调整范围信息
    Object.assign(box, {
      panelItem,
      containerSize: vertical ? height : width,
      containerHeight: height,
      minSize: config.minSize || 0,
      maxSize: maxSize < config.maxSize ? maxSize : config.maxSize,
      vertical,
      config,
    });
    return box;
  };

  // 拖拽标记
  let currentBox = null;
  const setDragMark = debounce(function (e) {
    const dom = e.target;
    const box = getSplitContainerBox(dom);
    if (e.which === 1 && box) {
      const config = box.config;
      // 收起状态，不允许拖拽
      if (config.collapsed?.value) {
        return;
      }
      // 记录起始位置
      currentBox = Object.assign(
        {
          beginX: e.clientX,
          beginY: e.clientY,
        },
        box,
      );
      toggleClass(box.splitter, 'dragging', true);
    }
  }, 100);
  const cancelDragMark = function () {
    setDragMark.cancel();
  };

  // 鼠标放下，打开拖拽标记
  const onMousedown = function (...args) {
    setDragMark(...args);
  };

  // 鼠标离开，重置拖动标记
  const onMouseleave = function () {
    // 取消点击事件
    cancelDragMark();
    if (!currentBox) return;
    toggleClass(currentBox.splitter, 'dragging', false);
    currentBox = null;
    // 更新尺寸
    panel.refreshLayout();
  };
  // 鼠标抬起，设置尺寸，重置拖动标记
  const onMouseup = function () {
    // 取消点击事件
    cancelDragMark();
    if (!currentBox) return;
    const { moveSize = 0, containerSize, panelItem } = currentBox;
    // 更新尺寸
    panelItem.setSize(containerSize + moveSize);
    onMouseleave();
  };
  // 鼠标拖动，记录移动位置
  const onMousemove = (e) => {
    if (!currentBox) return;
    e.returnValue = false; // 拖动时禁止选中文字
    const dom = currentBox.splitter;
    const { panelItem, beginX, beginY, vertical, region, maxSize, minSize, containerSize } =
      currentBox;
    // 移动距离
    let mouseMoveSize = vertical ? e.clientY - beginY : e.clientX - beginX;
    // 下，右面板 拖动时，移动数据取反
    let moveSize = ['top', 'left'].includes(region) ? mouseMoveSize : -mouseMoveSize;
    // 拖动范围判断
    const offsetWidth = panelItem?.$el.value.offsetWidth;
    if (offsetWidth + moveSize > maxSize || offsetWidth + moveSize < minSize) {
      return false;
    }
    // 设置splitter位置
    currentBox.moveSize = moveSize;
    const regionSize = containerSize + moveSize;
    dom.style[region] = regionSize + 'px';
    setDrawerSize(panelItem?.$el.value, regionSize);
  };

  return { onMousedown, onMouseleave, onMouseup, onMousemove };
}
