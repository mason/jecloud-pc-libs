import { defineComponent, ref, computed } from 'vue';
import { computedDomSize } from '@jecloud/utils';
import { useMehtod } from './hooks/use-method';
import { useSplitter } from './hooks/use-splitter';
import { bars, items, slots as _slots, props, emits } from './config';
import { filterEmpty } from '../../utils/props';
import PanelItem from './item';
export default defineComponent({
  name: 'JePanel',
  inheritAttrs: false,
  props: props,
  slots: _slots,
  emits: [...emits, 'layout-refresh'],
  setup(props, { attrs, slots, expose, emit }) {
    const prefixCls = 'je-panel';
    const refMaps = {
      el: ref(),
      body: ref(),
      center: ref(),
      items: [],
      bars: [],
    };
    // 样式style
    const computeStyle = computed(() => {
      // 监听style，控制display显隐计算布局
      const style = { ...props.style };
      props.width && (style.width = `${computedDomSize({ size: props.width, unit: 'px' })}`);
      props.height && (style.height = `${computedDomSize({ size: props.height, unit: 'px' })}`);
      return style;
    });

    // 方法
    const panel = useMehtod({ props, slots, refMaps, emit, computeStyle });
    const $panel = panel;
    const { propsReactive } = panel;
    expose(panel);
    // 调整器
    const events = useSplitter({
      props,
      refMaps,
      panel,
    });

    const renderItem = ({ region, vnode }) => {
      // 1. 提取插槽，兼容插槽
      const slot = slots[region];
      // 2. 提取插槽配置
      const config = propsReactive[region] ?? {};
      // 3. 设置PanelItem
      // 优先级：PanelItem > slot
      if (vnode) {
        return vnode;
      } else if (slot) {
        const vnodes = filterEmpty(slot({ $panel }));
        const panelItem = vnodes.find((item) => {
          return item.type?.name === 'JePanelItem';
        });
        if (panelItem) return panelItem;
        return (
          <PanelItem {...config} region={region} v-model:collapsed={config.collapsed}>
            {vnodes}
          </PanelItem>
        );
      } else return null;
    };

    // 布局
    return () => {
      // console.log($panel.id);
      const vnodeMaps = {};
      // 提取JePanelItem
      const vnodes = filterEmpty(slots.default?.());
      vnodes.forEach((item) => {
        if (item.type?.name === 'JePanelItem') {
          vnodeMaps[item.props?.region ?? 'default'] = item;
          // 如果有panel，默认default插槽
        } else if (item.type?.name === 'JePanel') {
          vnodeMaps['default'] = <PanelItem>{item}</PanelItem>;
        }
      });
      return (
        <div
          class={prefixCls}
          ref={refMaps.el}
          {...attrs}
          {...events}
          data-id={$panel.id}
          style={computeStyle.value}
        >
          <div class="je-panel-body" ref={refMaps.body}>
            <div class="je-panel-wrapper" ref={refMaps.center}>
              {renderItem({ region: 'default', vnode: vnodeMaps.default })}
              {bars.map((item) => {
                return renderItem({ region: item, vnode: vnodeMaps[item] });
              })}
            </div>
            {items.map((item) => {
              return renderItem({ region: item, vnode: vnodeMaps[item] });
            })}
          </div>
        </div>
      );
    };
  },
});
