import { inject, provide } from 'vue';

export const PanelContextKey = Symbol('jePanelContextKey');
export const PanelItemContextKey = Symbol('jePanelItemContextKey');

export const useProvidePanel = (state) => {
  provide(PanelContextKey, state);
};

export const useInjectPanel = () => {
  return inject(PanelContextKey, null);
};

export const useProvidePanelItem = (state) => {
  provide(PanelItemContextKey, state);
};

export const useInjectPanelItem = () => {
  return inject(PanelItemContextKey, null);
};
