import { defineComponent } from 'vue';
import { TimePicker } from 'ant-design-vue';
import { useModelValue } from '../../hooks';
import { useAddonSlot, addonProps, addonSlots } from '../../hooks/use-addon';

export default defineComponent({
  name: 'JeTimeRangePicker',
  inheritAttrs: false,
  props: {
    format: { type: String, default: 'HH:mm:ss' },
    valueFormat: { type: String, default: 'HH:mm:ss' },
    value: { type: [String, Date] },
    ...addonProps,
  },
  slots: addonSlots,
  emits: ['update:value', 'change'],
  setup(props, context) {
    const { slots, attrs } = context;
    const value = useModelValue({ props, context, changeEvent: true });

    return () => {
      const element = (
        <TimePicker.TimeRangePicker
          {...attrs}
          v-model:value={value.value}
          format={props.format}
          valueFormat={props.valueFormat}
          v-slots={slots}
        />
      );
      return useAddonSlot({ props, context, element });
    };
  },
});
