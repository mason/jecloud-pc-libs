import JeAlert from './src/alert';
import { withInstall } from '../utils';
export const Alert = withInstall(JeAlert);
export default Alert;
