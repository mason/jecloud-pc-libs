import JeRate from './src/rate';
import { withInstall } from '../utils';

export const Rate = withInstall(JeRate);
export default Rate;
