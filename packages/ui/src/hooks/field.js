import { computed, ref, getCurrentInstance, isRef } from 'vue';
import { useInjectForm, useInjectFormItemContext } from '../form/src/context';
import { useInjectGrid } from '../grid/src/context';
import { useModelValue } from '.';
import { useFunc } from './use-func';
import { isString, isNumber } from '@jecloud/utils';

export function useField({ props, context }) {
  const formContext = useInjectForm();
  const formItemContext = useInjectFormItemContext();
  const gridContext = useInjectGrid();
  const propsModel = props.model ? useModelValue({ props, context, key: 'model' }) : ref();
  const func = useFunc();
  let name = null;
  let label = null;
  const instance = getCurrentInstance();
  // 父对象的类型
  const parentType = computed(() => {
    let parent = instance.parent;
    let count = 10;
    let type;
    while (count && parent) {
      if (['VxeTable', 'JeFormItem'].includes(parent?.type?.name)) {
        type = parent?.type?.name == 'VxeTable' ? 'grid' : 'formItem';
        count = 0;
      } else {
        parent = parent?.parent;
        count--;
      }
    }
    return type;
  }).value;
  /**
   * 获得数据对象的组件
   * @returns
   */
  const getModelOwner = function () {
    return { $form: formContext, $grid: gridContext };
  };

  /**
   * 获得数据对象
   * @returns
   */
  const getModel = function () {
    let model = {};
    if (propsModel.value) {
      model = propsModel.value;
    } else if (parentType === 'formItem' && formContext?.enabled && formItemContext?.enabled) {
      model = isRef(formContext.model) ? formContext.model.value : formContext.model;
    } else if (parentType === 'grid' && gridContext) {
      const { row, column } = gridContext.getActiveRecord() ?? {};
      model = row;
      name = column?.field;
      label = column?.title;
    } else {
      console.warn('未找到model数据，组件将无法正确赋值！');
    }
    return model ?? {};
  };

  /**
   * 修改数据对象
   * @param {Object} values
   * @param {Boolean} assign 覆盖属性
   */
  const setModel = function (values = {}, assign = false) {
    let model = getModel();
    // 覆盖数据对象
    if (assign) {
      Object.assign(model, values);
    } else {
      // 防止污染对象，只修改有效属性
      Object.keys(values).forEach((name) => {
        if (Object.prototype.hasOwnProperty.call(model, name)) {
          model[name] = values[name];
        }
      });
    }
    if (propsModel.value) {
      propsModel.value = model;
    }
  };

  const resetModel = function (keys = []) {
    const model = getModel();
    keys.forEach((key) => {
      let val = model[key];
      if (isString(val)) {
        model[key] = '';
      } else if (isNumber(val)) {
        model[key] = 0;
      } else {
        model[key] = null;
      }
    });
    if (propsModel.value) {
      propsModel.value = model;
    }
  };

  /**
   * 获取字段名
   * @returns
   */
  const getName = function () {
    if (name) {
      return name;
    } else if (props.name) {
      return props.name;
    } else if (parentType === 'formItem' && formItemContext?.enabled) {
      return formItemContext.name;
    } else if (parentType === 'grid' && gridContext) {
      const { column } = gridContext.getActiveRecord() ?? {};
      return column?.field;
    }
  };
  const getLabel = function () {
    if (label) {
      return label;
    } else if (props.label) {
      return props.label;
    } else if (parentType === 'formItem' && formItemContext?.enabled) {
      return formItemContext.label;
    } else if (parentType === 'grid' && gridContext) {
      const { column } = gridContext.getActiveRecord() ?? {};
      return column?.title;
    }
  };

  // const editable = computed(() => {
  //   return props.editable ?? false;
  // });
  // const readonly = computed(() => {
  //   return !editable.value || props.readonly;
  // });
  // const disabled = computed(() => {
  //   return props.disabled;
  // });
  return {
    parentType,
    getModel,
    setModel,
    getModelOwner,
    getName,
    getLabel,
    resetModel,
    // editable,
    // readonly,
    // disabled,
    func,
  };
}
