import { useJE } from '@jecloud/utils';
export function useFunc() {
  const func = useJE().useFunc();
  if (!func) {
    console.error(`未安装@jecloud/func，无法使用功能选择字段！`);
  }
  return func;
}
