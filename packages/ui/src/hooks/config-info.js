import { ref } from 'vue';
import { useField } from './field';
import { useFunc } from './use-func';
import {
  isArray,
  isEmpty,
  isNotEmpty,
  cloneDeep,
  pick,
  getDDCache,
  DDTypeEnum,
  parseEventResult4Promise,
  encode,
} from '@jecloud/utils';
import { useModelValue, useListeners } from '.';

export function useConfigInfo({ props, context }) {
  const loading = ref(false);
  const { fireListener } = useListeners({ props, context });
  const options = useModelValue({ props, context, key: 'options' });
  const multiple = ref(props.multiple);
  const func = useFunc();
  let setValues = () => {};
  let onDropdownVisibleChange = () => {}; // 下拉框展开请求数据，非列表字典起效
  const startSearchLoad = ref(false); // 下拉框可选可过滤是否调接口请求数据
  let configInfo = {};
  // 配置项
  const config = pick(props, ['configInfo', 'querys', 'dicValueConfig']);
  if (config.configInfo && func) {
    // 功能对象
    const $func = func.useInjectFunc();
    const { setModel, getModelOwner, getModel } = useField({ props, context });
    // 解析配置信息
    configInfo = func.parseConfigInfo({ configInfo: config.configInfo });
    // 字典缓存信息
    const ddCache = getDDCache(configInfo.code);
    multiple.value = configInfo.multiple;
    // 设置值
    setValues = (values = []) => {
      if (isNotEmpty(values)) {
        values = isArray(values) ? values : [values];
        // 设置配置项的值
        const data = func.getConfigInfo({ configInfo, rows: values });
        setModel(data);
      }
    };
    // 下拉框下拉框可选可过滤是是否调接口请求数据
    // 1. 可选可过滤并且对应的字典不是列表字典
    if (props.showSearch && ddCache?.type !== DDTypeEnum.LIST) {
      //2. 过滤条件是否包含组件值字段
      if (isNotEmpty(props.querys)) {
        startSearchLoad.value = encode(props.querys).indexOf(`{${props.name}}`) != -1;
      }
      //过滤传参是否包含组件值字段
      if (isNotEmpty(props.dicValueConfig)) {
        startSearchLoad.value = encode(props.dicValueConfig).indexOf(`{${props.name}}`) != -1;
      }
    }

    // 请求字典项数据
    if (isEmpty(options.value)) {
      // 树形请求
      onDropdownVisibleChange = (open, searchParams) => {
        if (!open) return;
        loading.value = true;
        // 配置信息
        const cloneConfig = cloneDeep(config);
        // 加载前事件，可以修改配置信息
        parseEventResult4Promise(
          fireListener('before-select', {
            ...getModelOwner(),
            options: cloneConfig,
          }),
        )
          .then((data) => {
            // 树形或sql查询
            if (ddCache?.type !== DDTypeEnum.LIST || isNotEmpty(cloneConfig?.querys)) {
              // 查询前，清空数据
              options.value = [];
            }
            // 自定义数据
            if (isArray(data)) {
              loading.value = false;
              options.value = data;
            } else {
              const model = getModel();
              // 加载数据
              func
                .loadSelectOptions({
                  ...cloneConfig,
                  ddCode: ddCache?.code,
                  model: { ...model, ...searchParams },
                  parentModel: $func?.parentFunc?.store.activeBean,
                })
                .then((data) => {
                  loading.value = false;
                  options.value = data;
                });
            }
          })
          .catch((error) => {
            error && console.log(error);
          });
      };
      // 判断当前字段对应的是不是text
      let initType = false;
      if (
        configInfo &&
        configInfo?.sourceFields?.length > 0 &&
        configInfo?.targetFields?.length > 0 &&
        isNotEmpty(props.name)
      ) {
        const valueCode = configInfo.sourceFields[configInfo.targetFields.indexOf(props.name)];
        if (valueCode == 'text') {
          initType = true;
        }
      }
      // 初始化数据
      // 在可编辑的情况下如果当前字段对应的是text那么数据不用初始化
      if (!(initType && props.editable)) {
        onDropdownVisibleChange(true);
      }
    }
  }

  return {
    func,
    options,
    multiple,
    setValues,
    onDropdownVisibleChange,
    loading,
    startSearchLoad,
    configInfo,
  };
}
