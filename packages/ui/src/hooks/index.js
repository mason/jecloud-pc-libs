import { watch, reactive, computed, ref, getCurrentInstance } from 'vue';
import { isObject, pick, defaultsDeep, camelCase, split, join } from '@jecloud/utils';
import { useField } from './field';
import { useConfigInfo } from './config-info';
import { useInjectFormItemContext } from '../form/src/context';
export { useField, useConfigInfo };
/**
 * model:value
 * @param {*} props
 * @returns
 */
export function useModelValue({
  props,
  context,
  key = 'value',
  multiple = false,
  separator = ',',
  changeEvent = false,
  changeValid = false,
}) {
  /**
   * 初始多选数据
   * 多选时，会将modelValue当做字符串，进行分割
   * @param {*} value props.value
   * @param {*} array 默认是字符串转数组,false 是将数组转字符串
   * @returns
   */
  const multipleValue = (value, array = true) => {
    return multiple ? (array ? split(value, separator) : join(value, separator)) : value;
  };
  const valueRef = ref(multipleValue(props[key]));
  const { emit } = context;
  // 监听值改变
  watch(
    () => props[key],
    (val) => {
      const newValue = multipleValue(val);
      valueRef.value !== newValue && (valueRef.value = newValue);
    },
    { deep: true },
  );
  // 表单校验
  const formItemContext = useInjectFormItemContext();
  const value = computed({
    set(val) {
      valueRef.value = multipleValue(val);
      emit('update:' + key, multipleValue(val, false));
      if (changeValid) {
        setTimeout(() => {
          formItemContext?.$plugin?.value?.onFieldChange();
        });
      }
    },
    get() {
      return valueRef.value;
    },
  });
  // 监听v-model:value的chenge事件
  if (changeEvent && key === 'value') {
    const { hasListener } = useListeners({ props });
    hasListener('value-change') &&
      watch(
        () => value.value,
        (newValue, oldValue) => {
          emit('value-change', newValue, oldValue);
        },
      );
  }
  return value;
}
/**
 * 混入默认配置
 *
 * @export
 * @param {*} config  attrs | props
 * @param {*} defaultConfig
 * @return {*} {reactiveAttrs,excloudKeys}
 */
export function useMixinsConfig(config, defaultConfig) {
  const defaultConfigKeys = Object.keys(defaultConfig);
  // 获取有效的属性配置:attrs || props
  const attrs = pick(config, defaultConfigKeys);
  // 获取有效的属性配置key
  const attrKeys = Object.keys(attrs);
  // 设置默认值
  defaultsDeep(attrs, defaultConfig);
  // 生成响应式对象
  const reactiveAttrs = reactive(attrs);
  // 增加属性监听
  attrKeys.forEach((key) => {
    watch(
      () => config[key],
      (attr) => {
        const configItem = defaultConfig[key]; // 默认配置
        reactiveAttrs[key] = isObject(attr) ? defaultsDeep(attr, configItem) : attr; // 修改值
      },
    );
  });

  return reactiveAttrs;
}

/**
 * 继承插件方法
 *
 * @export
 * @param {*} { plugin, pluginMethodKeys }
 * @return {*}
 */
export function useExtendMethods({ plugin, keys: pluginMethodKeys }) {
  const funcs = {};
  pluginMethodKeys.forEach((name) => {
    funcs[name] = (...args) => {
      const $plugin = plugin.value;
      return $plugin && $plugin[name](...args);
    };
  });
  return funcs;
}

/**
 * 继承插件事件
 *
 * @export
 * @param {*} { pluginEventKeys, emit }
 * @return {*}
 */
export function useExtendEvents({ keys: pluginEventKeys, emit }) {
  const events = {};
  pluginEventKeys.forEach((name) => {
    const type = camelCase(`on-${name}`);
    events[type] = (...args) => emit(name, ...args);
  });
  return events;
}

/**
 *  事件处理器
 * @param {*} param
 * @returns
 */
export function useListeners({ props }) {
  const { vnode } = getCurrentInstance();
  const hasListener = function (name) {
    const listener = camelCase(`on-${name}`);
    return props[listener] ?? vnode.props?.[listener];
  };

  // 触发事件
  const fireListener = function (name, ...args) {
    const listener = hasListener(name);
    if (listener) {
      return listener?.(...args);
    }
  };

  return { hasListener, fireListener };
}

/**
 * 重写类方法
 * @param {*} plugin
 * @param {*} methods
 */
export const useOverrideMethods = function (plugin, methods) {
  Object.keys(methods).forEach((key) => {
    plugin['_' + key] = plugin[key];
    plugin[key] = methods[key];
  });
};

/**
 * 组件尺寸
 * @param {*} param0
 * @returns
 */
export function useStyle4Size({ props }) {
  const style = {};
  if (props.height) {
    style.height = `${props.height}px`;
  }
  if (props.width) {
    style.width = `${props.width}px`;
    style.maxWidth = '100%';
  }
  return style;
}

export const Hooks = {
  useField,
  useModelValue,
  useExtendEvents,
  useExtendMethods,
  useOverrideMethods,
  useListeners,
  useStyle4Size,
};

export default Hooks;
