import ConfigProvider from '../config-provider';
/**
 * 发布目录，用于资源引用
 * @returns
 */
export function usePublicPath() {
  return ConfigProvider.getGlobalConfig('publicPath', '/');
}
