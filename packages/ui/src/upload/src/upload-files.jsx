import { defineComponent } from 'vue';
import { useUploadFiles } from './hooks/use-upload-files';
import Button from '../../button';
import Progress from '../../progress';
import { isEmpty } from '@jecloud/utils';
import Dropdown from '../../dropdown';
import Menu from '../../menu/src/menu';
export default defineComponent({
  name: 'JeUploadFiles',
  props: {
    placeholder: String,
    url: String, // 接口路径
    params: Object, // 上传参数
    maxSize: Number, //允许上传的文件最大值，M
    maxCount: Number, //允许上传的图片最大数量
    includeSuffix: Array, //允许上传的文件后缀
    excludeSuffix: Array, //禁用上传的文件后缀
    menuButtons: {
      // 右键菜单展示的按钮
      type: Array,
      default: () => {
        return ['preview', 'downLoad', 'remove', 'rName', 'property'];
      },
    },
    editable: Boolean,
    readonly: Boolean,
    disabled: Boolean,
    height: String,
    value: { type: String, default: '' },
    model: Object, // 当前model数据
    securityMarkSlot: Function,
    securityActionSlot: Function,
    afterFileRemove: Function,
  },
  emits: ['change', 'before-file-action'],
  setup(props, context) {
    const {
      state,
      tbarButtons,
      contextMenu,
      actionButtons,
      computeReadonly,
      actionsSlot,
      actionClick,
      iconSlot,
    } = useUploadFiles({
      props,
      context,
    });
    const style = props.height ? 'height:' + props.height + 'px' : '';
    return () => (
      <div class="je-upload-files" style={style}>
        <div class="je-upload-files-tbar">
          {tbarButtons.map((item) => {
            return (
              <Button
                type="text"
                disabled={computeReadonly.value && !item.noReadonly}
                onClick={() => actionClick({ type: item.code })}
                class="je-upload-files-tbar-button"
                v-slots={{
                  icon: () => {
                    return <i class={item.icon}></i>;
                  },
                }}
              >
                {item.text}
              </Button>
            );
          })}
        </div>
        <div
          class={{
            'je-upload-files-context white-background': true,
            'disabled-background': props.disabled,
          }}
        >
          {state.files?.length > 0 ? (
            state.files.map((file) => {
              return (
                <Dropdown
                  trigger="['contextmenu']"
                  v-slots={{
                    overlay: () => {
                      return (
                        <Menu onClick={({ key }) => actionClick({ type: key, file })}>
                          {contextMenu
                            .filter(
                              (button) =>
                                isEmpty(props.menuButtons) ||
                                props.menuButtons.includes(button.code),
                            )
                            .map((button) => (
                              <Menu.Item
                                key={button.code}
                                disabled={computeReadonly.value && !button.noReadonly}
                              >
                                <i
                                  style="font-size: 14px; margin-right: 10px"
                                  class={button.icon}
                                />
                                {button.text}
                              </Menu.Item>
                            ))}
                        </Menu>
                      );
                    },
                  }}
                >
                  <div class="je-upload-files-context-file">
                    {isEmpty(file.fileKey) ? (
                      <div class="je-upload-files-context-file-progress">
                        <Progress
                          type="circle"
                          percent={state.progress[file.relName] || 0}
                          width={40}
                        />
                      </div>
                    ) : (
                      iconSlot(file)
                    )}
                    <div class="je-upload-files-context-file-text">{file.relName}</div>
                    {actionsSlot({ file, buttons: actionButtons })}
                  </div>
                </Dropdown>
              );
            })
          ) : (
            <span class="je-upload-files-context-empty">{/* 拖拽到这里即可上传 */}</span>
          )}
        </div>
      </div>
    );
  },
});
