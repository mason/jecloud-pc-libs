import { FileActions } from '../utils';
import { useUploadCommon } from './use-upload-common';
import { ref } from 'vue';
export function useUploadFiles({ props, context }) {
  const { state, computeReadonly, iconSlot, actionClick, actionsSlot } = useUploadCommon({
    props,
    context,
    multiple: true,
  });
  const tbarButtons = [FileActions.upload];
  const actionButtons = ref([]);
  // 根据配置添加按钮
  if (props.menuButtons.includes('preview')) {
    actionButtons.value.push(FileActions.preview);
  }
  if (props.menuButtons.includes('downLoad')) {
    tbarButtons.push(FileActions.downloadAll);
    actionButtons.value.push(FileActions.download);
  }
  if (props.menuButtons.includes('remove')) {
    tbarButtons.push(FileActions.removeAll);
  }
  const contextMenu = [
    FileActions.preview,
    FileActions.download,
    FileActions.remove,
    FileActions.rename,
    FileActions.property,
  ];

  return {
    state,
    tbarButtons,
    actionButtons,
    computeReadonly,
    contextMenu,
    actionClick,
    iconSlot,
    actionsSlot,
  };
}
