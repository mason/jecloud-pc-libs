import { reactive, watch, computed } from 'vue';
import { pick, decode, getFileTypeInfo, getFileUrlByKey, encode } from '@jecloud/utils';
import { useListeners, useModelValue } from '../../../hooks';
import { useInjectFormItemContext, useInjectForm } from '../../../form/src/context';
import { RetainKeys, doFileAction } from '../utils';
export function useUploadCommon({ props, context, multiple }) {
  const { expose } = context;
  const modelValue = useModelValue({ props, context });
  const { fireListener } = useListeners({ props, context });
  const computeReadonly = computed(() => {
    return props.readonly || props.disabled;
  });
  const $form = useInjectForm();
  const formItem = useInjectFormItemContext(); // formItem对象
  const state = reactive({
    uploading: false, // 上传中标识
    errorMessage: '', // 错误信息
    name: formItem?.name,
    files: [],
    progress: {},
  });
  const $upload = {
    props,
    context,
    state,
    modelValue,
    formItem,
    multiple,
    doFilesValidate() {
      $form?.$plugin?.value?.validate(formItem?.name);
    },
    /**
     * 刷新value
     */
    refreshValue() {
      modelValue.value = state.files.length > 0 ? encode(state.files) : '';
    },
    /**
     * 操作前事件
     * @param {*} type
     * @param {*} options
     * @returns
     */
    fireBeforeFileAction(type, options) {
      return (
        fireListener('before-file-action', { ...options, $upload, actionType: type }) !== false
      );
    },
    /**
     * 取消上传
     */
    cancelUpload() {
      this.flowObj?.cancel();
      state.uploading = false;
    },
    /**
     * 是否上传中
     * @returns
     */
    isUploading() {
      return state.uploading;
    },
    /**
     * 通过名称获得文件对象
     * @param {*} name
     * @returns
     */
    getFileByName(name) {
      return state.files.find((file) => file.relName === name);
    },
    /**
     * 追加文件数据
     * @param {*} files
     */
    addFiles(files = []) {
      if (!multiple) {
        state.files = [];
      }
      files.forEach((file) => {
        const rawFile = pick(file, RetainKeys);
        rawFile.relName = file.name || file.relName; // 兼容数据
        const index = state.files.map((f) => f.relName).indexOf(rawFile.relName);
        if (index > -1) {
          state.files[index] = rawFile;
        } else {
          state.files.push(rawFile);
        }
      });
    },
    validator: (rule = {}) => {
      if (state.uploading) {
        rule.message = '附件没有上传完成，请耐心等待';
        state.errorMessage = rule.message;
        return Promise.reject(rule.message);
      } else if (rule.required && state.files.length === 0) {
        rule.message = '该输入项为必填项';
        state.errorMessage = rule.message;
        return Promise.reject(rule.message);
      } else {
        state.errorMessage = '';
        return Promise.resolve();
      }
    },
    clearValidate: () => {
      state.errorMessage = '';
    },
  };
  expose($upload);

  // 修改文件对象
  watch(
    () => modelValue.value,
    (value) => {
      let files = value ? decode(value) : [];
      if (!Array.isArray(files)) {
        files = [files];
      }
      state.files = files;
      // 自定义校验方法的时候，校验了错误信息，在文件改变的时候，是不需要清空错误信息的， 但是必填的时候，就需要清除校验了
      $form?.validate?.(formItem?.name);
    },
    {
      immediate: true,
    },
  );

  // 文件的图标插槽
  const iconSlot = (file) => {
    const icon = getFileTypeInfo(file.relName);
    const securityVNodes = props.securityMarkSlot?.({ $upload, file });
    // 图片展示缩略图
    return (
      <div class="je-upload-icon-wrapper">
        {icon.type == 'image' && !securityVNodes ? (
          <img src={getFileUrlByKey(file.fileKey, 'thumbnail')} />
        ) : (
          <i class={icon.icon} style={{ color: icon.color }} />
        )}
        {securityVNodes}
      </div>
    );
  };

  // 操作按钮点击
  const actionClick = ({ type, file }) => {
    doFileAction({ type, file, $upload });
  };

  // 多附件操作按钮插槽
  const actionsSlot = ({ file, buttons }) => {
    const securityVNodes = props.securityActionSlot?.({ $upload, file });
    return (
      <div class="je-upload-actions-wrapper">
        {buttons.value.map((button) => (
          <i
            class={button.icon}
            title={button.text}
            onClick={
              button.clickEvent !== false ? () => actionClick({ type: button.code, file }) : null
            }
          />
        ))}
        {securityVNodes}
      </div>
    );
  };

  return { $upload, state, modelValue, computeReadonly, actionClick, iconSlot, actionsSlot };
}
