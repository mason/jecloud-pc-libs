import { getFileSystemIncludeSuffix } from '@jecloud/utils';
import { nextTick, ref, onMounted, computed } from 'vue';
import { useUploadCommon } from './use-upload-common';
import { FileActions } from '../utils';
import Sortable from 'sortablejs';
export function useUploadImage({ props, context }) {
  const { $upload, state, computeReadonly, iconSlot, actionsSlot, actionClick } = useUploadCommon({
    props,
    context,
    multiple: true,
  });
  const fileSuffix = ref(getFileSystemIncludeSuffix(props.includeSuffix));
  const imageRef = ref();
  const actionButtons = computed(() => {
    // 操作按钮
    return [
      FileActions.sortable,
      FileActions.preview,
      FileActions.download,
      FileActions.remove,
    ].filter((button) => !computeReadonly.value || button.noReadonly);
  });

  onMounted(() => {
    nextTick(() => {
      const sortItem = {
        handle: '.sortable-icon',
        draggable: '.je-upload-images-finish',
        ghostClass: 'je-sortable-img',
        group: {
          name: 'sortable-image',
          pull: true,
          put: false,
        },
        sort: true,
        onEnd: function ({ newIndex, oldIndex }) {
          // 更改位置
          state.files[oldIndex] = state.files.splice(newIndex, 1, state.files[oldIndex])[0];
          nextTick(() => {
            $upload.refreshValue();
          });
        },
      };
      Sortable.create(imageRef.value, sortItem);
    });
  });

  return {
    state,
    fileSuffix,
    computeReadonly,
    imageRef,
    actionButtons,
    actionClick,
    actionsSlot,
    iconSlot,
  };
}
