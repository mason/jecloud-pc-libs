import { defineComponent } from 'vue';
import { isEmpty } from '@jecloud/utils';
import Progress from '../../progress';
import { useUploadImage } from './hooks/use-upload-image';

export default defineComponent({
  name: 'JeUploadImage',
  props: {
    url: String, // 接口路径
    params: Object, // 上传参数
    maxSize: Number, //允许上传的文件最大值，M
    maxCount: Number, //允许上传的图片最大数量
    includeSuffix: {
      type: Array,
      default() {
        return ['jpg', 'jpeg', 'png', 'gif', 'ico', 'bmp'];
      },
    }, //允许上传的文件后缀
    excludeSuffix: Array, //禁用上传的文件后缀
    editable: Boolean,
    readonly: Boolean,
    disabled: Boolean,
    height: String,
    width: String,
    value: { type: String, default: '' },
    model: Object, // 当前model数据
    securityMarkSlot: Function,
    securityActionSlot: Function,
    afterFileRemove: Function,
  },
  emits: ['change', 'before-file-action'],
  setup(props, context) {
    const {
      imageRef,
      state,
      fileSuffix,
      computeReadonly,
      actionButtons,
      actionClick,
      actionsSlot,
      iconSlot,
    } = useUploadImage({
      props,
      context,
    });
    return () => (
      <div
        class={{
          'je-upload-images white-background': true,
          'disabled-background': props.disabled,
        }}
        style={{
          height: props.height ? props.height + 'px' : '100%',
          width: props.width ? props.width + 'px' : '100%',
        }}
      >
        <div
          ref={imageRef}
          class="je-upload-images-content"
          style={{ height: props.height ? props.height - 40 + 'px' : '150px' }}
        >
          {state.files.map((file) => (
            <div class="je-upload-images-item je-upload-images-finish" key={file.fileKey}>
              {isEmpty(file.fileKey) ? (
                <div class="je-upload-files-context-file-progress">
                  <Progress
                    type="circle"
                    percent={state.progress[file.relName] || 0}
                    width={110}
                    style={{ width: '110px', height: '110px', margin: '10px' }}
                  />
                </div>
              ) : (
                iconSlot(file)
              )}
              {actionsSlot({ file, buttons: actionButtons })}
            </div>
          ))}
          {/* 只读，或者超过上传数量，隐藏上传按钮 */}
          {computeReadonly.value ||
          (props.maxCount && props.maxCount <= state.files.length) ? null : (
            <div
              class="je-upload-images-item je-upload-images-add"
              onClick={() => actionClick({ type: 'upload' })}
            >
              <div class="image-add-item">
                <i class="fas fa-add"></i>
                <div class="add-desc">添加图片</div>
              </div>
            </div>
          )}
        </div>
        <div class="je-upload-images-explain">
          {fileSuffix.value === false ? (
            <span>未设置有效的上传文件格式，请检查字段和平台全局的配置！</span>
          ) : (
            <span>允许上传图片的格式为【{fileSuffix.value.toString()}】</span>
          )}
          {props.maxCount ? <span>，最多上传{props.maxCount}张图片</span> : null}
          {props.maxSize ? <span>，大小限制在{props.maxSize}M之内</span> : null}
        </div>
      </div>
    );
  },
});
