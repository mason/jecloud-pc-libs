import { ref } from 'vue';
import { uniqueId } from '@jecloud/utils';
import Modal from './modal';
import { renderVNode } from '../../utils/vnode';

/**
 * 对话框
 * @param {Object} config 配置项
 * @param {string} [config.title=消息] 标题
 * @param {string} [config.status] 状态，info|success|warning|error|question
 * @param {string|Function} config.content 内容
 * @param {number|string} [config.width] 宽
 * @param {number|string} [config.height] 高
 * @param {boolean|string|Function|Object} [config.okButton] 窗口底部确认按钮，具体可参考buttons项
 * @param {boolean|string|Function|Object} [config.cancelButton] 窗口底部取消按钮，具体可参考buttons项
 * @param {string} [config.buttonAlign=center] 窗口底部按钮组对齐方式
 * @param {Object[]} [config.buttons] 窗口底部按钮组，buttons[] = {text:'',icon:'',type:'primary',closeable:true,handler({$modal}){}}
 * @param {Object[]} [config.tools] 窗口右上角按钮组，tools[] = {text:'',icon:'',handler({$modal}){}}
 * @param {Object} [config.slots] 插槽
 * @param {Function} [config.onBeforeshow] 打开前函数
 * @param {Function} [config.onShow] 打开后函数
 * @param {Function} [config.onBeforeclose] 关闭前函数
 * @param {Function} [config.onClose] 关闭后函数
 * @param {boolean} [config.mask=true] 显示遮罩
 * @param {boolean} [config.maskClosable=true] 点击遮罩关闭
 * @param {string} [config.icon] 标题图标
 * @param {string} [config.className] 样式名
 * @param {string|Object} [config.bodyStyle] body样式
 * @param {string|Object} [config.headerStyle] header样式
 * @param {string|Object} [config.footerStyle] footer样式
 * @param {boolean} [config.showHeader=true] 显示header
 * @param {boolean} [config.showFooter=true] 显示footer
 * @param {boolean} [config.resize=true] 允许调整大小
 * @param {boolean} [config.maximizable=true] 允许全屏
 * @param {boolean} [config.maximized] 打开默认全屏
 * @param {boolean} [config.closable=true] 允许关闭
 * @param {boolean} [config.draggable=true] 允许拖动
 * @param {number|string} [config.minWidth=460] 最小宽
 * @param {number|string} [config.minHeight=200] 最小高
 * @returns {$modal}
 */
const dialog = (config) => {
  const visible = ref(true);
  let modalOpts = Object.assign(config, {
    key: uniqueId(),
    visible: visible.value,
    teleport: true,
  });
  // 默认是dialog
  if (config.dialog !== false) {
    modalOpts = {
      type: 'dialog',
      maximizable: false,
      showFooter: true,
      buttonAlign: 'right',
      ...modalOpts,
    };
  }
  const refModal = ref();
  // 销毁
  const _onClose = modalOpts.onClose;
  modalOpts.onClose = (...args) => {
    visible.value = false;
    _onClose?.(...args);
    // 删除元素
    refModal.value.$el.parentNode?.remove();
  };
  // 渲染组件
  renderVNode(() => (visible.value ? <Modal ref={refModal} {...modalOpts} /> : null));
  return refModal.value;
};

export default dialog;
