const GlobalConfig = {
  resize: true,
  maximizable: true,
  top: 15,
  showHeader: true,
  minWidth: 460,
  minHeight: 200,
  lockView: true,
  mask: true,
  maskClosable: false,
  duration: 3000,
  marginSize: 100,
  dblclickMaximized: true,
  showTitleOverflow: true,
  animat: true,
  closable: true,
  draggable: true,
  transfer: true,
  escClosable: true,
  teleport: false,
  showFooter: true,
  buttonAlign: 'center',
  icon: {
    // modal
    MODAL_ZOOM_IN: 'fal fa-window-maximize',
    MODAL_ZOOM_OUT: 'fal fa-window-restore',
    MODAL_CLOSE: 'fal fa-times',
    MODAL_INFO: 'fal fa-info-circle',
    MODAL_SUCCESS: 'fal fa-check-circle',
    MODAL_WARNING: 'fal fa-exclamation-circle',
    MODAL_ERROR: 'fal fa-times-circle',
    MODAL_QUESTION: 'fal fa-question-circle',
  },
  i18n: (key) => {
    const langs = {
      title: '消息',
      okText: '确定',
      cancelText: '取消',
      zoomIn: '最大化',
      zoomOut: '还原',
      close: '关闭',
    };
    return langs[key];
  },
};
export default GlobalConfig;
