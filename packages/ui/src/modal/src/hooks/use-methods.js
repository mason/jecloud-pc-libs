import { nextTick } from 'vue';
import {
  isString,
  getBodyHeight,
  getBodyWidth,
  getDomNode,
  computedDomSize,
  isNumber,
  uuid,
  parseEventResult4Promise,
} from '@jecloud/utils';
import { getLastZIndex, nextZIndex } from './use-manager';
import { useListeners } from '../../../hooks';

export const msgQueue = [];
export function useMethods($modal) {
  const { props, reactData, getRef } = $modal;
  const { emit } = $modal.context;
  const { fireListener } = useListeners({ props });
  const fireEvent = function (name, params, ...args) {
    return fireListener(name, Object.assign({ $modal }, params), ...args);
  };

  // 弹框类型
  const isMessage = () => props.type === 'message';

  // 重新计算尺寸
  const recalculate = () => {
    const boxElem = getRef('box', true);
    const boxHeight = boxElem.offsetHeight;
    const boxWidth = boxElem.offsetWidth;

    const bodyWidth = getBodyWidth();
    const bodyHeight = getBodyHeight();
    const box = {
      width: {
        val: computedDomSize({ size: props.width, parentSize: bodyWidth }),
        min: computedDomSize(props.minWidth),
        max: bodyWidth,
        domVal: boxWidth,
      },
      height: {
        val: computedDomSize({ size: props.height, parentSize: bodyHeight }),
        min: computedDomSize(props.minHeight),
        max: bodyHeight,
        domVal: boxHeight,
      },
    };
    Object.keys(box).forEach((key) => {
      let { val, min, max, domVal } = box[key];
      if (isNumber(val)) {
        val = Math.max(Math.min(val, max), min);
      } else if (val === 'auto') {
        val = Math.max(Math.min(domVal, max), min);
        val = domVal === val ? 'auto' : val;
      } else if (props.type === 'modal') {
        val = max - 100;
      }
      boxElem.style[key] = `${val ? (isNaN(val) ? val : `${val}px`) : ''}`;
    });

    return nextTick();
  };

  // 更新zIndex
  const updateZindex = () => {
    const { zIndex } = props;
    const { modalZindex } = reactData;
    if (zIndex) {
      reactData.modalZindex = zIndex;
    } else if (modalZindex < getLastZIndex()) {
      reactData.modalZindex = nextZIndex();
    }
  };

  // 更新位置
  const updatePosition = () => {
    return nextTick().then(() => {
      const { position } = props;
      const marginSize = 0;
      const boxElem = getRef('box', true);
      const clientVisibleWidth = document.documentElement.clientWidth || document.body.clientWidth;
      const clientVisibleHeight =
        document.documentElement.clientHeight || document.body.clientHeight;
      const isPosCenter = position === 'center';
      const { top, left } = isString(position)
        ? { top: position, left: position }
        : Object.assign({}, position);
      const topCenter = isPosCenter || top === 'center';
      const leftCenter = isPosCenter || left === 'center';
      let posTop = '';
      let posLeft = '';
      if (left && !leftCenter) {
        posLeft = isNaN(left) ? left : `${left}px`;
      } else {
        posLeft = `${Math.max(marginSize, clientVisibleWidth / 2 - boxElem.offsetWidth / 2)}px`;
      }
      if (top && !topCenter) {
        posTop = isNaN(top) ? top : `${top}px`;
      } else {
        posTop = `${Math.max(marginSize, clientVisibleHeight / 2 - boxElem.offsetHeight / 2)}px`;
      }
      boxElem.style.top = posTop;
      boxElem.style.left = posLeft;
    });
  };

  const close = (type) => {
    const { remember } = props;
    const { active } = reactData;
    const result = parseEventResult4Promise(active && fireEvent('beforeclose', { type }));
    result.then(() => {
      reactData.contentVisible = false;
      if (!remember) {
        reactData.zoomLocation = null;
      }
      reactData.active = false;
      setTimeout(() => {
        emit('update:visible', false);
        fireEvent('close', { type });
      }, 200);
    });
    return nextTick();
  };

  const show = () => {
    const { showFooter, buttonFocus } = props;
    const { inited, active } = reactData;
    // 打开前事件
    if (fireEvent('beforeshow') === false) {
      return nextTick();
    }

    if (!inited) {
      reactData.inited = true;
    }
    if (!active) {
      recalculate();
      reactData.active = true;
      reactData.contentVisible = false;
      updateZindex();

      setTimeout(() => {
        reactData.contentVisible = true;
        nextTick(() => {
          const elDom = getRef('el', true);
          elDom?.focus();
          if (showFooter && buttonFocus) {
            const footer = getRef('footer', true);
            const button = footer?.querySelector('.je-modal--button');
            button?.focus();
          }
          emit('update:visible', true);
          // 打开后事件
          fireEvent('show');
          // 计算尺寸
          recalculate();
          // 计算位置
          updatePosition().then(() => {
            setTimeout(() => updatePosition(), 20);
          });
          // 最大化
          const { maximized } = props;
          if (maximized) {
            nextTick(() => maximize());
          }
        });
      }, 10);
    }
    return nextTick();
  };

  // 是否最大化窗口
  const isMaximized = () => {
    return !!reactData.zoomLocation;
  };

  // 最大化窗口
  const maximize = () => {
    return nextTick().then(() => {
      if (!reactData.zoomLocation) {
        const marginSize = 0;
        const boxElem = getRef('box', true);
        const { height, width } = getDomNode();
        reactData.zoomLocation = {
          top: boxElem.offsetTop,
          left: boxElem.offsetLeft,
          width: boxElem.offsetWidth + (boxElem.style.width ? 0 : 1),
          height: boxElem.offsetHeight + (boxElem.style.height ? 0 : 1),
        };
        Object.assign(boxElem.style, {
          top: `${marginSize}px`,
          left: `${marginSize}px`,
          width: `${width - marginSize * 2}px`,
          height: `${height - marginSize * 2}px`,
        });
      }
    });
  };
  // 恢复窗口
  const revert = () => {
    return nextTick().then(() => {
      const { zoomLocation } = reactData;
      if (zoomLocation) {
        const boxElem = getRef('box', true);
        reactData.zoomLocation = null;
        Object.assign(boxElem.style, {
          top: `${zoomLocation.top}px`,
          left: `${zoomLocation.left}px`,
          width: `${zoomLocation.width}px`,
          height: `${zoomLocation.height}px`,
        });
      }
    });
  };
  // 缩放窗口
  const zoom = () => {
    if (reactData.zoomLocation) {
      return revert().then(() => isMaximized());
    }
    return maximize().then(() => isMaximized());
  };
  // 触发事件
  const dispatchEvent = (type, params, evnt) => {
    emit(type, Object.assign({ $modal, $event: evnt }, params));
  };

  return {
    fireEvent,
    isMaximized,
    maximize,
    revert,
    zoom,
    show,
    close,
    recalculate,
    isMessage,
    updateZindex,
    dispatchEvent,
  };
}
/**
 *  创建modal容器
 */
export function renderModalContainer(className = 'je-modal--container') {
  let modalContainer = document.querySelector(`.${className}`);
  if (!modalContainer) {
    modalContainer = document.createElement('div');
    modalContainer.className = className;
    document.body.appendChild(modalContainer);
  }
  const container = document.createElement('div');
  container.setAttribute('id', 'modal-' + uuid());
  modalContainer.appendChild(container);
  return container;
}
