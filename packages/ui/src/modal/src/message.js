import { message } from 'ant-design-vue';
import { transformOpenFn } from './hooks/util';
transformOpenFn(message, true);
export default message;
