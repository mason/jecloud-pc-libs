import JeModalComponent from './src/modal';
import _dialog from './src/dialog';
import _message from './src/message';
import notification from './src/notification';
import { transformArgs } from './src/hooks/util';
import { useInjectModal } from './src/context';
/**
 * 消息状态
 */
const MessageStatus = {
  /**
   * 消息
   */
  info: 'info',
  /**
   * 成功
   */
  success: 'success',
  /**
   * 警告
   */
  warning: 'warning',
  /**
   * 错误
   */
  error: 'error',
  /**
   * 疑问
   */
  question: 'question',
};
/**
 * 提示对话框
 * @param {string} content 消息体
 * @param {string} [status] 状态，info|success|warning|error|question，默认info
 * @returns {$model}
 */
function alert(content, status = MessageStatus.info) {
  return _dialog(
    Object.assign(
      {
        okButton: true,
      },
      transformArgs({ content, status }),
    ),
  );
}
/**
 * 确认对话框
 * @param {string} content 消息体
 * @param {Function} okButton 确认操作
 * @returns {$model}
 */
function confirm(content, okButton) {
  return _dialog(
    Object.assign(
      {
        status: MessageStatus.question,
        okButton: true,
        cancelButton: true,
      },
      transformArgs({ content, okButton }),
    ),
  );
}
/**
 * 对话框
 * @param {Object} options 配置信息
 * @returns {$model}
 */
function dialog(options) {
  return _dialog(
    Object.assign(
      {
        okButton: true,
        cancelButton: true,
      },
      options,
    ),
  );
}
/**
 * 窗口
 * @param {Object} options 配置信息
 * @returns {$model}
 */
function window(options) {
  return _dialog(Object.assign(options, { dialog: false }));
}
/**
 * 消息提醒框
 * @param {string} content 消息体
 * @param {string} [status] 状态，info|success|warning|error|question，默认info
 * @param {string} [duration] 描述
 */
function message(content, status = MessageStatus.info, duration) {
  return _message.open(transformArgs({ content, status, duration }));
}
/**
 * 消息通知框
 * @param {string} content 消息体
 * @param {string} [status] 状态，info|success|warning|error|question，默认info
 */
function notice(content, status = MessageStatus.info) {
  return notification.open(transformArgs({ content, status }));
}
const ModalController = {
  alert,
  confirm,
  dialog,
  message,
  notice,
  window,
  useInjectModal,
  notification,
  status: MessageStatus,
  mixinMethodKeys: ['alert', 'confirm', 'dialog', 'message', 'notice', 'window'],
};

export const modal = ModalController;

export const Modal = Object.assign(JeModalComponent, {
  install: function (app) {
    app.component(JeModalComponent.name, JeModalComponent);
  },
  ...ModalController,
});
export default Modal;
