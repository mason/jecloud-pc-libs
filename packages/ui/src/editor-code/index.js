import { withInstall } from '../utils';
import JeEditorCode from './src/editor-code';
export const EditorCode = withInstall(JeEditorCode);

export default EditorCode;
