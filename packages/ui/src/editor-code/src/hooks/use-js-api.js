import { ref, nextTick, reactive } from 'vue';
import { getEventTargetNode, Data, loadApiData } from '@jecloud/utils';

export function useJsApi({ props, context }) {
  const $apiTree = ref(null);
  const treeStore = Data.useTreeStore({ data: [] });
  const state = reactive({
    visible: false,
    value: '',
    method: '',
    showButton: false,
    node: null,
  });
  // 获得api数据
  loadApiData().then((data) => {
    treeStore.loadData(data);
    nextTick(() => {
      // 默认展开第一层节点
      treeStore.data.forEach((item) => {
        $apiTree.value.setTreeExpand(item, true);
      });
    });
  });

  // tree选中切换
  const handlerSelect = (options) => {
    const { row, $event, cell } = options;
    if ($event && getEventTargetNode($event, cell, 'vxe-tree-cell').flag) {
      state.visible = true;
      const treeStore = $apiTree.value.store;
      nextTick(() => {
        state.value = row.bean.JEAPI_EXAMPLE || `/**\n * ${row.bean.JEAPI_REMARK || row.text}\n */`;
        state.showButton = row.bean.JEAPI_TYPE !== 'folder';
        if (state.showButton) {
          state.node = row;
          state.method = treeStore
            .getNodePathArray(state.node)
            .filter((id) => {
              const item = treeStore.getRecordById(id);
              return item.bean.JEAPI_TYPE !== 'folder';
            })
            .map((id) => treeStore.getRecordById(id).text)
            .join('.');
        }
      });
    }
  };

  // 应用方法
  const handlerClick = ({ type, close }) => {
    if (!close) {
      if (type === 'fn') {
        props.setEditorCursorValue('\n' + state.method + ';');
      } else {
        const code = state.value.replace(/\/\*\*((?!\*\/).)*\*\//s, '');
        props.setEditorCursorValue(code);
      }
    }
    state.visible = false;
  };

  return { $apiTree, treeStore, handlerSelect, state, handlerClick };
}
