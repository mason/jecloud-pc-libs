import { ref, onMounted, nextTick, watch, onUnmounted, onBeforeUnmount } from 'vue';
import {
  addResizeListener,
  removeResizeListener,
  debounce,
  isNotEmpty,
  uuid,
} from '@jecloud/utils';
import { loadMonaco } from './monaco-loader';
import { useEditor } from './use-editor';
export function useMonaco({ props, context }) {
  const { emit } = context;
  const container = ref();
  const editorId = uuid();
  const loading = ref(true);
  const { value, style } = useEditor({ props, context });
  let editor;
  let monacoEditor;
  let defaultChangeEvent = true;
  watch(
    () => value.value,
    (newValue, oldValue) => {
      emit('change', newValue, oldValue);
      // 只有外部赋值时才会为editor赋值
      if (defaultChangeEvent && editor) {
        editor.setValue(newValue || '');
      }
    },
  );
  watch(
    () => props.language,
    () => {
      setLanguages({ editor, language: props.language, monaco: monacoEditor });
    },
  );

  // 防止频繁刷新
  const refreshEditor = debounce(() => {
    editor?.layout();
  }, 100);
  onMounted(() => {
    nextTick(() => {
      loadMonaco().then((monaco) => {
        loading.value = false;
        monacoEditor = monaco;
        const editorConfig = {
          theme: 'vs-dark', // 主题   vs, hc-black, vs-dark
          emptySelectionClipboard: true, // 没有选中内容复制，就复制整行
          fontSize: 16, // 字体显示大小
          ...props.editorOptions,
          value: props.value,
          language: props.language, // 语言
          readOnly: props.readonly, // 是否为只读状态
        };
        // 初始化配置信息
        emit('editor-config', { editorConfig });
        // 创建编辑器
        editor = monaco.editor.create(container.value, editorConfig);
        emit('init', editor);

        // TODO: editor.addCommand在多编辑器时，会导致注册事件失效，所有编辑器会使用最新编辑器注册的事件
        // editor.addCommand更像是一个全局事件，组件不适用，所以弃用，采用editor.addAction
        // ctrl + S   监听保存
        editor.addAction({
          id: 'command' + editorId,
          label: 'Save',
          keybindings: [monaco.KeyMod.CtrlCmd | monaco.KeyCode.KeyS], // Ctrl/Cmd + s
          run: function (evt) {
            emit('save', editor.getValue(), evt);
            // 兼容save
            emit('editor-save', { value: editor.getValue() });
          },
        });

        // change事件
        editor.onDidChangeModelContent(function (evt) {
          const val = editor.getValue();
          // 禁用默认编辑事件，防止冲突
          defaultChangeEvent = false;
          // 防止重复给value赋值，导致表单会记录字段变动
          if (isNotEmpty(value.value, true) || isNotEmpty(val)) {
            value.value = val;
          }
          setTimeout(() => {
            defaultChangeEvent = true;
          }, 10);
        });
        // 刷新editor尺寸
        addResizeListener(container.value, refreshEditor);
      });
    });
  });

  // 销毁resize事件
  onBeforeUnmount(() => {
    removeResizeListener(container.value, refreshEditor);
  });

  // 销毁编辑器
  onUnmounted(() => {
    editor?.dispose();
  });

  // 在编辑器 光标位置后插入代码
  const setCursorValue = (value) => {
    // 在编辑器 光标位置后插入代码
    const position = editor.getPosition();
    editor.executeEdits('', [
      {
        range: {
          startLineNumber: position.lineNumber,
          startColumn: position.column,
          endLineNumber: position.lineNumber,
          endColumn: position.column,
        },
        text: value,
      },
    ]);
  };

  return {
    style,
    container,
    loading,
    editorId,
    getEditor() {
      return editor;
    },
    setCursorValue,
  };
}

function setLanguages({ editor, language, monaco }) {
  if (!editor || !monaco) return;
  const oldLanguage = editor.getModel();
  const value = editor.getValue();
  const newLanguage = monaco.editor.createModel(value, language);
  editor.setModel(newLanguage);
  if (oldLanguage) {
    oldLanguage.dispose();
  }
}
