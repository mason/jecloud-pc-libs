import { loadjs, createDeferred } from '@jecloud/utils';
import { usePublicPath } from '../../../hooks/use-public-path';
/**
 * 加载外部静态资源，不使用npm安装
 * @returns
 */
let waitLoadings = [];
export function loadMonaco() {
  const publicPath = usePublicPath();
  const baseDir = `${publicPath}static/monaco-editor/vs`;
  if (window.monaco) {
    return Promise.resolve(window.monaco);
  } else if (window.monacoLoading) {
    // 处理多个组件同时加载
    const defeered = createDeferred();
    waitLoadings.push(defeered);
    return defeered.promise;
  } else {
    window.monacoLoading = true;
    window.require = { paths: { vs: baseDir } };
    return loadjs(`${baseDir}/loader.js`, { returnPromise: true }).then(() => {
      return new Promise((resolve) => {
        window.require(['vs/editor/editor.main'], () => {
          resolve(window.monaco);
          waitLoadings.forEach((item) => {
            item.resolve(window.monaco);
          });
          waitLoadings = [];
          window.monacoLoading = false;
          /*
            问题描述：
            打开monaco编辑器后，再打开含有amd加载方式的js文件时，报错 Can only have one anonymous define call per script file
            
            原因分析：
            这是因为monaco的资源文件中有个判断，不能一个项目中有两个define的定义函数，因为monaco需要自己的定义。所以避免define方法互相污染而报错，或者资源文件没加载！
          */
          if ('function' == typeof window.define && window.define.amd) {
            delete window.define.amd;
          }
        });
      });
    });
  }
}
