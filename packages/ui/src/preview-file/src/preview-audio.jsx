import { defineComponent, nextTick, ref, onBeforeUnmount } from 'vue';
import { getFileUrlByKey } from './utils';

export default defineComponent({
  name: 'JePreviewAudio',
  inheritAttrs: false,
  props: {
    previewFile: {
      type: Object,
      default: () => null,
    },
    isFuncFile: {
      //是否为子功能附件
      type: Boolean,
      default: () => false,
    },
  },
  setup(props) {
    const audioId = ref(props.isFuncFile ? 'funJeAudio' : 'normalJeAudio');
    const canvasId = ref(props.isFuncFile ? 'funMyCanvas' : 'normalMyCanvas');
    const audioUrl = ref(getFileUrlByKey(props.previewFile?.value));
    let isInit = false;
    let analyser = {};
    let dataArry = [];
    const initCancas = () => {
      let canvas = document.getElementById(canvasId.value);
      let ctx = canvas.getContext('2d');
      ctx.fillRect(10, 10, 280, 130);
    };
    const onPlay = () => {
      if (isInit) {
        return;
      }
      let audioEle = document.getElementById(audioId.value);
      //初始化
      let audCtx = new AudioContext(); //创建音频上下文
      analyser = audCtx.createAnalyser();
      const source = audCtx.createMediaElementSource(audioEle); //创建音频源节点
      analyser.fftSize = 512; //一个无符号长整形 (unsigned long) 的值，代表了快速傅里叶变换(分析器)的窗口大小
      //创建数组，用于接受节点分析器分析的数据
      dataArry = new Uint8Array(analyser.frequencyBinCount); //这里并不是声明一个普通数组，而是需要声明一个无符号的八位整数，刚好是一个字节。并且数组长度需要刚好等于频谱图横坐标长度
      source.connect(analyser);
      analyser.connect(audCtx.destination);

      isInit = true;
    };
    //把分析器分析的数据不断绘制到canvas
    const draw = () => {
      let cvs = document.getElementById(canvasId.value);
      let ctx = cvs.getContext('2d');
      requestAnimationFrame(draw);
      //清空画布
      const { width, height } = cvs;
      ctx.clearRect(0, 0, width, height);
      //判断分析器是否初始化
      if (!isInit) {
        return;
      }
      //分析器节点分析出数据到数组中
      analyser.getByteFrequencyData(dataArry); //让分析器节点分析当前音频源数据，把分析结果添加到数组
      const len = dataArry.length;
      const barWidth = width / len / 1.5;
      const linear = ctx.createLinearGradient(0, 0, width, height);
      linear.addColorStop(0.1, '#00df9b');
      linear.addColorStop(0.3, '#00cab0');
      linear.addColorStop(0.6, '#00a8d1');
      linear.addColorStop(0.9, '#3275f5');
      ctx.fillStyle = linear;
      for (let i = 0; i < len; i++) {
        const data = dataArry[i]; //<256
        const barHeight = (data / 255) * height;
        const x1 = i * barWidth + width / 2;
        const x2 = width / 2 - (i + 1) * barWidth;
        const y = height - barHeight;
        ctx.fillRect(x1, y, barWidth, barHeight);
        ctx.fillRect(x2, y, barWidth, barHeight);
      }
    };
    nextTick(() => {
      initCancas();
      draw();
    });
    onBeforeUnmount(() => {
      cancelAnimationFrame(draw);
    });
    return () => (
      <div class="je-audio-preview-box">
        <div class="audio-play-box">
          <canvas id={canvasId.value} style={{ width: '100%', height: '130px' }}></canvas>
          <audio
            controls
            onPlay={onPlay}
            src={audioUrl.value}
            id={audioId.value}
            style={{ width: '100%', marginTop: '50px' }}
          ></audio>
        </div>
      </div>
    );
  },
});
