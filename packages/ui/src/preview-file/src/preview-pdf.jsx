import { defineComponent, nextTick, watch, ref } from 'vue';
import { loadPdfObject, getFileUrlByKey } from './utils';
import { isNotEmpty, ajax } from '@jecloud/utils';

export default defineComponent({
  name: 'JePreviewPdf',
  inheritAttrs: false,
  props: {
    previewFile: {
      type: Object,
      default: () => null,
    },
    isFuncFile: {
      //是否为子功能附件
      type: Boolean,
      default: () => false,
    },
  },
  setup(props) {
    const loading = ref(false); //初始化loading状态
    const errorStatus = ref(false); //请求失状态
    const errorMsg = 'errorPdfMsg'; //请求失败返回的字符串
    const fileId = ref(
      props.isFuncFile
        ? 'funFdfContent' + props.previewFile.value
        : 'pdfContent' + props.previewFile.value,
    );

    watch(
      () => props.previewFile,
      (newValue) => {
        loading.value = true;
        const curPdfDom = document.getElementById(fileId.value);
        const embedObj = curPdfDom?.getElementsByClassName('pdfobject');
        if (curPdfDom && embedObj[0]) {
          embedObj[0].remove();
        }
        if (isNotEmpty(newValue)) {
          loadPdfObject().then((pdf) => {
            switch (newValue.suffix.toLowerCase()) {
              case 'pdf':
                let previewFileKey = getFileUrlByKey(newValue.value);
                loading.value = false;
                errorStatus.value = false;
                nextTick(() => {
                  const options = {
                    title: newValue?.label ?? newValue?.relName,
                  };
                  pdf.embed(previewFileKey, `#${fileId.value}`, options);
                });
                break;
              case 'doc':
              case 'docx':
              case 'xls':
              case 'xlsx':
              case 'ppt':
              case 'pptx':
                errorStatus.value = false;
                ajax({
                  url: '/je/document/getPreviewOfficeUrl',
                  method: 'GET',
                  params: { fileKey: newValue.value },
                }).then((res) => {
                  if (res.success) {
                    loading.value = false;
                    const options = {
                      title: newValue?.label ?? newValue?.relName,
                    };
                    nextTick(() => {
                      let pdfUrl = res.data;
                      if (window.AJAX_BASE_URL) {
                        pdfUrl = window.AJAX_BASE_URL + res.data;
                      }
                      pdf.embed(pdfUrl, `#${fileId.value}`, options);
                    });
                  } else {
                    loading.value = false;
                    errorStatus.value = true;
                  }
                });
                break;
            }
          });
        }
      },
      { immediate: true, deep: true },
    );

    return () => (
      <div>
        {loading.value ? (
          <div class="je-pdf-file-loading">
            {!errorStatus.value ? (
              <div class="sk-circle">
                <div class="sk-circle1 sk-child"></div>
                <div class="sk-circle2 sk-child"></div>
                <div class="sk-circle3 sk-child"></div>
                <div class="sk-circle4 sk-child"></div>
                <div class="sk-circle5 sk-child"></div>
                <div class="sk-circle6 sk-child"></div>
                <div class="sk-circle7 sk-child"></div>
                <div class="sk-circle8 sk-child"></div>
                <div class="sk-circle9 sk-child"></div>
                <div class="sk-circle10 sk-child"></div>
                <div class="sk-circle11 sk-child"></div>
                <div class="sk-circle12 sk-child"></div>
              </div>
            ) : null}

            {errorStatus.value ? (
              <div class="desc-message">文件获取失败，无法预览！</div>
            ) : (
              <div class="desc-message">文档正在转换中，请稍等！</div>
            )}
          </div>
        ) : (
          <div id={fileId.value} style={{ height: '100%' }}></div>
        )}
      </div>
    );
  },
});
