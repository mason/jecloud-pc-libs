import { defineComponent } from 'vue';
import { Button } from '../../';
import { downloadFile } from '@jecloud/utils';
import noSupportPreview from './assets/no-support-preview.png';

export default defineComponent({
  name: 'JePreviewNosupport',
  inheritAttrs: false,
  props: {
    previewFile: {
      type: String,
      default: () => null,
    },
  },
  setup(props) {
    //下载文件
    const handleDownloadFile = () => {
      downloadFile(props.previewFile?.value);
    };
    return () => (
      <div class="je-preview-file-no-support">
        <div class="no-support-preview-box">
          <img src={noSupportPreview} alt="" />
          <div class="no-support-info">该文件类型不支持预览，请点击下载后进行查看。</div>
          <Button type="primary" icon="fal fa-download" onClick={handleDownloadFile}>
            下载
          </Button>
        </div>
      </div>
    );
  },
});
