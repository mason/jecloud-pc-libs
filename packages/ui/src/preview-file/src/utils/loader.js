import { loadjs, getAxiosBaseURL } from '@jecloud/utils';
import { usePublicPath } from '../../../hooks/use-public-path';
import JsCookie from 'js-cookie';
/**
 * 加载外部静态资源，不使用npm安装
 * @returns
 */
export function loadPdfObject() {
  const publicPath = usePublicPath();
  const basePath = `${publicPath}static/pdfobject`;
  if (window.PDFObject) {
    return Promise.resolve(window.PDFObject);
  } else {
    window.define = undefined;
    return loadjs([`${basePath}/pdfobject.min.js`], { returnPromise: true }).then(() => {
      return Promise.resolve(window.PDFObject);
    });
  }
}

/**
 * 加载图片预览静态资源
 */
export function loadImageObject() {
  const publicPath = usePublicPath();
  const basePath = `${publicPath}static/imageObj`;
  if (window.ZxImageView) {
    return Promise.resolve(window.ZxImageView);
  } else {
    window.define = undefined;
    return loadjs([`${basePath}/zx-image-view.js`], {
      returnPromise: true,
    }).then(() => {
      return Promise.resolve(window.ZxImageView);
    });
  }
}

/**
 * 文件类型常量
 */
export const FileType = {
  PDF: 'PDF', //pdf
  IMAGE: 'IMAGE', //图片
  VIDEO: 'VIDEO', //视频
  AUDIO: 'AUDIO', //音频
  NOSUPPORT: 'NO', //不支持
  NOFIND: 'NOFIND', //已丢失
};

const API_DOCUMENT_PREVIEW = '/je/document/preview'; //文件预览地址
//获取预览文件
export const getFileUrlByKey = (fileKey) => {
  if (fileKey?.startsWith('http')) {
    return fileKey;
  } else {
    let url = `${getAxiosBaseURL()}${API_DOCUMENT_PREVIEW}/${fileKey}`;
    // 处理iframe请求头没有token
    const token = JsCookie.get('authorization');
    const localStorageToken = localStorage.getItem('authorization');
    if (localStorageToken && !token) {
      url += '?authorization=' + localStorageToken;
    }
    return url;
  }
};

//给文件树结构数据增加value和title属性，共TreeSelect组件使用
export const calculateValueAndTitle = (treeList) => {
  treeList.forEach((element) => {
    if (element.nodeType && element.nodeType === 'LEAF') {
      element.title = element.text;
      element.label = element.text;
      element.value = element.id;
      element.suffix = element.nodeInfoType;
      element.selectable = true;
    } else {
      element.title = element.text;
      element.label = element.text;
      element.value = element.id;
      element.suffix = element.nodeInfoType;
      element.selectable = false;
    }

    if (element.children) {
      calculateValueAndTitle(element.children);
    }
  });
  return treeList;
};

/**
 * 树结构转列表
 */
export const treeToList = (tree, result = [], level = 0) => {
  tree.forEach((node) => {
    if (node.nodeType && node.nodeType === 'LEAF') {
      result.push(node);
    }
    node.children && treeToList(node.children, result, level + 1);
  });
  return result;
};
