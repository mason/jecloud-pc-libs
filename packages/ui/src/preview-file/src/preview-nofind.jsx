import { defineComponent } from 'vue';
import noFindPreview from './assets/no-find-preview.png';

export default defineComponent({
  name: 'JePreviewNofind',
  inheritAttrs: false,
  props: {
    previewFile: {
      type: String,
      default: () => null,
    },
  },
  setup(props) {
    return () => (
      <div class="je-preview-file-no-support">
        <div class="no-support-preview-box">
          <img src={noFindPreview} alt="" />
          <div class="no-support-info">该文件已丢失，无法预览！</div>
        </div>
      </div>
    );
  },
});
