import { defineComponent, ref } from 'vue';
import { getFileUrlByKey } from './utils';

export default defineComponent({
  name: 'JePreviewVideo',
  inheritAttrs: false,
  props: {
    previewFile: {
      type: Object,
      default: () => null,
    },
  },
  setup(props) {
    const videoUrl = ref(getFileUrlByKey(props.previewFile?.value));

    return () => (
      <div style={{ width: '100%', height: '100%', overflow: 'hidden' }}>
        <video controls src={videoUrl.value} style={{ width: '100%', height: '100%' }}></video>
      </div>
    );
  },
});
