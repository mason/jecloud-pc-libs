import { computed, onMounted, nextTick, watch, ref } from 'vue';
import { pick, omit } from '@jecloud/utils';
import { useRowDD } from '../../../grid/src/plugins/sortablejs';
import { useTable } from '../../../grid/src/table';
import { useSearch } from '../hooks/use-search';
import { useAction } from '../hooks/use-action';
export function useTree({ props, context }) {
  const table = useTable({ props, context, tree: true });
  const { $table, $plugin, tableCompEvents, tableCompProps, tableCompSlots } = table;
  // 处理tree中的search值
  Object.assign($table, {
    treeSearchValue: ref(''),
    resetTreeSearchValue() {
      $table.treeSearchValue.value = '';
    },
  });
  const { slots } = context;
  // 树形属性
  const treeProps = computed(() => {
    const _props = pick(tableCompProps.value, Object.keys(tableCompProps.value));
    // 单选，多选
    if (props.multiple) {
      Object.assign(_props.checkboxConfig, { labelField: 'text' });
      _props.rowConfig.isCurrent = false;
    } else {
      _props.checkboxConfig = null;
      _props.rowConfig.isCurrent = true;
    }
    _props.showHeader = false;
    return _props;
  });
  // 树形插槽
  const treeSlots = omit(tableCompSlots, ['default']);

  // 树形列插槽
  const treeColumnSlots = {
    action: useAction($table),
    nodeIcon: slots.nodeIcon,
    default: slots.nodeDefault,
  };

  // 树形列配置
  const treeColumn = function (multiple) {
    return {
      treeNode: true,
      type: multiple ? 'checkbox' : '',
      showOverflow: 'title',
      width: 'auto',
      field: 'text',
      rowField: 'id',
      ...props.nodeColumn,
      slots: treeColumnSlots,
    };
  };
  // 动态修改树形选择模式
  watch(
    () => props.multiple,
    (multiple) => {
      $table.clearSelectedRecords();
      $table.loadColumn([treeColumn(multiple)]);
    },
    { immediate: true },
  );
  // 加载树形列
  const column = treeColumn(props.multiple);
  onMounted(() => {
    nextTick(() => {
      $table.loadColumn([column]);
    });
  });

  // 启用拖拽
  if (treeProps.value.draggable) {
    useRowDD({ ...$table, delay: 50 });
  }

  // 查询
  const { searchSlot } = useSearch($table);

  return { ...table, treeProps, treeSlots, treeEvents: tableCompEvents, searchSlot };
}
