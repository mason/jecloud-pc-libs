import { cascadeTree, pinyin, isFunction } from '@jecloud/utils';
import { Search } from '../../../search';
/**
 * 查询工具条
 *
 * @param {*} { props }
 * @return {*}
 */
export function useSearch($tree) {
  const { props, context, treeSearchValue } = $tree;
  const { slots, emit } = context;
  // 选中属性节点
  const onSelect = (value, option) => {
    // 展开
    $tree
      .setTreeExpand4Path({
        row: $tree.getRowById(option.id),
        path: option.nodePath,
        async: option.async,
        expand: true,
      })
      .then(() => {
        const row = $tree.getRowById(option.id);
        // 选中
        $tree.setSelectRow(row);
        // 滚动
        $tree.scrollToRow(row);
        // 触发查询选中事件
        emit('search-select', { $tree, row, option });
      });
  };
  // 节点过滤函数
  const filter =
    props.search?.filter ?? // 已定义过滤函数
    ((item, keyword) => {
      const code = item.code?.toString().toLocaleLowerCase() ?? '';
      const text = item.text?.toString().toLocaleLowerCase() ?? '';
      const pinyinText = pinyin(text, 'pinyin');
      if (code.includes(keyword) || text.includes(keyword) || pinyinText.includes(keyword)) {
        return true;
      }
      return false;
    });
  // 查询关键字
  const onSearch = (keyword, resolve) => {
    if (isFunction(props.search) || isFunction(props.search?.search)) {
      const searchFn = props.search?.search ?? props.search;
      searchFn(keyword, resolve);
    } else {
      const items = [];
      keyword = keyword.toLocaleLowerCase();
      cascadeTree(props.data ?? props.store?.data ?? [], (item) => {
        if (filter(item, keyword)) {
          items.push({
            id: item.id,
            text: item.text,
            code: item.code,
            value: item.text,
            item,
          });
        }
      });
      resolve(items);
    }
  };

  const searchSlot = () => {
    return [
      slots.tbar?.(),
      props.search ? (
        <div class="je-tree-search-wrapper" style={props.search?.style}>
          <Search
            labelField="text"
            valueField="code"
            onSearch={onSearch}
            onSelect={onSelect}
            v-model:value={treeSearchValue.value}
            enterButton={props.search?.enterButton ?? ''}
            subLabelField={props.search?.subLabelField ?? 'code'}
            placeholder={props.search?.placeholder ?? ''}
          />
        </div>
      ) : null,
    ];
  };

  return { searchSlot };
}
