import { withInstall } from '../utils';
import JePinyin from './src/pinyin';

export const Pinyin = withInstall(JePinyin);

export default Pinyin;
