import { inject, provide } from 'vue';

export const InputContextKey = Symbol('jeInputContextKey');
export const useProvideInput = (state) => {
  provide(InputContextKey, state);
};

export const useInjectInput = () => {
  return inject(InputContextKey, {});
};
