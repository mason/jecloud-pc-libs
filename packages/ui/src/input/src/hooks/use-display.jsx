import { useListeners, useModelValue } from '../../../hooks';
import { useInjectForm } from '../../../form/src/context';
import { filterEmpty } from '../../../utils/props';
import { isEmpty, isFunction, mitt } from '@jecloud/utils';
import { nextTick, onMounted, onUnmounted, ref, watch } from 'vue';

export function useDisplay({ props, context }) {
  const { slots, expose } = context;
  const modelValue = useModelValue({ props, context });
  const { fireListener } = useListeners({ props });
  const $form = useInjectForm();
  const padding = ref(props.padding);
  const rendered = ref(false);
  const slotRef = ref();
  let rendererSlot;
  const $display = {
    emitter: mitt(),
    modelValue,
    on(...args) {
      return $display.emitter.on(...args);
    },
    setValue(value) {
      modelValue.value = value;
    },
    getValue() {
      return modelValue.value;
    },
    getModel() {
      return $form?.model;
    },
    getForm() {
      return $form;
    },
  };
  expose($display);

  const getParams = () => ({
    $form,
    $display,
    model: $form?.model,
    value: modelValue.value,
  });
  // 添加事件触发机制，方便插槽函数使用
  const propsWatchFn = watch(
    () => props,
    () => {
      $display.emitter.emit('props-change', props);
    },
    { deep: true },
  );
  const valueWatchFn = watch(
    () => modelValue.value,
    () => {
      $display.emitter.emit('change', modelValue.value);
    },
  );
  onUnmounted(() => {
    propsWatchFn();
    valueWatchFn();
  });

  onMounted(() => {
    nextTick(() => {
      const args = getParams();
      try {
        // 解析插槽函数
        const result = fireListener('renderer', args);
        if (isFunction(result)) {
          rendererSlot = result;
        }
      } catch (error) {
        error;
      }
      rendered.value = true;
      // 渲染插槽
      nextTick(() => {
        rendererSlot?.(slotRef.value, args);
      });
    });
  });
  // inputStyle事件
  const getInputStyle = () => {
    return fireListener('input-style', getParams());
  };
  // renderer事件
  const displaySlot = () => {
    let vnode;
    if (!rendererSlot && rendered.value) {
      const params = getParams();
      vnode = filterEmpty(slots.display?.(params) ?? fireListener('renderer', params)) || [];
      vnode = isEmpty(vnode) ? modelValue.value : vnode;
    }
    return (
      <div class="je-input-display-renderer" ref={slotRef}>
        {vnode}
      </div>
    );
  };

  return { displaySlot, getInputStyle, padding, modelValue };
}
