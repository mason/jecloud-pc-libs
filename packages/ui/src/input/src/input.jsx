import { defineComponent, getCurrentInstance, ref } from 'vue';
import { Input } from 'ant-design-vue';
import { useModelValue, useExtendMethods, useStyle4Size, useListeners } from '../../hooks';
import { omit } from '@jecloud/utils';
import { useProvideInput } from './context';
import { addonProps } from '../../hooks/use-addon';
export default defineComponent({
  name: 'JeInput',
  components: { Input },
  inheritAttrs: false,
  props: {
    value: { type: String, default: '' },
    width: Number,
    height: Number,
    inputStyle: Object,
    ...addonProps,
  },
  emits: ['update:value', 'reset', 'before-reset', 'change'],
  setup(props, context) {
    const { slots, attrs, expose, emit } = context;
    const { fireListener } = useListeners({ props, context });
    const value = useModelValue({ props, context, changeEvent: true });
    const $plugin = ref();
    const methods = useExtendMethods({ plugin: $plugin, keys: ['blur', 'focus'] });
    const onChange = () => {
      emit('change', value.value);
    };
    // TODO: 注入对象，方便触发reset事件
    useProvideInput({
      type: 'input',
      instance: getCurrentInstance(),
      props,
      context,
      onBeforeReset() {
        return fireListener('before-reset');
      },
    });
    expose({
      ...methods,
      $plugin,
    });
    return () => (
      <Input
        ref={$plugin}
        v-model:value={value.value}
        autocomplete="off"
        allow-clear={true}
        {...attrs}
        {...omit(props, ['value'])}
        onChange={onChange}
        style={useStyle4Size({ props })}
        v-slots={slots}
        class="je-input"
      ></Input>
    );
  },
});
