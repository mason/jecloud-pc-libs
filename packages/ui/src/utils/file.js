import { h } from 'vue';
import { Modal, PreviewFile } from '../components';
import { decode, getFileUrlByKey, getSystemConfig } from '@jecloud/utils';

//预览文件
export function previewFile(fileItem = null, fileList = []) {
  if (getSystemConfig('JE_DOC_PREVIEW_BROWSER') == '1') {
    window.open(getFileUrlByKey(fileItem?.fileKey));
    return false;
  }
  previewFileModal(fileItem, decode(fileList));
}

//文件整合预览
function previewFileModal(fileItem, fileList) {
  //关闭
  const onClose = () => {
    $modal?.close();
  };
  //最大化
  const onMaxStaus = () => {
    $modal?.maximize();
  };
  //还原
  const onRevertModal = () => {
    $modal?.revert();
  };

  const $modal = Modal.window({
    showHeader: false,
    bodyStyle: { height: '100%', padding: '0px' },
    content() {
      return h(PreviewFile, {
        fileItem,
        fileList,
        handleStatus: true,
        showOpenNewView: true,
        onClose,
        onMaxStaus,
        onRevertModal,
      });
    },
  });
}
