import { isPlainObject, isString } from '@jecloud/utils';
import { Fragment, Comment, Text } from 'vue';
export function filterEmpty(children = []) {
  const res = [];
  if (isString(children) || isPlainObject(children)) {
    children = [children];
  }
  children.forEach((child) => {
    if (Array.isArray(child)) {
      res.push(...child);
    } else if (child.type === Fragment) {
      res.push(...child.children);
    } else {
      res.push(child);
    }
  });
  return res.filter((c) => !isEmptyElement(c));
}

export function isEmptyElement(c) {
  return (
    c &&
    (c.type === Comment ||
      (c.type === Fragment && c.children.length === 0) ||
      (c.type === Text && c.children.trim() === ''))
  );
}

export function isValidSlot(value) {
  return (
    value !== undefined &&
    value !== null &&
    (Array.isArray(value) ? filterEmpty(value).length : true)
  );
}
