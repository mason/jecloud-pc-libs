import { getCurrentInstance } from 'vue';
import { forEach } from '@jecloud/utils';
import ConfigProvider from '../config-provider';
import { setAppContext } from '../index';
/**
 * 安装注册事件
 * @param comp 组件
 * @returns
 */
export function withInstall(comp, afterFn) {
  // 绑定依赖组件
  forEach(comp.installComps, (item, key) => {
    comp[key] = item.comp;
  });
  // 安装插件方法
  comp.install = function (app) {
    setAppContext(app);
    // 注册组件，默认使用自己的组件，如果封装ant组件，则注册ant组件
    app.component(comp.displayName || comp.name, comp);
    // 注册依赖组件
    forEach(comp.installComps, (item) => {
      app.component(item.name, item.comp);
    });

    afterFn?.(app);
  };
  return comp;
}

/**
 * 安装依赖的第三方插件，在setup中调用
 *
 * @export
 * @param {*} plugins
 */
export function installPlugins(...plugins) {
  const appContext =
    ConfigProvider.getGlobalConfig('appContext') ?? getCurrentInstance()?.appContext?.app;
  if (appContext) {
    // 已经安装过的插件
    const installeds = appContext.jeui_installeds || [];
    forEach(plugins, (plugin) => {
      if (
        !appContext.component(plugin.displayName || plugin.name) &&
        !installeds.includes(plugin)
      ) {
        appContext.use(plugin);
        installeds.push(plugin);
      }
    });
    appContext.jeui_installeds = installeds;
  }
}

export function isEnableConf(conf) {
  return conf && conf.enabled !== false;
}
