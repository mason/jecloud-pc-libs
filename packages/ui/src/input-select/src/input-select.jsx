import { computed, defineComponent, h, watch } from 'vue';
import Input from '../../input';
import { useListeners, useModelValue } from '../../hooks';
import { isEmpty, hasClass } from '@jecloud/utils';
export default defineComponent({
  name: 'JeInputSelect',
  props: {
    value: { type: String, default: '' },
    editable: Boolean,
    readonly: Boolean,
    disabled: Boolean,
    textarea: Boolean,
    display: Boolean, // 使用展示字段
    type: { type: String },
    icon: { type: String, default: 'fal fa-list-ul' },
  },
  emits: ['update:value', 'select', 'reset', 'change', 'link-click'],
  setup(props, context) {
    const { slots, attrs, emit } = context;
    const { hasListener } = useListeners({ props, context });
    const linkClick = hasListener('link-click');
    const value = useModelValue({ props, context, changeValid: true });
    const computeReadonly = computed(() => {
      return props.readonly || props.disabled;
    });
    watch(
      () => value.value,
      () => {
        emit('change', value.value);
      },
    );
    // 默认重置事件
    const onReset = (...args) => {
      emit('reset', ...args);
    };
    // 默认选择事件
    const onSelect = () => {
      !computeReadonly.value && emit('select');
    };

    return () =>
      h(
        'div',
        {
          class: 'je-input-select-wrapper',
          onClick(e) {
            // 解决input为disabled时单击失效问题，hasClass(e.target, 'je-input')没有后缀的判断条件
            // hasClass(e.target, 'ant-input-affix-wrapper'）有后缀的判断条件
            if (hasClass(e.target, 'je-input') || hasClass(e.target, 'ant-input-affix-wrapper')) {
              if (linkClick && !isEmpty(value.value) && !props.editable) {
                emit('link-click');
              }
            }
          },
        },
        h(
          props.display ? Input.Display : props.textarea ? Input.TextArea : Input,
          {
            ...attrs,
            readonly: !props.editable,
            disabled: computeReadonly.value,
            value: value.value,
            'onUpdate:value': function (val) {
              value.value = val;
            },
            allowClean: true,
            onClick() {
              if (isEmpty(value.value) && !props.editable) {
                emit('select');
              }
            },
            class: [
              {
                'je-input-select': true,
                'je-input-select-readonly': computeReadonly.value,
                'je-input-select-link': linkClick && !isEmpty(value.value),
                textarea: props.textarea,
              },
              props.type ? `je-input-select-${props.type}` : '',
            ],
            onReset,
          },
          { ...slots, suffix: () => <i class={['icon-button', props.icon]} onClick={onSelect} /> },
        ),
      );
  },
});
