import { defineComponent } from 'vue';
import { pick } from '@jecloud/utils';
import InputSelect from './input-select';
import { useFuncSelect } from './hooks/use-func-select';
import { useModelValue } from '../../hooks';
export const selectProps = {
  name: String,
  idProperty: String,
  value: { type: String, default: '' },
  configInfo: { type: String, default: '' },
  model: Object,
  parentModel: Object,
  querys: Array,
  orders: Array,
  product: String,
  selectOptions: Object,
  editable: Boolean,
  readonly: Boolean,
  disabled: Boolean,
  selectExp: Object, // 选择表达式
  dicValueConfig: Array,
  multiple: { type: Boolean, default: undefined },
};
export const selectEmits = ['update:value', 'update:model', 'before-select', 'select', 'reset'];

export default defineComponent({
  name: 'JeInputSelectGrid',
  inheritAttrs: false,
  props: selectProps,
  emits: selectEmits,
  setup(props, context) {
    const { attrs, slots } = context;
    const value = useModelValue({ props, context, changeValid: true });
    const type = 'grid';
    // 功能选择
    const { onReset, onSelect } = useFuncSelect({ props, context, value, type });
    return () => (
      <InputSelect
        {...attrs}
        {...pick(props, Object.keys(selectProps))}
        v-model:value={value.value}
        type={type}
        icon="fas fa-list-ul"
        v-slots={slots}
        onSelect={onSelect}
        onReset={onReset}
      />
    );
  },
});
