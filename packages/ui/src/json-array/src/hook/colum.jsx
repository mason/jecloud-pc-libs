import { h, resolveComponent } from 'vue';
import { cloneDeep, isNotEmpty, isObject, isString } from '@jecloud/utils';
import { Grid } from '../../../grid';
import { FuncFieldTypeEnum } from '@jecloud/utils/src/func/enum/func-field-enum';

/**
 *  初始化列
 * @param {*} param0
 * @returns
 */
export function useColumns(columns, $grid, state, $jsonArray) {
  const columnFileds = [];
  const columnData = [];
  const rules = {};
  cloneDeep(columns).forEach((item) => {
    // 没有隐藏
    if (!item.hidden) {
      // 处理插槽
      item.slots = {};
      // 处理default插槽
      if (item.renderer) {
        item.slots['default'] = item.renderer;
      }
      // 处理编辑插槽
      item.slots['edit'] = editSlot(item);

      // 处理校验规则
      if (item.rules) {
        rules[item.field] = item.rules;
      }
      // 处理必填项
      if (item.required) {
        if (!rules[item.field]) {
          rules[item.field] = [];
        }
        rules[item.field].push({
          required: true,
          message: '该输入项为必填项',
        });
      }
      columnData.push(item);
    }

    columnFileds.push(item.field);
  });
  // 默认值
  const defaultValues = getDefaultValues(columnFileds);
  // 添加action列
  if (state.enableRemove || state.enableAdd) {
    parseActionColumn(columnData, $grid, defaultValues, state, $jsonArray);
  }
  return { columnData, rules, columnFileds };
}
/**
 *  列表编辑插槽
 * @param {*} column
 * @returns
 */
function editSlot(column) {
  const { editField, configInfo } = column;
  if (editField == false) {
    return null;
  }
  // 默认文本框
  let component = 'textfield';
  const confing = {}; // 参数
  let slots = {}; // 插槽
  // 如果是字符串
  if (isString(editField)) {
    component = editField;
    confing['configInfo'] = configInfo || '';
  }
  // 如果是对象
  if (isObject(editField)) {
    component = editField.component;
    Object.assign(confing, editField.props);
    slots = editField.slots || {};
  }
  // 下拉框处理
  if (component == 'cbbfield') {
    confing.getPopupContainer = () => {
      return document.body;
    };
  }
  column.editRender = Grid.Renderer[FuncFieldTypeEnum.getComponent(component)] ?? {};
  return (options) => {
    const { row } = options;
    return h(
      resolveComponent(FuncFieldTypeEnum.getComponent(component)),
      {
        value: row[column.field],
        model: row,
        'onUpdate:value': function (val) {
          row[column.field] = val;
        },
        ...confing,
      },
      slots,
    );
  };
}
/**
 *  操作列
 * @param {*} columns
 */
function parseActionColumn(columns, $grid, defaultValues, state, $jsonArray) {
  const actionColumn = {
    width: 42,
    resizable: false,
    align: 'center',
    field: 'actionColumn',
    headerAlign: 'center',
    fixed: 'right',
    className: 'je-json-array-action-cell',
    headerClassName: 'je-json-array-action-header',
    slots: {
      header: () => {
        return state.enableAdd ? (
          <i
            class="action-item green fas fa-plus-circle"
            onClick={() => {
              addRecord($grid, defaultValues);
            }}
          ></i>
        ) : null;
      },
      default: (options) => {
        // 删除按钮
        return state.enableRemove ? (
          <i
            class="action-item red fas fa-times-circle"
            onClick={() => {
              removeRecord($grid, options, $jsonArray);
            }}
          ></i>
        ) : null;
      },
    },
  };
  // 按钮位置
  if (state.buttonAlign === 'right') {
    actionColumn.fixed = 'right';
    columns.push(actionColumn);
  } else {
    actionColumn.fixed = 'left';
    columns.unshift(actionColumn);
  }
}
/**
 * 添加数据
 * @param {*} $grid
 */
function addRecord($grid, defaultValues) {
  const insertRecord = $grid.value.store.insert([cloneDeep(defaultValues)]);
  // 设置激活行，可编辑
  $grid.value.setActiveRow?.(insertRecord[0]);
}
/**
 *  删除数据
 * @param {*} $grid
 * @param {*} param1
 */
function removeRecord($grid, { row }, $jsonArray) {
  $grid.value.store.remove(row);
  $jsonArray.refreshJsonArrayData();
}
/**
 * 处理默认值
 */
function getDefaultValues(columnFileds) {
  const defaultValues = {};
  columnFileds.forEach((field) => {
    defaultValues[field] = '';
  });
  return defaultValues;
}
