import { ref, onMounted, nextTick, reactive, computed, watch } from 'vue';
import {
  Data,
  isEmpty,
  decode,
  encode,
  cloneDeep,
  parseEventResult4Promise,
  useListeners,
} from '@jecloud/utils';
import { useColumns } from './colum';
import { useModelValue } from '../../../hooks';
import Form from '../../../form';
import { Form as AForm } from 'ant-design-vue';
export function useJsonArray({ props, context }) {
  const { fireListener } = useListeners({ props, context });
  // 执行事件方法
  const doActionEvent = (eventName, options = {}) => {
    const result = fireListener(eventName, {
      model: props.model,
      ...options,
    });
    return result;
  };
  const $grid = ref(); // 列表对象
  const validRules = ref({}); // 列表校验数据
  const columnArray = ref([]); // 列数据
  const modelValue = useModelValue({ props, context });
  const gridStore = Data.Store.useGridStore({
    data: [],
  });
  const computeReadonly = computed(() => {
    return props.readonly || props.disabled;
  });
  const state = reactive({
    errorMessage: '', // 错误提示
    columnFileds: [], // 列字段集合
    enableRemove: !(props.actionColumn.remove == '0'), // 是否有删除按钮,默认有
    enableAdd: !(props.actionColumn.add == '0'), // 是否有添加按钮,默认有
    enableDrag: props.actionColumn.drag == '1', // 是否可以拖拽,默认不能
    buttonAlign: props.actionColumn.buttonAlign || 'right', // 操作列的位置,默认右侧
    enableDataPanel: props.actionColumn.enableDataPanel == '1', // 打开数据面板
    gridConfig: {},
  });
  const $jsonArray = {
    validator(rule = {}) {
      // 如果没有配置功能就通过必填校验
      if (isEmpty(columnArray.value)) {
        return Promise.resolve();
      }
      if (rule.required && isEmpty($grid.value.store.data)) {
        state.errorMessage = rule.message;
        return Promise.reject(rule.message);
      } else {
        state.errorMessage = '';
        rule.message = '列表校验未通过';
        return $grid.value
          .validate(true)
          .then((errorMaps) =>
            isEmpty(errorMaps) ? Promise.resolve() : Promise.reject(errorMaps),
          );
      }
    },
    clearValidate() {
      state.errorMessage = '';
    },
    setReadOnly(readonly) {
      computeReadonly.value = readonly;
      if (!$grid.value) return;
      const actionColumnObj = $grid.value.getColumnByField('actionColumn');
      // 想要操作列
      if (!computeReadonly.value) {
        $grid.value.showColumn(actionColumnObj);
      } else {
        $grid.value.hideColumn(actionColumnObj);
      }
    },
    // 获得组件最终的数据
    refreshJsonArrayData() {
      const gridData = cloneDeep(gridStore.data);
      const generateData = [];
      gridData.forEach((record) => {
        const newRecord = {};
        state.columnFileds.forEach((field) => {
          const fieldValue = record[field] || '';
          newRecord[field] = fieldValue;
        });
        generateData.push(newRecord);
      });

      modelValue.value = encode(generateData);

      return modelValue.value;
    },
  };
  // 注册formitem插槽对象
  const formItemContext = Form.useInjectFormItemContext();
  formItemContext?.addSlotItem($jsonArray);
  // 重置FormItem，防止列表编辑组建触发本子功能集合的校验函数
  AForm.useInjectFormItemContext();

  // 执行column-render加载列数据
  const { columns = [], gridConfig } = doActionEvent('grid-render') || {};
  if (gridConfig) {
    state.gridConfig = gridConfig;
  }
  // 分装列信息
  const { columnData, rules, columnFileds } = useColumns(columns, $grid, state, $jsonArray);
  columnArray.value = columnData;
  validRules.value = rules;
  state.columnFileds = columnFileds;

  onMounted(() => {
    nextTick(() => {
      // 初始化列表数据
      watch(
        () => modelValue.value,
        (value) => {
          const gridData = value ? decode(value) : [];
          gridStore.loadData(gridData);
        },
        {
          immediate: true,
        },
      );
      // 监听只读变量
      watch(
        () => computeReadonly.value,
        () => {
          $jsonArray.setReadOnly(computeReadonly.value);
        },
        {
          immediate: true,
        },
      );
      watch(
        () => gridStore.data,
        () => {
          const options = {
            $field: $jsonArray,
            $fieldGrid: $grid.value,
          };
          doActionEvent('change', options);
          // 用于功能业务事件处理，用户使用上面的change事件
          doActionEvent('private-change', options);
        },
        {
          deep: true,
        },
      );
    });
  });
  // 编辑后事件触发
  const doEditClosed = () => {
    $jsonArray.refreshJsonArrayData();
  };
  //拖拽后事件触发
  const doDrop = () => {
    $jsonArray.refreshJsonArrayData();
  };
  context.expose($jsonArray);
  return {
    $grid,
    columnArray,
    gridStore,
    validRules,
    computeReadonly,
    state,
    doEditClosed,
    doDrop,
  };
}
