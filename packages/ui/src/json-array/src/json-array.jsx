import { defineComponent, reactive, ref } from 'vue';
import { useJsonArray } from './hook/hook';
import { Grid } from '../../grid';
import { Hooks } from '../../hooks';
export default defineComponent({
  name: 'JeJsonArray',
  props: {
    columns: {
      type: Array,
      default: () => {
        return [];
      },
    },
    readonly: Boolean,
    disabled: Boolean,
    value: { type: String, default: '' },
    name: String,
    width: Number,
    height: Number,
    label: { type: String, default: '' },
    actionColumn: {
      type: Object,
      default: () => {
        return {};
      },
    },
  },
  setup(props, context) {
    const fullScreen = ref(false);
    const style = reactive({ height: '192px', ...Hooks.useStyle4Size({ props }) });
    const {
      $grid,
      columnArray,
      gridStore,
      validRules,
      computeReadonly,
      state,
      doEditClosed,
      doDrop,
    } = useJsonArray({
      props,
      context,
    });
    return () => (
      <Grid
        ref={$grid}
        {...style}
        class={{
          'je-json-array': true,
          'is--fullscreen': fullScreen.value,
        }}
        size="mini"
        draggable={!computeReadonly.value && state.enableDrag}
        store={gridStore}
        edit-rules={validRules.value}
        edit-config={{
          enabled: !computeReadonly.value,
          trigger: 'click',
          mode: 'cell',
          showStatus: true,
          showInsertStatus: false,
        }}
        valid-config={{ message: 'tooltip', autoClear: true }}
        tooltip-config={{ isArrow: true, enterDelay: 100, leaveDelay: 100 }}
        onEditClosed={doEditClosed}
        onDrop={doDrop}
        {...state.gridConfig}
        v-slots={{
          tbar: () => {
            // 标题
            return fullScreen.value ? (
              <div class="je-json-array-tbar">
                <span class="title">{props.label}</span>
                <i
                  class="jeicon jeicon-times close"
                  onClick={() => {
                    fullScreen.value = false;
                  }}
                />
              </div>
            ) : null;
          },
          bbar: () => {
            // 打开全屏
            return !state.enableDataPanel || fullScreen.value ? null : (
              <div class="je-json-array-bbar">
                {state.errorMessage ? (
                  <span class="ant-form-item-explain-error">{state.errorMessage}</span>
                ) : null}
                <span
                  class="open"
                  onClick={() => {
                    fullScreen.value = true;
                  }}
                >
                  打开数据面板
                </span>
              </div>
            );
          },
        }}
      >
        {columnArray.value.map((item) => {
          return <Grid.Column {...item} v-slots={item.slots} />;
        })}
      </Grid>
    );
  },
});
