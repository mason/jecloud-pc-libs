import { withInstall } from '../utils';
import JeJsonArray from './src/json-array';
export const JsonArray = withInstall(JeJsonArray);
export default JsonArray;
