import { withInstall } from '../utils';
import JeCheckbox from './src/checkbox';
import JeCheckboxGroup from './src/group';

// 注册依赖组件
JeCheckbox.installComps = {
  Group: { name: JeCheckboxGroup.name, comp: JeCheckboxGroup },
};

export const Checkbox = withInstall(JeCheckbox);

export default Checkbox;
