import { withInstall } from '../utils';
import JeDrawer from './src/drawer';
import { show } from './src/utils';
export const Drawer = withInstall(JeDrawer);
Drawer.show = show;
export default Drawer;
