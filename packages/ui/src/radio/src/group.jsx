import { defineComponent } from 'vue';
import { Radio } from 'ant-design-vue';
import { useModelValue, useStyle4Size } from '../../hooks';
import { useGroup, groupEmits, groupProps, groupSlots } from '../../checkbox/src/hooks/use-group';
import JeRadio from './radio';
import JeRadioButton from './radio-button';
import { filterEmpty } from '../../utils/props';
import { isEmpty } from '@jecloud/utils';
export default defineComponent({
  name: 'JeRadioGroup',
  inheritAttrs: false,
  props: { ...groupProps, optionType: String, height: Number, width: Number },
  emits: groupEmits,
  slots: groupSlots,
  setup(props, context) {
    const { slots, attrs } = context;
    const useButton = props.optionType === 'button';
    const { modelValue, optionsSlot, onChange, useAddonSlot } = useGroup({
      props,
      context,
      component: useButton ? JeRadioButton : JeRadio,
      useRow: !useButton,
    });

    return () => {
      const vnodes = filterEmpty(slots.default?.());
      const element = (
        <Radio.Group
          class={{
            'je-radio-group white-background': true,
            'disabled-background': attrs.disabled,
          }}
          style={useStyle4Size({ props })}
          {...attrs}
          v-model:value={modelValue.value}
          onChange={onChange}
        >
          {isEmpty(vnodes) ? optionsSlot() : vnodes}
        </Radio.Group>
      );
      return useAddonSlot({ props, context, element });
    };
  },
});
