import { withInstall } from '../utils';
import JeRadio from './src/radio';
import JeRadioGroup from './src/group';
import JeRadioButton from './src/radio-button';
JeRadio.installComps = {
  Group: { name: JeRadioGroup.name, comp: JeRadioGroup },
  Button: { name: JeRadioButton.name, comp: JeRadioButton },
};

export const Radio = withInstall(JeRadio);

export default Radio;
