import toolbar from './toolbar';
import plugins from './plugins';
import { useUrls, onDblClickPreviewImage } from './urls';
import { ref } from 'vue';
import { upload, getFileUrlByKey } from '@jecloud/utils';

export function useConfig({ props }) {
  let editor = null;
  const urls = useUrls({ plugins });
  const loading = ref(true);
  const init = {
    width: '100%',
    height: '100%',
    menubar: false, // 隐藏菜单
    statusbar: false, // 隐藏状态栏
    cache_suffix: '?v=5.0.0', // 缓存后缀
    language: 'zh_CN', //语言类型
    suffix: '.min',
    plugins,
    convert_urls: false, // 转换图片url地址，拼接../
    paste_data_images: true, // 支持粘贴图片数据
    images_upload_handler: function (blobInfo, success, failure, progress) {
      // 自定义上传
      upload({
        fileInfo: {
          file: blobInfo.blob(),
          fileName: blobInfo.filename(),
        },
        onUploadProgress(progressEvent) {
          progress((progressEvent.loaded / progressEvent.total) * 100);
        },
      })
        .then((data) => {
          const file = data.data[0];
          let previewUrl = getFileUrlByKey(file.fileKey, 'preview', false);
          const devProxyUrl = '/jeapi'; // 系统默认dev调试前缀，存储时需要去掉
          if (previewUrl.startsWith(devProxyUrl)) {
            previewUrl = previewUrl.substring(devProxyUrl.length);
          }
          success(previewUrl);
        })
        .catch((error) => {
          failure('HTTP Error: ' + error.status, { remove: true });
        });
    },
    ...urls,
    ...toolbar,
    ...props.editorOptions,
  };
  // 主文件
  const tinymceScriptSrc = urls.file_url + init.cache_suffix;
  // 初始化
  const customSetup = init.setup;
  init.setup = function (_editor) {
    editor = _editor;
    customSetup?.(editor);
    setTimeout(() => {
      loading.value = false;
      // 双击预览图片
      onDblClickPreviewImage(editor);
      editor.setMode(props.disabled ? 'readonly' : 'design');
    }, 200);
  };
  // 初始化配置
  window.tinyMCEPreInit = { baseURL: urls.baseURL, suffix: '' };
  return {
    init,
    tinymceScriptSrc,
    loading,
    getEditor() {
      return editor;
    },
  };
}
