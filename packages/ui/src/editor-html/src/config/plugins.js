const plugins = [
  'table', // 表格
  'lists', //高级列表
  'advlist',
  'code', //编辑源码
  'codesample', // 代码示例
  'fullscreen', // 全屏
  'image', // 图片
  'imagetools', // 图片编辑
  'link', // 超链接
  'media', // 多媒体
  'pagebreak', // 分页符
  'preview', // 预览
  'print', // 打印
  'searchreplace', // 查找
  'wordcount', // 字数统计
  'paste',
];
export default plugins;
