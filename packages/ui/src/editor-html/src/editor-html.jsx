import { defineComponent, watch, onMounted, nextTick } from 'vue';
import Editor from '@tinymce/tinymce-vue';
import { useConfig } from './config';
import { isEmpty, omit } from '@jecloud/utils';
import { useEditor } from '../../editor-code/src/hooks/use-editor';
export default defineComponent({
  name: 'JeEditorHtml',
  inheritAttrs: false,
  props: {
    value: { type: String, default: '' },
    width: [String, Number],
    height: { type: [String, Number], default: 400 },
    editorOptions: Object,
    disabled: { type: Boolean, default: false },
  },
  emits: ['update:value', 'change'],
  setup(props, context) {
    const { slots, attrs, expose, emit } = context;
    const { value, style, $plugin } = useEditor({ props, context });
    const { init, tinymceScriptSrc, getEditor, loading } = useConfig({ props });
    watch(
      () => value.value,
      (newValue) => {
        // 处理null值
        if (isEmpty(newValue, true)) {
          value.value = '';
        } else {
          emit('change', value.value);
        }
      },
    );
    // 解决只读首次不加载问题
    onMounted(() => {
      nextTick(() => {
        watch(
          () => props.disabled,
          (newValue) => {
            setTimeout(() => {
              getEditor()?.setMode(newValue ? 'readonly' : 'design');
            }, 800);
          },
        );
      });
    });
    expose({ getEditor, $plugin });
    return () => (
      <div class="je-editor-html" style={{ ...style, ...attrs.style }} v-loading={loading.value}>
        <Editor
          ref={$plugin}
          v-model={value.value}
          {...omit(attrs, ['style', 'onChange'])}
          v-slots={slots}
          init={init}
          tinymceScriptSrc={tinymceScriptSrc}
          disabled={props.disabled}
        />
      </div>
    );
  },
});
