import { withInstall } from '../utils';
import JeSwitch from './src/switch';

export const Switch = withInstall(JeSwitch);

export default Switch;
