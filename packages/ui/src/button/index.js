import JeButton from './src/button';
import { withInstall } from '../utils';
import { Button as AButton } from 'ant-design-vue';

JeButton.installComps = {
  // 注册依赖组件
  Group: { name: 'JeButtonGroup', comp: AButton.Group },
};
export const Button = withInstall(JeButton);

export default Button;
