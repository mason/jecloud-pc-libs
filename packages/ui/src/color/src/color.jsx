import { defineComponent, nextTick, onMounted, onUnmounted, ref } from 'vue';
import { isNumber, isArray, computedDomSize, darken } from '@jecloud/utils';
import Input from '../../input';
import { useListeners, useModelValue } from '../../hooks';
import Pickr from '@simonwep/pickr';
import '@simonwep/pickr/dist/themes/monolith.min.css';
const old_rePositioningPicker = Pickr.prototype._rePositioningPicker;
Pickr.prototype._rePositioningPicker = function () {
  old_rePositioningPicker.call(this);
  // 增加偏移量参数
  const { options } = this;
  if (!options.inline) {
    const { offset } = options;
    const el = this._root.app;
    if (isNumber(offset)) {
      el.style.top = `${computedDomSize(el.style.top) + offset}px`;
      el.style.left = `${computedDomSize(el.style.left) + offset}px`;
    } else if (isArray(offset)) {
      const x = offset[0] ?? 0;
      const y = offset[1] ?? 0;
      el.style.top = `${computedDomSize(el.style.top) + y}px`;
      el.style.left = `${computedDomSize(el.style.left) + x}px`;
    }
  }
};
// 默认颜色值
const defaultColors = [
  '#F53F3F',
  '#FF7D00',
  '#FFF258',
  '#00B42A',
  '#14C9C9',
  '#4E83FD',
  '#722ED1',
  '#333333',
  '#555555',
  '#7F7F7F',
  '#AAAAAA',
  '#D7D7D7',
  '#E5E6EB',
  '#FFFFFF',
];
export default defineComponent({
  name: 'JeColor',
  components: { Input },
  inheritAttrs: false,
  props: {
    value: { type: String, default: '' },
    theme: {
      type: String,
      default: 'input',
      validator: (value) => ['input', 'mini', 'minitext'].includes(value),
    },
    colors: {
      type: Array,
      default: () => defaultColors,
    },
  },
  emits: ['update:value', 'select'],
  setup(props, context) {
    const { slots, attrs } = context;
    const { fireListener } = useListeners({ props, context });
    const value = useModelValue({ props, context, changeValid: true });
    const $input = ref();
    let pickr = null;
    const inputTheme = props.theme === 'input';
    onMounted(() => {
      nextTick(() => {
        const el = inputTheme ? $input.value.$el : $input.value;
        pickr = Pickr.create({
          el,
          theme: 'monolith', // or 'monolith', or 'nano'
          useAsButton: true,
          position: inputTheme ? 'bottom-end' : 'bottom-start',
          appClass: 'je-color-picker',
          offset: [0, -4],
          swatches: props.colors,

          components: {
            // Main components
            preview: true,
            opacity: true,
            hue: true,
            // Input / output Options
            interaction: {
              input: true,
              clear: props.theme !== 'input',
              save: true,
            },
          },
          i18n: {
            // Strings visible in the UI
            'btn:save': '确定',
            'btn:cancel': '取消',
            'btn:clear': '清空',
          },
        });
        pickr
          .on('save', (val) => {
            const color = val ? pickr.getColor().toHEXA().toString() : '';
            const selectResult = fireListener('select', { value: color, pickr });
            if (selectResult !== false) {
              value.value = color;
              pickr.hide();
            }
          })
          .on('show', () => {
            props.value && pickr.setColor(props.value, true);
          });
      });
    });
    onUnmounted(() => {
      pickr && pickr.destroyAndRemove();
    });

    // 颜色提示插槽
    const colorIconSlot = function () {
      return value.value ? (
        <div
          class={['color-icon']}
          style={{ backgroundColor: value.value, borderColor: darken(value.value, 15) }}
        />
      ) : props.theme !== 'input' ? (
        <i class={['color-icon-empty', 'jeicon jeicon-no-color']} />
      ) : null;
    };
    //.ant-input
    return () =>
      props.theme === 'input' ? (
        <Input
          {...attrs}
          allow-clear
          v-model:value={value.value}
          class="je-color je-color-input"
          ref={$input}
          v-slots={{
            ...slots,
            prefix: () => colorIconSlot(),
            suffix: () => <i class="fal fa-palette color-button" />,
          }}
        ></Input>
      ) : (
        <span class="je-color je-color-mini" {...attrs}>
          <span ref={$input} class="color-wrapper">
            {colorIconSlot()}
            {props.theme === 'minitext' ? <span class="color-text">{value.value}</span> : ''}
          </span>
        </span>
      );
  },
});
