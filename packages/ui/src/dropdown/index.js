import JeDropdown from './src/dropdown';
import { withInstall } from '../utils';

export const Dropdown = withInstall(JeDropdown);

export default Dropdown;
