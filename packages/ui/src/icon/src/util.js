import iconI18n from '../../assets/fonts/i18n.json';
import faIconArray from '../../assets/fonts/fa/fa.json';
import { isNotEmpty } from '@jecloud/utils';
import { ref } from 'vue';
const systemIcons = {
  light: {
    code: 'light',
    name: '细体图标',
    fontClass: 'fal',
    fontPrefix: 'fa',
    fontIcons: faIconArray,
  },
  solid: {
    code: 'solid',
    name: '实心图标',
    fontClass: 'fas',
    fontPrefix: 'fa',
    fontIcons: faIconArray,
  },
};
export const fontIcons = ref(['light', 'solid'].map((code) => systemIcons[code]));
export const selectedKeys = ref(fontIcons.value[0].code);
/**
 * 设置全局图标库
 * @param {*} customIcons
 */
export function setIcons(customIcons) {
  if (isNotEmpty(customIcons)) {
    fontIcons.value = [];
    customIcons.forEach((icon) => {
      if (icon.system && systemIcons[icon.code]) {
        fontIcons.value.push({ ...systemIcons[icon.code], name: icon.name });
      } else if (icon.fontClass && icon.fontIcons?.length) {
        fontIcons.value.push(icon);
      }
    });
    selectedKeys.value = fontIcons.value[0].code;
  }
}
/**
 * 加载图标
 * @param {*} param0
 * @returns
 */
export function loadIcons({ keyword, type, page = 1, limit }) {
  const icon = fontIcons.value.find((item) => {
    return item.code === type;
  });
  let data = icon.fontIcons;
  if (keyword) {
    data = data.filter((item) => {
      return (
        item.includes(keyword) ||
        iconI18n[item]?.includes(keyword) ||
        !!item.split('-').find((key) => iconI18n[key]?.includes(keyword))
      );
    });
  }
  const totalCount = data.length;
  const start = (page - 1) * limit;
  const end = start + limit;
  return Promise.resolve({ data: data.slice(start, end), totalCount });
}
