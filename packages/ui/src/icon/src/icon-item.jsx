import { defineComponent } from 'vue';
export default defineComponent({
  name: 'JeIconItem',
  inheritAttrs: false,
  props: { icon: String },
  setup(props, { slots, attrs }) {
    return () => slots.default?.() ?? <i {...attrs} class={props.icon}></i>;
  },
});
