import JeQrcode from './src/qrcode';
import { withInstall } from '../utils';

export const Qrcode = withInstall(JeQrcode);
export default Qrcode;
