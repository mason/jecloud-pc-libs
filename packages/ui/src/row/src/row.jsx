import { defineComponent } from 'vue';
import { Row } from 'ant-design-vue';

export default defineComponent({
  name: 'JeRow',
  inheritAttrs: false,
  setup(props, { slots, attrs }) {
    return () => <Row {...attrs} v-slots={slots} />;
  },
});
