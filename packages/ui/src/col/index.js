import { withInstall } from '../utils';
import JeCol from './src/col';
export const Col = withInstall(JeCol);
export default Col;
