import { defineComponent } from 'vue';
import { Cascader } from 'ant-design-vue';

export default defineComponent({
  name: 'JeCascader',
  components: { ACascader: Cascader },
  inheritAttrs: false,
  setup(props, { slots, attrs }) {
    return () => <a-cascader {...attrs} v-slots={slots}></a-cascader>;
  },
});
