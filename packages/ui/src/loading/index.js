import { withInstall } from '../utils';
import JeLoading from './src/loading';
import LoadingDirective from './src/directive';
JeLoading.setupDirective = function (app) {
  app.directive('loading', LoadingDirective);
};
export const Loading = withInstall(JeLoading, (app) => {
  JeLoading.setupDirective(app);
});
export default Loading;
