import { isBoolean } from '@jecloud/utils';
import { createApp } from 'vue';
import Loading from './loading';
/**
 * v-loading
 * 自定义加载指令
 * @param {String} title
 * @param {Boolean} loading
 */
const LoadingDirective = {
  mounted(el, binding) {
    const app = createApp(Loading);
    const instance = app.mount(document.createElement('div'));
    el.instance = instance;
    const { loading, title } = parseValue(binding.value);
    el.instance.setTitle(title);
    if (loading) {
      appendEl(el);
    }
  },
  updated(el, binding) {
    if (binding.value !== binding.oldValue) {
      const { loading, title } = parseValue(binding.value);
      el.instance.setTitle(title);
      loading ? appendEl(el) : removeEl(el);
    }
  },
};
export default LoadingDirective;
const appendEl = (el) => {
  el.appendChild(el.instance.$el);
};
const removeEl = (el) => {
  el.removeChild(el.instance.$el);
};
const parseValue = (value) => {
  if (isBoolean(value)) {
    value = { loading: value };
  }
  return value;
};
