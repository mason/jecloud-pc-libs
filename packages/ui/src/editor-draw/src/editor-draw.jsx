import { defineComponent, nextTick, onMounted, ref } from 'vue';
import { useEditor } from '../../editor-code/src/hooks/use-editor';
import { useEditorUi } from './mxgraph/src/EditorUi';
import { loadMxGraph } from './mxgraph/utils';
import Panel from '../../panel';
export default defineComponent({
  name: 'JeEditorDraw',
  inheritAttrs: false,
  props: {
    value: { type: String, default: '' },
    width: [String, Number],
    height: { type: [String, Number], default: 400 },
    editorOptions: Object,
  },
  emits: ['update:value'],
  setup(props, context) {
    const { slots, attrs, emit, expose } = context;
    const { value, style, $plugin } = useEditor({ props, context });
    const $el = ref();
    const $sidebar = ref();

    onMounted(() => {
      nextTick(() => {
        loadMxGraph().then((mxgraph) => {
          const { EditorUi, Editor, Graph } = useEditorUi();
          const { mxUtils, STYLE_PATH } = mxgraph;
          const themes = {
            [Graph.prototype.defaultThemeName]: mxUtils
              .load(STYLE_PATH + '/default.xml')
              .getDocumentElement(),
          };
          setTimeout(() => {
            const ui = new EditorUi(new Editor(false, themes), $plugin.value);
            ui.createSidebar($sidebar.value);
          }, 100);
        });
      });
    });
    return () => (
      <Panel class="je-editor-draw" ref={$el} style={{ ...style, ...attrs.style }}>
        <Panel.Item region="left" size="200" split>
          <div ref={$sidebar} class="je-editor-draw-sidebar geSidebar"></div>
        </Panel.Item>
        <Panel.Item>
          <div class="je-editor-draw-body" ref={$plugin}></div>
        </Panel.Item>
      </Panel>
    );
  },
});
