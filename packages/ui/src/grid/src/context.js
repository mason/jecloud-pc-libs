import { inject, provide } from 'vue';

export const GridContextKey = Symbol('jeGridContextKey');

export const useProvideGrid = (state) => {
  provide(GridContextKey, state);
};

export const useInjectGrid = () => {
  return inject(GridContextKey, null);
};
