import { filterEmpty } from '../../../utils/props';

/**
 * 树形节点
 * @param {*} config
 * @returns
 */
export function useTreeNode({ context }) {
  const { slots } = context;
  const config = { slots: {}, props: {} };
  // 默认插槽
  const defaultSlot = slots ? slots.default : null;
  // 图标插槽
  const iconSlot = (args) => {
    const { row, $table } = args;
    const folder = (row.async && !row.leaf) || row.children?.length > 0;
    const expand = $table.isTreeExpandByRow(row);
    // 自定义图标插槽
    const vnodes = filterEmpty(slots.nodeIcon?.(args));
    return vnodes.length ? (
      vnodes
    ) : (
      <i
        class={[
          row.icon
            ? row.icon
            : {
                'is--leaf': !folder,
                'is--folder': folder,
                'is--expand': folder && expand,
                'fal fa-file': !folder,
                'fas fa-folder-open': folder && expand,
                'fas fa-folder': folder && !expand,
              },
        ]}
        style={{ color: row.iconColor || undefined }}
      ></i>
    );
  };

  config.slots.default = (args) => {
    const defaultVNodes = filterEmpty(defaultSlot?.(args));
    // 判断是否已经处理默认插槽，防止重复渲染
    const transformSlot = defaultVNodes.find(
      (item) => 'je-tree-node-content' === item.props?.class,
    );
    if (transformSlot) {
      return defaultVNodes;
    }
    const { row, $table } = args;
    const { computeTreeOpts } = $table.getComputeMaps();
    const treeOpts = computeTreeOpts.value;
    const textField = treeOpts.textField || 'text';
    return [
      <span class="je-tree-node-icon">{iconSlot(args)}</span>,
      <span class="je-tree-node-content">
        {defaultVNodes.length ? defaultVNodes : row[textField]}
      </span>,
      <span class="je-tree-node-actions">
        {row.badge ? (
          <span class={['je-tree-node-badge', row.badgeClass]} style={row.badgeStyle}>
            {row.badge}
          </span>
        ) : (
          ''
        )}
        {slots.action?.(args)}
      </span>,
    ];
  };
  return config;
}
