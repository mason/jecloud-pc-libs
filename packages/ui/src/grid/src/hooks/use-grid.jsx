import { watch, nextTick, onMounted } from 'vue';
import { useColumnDD, useRowDD } from '../plugins/sortablejs';
import { useTable } from '../table';
export function useGrid({ props, context }) {
  const table = useTable({ props, context });
  const { $table, $plugin, tableCompSlots } = table;
  // 监听列数据
  watch(
    () => props.columns,
    (value) => {
      nextTick(() => $table.loadColumn(value || []));
    },
  );

  // 支持高级表格columns配置
  onMounted(() => {
    nextTick(() => {
      const { columns } = props;
      // 如果有插槽，执行插槽列
      if (!tableCompSlots.default && columns && columns.length) {
        $table.loadColumn(columns);
      }
    });
  });

  // 列拖拽
  useColumnDD($table);
  // 行拖拽
  if (props.draggable) {
    useRowDD({ ...$table, delay: 50 });
  }

  return table;
}
