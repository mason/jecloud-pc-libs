import { defineComponent, watch } from 'vue';
import { tableEmits, tableProps, tableSlots, panelSlots, panelProps } from './table/index';
import { Table } from 'vxe-table';
import Panel from '../../panel';
import Pager, { usePager, pagerEmits, pagerProps } from '../../pager';
import { useGrid } from './hooks/use-grid';
import { pick } from '@jecloud/utils';
import { useProvideGrid } from './context';
import { useModelValue } from '../../hooks';

export default defineComponent({
  name: 'JeGrid',
  components: { Table, Panel, Pager },
  inheritAttrs: false,
  props: {
    ...tableProps,
    ...panelProps,
    ...pagerProps,
    seqConfig: {
      type: Object,
      default: () => ({ startIndex: 0 }),
    },
    store: Object,
    autoLoad: Boolean,
    multiple: { type: Boolean, default: undefined },
  },
  emits: [...tableEmits, ...pagerEmits, 'update:size', 'update:seqConfig'],
  slots: [...tableSlots, ...panelSlots],
  setup(props, context) {
    const { slots, attrs } = context;

    const { $table, $panel, $plugin, tableCompEvents, tableCompProps, tableCompSlots, loading } =
      useGrid({
        props,
        context,
      });

    // 绑定更新序号
    $table.seqConfig = useModelValue({ props, context, key: 'seqConfig' });
    $table.updateSeqConfig = function ({ currentPage, pageSize }) {
      const startIndex = (currentPage - 1) * pageSize;
      $table.seqConfig.value = {
        seqMethod: ({ rowIndex, data, rowid }) => {
          if (rowIndex == -1) {
            data.forEach((item, index) => {
              if ($table.store.getRecordId(item) == rowid) {
                rowIndex = index;
              }
            });
          }
          return rowIndex + 1 + startIndex;
        },
      };
    };

    // 支持动态修改
    $table.size = useModelValue({ props, context, key: 'size' });
    watch(
      () => $table.size.value,
      () => {
        $panel.value.refreshLayout();
      },
    );
    useProvideGrid($table);

    // 分页条
    const { pagerSlot } = usePager($table);
    // 底部工具条
    const bbarSlot = () => {
      return [slots.pager?.({ $table }) ?? pagerSlot?.({ $table }), slots.bbar?.({ $table })];
    };
    return () => (
      <Panel
        class={{ 'je-grid': true, 'is--loading': loading.value }}
        ref={$panel}
        {...pick(attrs, ['style', 'class'])}
        {...pick(props, Object.keys(panelProps))}
        v-slots={{ ...pick(slots, panelSlots), bbar: bbarSlot }}
      >
        <Table
          ref={$plugin}
          {...tableCompEvents}
          {...tableCompProps.value}
          size={$table.size.value}
          v-slots={tableCompSlots}
          height="100%"
          width="100%"
          style={props.bodyStyle}
          seqConfig={$table.seqConfig.value}
          v-loading={loading.value}
        />
      </Panel>
    );
  },
});
