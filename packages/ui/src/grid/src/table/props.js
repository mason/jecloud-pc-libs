import { computed } from 'vue';
import { pick, defaultsDeep } from '@jecloud/utils';
import vxeTableProps from 'vxe-table/es/table/src/props';
import { tableDefConfig, treeTableDefConfig, treeDefConfig } from './config';
import { dragClass } from '../plugins/sortablejs';
export const tableProps = {
  ...vxeTableProps,
  columns: Array,
  size: {
    type: String,
    default: 'middle',
    validator: (value) => ['large', 'middle', 'small', 'mini'].includes(value),
  },
  draggable: { type: [Boolean, Object], default: false },
  fieldConfig: { type: Array, default: null }, // 校验列的修改标识，建议配置完整表字段编码，如果未配置，默认只校验显示的列 ，数据格式：['field1','field2',{name:'field3'},...]
  bodyStyle: { type: [String, Object], default: null },
  allowDeselect: { type: Boolean, default: true }, // 允许取消选中，单选时有效
  idProperty: String,
};

export function useTableProps($table, tree) {
  const { props } = $table;
  // table属性，提取table自有属性，增加默认属性
  const tableCompProps = computed(() => {
    let _props = pick(props, Object.keys(vxeTableProps));
    const config = tree ? treeDefConfig : props.treeConfig ? treeTableDefConfig : tableDefConfig;
    _props.draggable = props.draggable; // 拖拽
    // 拖拽样式，树形默认行拖拽
    if (_props.draggable) {
      _props.rowClassName = dragClass;
    }
    _props = defaultsDeep(_props, config);
    // sotre数据
    _props.data = props.store?.data ?? props.data;

    // keyField,自定义行数据唯一主键的字段名
    if (props.idProperty) {
      Object.assign(_props.rowConfig, { keyField: props.idProperty });
    }

    delete _props.loading;
    return _props;
  });
  return tableCompProps;
}
