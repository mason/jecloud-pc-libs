import {
  Header,
  Footer,
  Filter,
  Edit,
  Menu,
  Export,
  Keyboard,
  Validator,
  Tooltip,
} from 'vxe-table';
import { installPlugins } from '../../../utils';
export function usePlugins() {
  installPlugins(Header, Footer, Filter, Edit, Menu, Export, Keyboard, Validator, Tooltip);
}
