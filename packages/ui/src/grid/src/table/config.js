export const tableDefConfig = {
  keepSource: true,
  size: 'middle',
  height: 'auto',
  showOverflow: 'ellipsis', // 当内容过长时显示为省略号
  showHeaderOverflow: 'ellipsis', // 当内容过长时显示为省略号
  showFooterOverflow: 'ellipsis', // 当内容过长时显示为省略号
  showHeader: true, // 显示头
  border: true, // 显示边框
  stripe: true, // 隔行变色
  rowConfig: { isHover: true, isCurrent: true, useKey: true }, // useKey 行拖动使用
  highlightCurrentRow: true,
  checkboxConfig: { highlight: true },
  editConfig: { showIcon: false, trigger: 'click', mode: 'cell', showStatus: true },
  columnConfig: { resizable: true, useKey: true, isHover: true }, // useKey 列拖动使用
  validConfig: { message: 'tooltip' },
  tooltipConfig: { isArrow: true, enterDelay: 100, leaveDelay: 100 },
  mouseConfig: { selected: false },
  keyboardConfig: {
    isArrow: true,
    isTab: true,
    isEdit: true,
    editMethod({ $table, row, column }) {
      // 重写默认的覆盖式，改为追加式
      $table.setEditCell(row, column);
    },
  },
  scrollY: {
    enabled: false,
    gt: 200, // 超过200条，启动虚拟滚动
    oSize: 100, // 优化浏览器渲染
  },
};
const treeConfig = {
  expandRowKeys: [],
  trigger: 'row-dblclick',
  rowField: 'id',
  parentField: 'parent',
  children: 'children',
  textField: 'text',
  codeField: 'code',
  idField: 'id',
  iconOpen: 'fas fa-caret-down',
  iconClose: 'fas fa-caret-right',
  iconLoaded: 'fal fa-loader roll',
  indent: 10,
  // checkStrictly: true, // 严格模式
  // cascadeChild: true,  // 级联选中子节点，严格模式下起效
  // cascadeParent: true, // 级联选中父节点, 严格模式下起效
};
export const treeTableDefConfig = Object.assign({}, tableDefConfig, {
  rowId: 'id',
  stripe: false,
  treeConfig,
});

export const treeDefConfig = Object.assign({}, treeTableDefConfig, {
  rowId: 'id',
  stripe: false,
  size: 'small',
  border: 'none',
  showHeader: false,
  treeConfig: {
    ...treeConfig,
    trigger: 'cell-dblclick',
  },
});
