export const renderer = {
  JeSelect: {
    autofocus: 'input.ant-select-selection-search-input',
  },
  JeInput: {
    autofocus: 'input.ant-input',
  },
  JeInputNumber: {
    autofocus: 'input.ant-input-number-input',
  },
};
['JePinyin'].forEach((name) => {
  renderer[name] = renderer.JeInput;
});

['JeSearch'].forEach((name) => {
  renderer[name] = renderer.JeSelect;
});
