import Sortable from 'sortablejs';
import { nextTick, onMounted, onUnmounted } from 'vue';
import { hasClass, isEmpty } from '@jecloud/utils';
import { dragClass } from './index';

export function useColumnDD($grid) {
  let initTime;
  onMounted(() => {
    nextTick(() => {
      // 加载完成之后在绑定拖动事件
      initTime = setTimeout(() => {
        initColumnDD($grid);
      }, 500);
    });
  });

  onUnmounted(() => {
    clearTimeout(initTime);
    destroyColumnDD($grid);
  });
}

/**
 * 销毁拖动插件
 * @param {*} $grid
 */
export const destroyColumnDD = function ($grid) {
  $grid.columnSortables?.forEach((sortable) => {
    sortable?.destroy();
  });
};

/**
 * 多表头，锁定列
 * @param {*} $grid
 */
export const initColumnDD = function ($grid) {
  const $table = $grid.$plugin.value;
  const sortables = [];
  ['refTableHeader', 'refTableLeftHeader', 'refTableRightHeader'].forEach((key) => {
    const header = $table.getRefMaps()[key];
    if (header?.value) {
      sortables.push(...columnDD({ ...$grid, header: header.value, group: key }));
    }
  });
  $grid.columnSortables = sortables;
  return sortables;
};

/**
 * 列拖动
 * 锁定列，只能在锁定范围拖动，无法夸区域拖动，基础表格组件不支持
 * @param {*} $grid
 * @return {*}
 */
const columnDD = function ({ context, $plugin, header, group }) {
  const { emit } = context;
  const $table = $plugin.value;
  // 拖动元素
  const dragDoms = header.$el.querySelectorAll('.vxe-table--header .vxe-header--row');
  if (isEmpty(dragDoms)) return;
  const { refElem, refTableHeader } = $table.getRefMaps();
  // 拖拽标记Root容器
  const indicatorRootEl = refElem.value.querySelector('.vxe-table--render-wrapper');
  // 拖拽标记滚动条计算容器
  const indicatorScrollEl = refTableHeader.value.$el;
  // 拖动对象，考虑多表头
  const sortables = [];
  dragDoms.forEach((dragDom) => {
    const sortable = Sortable.create(dragDom, {
      handle: '.' + dragClass,
      forceFallback: true, // 不使用H5原生拖动
      dragClass: 'je-sortable-drag', // 拖动元素样式
      group, // 拖动组
      indicator: true, // 拖拽标记
      indicatorRootEl, //拖拽标记Root容器
      indicatorScrollEl, // 拖拽标记滚动条计算容器
      preventOnFilter: false, // 禁用原生事件，防止点击冲突
      /**
       * 过滤可拖动元素
       * @param {*} e
       * @param {*} dragTarget
       * @returns
       */
      filter: function (e, dragTarget) {
        // 过滤可拖动的元素
        const target = e.target;
        return (
          !hasClass(dragTarget, dragClass) ||
          !(
            hasClass(target, 'vxe-header--column') ||
            hasClass(target, 'vxe-cell') ||
            hasClass(target, 'vxe-cell--title')
          )
        );
      },
      onMove(sortableEvent) {
        // 过滤允许拖放的元素
        const { dragEl, target } = sortableEvent;
        return hasClass(target, dragClass) && dragEl !== target;
      },
      onStart(sortableEvent) {
        // 重置拖动元素样式
        const { from } = sortableEvent;
        const dargItem = from.querySelector('.' + sortable.options.dragClass);
        dargItem.style.lineHeight = dargItem.style.height;
      },
      onEnd() {
        const { dragEl, swapEl, actions } = this.indicator;
        // 拖动列
        const dragColumn = $table.getColumnNode(dragEl);
        // 替换列
        const swapColumn = $table.getColumnNode(swapEl);

        if (dragEl === swapEl) {
          console.log('没有改变');
          return;
        }

        // 全量列
        const { collectColumn } = $table.getTableColumn();
        // 列索引
        let oldIndex = dragColumn.index;
        let newIndex = swapColumn.index;
        // 多表头特殊处理
        if (dragColumn.parent !== swapColumn.parent && actions.next) {
          newIndex++;
        }

        // 获取拖动列所在的集合
        const dragColumns = dragColumn.parent ? dragColumn.items : collectColumn;
        // 获取目标兄弟列所在的集合
        const targetColumns = swapColumn.parent ? swapColumn.items : collectColumn;

        // 移除原始位置
        const currRow = dragColumns.splice(oldIndex, 1)[0];
        // 插入新位置
        targetColumns.splice(newIndex, 0, currRow);

        // 更新列
        $table.loadColumn(collectColumn).then(() => {
          // 列拖动事件
          emit('column-drop', { $table });
        });
      },
    });
    sortables.push(sortable);
  });
  // 移除拖动属性，防止整体可拖动
  $table.$el.removeAttribute('draggable');
  return sortables;
};
