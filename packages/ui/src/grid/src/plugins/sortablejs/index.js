import Sortable from 'sortablejs';
import { usePlugin } from './plugin';
export { useColumnDD } from './column-dd';
export { useRowDD } from './row-dd';
export const dragClass = 'is--draggable';

usePlugin(Sortable);
