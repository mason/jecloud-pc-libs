import { ref, onMounted, onBeforeUnmount, watch } from 'vue';
import OverlayScrollbars from 'overlayscrollbars';
export function useOverlayScrollbars(props, expose) {
  const scroller = ref(null); // 滚动容器
  const $plugin = ref(null); // 滚动插件
  const _valid = function () {
    return OverlayScrollbars.valid($plugin.value);
  };
  // 初始化插件
  onMounted(() => {
    $plugin.value = OverlayScrollbars(scroller.value, props.options || {});
  });
  // 卸载插件
  onBeforeUnmount(() => {
    if (_valid()) {
      $plugin.value.destroy();
      $plugin.value = null;
    }
  });
  // 重置插件配置项
  watch(
    () => props.options,
    (options) => {
      _valid() && $plugin.value.options(options);
    },
  );
  // 获得滚动容器
  const getScrollWrap = function () {
    return _valid() && $plugin.value.getElements('viewport');
  };

  // 提取函数
  const fns = {};
  ['scroll', 'scrollStop', 'options', 'update', 'sleep'].forEach((methodName) => {
    fns[methodName] = function (...args) {
      return _valid() && $plugin.value[methodName](...args);
    };
  });

  expose({
    $plugin,
    getScrollWrap,
    ...fns,
  });

  return {
    scroller,
  };
}
