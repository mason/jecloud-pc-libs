import JeToolbar from './src/toolbar';
import Fill from './src/fill';
import Separator from './src/separator';
import Tool from './src/tool';
import { withInstall } from '../utils';
JeToolbar.installComps = {
  Fill: { name: Fill.name, comp: Fill }, // 填充元素
  Separator: { name: Separator.name, comp: Separator }, // 分隔元素
  Tool: { name: Tool.name, comp: Tool }, // 工具按钮
};

export const Toolbar = withInstall(JeToolbar);

export default Toolbar;
