import { defineComponent } from 'vue';

export default defineComponent({
  name: 'JeToolbarTool',
  props: { icon: String },
  setup(props, { attrs, slots }) {
    return () => (
      <i {...attrs} class={['je-toolbar-item-tool', props.icon]}>
        {slots.default?.()}
      </i>
    );
  },
});
