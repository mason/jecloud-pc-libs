import { defineComponent } from 'vue';
import { Select, Empty } from 'ant-design-vue';
import { useStyle4Size } from '../../hooks';
import { useAddonSlot, addonProps, addonSlots } from '../../hooks/use-addon';
import { isString, split, pinyin, isEmpty, isNumber } from '@jecloud/utils';
import { useSelect } from './hooks';
import JeSelectLoading from './select-loading';

export default defineComponent({
  name: 'JeSelect',
  components: { Select, JeSelectLoading, Empty },
  inheritAttrs: false,
  props: {
    editable: Boolean,
    readonly: Boolean,
    disabled: Boolean,
    value: [String, Number, Object],
    multiple: { type: Boolean, default: undefined },
    configInfo: String,
    options: Array,
    showSearch: Boolean,
    height: Number,
    width: Number,
    model: Object,
    parentModel: Object,
    querys: Array,
    dicValueConfig: Array,
    name: String,
    ...addonProps,
  },
  slots: addonSlots,
  emits: [
    'update:value',
    'update:options',
    'change',
    'before-select',
    'dropdownVisibleChange',
    'search',
  ],
  setup(props, context) {
    const { slots, attrs } = context;
    const {
      selectValue,
      computeMultiple,
      computeReadonly,
      onDropdownVisibleChange,
      onChange,
      options,
      filterOption,
      $plugin,
      searchValue,
      loading,
      handleSearch,
    } = useSelect({
      props,
      context,
    });

    // 注意，如果发现下拉菜单跟随页面滚动，或者需要在其他弹层中触发 Select，
    // 请尝试使用 getPopupContainer={triggerNode => triggerNode.parentNode} 将下拉弹层渲染节点固定在触发器的父元素中。
    return () => {
      // 个性化配置
      const customProps = {};
      // 如果是数值转字符串
      if (isNumber(selectValue.value)) {
        selectValue.value = selectValue.value.toString();
      }
      // 多选
      if (computeMultiple.value) {
        Object.assign(customProps, {
          tokenSeparators: [','],
          mode: props.editable ? 'tags' : 'multiple',
        });
        // 字符串转数组
        if (isString(selectValue.value)) {
          selectValue.value = split(selectValue.value, ',');
        }
        // 单选
      } else {
        Object.assign(customProps, {
          showSearch: props.showSearch,
        });
        // 可选可编辑
        if (props.editable) {
          Object.assign(customProps, {
            mode: Select.SECRET_COMBOBOX_MODE_DO_NOT_USE,
          });
        }
      }
      // 如果启用可选可过滤才有search事件
      if (props.showSearch) {
        customProps.onSearch = handleSearch;
      } else {
        customProps.filterOption = false;
      }
      const element = (
        <Select
          ref={$plugin}
          showArrow
          allowClear
          maxTagCount="responsive"
          getPopupContainer={(triggerNode) => triggerNode.parentNode} // 绑定
          style={useStyle4Size({ props })}
          onDropdownVisibleChange={(open) => {
            onDropdownVisibleChange(open);
            context.emit('dropdownVisibleChange', open);
          }}
          onChange={onChange}
          filterOption={filterOption}
          searchValue={searchValue.value}
          {...attrs}
          {...customProps}
          options={options.value}
          disabled={computeReadonly.value}
          v-model:value={selectValue.value}
          v-slots={{
            notFoundContent() {
              return !loading.value ? (
                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
              ) : (
                <JeSelectLoading />
              );
            },
            ...slots,
          }}
        ></Select>
      );

      return useAddonSlot({ props, context, element });
    };
  },
});
