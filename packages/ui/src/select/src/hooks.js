/* 下拉框加载数据逻辑:
  1. 组件可编辑不需要初始化options数据
  2. 组件值改变时,如果数据在options不存在并且不可编辑,是要加载options数据的
  3. 如果组件可搜索,并且有过滤条件或者过滤传参里面的动态参数是当前字段 那么组件要实时调用接口查询options数据
  */
import { ref, computed, watch } from 'vue';
import { useModelValue, useConfigInfo, useExtendMethods } from '../../hooks';
import { isEmpty, pinyin, isNotEmpty, isString, isNumber, debounce } from '@jecloud/utils';

export function useSelect({ props, context }) {
  const { emit, expose } = context;
  const {
    options,
    multiple,
    setValues,
    onDropdownVisibleChange,
    loading,
    startSearchLoad,
    configInfo,
  } = useConfigInfo({
    props,
    context,
  });
  const computeReadonly = computed(() => {
    return props.readonly || props.disabled;
  });
  const computeMultiple = computed(() => {
    return multiple.value || props.multiple;
  });
  const modelValue = useModelValue({
    props,
    context,
    multiple: multiple.value,
    changeEvent: true,
  });
  // 搜索方法
  const selectFn = debounce((open, searchParam) => {
    onDropdownVisibleChange(open, searchParam);
  }, 300);

  // 监听组件的值变化,来改变下拉框options数据
  watch(
    () => modelValue.value,
    (newValue) => {
      // 可编辑和值为空不做处理
      if (!props.editable && isNotEmpty(newValue)) {
        // 下拉框options数据中是否有该value值
        let selectValues = newValue;
        // 如果是数值转字符串
        if (isNumber(newValue)) {
          selectValues = [newValue.toString()];
        }
        if (isString(newValue)) {
          selectValues = [newValue];
        }
        let falg = false;
        // 处理多选情况
        for (let i = 0; i < selectValues.length; i++) {
          const valItem = selectValues[i];
          if (options.value && !options.value.some((item) => item.value == valItem)) {
            falg = true;
          }
        }
        // 如果值在options里面没有并且是不可编辑的状态下,需要查一下options的数据
        if (falg) {
          selectFn(true);
        }
      }
    },
  );
  // 继承方法
  const $plugin = ref();
  const methods = useExtendMethods({ plugin: $plugin, keys: ['blur', 'focus'] });
  expose({
    ...methods,
    $plugin,
  });
  // 查询过滤选项
  const filterOption = (inputValue, option) => {
    if (!inputValue || startSearchLoad.value) return true;
    inputValue = inputValue?.toString().toLocaleLowerCase() ?? inputValue;
    // 模糊查询value，label
    const value = option.value.toString().toLocaleLowerCase();
    const label = option.label?.toString().toLocaleLowerCase() ?? value;
    const labelPinyin = pinyin(label, 'pinyin');
    return (
      value.includes(inputValue) || label.includes(inputValue) || labelPinyin.includes(inputValue)
    );
  };
  // change事件
  const onChange = (value, valueOptions) => {
    // 多选可编辑，处理级联赋值操作
    if (computeMultiple.value && props.editable) {
      (value ?? []).forEach((val, index) => {
        if (isEmpty(valueOptions[index])) {
          // 格式化数据项
          valueOptions[index] = { label: val, value: val, text: val, code: val };
        }
      });
    }
    emit('change', value, valueOptions);
    // 级联更新configInfo配置字段
    if (
      isNotEmpty(valueOptions) &&
      Object.keys(valueOptions).length === 0 &&
      valueOptions.constructor === Object
    ) {
      return false;
    }
    setValues(valueOptions);
  };
  // 下拉框值
  let selectValue = modelValue;

  // 查询值，解决可选可编辑状态下查询后，无法重置选项问题
  const searchValue = ref();
  // 可选可编辑
  if (!computeMultiple.value && props.editable) {
    // 解决刷新表单页面，下拉框数据回显code的问题
    // TODO这块监听逻辑有问题
    watch(
      () => options.value,
      (newValue) => {
        const item = newValue?.find((item) => item.value == selectValue.value);
        modelValue.value = item ? item.label : modelValue.value;
      },
      { immediate: true },
    );
    selectValue = ref(modelValue.value);
    watch(
      () => modelValue.value,
      () => {
        const item = options.value?.find((item) => item.value == modelValue.value);
        selectValue.value = item ? item.label : modelValue.value;
      },
      { immediate: true },
    );
    watch(
      () => selectValue.value,
      () => {
        const item = options.value?.find(
          (item) => item.value == selectValue.value || item.label == selectValue.value,
        );
        if (item) {
          let valueCode = 'value';
          // 获得当前字段对应的取值字段
          if (
            configInfo &&
            configInfo?.sourceFields?.length > 0 &&
            configInfo?.targetFields?.length > 0 &&
            isNotEmpty(props.name)
          ) {
            valueCode = configInfo.sourceFields[configInfo.targetFields.indexOf(props.name)];
          }
          modelValue.value = item[valueCode];
          selectValue.value = item.label;
        } else {
          modelValue.value = selectValue.value;
        }
        searchValue.value = selectValue.value;
      },
    );
  }
  // 搜索方法
  const handleSearch = (value) => {
    // 下拉框可选可过滤是否调接口请求数据
    if (startSearchLoad.value) {
      const searchParam = {};
      if (isNotEmpty(props.name)) {
        searchParam[props.name] = value;
      }
      selectFn(true, searchParam);
    }
    context.emit('search', value);
  };

  return {
    filterOption,
    selectValue,
    computeReadonly,
    computeMultiple,
    onChange,
    onDropdownVisibleChange,
    options,
    $plugin,
    searchValue,
    loading,
    handleSearch,
  };
}
