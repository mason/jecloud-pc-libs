import { defineComponent } from 'vue';
import { InputNumber } from 'ant-design-vue';
import { useModelValue, useStyle4Size } from '../../hooks';
import { useAddonSlot, addonProps, addonSlots } from '../../hooks/use-addon';
import { omit } from '@jecloud/utils';

export default defineComponent({
  name: 'JeInputNumber',
  components: { InputNumber },
  inheritAttrs: false,
  props: {
    width: Number,
    height: Number,
    value: [Number, String],
    ...addonProps,
  },
  slots: addonSlots,
  emits: ['update:value'],
  setup(props, context) {
    const { slots, attrs } = context;
    const value = useModelValue({ props, context, changeEvent: true });
    return () => {
      const element = (
        <InputNumber
          style={useStyle4Size({ props })}
          v-model:value={value.value}
          {...omit(attrs, Object.keys(addonProps))}
          v-slots={omit(slots, addonSlots)}
        />
      );

      return useAddonSlot({ props, context, element });
    };
  },
});
