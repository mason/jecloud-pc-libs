import JeInputNumber from './src/input-number';
import { withInstall } from '../utils';

export const InputNumber = withInstall(JeInputNumber);

export default InputNumber;
