import { defineComponent } from 'vue';
import { useDataTrance } from './hook';
import { isNotEmpty } from '@jecloud/utils';
import { Grid } from '../../grid';
import imgSrcData from './resource/nodata.png';
import JeDataTable from './data-table';
export default defineComponent({
  name: 'JeDataTrance',
  props: {
    funcCode: {
      type: String,
      default: '',
    },
    beanId: {
      type: String,
      default: '',
    },
  },
  setup(props, context) {
    const { historyData, loading } = useDataTrance({ props, context });
    return () => (
      <div class="je-data-trance" v-loading={loading.value}>
        {historyData.value.length > 0 ? (
          <div class="je-data-trance-boby">
            {historyData.value.map((item) => {
              return (
                <div class="je-data-trance-boby-item">
                  {isNotEmpty(item.week) ? (
                    <div class="item-week">
                      <h3 class="week">{item.week}</h3>
                      <span class="week-date">{item.date}</span>
                      <div class="week-circle"></div>
                    </div>
                  ) : null}
                  <div class="item-time">
                    <div class="time">{item.time}</div>
                    <div class="time-circle"></div>
                    {item.SY_CREATEUSERNAME}（{item.SY_CREATEORGNAME}）
                  </div>
                  <div class="item-grid">
                    <JeDataTable tableData={item.PAGENICKED_NICKED} />
                  </div>
                </div>
              );
            })}
          </div>
        ) : !loading.value ? (
          <div class="je-data-trance-nodata">
            <div class="context">
              <img class="nodata-img" src={imgSrcData}></img>
              <div class="nodata-text">暂无数据 ~ </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  },
});
