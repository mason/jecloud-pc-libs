import { defineComponent, ref, watch } from 'vue';
import { isNotEmpty, decode, isObject, cloneDeep, isArray } from '@jecloud/utils';
import { FuncFieldTypeEnum } from '@jecloud/utils/src/func/enum/func-field-enum';

export default defineComponent({
  name: 'JeDataTable',
  props: {
    tableData: {
      type: Array,
      default: () => {
        return [];
      },
    },
  },
  setup(props, context) {
    const tableArray = ref([]);
    const initData = () => {
      const fieldNames = [];
      const newTableArray = [];
      cloneDeep(props.tableData).forEach((item, index) => {
        let array = [];
        const { fieldName } = item;
        if (!fieldNames.includes(fieldName)) {
          array = props.tableData.filter((dataItem) => dataItem.fieldName == fieldName);
        }
        item.rowspan = array.length;
        fieldNames.push(fieldName);
        newTableArray.push(item);
      });
      tableArray.value = newTableArray;
    };

    // 处理单元格的展示
    const getTdContext = (data, item) => {
      if (
        isNotEmpty(data) &&
        [
          FuncFieldTypeEnum.UPLOAD_INPUT.xtype,
          FuncFieldTypeEnum.UPLOAD.xtype,
          FuncFieldTypeEnum.UPLOAD_IMAGE.xtype,
          FuncFieldTypeEnum.FUNC_CHILD_FIELD.xtype,
        ].includes(item.type)
      ) {
        const dataObj = decode(data);
        if (isObject(dataObj)) {
          // 子功能集合
          if (item.type == FuncFieldTypeEnum.FUNC_CHILD_FIELD.xtype) {
            return dataObj.map((item) => {
              return (
                <p>
                  {Object.keys(item).map((iteKey) => {
                    return (
                      <span>
                        {iteKey}：{getChildFieldFileContext(item[iteKey])}
                      </span>
                    );
                  })}
                </p>
              );
            });
          } else {
            // 附件
            return dataObj.map((item) => {
              return <p>{item.relName || item.fieldName}</p>;
            });
          }
        } else {
          return data;
        }
      } else {
        return data;
      }
    };
    /**
     * 子功能集合附加展示处理
     */
    const getChildFieldFileContext = (data) => {
      if (isNotEmpty(data)) {
        const dataArray = decode(data);
        if (isArray(dataArray)) {
          return dataArray.map((item, index) => {
            return (
              <span>
                {item.relName || item.fieldName}
                {dataArray.length - 1 > index ? '，' : ''}
              </span>
            );
          });
        } else {
          return data;
        }
      } else {
        return data;
      }
    };
    watch(
      () => props.tableData,
      () => {
        initData();
      },
      {
        immediate: true,
      },
    );

    return () => (
      <table class="je-data-table">
        <thead>
          <tr>
            <th width="220px">修改字段</th>
            <th min-width="220px">修改前</th>
            <th min-width="220px">修改后</th>
          </tr>
        </thead>
        <tbody>
          {tableArray.value.map((item) => {
            return (
              <tr>
                {item.rowspan > 0 ? <td rowspan={item.rowspan}>{item.fieldName}</td> : null}
                <td>{getTdContext(item.oldValue, item)}</td>
                <td>{getTdContext(item.newValue, item)}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  },
});
