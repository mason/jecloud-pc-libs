import { ref, onMounted, reactive } from 'vue';
import { cloneDeep, dateFormat, decode, ajax, encode, getWeekday } from '@jecloud/utils';

export function useDataTrance({ props, context }) {
  const loading = ref(true);
  // 历史数据
  const historyData = ref([]);
  const state = reactive({
    funcCode: props.funcCode,
    beanId: props.beanId,
  });
  // 初始化数据
  const initData = () => {
    loading.value = true;
    ajax({
      url: '/je/common/load',
      headers: { pd: 'meta' },
      params: {
        tableCode: 'JE_CORE_PAGENICKED',
        limit: -1,
        j_query: encode({
          custom: [
            { code: 'PAGENICKED_BUSINESSID', type: '=', value: state.beanId, cn: 'and' },
            { code: 'PAGENICKED_FUNCCODE', type: '=', value: state.funcCode, cn: 'and' },
          ],
          order: [{ code: 'SY_CREATETIME', type: 'desc' }],
        }),
      },
    }).then(({ data }) => {
      doProcessingData(data?.rows || []);
    });
  };
  // 处理数据
  const doProcessingData = (data) => {
    const cloneData = cloneDeep(data);
    const newDatas = [];
    cloneData.forEach((item, index) => {
      // 星期week处理
      // 上一级下标
      const parentIndex = index - 1;
      const thisDate = dateFormat(item.SY_CREATETIME, 'YYYY-MM-DD');
      // 上一级级下标得大于-1
      if (parentIndex > -1) {
        // 比较是一个日期吗
        const parentDate = dateFormat(cloneData[parentIndex].SY_CREATETIME, 'YYYY-MM-DD');
        // 不是一个日期就需要赋值星期
        if (parentDate != thisDate) {
          item.week = getWeekday(item.SY_CREATETIME);
          item.date = thisDate;
        }
      } else {
        // 如果上一级下标小于等于-1,那么就是第一位元素
        item.week = getWeekday(item.SY_CREATETIME);
        item.date = thisDate;
      }
      item.time = dateFormat(item.SY_CREATETIME, 'HH:mm:ss');
      // 处理修改列表数据
      item.PAGENICKED_NICKED = decode(item.PAGENICKED_NICKED);
      newDatas.push(item);
    });
    historyData.value = newDatas;
    loading.value = false;
  };
  // 刷新数据
  const refresh = ({ funcCode, beanId }) => {
    state.funcCode = funcCode;
    state.beanId = beanId;
    initData();
  };

  context.expose({ refresh });
  onMounted(() => {
    initData();
  });
  return { historyData, loading, refresh };
}
