import { withInstall } from '../utils';
import JeDataTrance from './src/data-trace';
export const DataTrance = withInstall(JeDataTrance);
export default DataTrance;
