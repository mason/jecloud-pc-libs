import { defineComponent, ref } from 'vue';
import { Form } from 'ant-design-vue';
import { useExtendMethods } from '../../hooks';
import { useProvideForm, MethodKeys } from './context';
import Panel from '../../panel';
export const useForm = Form.useForm;
export default defineComponent({
  name: 'JeForm',
  components: { Form, Panel },
  inheritAttrs: false,
  props: {
    validateType: { type: String, validator: (value) => ['error', 'tooltip'].includes(value) },
    itemStyle: { type: [String, Object], default: '' },
    size: {
      type: String,
      default: 'middle',
      validator: (value) => ['middle', 'small'].includes(value),
    },
  },
  setup(props, { slots, attrs, expose }) {
    const $plugin = ref();
    const fields = {};
    const addFormField = (eventKey, field) => {
      fields[eventKey] = field;
    };
    const methods = useExtendMethods({ plugin: $plugin, keys: MethodKeys });
    // 清空校验消息
    methods.clearValidate = function (name) {
      $plugin.value.clearValidate?.(name);
      // 清空字段的校验消息
      (name ? [name] : Object.keys(fields)).forEach((key) => {
        const field = fields[key];
        field?.clearValidate?.();
      });
    };
    expose(methods);

    useProvideForm({
      enabled: true,
      validateType: props.validateType,
      size: props.size,
      itemStyle: props.itemStyle,
      $plugin,
      addFormField,
      ...methods,
    });
    return () => <Form class="je-form" ref={$plugin} {...attrs} v-slots={slots} />;
  },
});
