import { withInstall } from '../utils';
import { Form as AForm } from 'ant-design-vue';
import JeForm, { useForm } from './src/form';
import JeFormItem from './src/form-item';
import { useInjectFormItemContext, MethodKeys } from './src/context';
Object.assign(JeForm, {
  useInjectFormItemContext,
  useForm,
  MethodKeys,
});
JeForm.installComps = {
  // 注册依赖组件
  Item: { name: 'JeFormItem', comp: JeFormItem },
  ItemRest: { name: 'JeFormItemRest', comp: AForm.ItemRest },
};

export const Form = withInstall(JeForm);

export default Form;
