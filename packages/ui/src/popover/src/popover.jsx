import { defineComponent } from 'vue';
import { Popover } from 'ant-design-vue';

export default defineComponent({
  name: 'JePopover',
  components: { APopover: Popover },
  inheritAttrs: false,
  setup(props, { slots, attrs }) {
    props;
    return () => <a-popover {...attrs} v-slots={slots}></a-popover>;
  },
});
