import { withInstall } from '../utils';
import { Menu as AMenu } from 'ant-design-vue';
import JeMenu from './src/menu';
import JeSubMenu from './src/sub-menu';
import JeMenuItem from './src/menu-item';

JeMenu.installComps = {
  // 注册依赖组件
  Item: { name: JeMenuItem.name, comp: JeMenuItem },
  Divider: { name: 'JeMenuDivider', comp: AMenu.Divider },
  SubMenu: { name: JeSubMenu.name, comp: JeSubMenu },
  ItemGroup: { name: 'JeMenuItemGroup', comp: AMenu.ItemGroup },
};

export const Menu = withInstall(JeMenu);

export default Menu;
