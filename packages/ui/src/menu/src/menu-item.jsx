import { defineComponent } from 'vue';
import { Menu } from 'ant-design-vue';
import IconItem from '../../icon/src/icon-item';
import { useMenu } from './hooks';
export default defineComponent({
  name: 'JeMenuItem',
  components: { MenuItem: Menu.Item, IconItem },
  inheritAttrs: false,
  props: { icon: String, iconColor: String },
  setup(props, { slots, attrs }) {
    const { iconSlot, key } = useMenu(props, { slots });
    return () => <MenuItem {...attrs} key={key} v-slots={{ ...slots, icon: iconSlot }} />;
  },
});
